/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.exceptions;

import com.openexchange.i18n.LocalizableStrings;

/**
 * {@link GuardCoreExceptionMessages}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.0
 */
public class GuardCoreExceptionMessages implements LocalizableStrings {

    // A database error occured followed by the error
    public static final String SQL_ERROR_MSG = "An SQL error occurred: %1$s";
    // A parameter in the server request is missing.
    public static final String PARAMETER_MISSING_MSG = "The parameter \"%1$s\" is missing.";
    // More of a diagnostic error message/administrative.  The JSON data is not correct/corrupted for some reason
    public static final String JSON_PARAMETER_MISSING_MSG = "The parameter \"%1$s\" is missing in the provided JSON data.";
    // A configuration error.  Unlikely to be displayed to user
    public static final String CONFIGURATION_ERROR_MSG = "\"%1$s\" not configured correctly: \"%2$s\"";
    // Unexpected error
    public static final String UNEXPECTED_ERROR_MSG = "An error occurred: %1$s";
    // Unexpected IO error
    public static final String IO_ERROR_MSG = "An I/O error occurred: %1$s";
    // Could not find the key for the specified email address in the specified context
    public static final String KEY_NOT_FOUND_FOR_MAIL_ERROR_MSG = "Could not find key for email \"%1$s\" in context %2$d";
    // Could not find key for the user with the specified id in the specified context
    public static final String KEY_NOT_FOUND_FOR_IDS_ERROR_MSG = "Could not find key for user with id \"%1$d\" in context %2$d";
    // Could not find the key with the specified key id
    public static final String KEY_NOT_FOUND_FOR_KEY_ID_ERROR_MSG = "Could not find key for key id \"%1$s\"";
    // Error while processing the JSON data.  error appended
    public static final String JSON_ERROR_MSG = "Error while processing provided JSON: %1$s.";
    // The action failed because it was disabled per configuration.  Examples for %1 would be 'delete key pair' or 'reset password'
    public static final String DISABLED_ERROR_MSG = "Failed to %1$s because this is disabled per configuration.";
    // The key wasn't found
    public static final String KEY_NOT_FOUND_MSG = "The requested key was not found";
    // The account is not a valid guest account
    public static final String NOT_A_GUEST_ACCOUNT_MSG = "The requested account is not a valid guest account";
    // The two parameters listed can't be specified at the same time
    public static final String PARAMETER_MISMATCH_MSG = "The parameters \"%1$s\" and \"%2$s\" cannot be specified at the same time";
    // The value specified by the parameter is not valid
    public static final String INVALID_PARAMETER_VALUE_MSG = "The parameter \"%1$s\" does not contain a valid value";
    // Missing a part of an HTML transaction. "multipart/form-data" is a technical type of a section of HTML data
    public static final String MULTIPART_UPLOAD_MISSING_MSG = "Missing multipart/form-data";
    // Error parsing an HTML transaction.  the "multipart/form-data" seems to be invalid
    public static final String MULTIPART_UPLOAD_ERROR_MSG = "Error parsing multipart/form-data: %1$s";
    // Couldn't find the master key of the key ring.  This is required for signing signatures
    public static final String KEY_NOT_FOUND_PRIVATE_MASTER_MSG = "Could not find the private master key of the specified key ring.";
    // Couldn't find the key for the email, followed by the address that is missing a key
    public static final String KEY_NOT_FOUND_FOR_MAIL_ONLY_ERROR_MSG = "Could not find key for email: Address \"%1$s\"";
    // The type of email signature is unknown.
    public static final String UNKOWN_SIGNATURE_TYPE_MSG = "Unknown signature type";
}
