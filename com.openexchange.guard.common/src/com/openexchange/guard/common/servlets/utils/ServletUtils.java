/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.common.servlets.utils;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.L;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.servlets.responses.CommonResponse;
import com.openexchange.guard.common.util.IDNUtil;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;

/**
 * Utility class for servlet handling
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
public class ServletUtils {

    private static final Logger LOG = LoggerFactory.getLogger(ServletUtils.class);

    /**
     * Gets the remote address of the requesting client
     *
     * @param request the request to get the client address for
     * @return the requesting client's remote address
     */
    public static String getClientIP(HttpServletRequest request) {
        if (request.getHeader("X-Forwarded-For") != null) {
            return (request.getHeader("X-Forwarded-For"));
        }
        return request.getRemoteAddr();
    }

    /**
     * Sends a response
     *
     * @param response the response object
     * @param returnCode the return code to send
     * @param answer the answer to send
     * @throws IOException In case response can't be sent
     */
    public static void sendResponse(HttpServletResponse response, int returnCode, String answer) throws IOException {
        response.setContentType("text/html; charset=UTF-8");
        // response.addHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(returnCode);
        response.getWriter().print(answer);
    }

    /**
     * Sends 200 - OK
     *
     * @param response the response to send the 200 code to
     * @param type The content type to use
     * @param answer an additional answer to put into the body
     */
    public static void sendOK(HttpServletResponse response, String type, String answer) {
        sendOK(response, type, answer.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * Sends 200 - OK
     *
     * @param response the response to send the 200 code to
     * @param type The content type to use
     * @param answer an additional answer to put into the body
     */
    public static void sendOK(HttpServletResponse response, String type, byte[] answer) {
        sendOK(response, type, new ByteArrayInputStream(answer));
    }

    /**
     * Sends 200 - OK
     *
     * @param response The reponse to send the 200 code to
     * @param type The content type to use
     * @param answer the answer to send
     */
    public static void sendOK(HttpServletResponse response, String type, InputStream answer) {
        try {
            response.setContentType(type);
            response.setStatus(HttpServletResponse.SC_OK);
            OutputStream out = response.getOutputStream();
            IOUtils.copy(answer, out);
            out.flush();
            out.close();
        } catch (IOException e) {
            LOG.error("Error sending OK message", e);
        } finally {
            try {
                answer.close();
            } catch (IOException e) {
                LOG.error("Error closing stream ", e);
            }
        }
    }

    /**
     * Sends 200 - OK
     *
     * @param response the response to send the 200 code to
     * @param answer an additional answer to put into the body with content type text/html
     */
    public static void sendHtmlOK(HttpServletResponse response, String answer) {
        response.setContentType("text/html; charset=UTF-8");
        // response.addHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_OK);
        try {
            response.getWriter().print(answer);
        } catch (IOException e) {
            LOG.error("Error sending OK message", e);
        }
    }

    /**
     * Send JSON response
     *
     * @param response the response to send the 200 code to
     * @param json JSON object to put into the body with content type application/json
     */
    public static void sendJsonOK(HttpServletResponse response) {
        sendJsonOK(response, new JsonObject());
    }

    /**
     * Send JSON response
     *
     * @param response the response to send the 200 code to
     * @param json JSON object to put into the body with content type application/json
     */
    public static void sendJsonOK(HttpServletResponse response, JsonElement json) {
        response.setStatus(HttpServletResponse.SC_OK);
        sendJson(response, json);
    }

    /**
     * Send JSON response
     *
     * @param response the response to send the 200 code to
     * @param error The error object to put into the body with content type application/json
     */
    public static void sendJsonError(HttpServletResponse response, String error) {
        JsonObject el = new JsonObject();
        el.addProperty("error", error);
        sendJson(response, el);

    }

    /**
     * Send JSON response
     *
     * @param response the response to send the 200 code to
     * @param json JSON object to put into the body with content type application/json
     */
    public static void sendJson(HttpServletResponse response, JsonElement json) {
        response.setContentType("application/json; charset=UTF-8");
        // response.addHeader("Access-Control-Allow-Origin", "*");
        try {
            if (json == null) {
                response.getWriter().write("{}");
            } else {
                response.getWriter().write(json.toString());
            }
        } catch (IOException e) {
            LOG.error("Error sending OK message", e);
        }
    }

    /**
     * Maps a {@link Closeable} as JSON, writes it to the response (as 200-OK) and closes it afterwards
     *
     * @param response The response to use
     * @param responseObject The {@link Closeable} to send and close
     */
    public static void sendObject(HttpServletResponse response, Closeable responseObject) {
        try {
            sendObject(response, (Object) responseObject);
        } finally {
            try {
                responseObject.close();
            } catch (IOException e) {
                LOG.error("IO Error", e);
            }
        }
    }

    /**
     * Maps an Object as JSON and writes it to the response as 200-OK
     *
     * @param response The respose to use
     * @param responseObject The object to write
     */
    public static void sendObject(HttpServletResponse response, Object responseObject) {
        try {
            Object obj = new CommonResponse(responseObject);
            response.setStatus(HttpServletResponse.SC_OK);
            response.setContentType("application/json");
            new ObjectMapper().writeValue(response.getOutputStream(), obj);
        } catch (IOException e) {
            LOG.error("IO Error", e);
            e.printStackTrace();
        }
    }

    public static void sendAnswer(HttpServletResponse response, int returnCode, String answer) throws IOException {
        response.setContentType("text/html; charset=UTF-8");
        // response.addHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(returnCode);
        response.getWriter().print(answer);
    }

    /**
     * Sends 406 - Not Acceptable
     *
     * @param response the response to send the 406 code to
     * @param answer body text
     */
    public static void sendNotAcceptable(HttpServletResponse response, String answer) {
        response.setContentType("text/html; charset=UTF-8");
        // response.addHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
        try {
            response.getWriter().print(answer);
        } catch (IOException e) {
            LOG.error("Error sending NOT ACCEPTABLE message", e);
        }
    }

    /**
     * Sends 405 - Internal server error
     *
     * @param response the response to send the 405 code to
     * @param e body text
     */
    public static void sendError(HttpServletResponse response, Throwable e) {
        String answer = (e.getMessage() == null ? "" : e.getMessage());
        if (e.getCause() != null && e.getCause().getMessage() != null) {
            answer += " caused by: " + e.getCause().getMessage();
        }
        sendError(response, answer);
    }

    /**
     * Sends 405 - Internal server error
     *
     * @param response the response to send the 405 code to
     * @param answer body text
     */
    public static void sendError(HttpServletResponse response, String answer) {
        response.setContentType("text/html; charset=UTF-8");
        // response.addHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        try {
            response.getWriter().print(answer);
        } catch (IOException ex) {
            LOG.error("Error sending Error message", ex);
        }
    }

    /**
     * Get String from HttpServletRequest
     *
     * @param request the request to get the string from
     * @param name the name of the parameter to get as String
     * @param mandatory true to throw a GuardMissingParameter if the parameter was not found, false to return null
     * @return The value of the requested parameter as string, or null if the parameter was not found and mandatory was set to false
     * @throws OXException If the requested parameter was not found and mandatory was set to true
     */
    public static String getStringParameter(HttpServletRequest request, String name, boolean mandatory) throws OXException {
        String item = request.getParameter(name);
        try {
            if (item != null) {
                if (item.equals("null")) {
                    item = null;
                }
            }
        } catch (Exception e) {
            LOG.error("Problem getting parameter " + name, e);
        }

        if (mandatory && (item == null)) {
            throw GuardCoreExceptionCodes.PARAMETER_MISSING.create(name);
        }
        return item;
    }

    /**
     * Get String from HttpServletRequest
     *
     * @param request the request to get the string from
     * @param name the name of the parameter to get as String
     * @return The value of the requested parameter as string, or null if the parameter was not found
     */
    public static String getStringParameter(HttpServletRequest request, String name) {
        try {
            return (ServletUtils.getStringParameter(request, name, false));
        } catch (OXException e) {
            LOG.debug("Unable to parse", e);
            return null;
        }
    }

    /**
     * Returns IDN ACE'd email from parameter
     *
     * @param request Http servlet request
     * @param name Parameter name
     * @return Ace encoded email address
     */
    public static String getEmailParameter(HttpServletRequest request, String name) {
        final String email = getStringParameter(request, name);
        if (email != null) {
            return IDNUtil.aceEmail(email);
        }
        return email;
    }

    /**
     * Get integer from HttpServletRequest
     *
     * @param request the request to get the parameter from
     * @param name the name of the parameter to get
     * @param mandatory true to throw a GuardMissingParameter if the parameter was not found, false to return null
     * @return The value of the requested parameter as integer, or 0 if the parameter was not found and mandatory was set to false
     * @throws OXException If the requested parameter was not found and mandatory was set to true
     */
    public static Integer getIntParameter(HttpServletRequest request, String name, boolean mandatory) throws OXException {
        String item = request.getParameter(name);
        if (item == null || item.equals("null")) {
            if (mandatory) {
                throw GuardCoreExceptionCodes.PARAMETER_MISSING.create(name);
            }
            return null;
        }
        try {
            int val = Integer.parseInt(item);
            return I(val);
        } catch (Exception e) {
            LOG.error("Problem getting integer from parameter", e);
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Get integer from HttpServletRequest
     *
     * @param request the request to get the integer from
     * @param name the name of the parameter to get as integer
     * @return The value of the requested parameter as integer, or null if the parameter was not found
     */
    public static Integer getIntParameter(HttpServletRequest request, String name) {
        try {
            return getIntParameter(request, name, false);
        } catch (OXException e) {
            LOG.debug("Unable to parser parameter", e);
            return null;
        }
    }

    /**
     * Get long value from HttpServletRequest
     *
     * @param request the request to get the integer from
     * @param name the name of the parameter to get as long
     * @param mandatory true to throw a GuardMissingParameter if the parameter was not found, false to return null
     * @return The value of the requested parameter as long, or NULL if the parameter was not found and mandatory was set to false
     * @throws OXException If the parameter was not found and mandatory was set to true
     */
    public static Long getLongParameter(HttpServletRequest request, String name, boolean mandatory) throws OXException {
        String item = request.getParameter(name);
        if (item == null || item.equals("null")) {
            if (mandatory) {
                throw GuardCoreExceptionCodes.PARAMETER_MISSING.create(name);
            }
            return null;
        }
        try {
            return L(Long.parseLong(item.trim()));
        } catch (Exception e) {
            LOG.error("Problem getting long from parameter", e);
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Get long from HttpServletRequest
     *
     * @param request the request to get the long from
     * @param name the name of the parameter to get as long
     * @return The value of the requested parameter as long, or NULL if the parameter was not found
     */
    public static Long getLongParameter(HttpServletRequest request, String name) {
        try {
            return getLongParameter(request, name, false);
        } catch (OXException e) {
            return null;
        }
    }

    /**
     * Get boolean from HttpServletRequest
     *
     * @param request the request to get the boolean value from
     * @param name the name of the parameter to get as boolean
     * @return The value of the requested parameter as boolean
     */
    public static boolean getBooleanParameter(HttpServletRequest request, String name) {
        try {
            return getBooleanParameter(request, name, false);
        } catch (OXException e) {
            return false;
        }
    }

    /**
     * Get boolean from HttpServletRequest
     *
     * @param request the request to get the boolean value from
     * @param name the name of the parameter to get as boolean
     * @param mandatory true, if the parameter is mandatory, false if not
     * @return The value of the requested parameter as boolean
     * @throws OXException If the requested parameter was not found and mandatory is set to true
     */
    public static boolean getBooleanParameter(HttpServletRequest request, String name, boolean mandatory) throws OXException {
        String var = getStringParameter(request, name, mandatory);
        if (var != null) {
            if (var.toLowerCase().contains("true")) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets a value indicating whether the given parameter is contained in the request or not
     *
     * @param request The request
     * @param name The parameter name
     * @return <code>true</code> if the parameter is in the request, <code>false</code> otherwise
     */
    public static boolean hasParameter(HttpServletRequest request, String name) {
        return request.getParameterMap().containsKey(name);
    }

    /**
     * Gets String from parameter map with specified key
     *
     * @param map Parameter map to search
     * @param name Key to look up
     * @param mandatory Throw missing expection if missing and mandatory
     * @return String value, null if missing, throws Error if missing and mandatory
     * @throws OXException
     */
    public static String getStringParameter(Map<String, String[]> map, String name, boolean mandatory) throws OXException {
        String[] items = map.get(name);
        if (items == null && mandatory) {
            throw GuardCoreExceptionCodes.PARAMETER_MISSING.create(name);
        }
        if (items == null) {
            return null;
        }
        String item = items[0];
        if (item.isEmpty() && mandatory) {
            throw GuardCoreExceptionCodes.PARAMETER_MISSING.create(name);
        }
        return item;
    }

    /**
     * Parse a long value from parameter map
     *
     * @param map Map to use
     * @param name Key value for lookup
     * @param mandatory Throw error if missing
     * @return Long value, null if missing, or error if mandatory and missing
     * @throws OXException
     */
    public static Long getLongParameter(Map<String, String[]> map, String name, boolean mandatory) throws OXException {
        String item = getStringParameter(map, name, mandatory);
        if (item == null || item.isEmpty()) {
            return null;
        }
        try {
            return L(Long.parseLong(item.trim()));
        } catch (Exception e) {
            LOG.error("Problem getting long from parameter", e);
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Parse a boolean value from parameter map
     *
     * @param map Map to use
     * @param name Key value for lookup
     * @param mandatory Throw error if missing
     * @return boolean value, false if missing, or error if mandatory and missing
     * @throws OXException
     */
    public static boolean getBooleanParameter(Map<String, String[]> map, String name, boolean mandatory) throws OXException {
        String item = getStringParameter(map, name, mandatory);
        if (item == null) {
            return false;
        }
        return item.equalsIgnoreCase("true");
    }

    /**
     * Parses the parameter map from the given request
     *
     * @param map The request to parse the parameters from
     * @return The flat map including all the parameters parsed from the request
     */
    public static Map<String, Object> getMapParameters(HttpServletRequest request) {
        return getMapParameters(request.getParameterMap());
    }

    /**
     * Parses the given parameter map into a flat map
     *
     * @param map The map to parse
     * @return The flat map including all the parameters parsed from the given map
     */
    public static Map<String, Object> getMapParameters(Map<String, String[]> map) {
        HashMap<String, Object> ret = new HashMap<String, Object>();
        for (final Entry<String, String[]> parameter : map.entrySet()) {
            final String key = parameter.getKey();
            final String value = parameter.getValue().length > 0 ? parameter.getValue()[0] : null;
            if (value != null) {
                ret.put(key, value);
            }
        }
        return ret;
    }

    /**
     * Returns true if parameter map contains key
     *
     * @param map Map to use
     * @param name Key value for lookup
     * @return True if present and not empty, false if missing
     * @throws OXException
     */
    public static boolean hasParameter(Map<String, String[]> map, String name) throws OXException {
        String item = getStringParameter(map, name, false);
        return (item != null && !item.isEmpty());
    }
}
