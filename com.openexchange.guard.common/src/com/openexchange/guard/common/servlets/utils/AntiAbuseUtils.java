/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.common.servlets.utils;

import javax.servlet.http.HttpServletRequest;
import com.openexchange.antiabuse.AllowParameters;
import com.openexchange.antiabuse.Protocol;
import com.openexchange.antiabuse.ReportParameters;
import com.openexchange.antiabuse.ReportValue;

/**
 * {@link AntiAbuseUtils}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.8.0
 */
public class AntiAbuseUtils {

    /**
     * Overload method to create allow Parameter from userid /cid combination
     * @param request
     * @param userid
     * @param cid
     * @param password
     * @return
     */
    public static AllowParameters getAllowParameters (HttpServletRequest request, int userid, int cid, String password) {
        return getAllowParameters (request, userid + "-" + cid, password);
    }

    /**
     * Create antiabuse AllowParmeter from HttpServlet Request
     * @param request
     * @param login
     * @param password
     * @return
     */
    public static AllowParameters getAllowParameters (HttpServletRequest request, String login, String password) {
        return getAllowParameter (
            login,
            password,
            ServletUtils.getClientIP(request),
            request.getHeader("User-Agent"),
            request.getScheme().toLowerCase().contains("https") ? Protocol.HTTPS : Protocol.HTTP
            );
    }

    public static AllowParameters getAllowParameters(String clientIp, String userAgent, String scheme, int userid, int cid, String password) {
        return getAllowParameters(clientIp, userAgent, scheme, userid + "-" + cid, password);
    }

    public static AllowParameters getAllowParameters (String clientIp, String userAgent, String scheme, String login, String password) {
        return getAllowParameter (
            login,
            password,
            clientIp,
            userAgent,
            scheme.toLowerCase().contains("https") ? Protocol.HTTPS : Protocol.HTTP
            );
    }

    /**
     * Create antiabuse AllowParameter from elements
     * @param login
     * @param password
     * @param ip
     * @param userAgent
     * @param protocol
     * @return
     */
    public static AllowParameters getAllowParameter (String login, String password, String ip, String userAgent, Protocol protocol) {
        AllowParameters.Builder parameters = AllowParameters.builder()
            .login(login)
            .password(password)
            .remoteAddress(ip)
            .userAgent(userAgent)
            .protocol(protocol);
        return (parameters.build());
    }

    /**
     * Overload method to create Report Parameter from http request using userid and cid
     * @param success
     * @param request
     * @param userId
     * @param cid
     * @param password
     * @return
     */
    public static ReportParameters getReportParameter (boolean success, HttpServletRequest request, int userId, int cid, String password) {
        return getReportParameter (success, request, userId + "-" + cid, password);
    }

    public static ReportParameters getReportParameter(boolean success, String ip, String userAgent, String scheme, int userId, int cid, String password) {
        return getReportParameter(success, ip, userAgent, scheme, userId + "-" + cid, password);
    }

    /**
     * Overload method to create report Parameter from Http Request object
     * @param success
     * @param request
     * @param login
     * @param password
     * @return
     */
    public static ReportParameters getReportParameter (boolean success, HttpServletRequest request, String login, String password) {
        return getReportParameter (
            success ? ReportValue.SUCCESS : ReportValue.FAILURE,
                login,
                password,
                ServletUtils.getClientIP(request),
                request.getHeader("User-Agent"),
                request.getScheme().toLowerCase().contains("https") ? Protocol.HTTPS : Protocol.HTTP);
    }

    public static ReportParameters getReportParameter (boolean success,  String ip, String userAgent, String scheme, String login, String password) {
        return getReportParameter (
            success ? ReportValue.SUCCESS : ReportValue.FAILURE,
            login,
            password,
            ip,
            userAgent,
            scheme.toLowerCase().contains("https") ? Protocol.HTTPS : Protocol.HTTP);
    }

    /**
     * Overload method to create report from AllowParameter
     * @param reportValue
     * @param allowParam
     * @return
     */
    public static ReportParameters getReportParameter (ReportValue reportValue, AllowParameters allowParam) {
        return getReportParameter (reportValue,
            allowParam.getLogin(),
            allowParam.getPassword(),
            allowParam.getRemoteAddress(),
            allowParam.getUserAgent(),
            allowParam.getProtocol());
    }

    /**
     * Create ReportParameter from individual values
     * @param reportValue
     * @param login
     * @param password
     * @param remoteAddress
     * @param userAgent
     * @param protocol
     * @return
     */
    public static ReportParameters getReportParameter (ReportValue reportValue, String login, String password, String remoteAddress, String userAgent, Protocol protocol) {
        ReportParameters.Builder parameters = ReportParameters.builder()
            .reportValue(reportValue)
            .login(login)
            .password(password)
            .remoteAddress(remoteAddress)
            .protocol(protocol)
            .userAgent(userAgent);
        return parameters.build();
    }

}
