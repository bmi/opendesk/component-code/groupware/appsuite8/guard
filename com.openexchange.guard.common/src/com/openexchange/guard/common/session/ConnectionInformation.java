/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.common.session;

/**
 * {@link ConnectionInformation}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.3
 */
public class ConnectionInformation {

    private final String remoteHost;
    private final String remoteAddress;
    private final int remotePort;
    private final boolean isSecure;

    /**
     * Initializes a new {@link ConnectionInformation}.
     *
     * @param remoteHost The remote hostname of the connection.
     * @param remoteAddress The remote address of the connection.
     * @param remotePort The remote port of the connection.
     * @param isSecure Whether the connection was made through a secure channel or not.
     */
    public ConnectionInformation(String remoteHost, String remoteAddress, int remotePort, boolean isSecure) {
        this.remoteHost = remoteHost;
        this.remoteAddress = remoteAddress;
        this.remotePort = remotePort;
        this.isSecure = isSecure;
    }

    /**
     * Gets the remoteHost
     *
     * @return The remoteHost
     */
    public String getRemoteHost() {
        return remoteHost;
    }

    /**
     * Gets the remoteAddress
     *
     * @return The remoteAddress
     */
    public String getRemoteAddress() {
        return remoteAddress;
    }

    /**
     * Gets the remotePort
     *
     * @return The remotePort
     */
    public int getRemotePort() {
        return remotePort;
    }

    /**
     * Gets the isSecure
     *
     * @return The isSecure
     */
    public boolean isSecure() {
        return isSecure;
    }

}
