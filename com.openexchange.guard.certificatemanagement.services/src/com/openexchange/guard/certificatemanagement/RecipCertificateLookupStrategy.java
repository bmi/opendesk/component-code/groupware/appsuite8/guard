/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement;

import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.RecipCertificate;

/**
 * {@link RecipCertificateLookupStrategy} defines a strategy for resolving recipient S/MIME keys / certificates ({@link RecipCertificate}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public interface RecipCertificateLookupStrategy {

    /**
     * Search for {@link RecipCertificate} objects
     *
     * @param userId The user ID of the sender; i.e the user who performs the lookup
     * @param contextId The context ID of the sender; i.e the user who performs the lookup
     * @param email The email of the recipient for which to lookup a {@link RecipCertificate} S/MIME key / certificate
     * @param timeout The remaining time for lookup
     * @return The found {@link RecipCertificate}, or null if no such {@link RecipCertificate} was found
     * @throws Exception
     */
    RecipCertificate lookup(int userId, int contextId, String email, int timeout) throws OXException;
}
