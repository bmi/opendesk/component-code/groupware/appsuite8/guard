/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement;

import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.RecipCertificate;

/**
 * {@link RecipCertificateService} defines default functionality for resolving recipient certificates (S/MIME keys)
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public interface RecipCertificateService {

    /**
     * Gets the preferred, current or most recently used, {@link RecipCertificate} for the given email address
     *
     * @param The ID of the user who wants to lookup a certificate for a given email address
     * @param senderContextId The ID of the user who wants to lookup a certificate for a given email address
     * @param email The email address to get the certificate for
     * @return The {@link RecipCertificate} found for the given email, or <code>null</code> if no certificate (S/MIME key) was found.
     * @throws OXException
     */
    RecipCertificate getRecipCertificate(int userId, int cid, String email) throws OXException;
}
