/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.email.storage.ogEmail;

import java.sql.Connection;
import java.util.List;
import com.openexchange.exception.OXException;

/**
 * {@link EmailStorage} provides access to stored ogEmail mappings
 */
public interface EmailStorage {

    /**
     * Creates a new Email item or updates an item, if there is already an item for the given email
     *
     * @param email the email to create or update the item for
     * @param contextId the context id
     * @param userId the user id
     * @param dbShard the db shard id pointing to the database containing the key information related to this email
     * @throws due an error
     */
    void insertOrUpdate(String email, int contextId, int userId, int dbShard) throws OXException;

    /**
     * Creates a new Email item or updates an item, if there is already an item for the given email
     *
     * @param connection the connection to use
     * @param email the email to create or update the item for
     * @param contextId the context id
     * @param userId the user id
     * @param dbShard the db shard id pointing to the database containing the key information related to this email
     * @throws due an error
     */
    void insertOrUpdate(Connection connection, String email, int contextId, int userId, int dbShard) throws OXException;

    /**
     * Deletes all ogEmail mappings for a given user
     *
     * @param contextId the context id
     * @param userId the user id to delete the mappings for
     * @throws due an error
     */
    void deleteAllForUser(int contextId, int userId) throws OXException;

    /**
     * Deletes all ogEmail mappings for a given context
     *
     * @param contextId the context id to delete the mappings for
     * @throws OXException due an error
     */
    void deleteAllForContext(int contextId) throws OXException;

    /**
     * Gets a mapping for a user's email
     *
     * @param email the email to get the mapping for
     * @return the mapping for the given email, or null if no mapping was found
     */
    Email getByEmail(String email) throws OXException;

    /**
     * Gets all existing mappings
     *
     * @return A list of all known emails
     * @throws OXException
     */
    List<Email> getEmails() throws OXException;

    /**
     * Retrieves the e-mail address for the specified user
     *
     * @param userId The user identifier
     * @param cid The context identifier
     * @return The e-mail address
     * @throws OXException
     */
    String getEmail(int userId, int cid) throws OXException;

    /**
     * Gets a list of Email items for a given user
     *
     * @param contextId the context id of the user
     * @param userId the user's id
     * @return a list of Email items for the given user
     * @throws due an error
     */
    List<Email> getById(int contextId, int userId) throws OXException;

    /**
     * Gets the highest existing user id from all Email entries which context id is < 0 (Guests)
     *
     * @return the highest guest id for all Email entries related to a guest account,
     * @throws OXException due an error
     */
    int getHighestGuestId() throws OXException;

    /**
     * Gets all emails that match the userid hash
     *
     * @param hu
     * @return
     * @throws OXException
     */
    List<Email> getByHu(String hu) throws OXException;

    /**
     * Checks to see if Hu has been set up
     *
     * @return
     * @throws OXException
     */
    boolean hasHu() throws OXException;

    /**
     * Simple count of number of email addresses in og_email table
     *
     * @return
     * @throws OXException
     */
    int getCount() throws OXException;

    /**
     * Get count of missing user hashes
     */
    int getMissingHuCount() throws OXException;

    /**
     * Update the user hash
     *
     * @param email
     * @param hu
     * @throws OXException
     */
    boolean updateHu(String email, String hu) throws OXException;

    /**
     * Get a list of all emails with userid > 0
     *
     * @return
     * @throws OXException
     */
    List<Email> getMissingHu(int limit) throws OXException;

    /**
     * Update a users email address
     */
    void updateEmail(String oldEmail, String newEmail) throws OXException;

    /**
     * Adds a new {@link EmailStorageListener}
     *
     * @param listener a listener which gets notified on storage changes
     */
    void addListener(EmailStorageListener listener);

    /**
     * Removes a new {@link EmailStorageListener}
     *
     * @param listener a listener which gets notified on storage changes
     * @return True, if the listener was found and deleted, false otherwise
     */
    boolean removeListener(EmailStorageListener listener);
}
