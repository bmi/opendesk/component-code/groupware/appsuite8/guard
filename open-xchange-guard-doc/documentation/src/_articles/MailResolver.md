---
title: Mail Resolver
description: Resolving OX users
---

# Overview
When a Guard user sends an email to a recipient, Guard must be able to determine if the recipient is an OX user, or if that person uses a different email service. 
If the recipient is an OX user, Guard must be able to determine the users ID and context. 
This is done through the OX backend by default, which in turn looks up the email address using the default login2context table. 
Guard can, however, be configured to use a different URL for third party or custom handlers. 

# Default Handler

By default, Guard will try to resolve the email address against the OX middleware. 
The OX middleware must be able to resolve the email address using the login lookup tables. 
Email domains must not span multiple contexts. 
In addition, if a context is defined as a default login, the context must also be defined with the domains that exist within.

For example, if you created a default context (say context 1) using

```
/opt/open-xchange/sbin/createcontext -A oxadminmaster -P admin_master_password -c 1 \
-u oxadmin -d "Context Admin" -g Admin -s User -p admin_password -L defaultcontext ...
```

You must add the domain (substitue for company.com) for that context using

```
./changecontext -A oxadminmaster -P secret -c 1 -L company.com
```

You can add multiple domains to the context at the same time, seperating by ”,” 

##Variant of the default handler supported starting with 7.8.0

The OX middleware can be configured via

```
com.openexchange.mailmapping.lookUpByDomain=false
```

in *mailresolver.properties* to search for the target email address within the whole local OX installation.

__This can have a major impact on performance in bigger installations since every request will search in all existing user databases!__ 

# Custom Mail Resolver

If you have a custom authenticaion / login system, Guard will need to be configured to access your custom mail Resolver, 
and your custom system will need to provide the information required for Guard to process the recipient.

Set the custom URL in the */etc/open-xchange/guard/etc/guard.properties* file (Guard backend) by adding the setting *com.openexchange.guard.mailResolverUrl*

```
com.openexchange.guard.mailResolverUrl = https://yyyyy.com/resolver/?email= 
```

Guard will be adding the email address to be resolved at the end of the URL. 
If the line ends with a ”=”, please include a whitespace after the line for the parser

Guard will expect a JSON response. If the user is not an OX user, then a blank JSON should be returned ”{}”

If the user exists, a JSON response should be returned in the following format:

```
{
 "tester@open-xchange.com":
   {"uid":4,"user":
     {"language":"en_US","displayName":"Tester, OX"},
    "cid":1}
 }
```

If the user does not exist, a blank JSON should be returned

```
{}
```

Email address should be the identifier, followed by the uid (user id), cid (the context Id), and user data including language and display name.

__Note__: If there are multiple OX Backends served by this mail resolver, then only the user information to which Guard has access should be provided. 
The mailResolver should not provide userid/contextid information for a database that Guard doesn't have access to.

Most probably, the OX CID is already available in one of your existing LDAP / databases systems, 
to map your customer/contract-IDs to the corresponding OX AppSuite tenant/context ID. 
Then you need to lookup the userid + language + displayname of this email address, if its an AppSuite email address.

We recommend that you do a loadtest against your custom resolver endpoint. E.g via JMETER + CSV input for random email addresses.

__Note__: A custom mailresolver can also be implemented as an OSGi plugin for the App Suite middleware. 
Please contact us for more information or an offer for custom implementation. 
