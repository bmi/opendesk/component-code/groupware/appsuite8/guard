---
title: FAQ
description: Frequently asked questions
icon: fa-question
---

#What is OX Guard?

OX Guard is a fully integrated security add-on to OX App Suite. 
It provides end users with flexible email and file encryption. 
OX Guard is highly scalable, multi server and feature rich, but still simple-to-use. 
It is highly customizable and enables advertising, branding and viral user acquisition.

#Does OX Guard only work within the OX App Suite environment?

No. OX Guard allows you to send encrypted email and files to external recipients as well as within the OX App Suite environment. 
That said sending and receiving emails and files is a smoother experience when both sender and recipient are within the OX App Suite environment.

#What happens if I forget my OX Guard password?

You can reset your password from the OX Guard security settings page. A new temporary password will be sent to your secondary email address if available.

#Is an OX Guard User Guide available?

Yes the OX Guard User Guide is available in several languages and can be found [here](http://oxpedia.org/wiki/index.php?title=AppSuite:Main_Page_userguides#OX_Guard)

#How do I install OX Guard?

The "Quick Install Guide" can be found [here](./Installation.html)

#Server side encryption is not as good as client side encryption is it?

This may be true, but security only works if you use it. 
For the average user it may be better to make the trade off between complexity and usability in order to encourage use and drive familiarity with encryption tools. 
Using SSL/TLS from client to server is good enough and then OX Guard from server to server is very secure. The advantage is full flexibility for the user across devices and a much better user experience. 
