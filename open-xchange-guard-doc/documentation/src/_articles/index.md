---
title: OX Guard 
description: Welcome to OX Guard
---

Welcome to the documentation about the inner workings of OX Guard. This technical documentation covers articles about different topics and features, grouped by different subtopics on the left.

#Overview

OX Guard is a fully integrated security add-on to OX App Suite that provides end users with a flexible email and file encryption solution. OX Guard is a highly scalable, multi server, feature rich solution that is so simple-to-use that end users will actually use it. With a single click a user can take control of their security and send secure emails and share encrypted files. This can be done from any device to both OX App Suite and non-OX App Suite users.

OX Guard uses standard PGP encryption for the encryption of email and files. PGP has been around for a long time, yet has not really caught on with the masses. This is generally blamed on the confusion and complications of managing the keys, understanding trust, PGP format types, and lack of trusted central key repositories. Guard simplifies all of this, making PGP encryption as easy as a one click process, with no keys to keep track of, yet the options of advanced PGP management for those that know how.


## Key Features

- Simple security at the touch of a button
- Provides user based security - Separate from provider
- Supplementary security to Provider based security - Layered
- Powerful features yet simple to use and understand
- Security - Inside and outside of the OX environment
- Email and Drive integration
- Uses proven PGP security

## Availability

If an OX App Suite customer would like to evaluate OX Guard integration, the first step is to contact OX Sales. OX Sales will then work on the request and send prices and license/API (for the hosted infrastructure) key details to the customer.
