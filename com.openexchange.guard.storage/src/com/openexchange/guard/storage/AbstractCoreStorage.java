/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.storage;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.filestore.FileStorage;
import com.openexchange.filestore.FileStorageService;
import com.openexchange.guard.cipher.dataobject.EncryptedObject;
import com.openexchange.guard.database.GuardShardingService;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.inputvalidation.StringNotContainsInputValidator;
import com.openexchange.guard.storage.cache.FileCacheItem;
import com.openexchange.guard.storage.cache.FileCacheStorage;
import com.openexchange.guard.storage.osgi.Services;

/**
 * {@link AbstractCoreStorage}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public abstract class AbstractCoreStorage implements Storage {

    /**
     * A pattern which matches the default directory prefix used by {@link AbstractCoreStorage} {
     */
    protected Pattern prefixPattern = Pattern.compile("ext[-_](\\d*)[-_](\\d*)");
    protected FileStorageService fileStorageService;
    private static Logger LOG = LoggerFactory.getLogger(AbstractCoreStorage.class);

    /**
     * Pull an file URI from the file location
     * @param location
     * @return
     */
    protected abstract URI uriFromLocation (String location);

    /**
     * Pull an file URI from the directory
     * @param directory
     * @return
     */
    protected abstract URI uriFromDirectory (String directory);

    /**
     * Get the filename from a file location
     * @param location
     * @return
     */
    protected String fileNameFromLocation (String location) {
        if (location.indexOf("/") < 0) return location;  // No leading path
        String filename = location.substring(location.lastIndexOf("/") + 1);
        return filename;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.storage.Storage#readObj(int, int, java.lang.String)
     */
    @Override
    public InputStream readObj(String objectId) throws OXException {
        filterObjId(objectId);
        // First try to pull from cache
        FileCacheStorage storage = Services.getService(FileCacheStorage.class);
        FileCacheItem item = storage.getById(objectId);
        if (item != null && objectExists(item.getLocation())) {
            return getObjectStream(item.getLocation());
        }
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.storage.Storage#saveEncrObj(int, int, java.lang.String, byte[])
     */
    @Override
    public Boolean saveEncrObj(int userId, int contextId, String objectId, byte[] data) throws OXException {
        filterObjId(objectId);
        GuardShardingService sharding = Services.getService(GuardShardingService.class);
        String directoryPrefix = getDirectoryPrefix(userId, contextId, sharding.getShard(userId, contextId));

        String filename = writeObj(directoryPrefix, objectId, data);

        if (filename != null) {
            FileCacheStorage fileCacheStorage = Services.getService(FileCacheStorage.class);
            //Only inserting the file cache item if it does not exist yet
            //(It could exists when storing an attachment for multiple recipients)
            if (fileCacheStorage.getById(objectId) == null) {
                fileCacheStorage.insert(objectId, userId, directoryPrefix + DELIMITER + filename);
            }
        }

        return filename != null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.storage.Storage#copy(int, int, java.lang.String, java.lang.String)
     */
    @Override
    public boolean copy(int userId, int contextId, String sourceObjectId, String destinationObjectId) throws OXException {

        filterObjId(destinationObjectId);

        GuardShardingService sharding = Services.getService(GuardShardingService.class);
        String directoryPrefix = getDirectoryPrefix(userId, contextId, sharding.getShard(userId, contextId));

        //Copy
        try(InputStream objectStream = readObj(sourceObjectId)) {
            if (objectStream == null) {
                throw GuardCoreExceptionCodes.IO_ERROR.create("Unable to read file for copy");
            }
            final String filename = writeObj(directoryPrefix, destinationObjectId, IOUtils.toByteArray(objectStream));
            if(filename != null) {
                FileCacheStorage fileCacheStorage = Services.getService(FileCacheStorage.class);
                //(It could exists when storing an attachment for multiple recipients)
                if (fileCacheStorage.getById(destinationObjectId) == null) {
                    fileCacheStorage.insert(destinationObjectId, userId, directoryPrefix + DELIMITER + filename);
                }
            }
            return filename != null;
        } catch (IOException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.storage.Storage#getDirectoryPrefix(int, int, int)
     */
    @Override
    public String getDirectoryPrefix(int userId, int contextId, int shardId) throws OXException {
        String directory = "";
        if (contextId > 0) {
            directory = "ctx-" + contextId;
        } else {
            directory = "ext-" + shardId + "-" + userId;
        }
        return (directory);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.storage.Storage#getGuestShardIdFromPrefix(java.lang.String)
     */
    @Override
    public Integer getGuestShardIdFromPrefix(String directoryPrefix) {
        directoryPrefix = Objects.requireNonNull(directoryPrefix, "directoryPrefix must not be null");
        Matcher matcher = prefixPattern.matcher(directoryPrefix);
        if(matcher.find()) {
           return Integer.parseInt(matcher.group(1));
        }
        return null;
    }

    /**
     * Filter for .. attack
     *
     * @param objectId
     * @throws OXException
     */
    private void filterObjId(String objectId) throws OXException {
        new StringNotContainsInputValidator("..").assertIsValid(objectId, "objectId");
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.storage.Storage#deleteEncrObj(java.lang.String)
     */
    @Override
    public void deleteEncrObj(String location) throws OXException {
        if (!location.isEmpty() && location.contains("/") && location.indexOf("/") < location.length()) {
            String prefix = location.substring(0, location.indexOf("/") + 1);
            FileStorage storage = fileStorageService.getFileStorage(uriFromLocation(prefix));
            storage.deleteFile(location.substring(location.indexOf("/") + 1));
        } else {
            LOG.error("Unexpected file format for delete: " + location);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.storage.Storage#getObjectStream(java.lang.String)
     */
    @Override
    public InputStream getObjectStream(String location) throws OXException {
        try {
            FileStorage storage = fileStorageService.getFileStorage(uriFromLocation(location));
            return storage.getFile(fileNameFromLocation(location));
        } catch (Exception e) {
            LOG.error("Error retrieving file ", e);
            throw GuardCoreExceptionCodes.IO_ERROR.create("Guest file not found at location " + location);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.storage.Storage#getObjectStream(java.lang.String, java.lang.String)
     */
    @Override
    public InputStream getObjectStream(String directoryPrefix, String ObjId) throws OXException {
        try {
            FileStorage storage = fileStorageService.getFileStorage(uriFromDirectory(directoryPrefix));
            return storage.getFile(ObjId);
        } catch (Exception e) {
            LOG.error("Error retrieving file ", e);
        }
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.storage.Storage#deleteObj(java.lang.String)
     */
    @Override
    public void deleteObj(String location) throws OXException {
        FileStorage storage = fileStorageService.getFileStorage(uriFromLocation(location));
        storage.deleteFile(fileNameFromLocation(location));

    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.storage.Storage#getEncrObj(java.lang.String, java.lang.String)
     */
    @Override
    public EncryptedObject getEncrObj(String directoryPrefix, String ObjId) throws OXException {
        InputStream in = getObjectStream(directoryPrefix, ObjId);
        EncryptedObject obj = null;
        try {
            obj = new EncryptedObject(in);
        } catch (IOException e) {
            LOG.error("Problem getting encrypted object from file storage", e);
        } finally {
            IOUtils.closeQuietly(in);
        }
        return (obj);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.storage.Storage#writeObj(java.lang.String, java.lang.String, byte[])
     */
    @Override
    public String writeObj(String directoryPrefix, String ObjId, byte[] data) throws OXException {
        FileStorage storage = fileStorageService.getFileStorage(uriFromDirectory(directoryPrefix));
        try (InputStream in = new ByteArrayInputStream(data)) {
            String filename = storage.saveNewFile(in);
            return filename;
        } catch (IOException e) {
            LOG.error("Error writing object to file", e);
            throw GuardCoreExceptionCodes.IO_ERROR.create(e);
        }
    }
}
