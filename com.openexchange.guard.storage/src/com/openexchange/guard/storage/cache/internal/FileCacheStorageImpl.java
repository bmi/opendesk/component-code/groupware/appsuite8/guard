/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.storage.cache.internal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.GuardShardingService;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.storage.Storage;
import com.openexchange.guard.storage.cache.FileCacheItem;
import com.openexchange.guard.storage.cache.FileCacheStorage;
import com.openexchange.guard.storage.osgi.Services;
import com.openexchange.server.ServiceLookup;

/**
 * {@link FileCacheStorageImpl} provides a REST DB implementation of the FileCacheStorage
 */
public class FileCacheStorageImpl implements FileCacheStorage {

    private final ServiceLookup lookup;

    /**
     * Initialises a new {@link FileCacheStorageImpl}.
     */
    public FileCacheStorageImpl(ServiceLookup lookup) {
        super();
        this.lookup = lookup;
    }

    private String getStoragePrefix (int userId, int cid) throws OXException {
        Storage currentStorage = lookup.getService(Storage.class);
        GuardShardingService shardingService = lookup.getService(GuardShardingService.class);
        if (currentStorage == null || shardingService == null) {
            return "";
        }
        int shard = shardingService.getShard(userId, cid);
        return currentStorage.getDirectoryPrefix(userId, 0, shard);
    }

    @Override
    public FileCacheItem getById(String itemId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(FileCacheSql.GET_BY_ID_STMT);
            stmt.setString(1, itemId);
            resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                return new FileCacheItem(resultSet.getString(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getDate(4));
            }
            return null;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.storage.cache.FileCacheStorage#getByIdForUser(java.lang.String, java.lang.String)
     */
    @Override
    public FileCacheItem getByIdForUser(String itemId, int userId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(FileCacheSql.GET_BY_ID_FOR_USER);
            stmt.setString(1, itemId);
            stmt.setInt(2, userId);
            resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                return new FileCacheItem(resultSet.getString(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getDate(4));
            }
            return null;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    /**
     * Old sproxyd format for file prefix was x_ctx_store where x was the context Id of the guest user
     * This caused issues if the original sender context was deleted.
     * This can probably be removed in a version or two after 2.10.4
     *
     * checkOldFormat
     *
     * @param location
     * @return True if old format
     */
    private static boolean checkOldFormat(String location) {
        return (location != null && location.contains("_ctx_store"));
    }

    /**
     * Check for legacy file prefixes. Legacy file system removed in Guard 3.0
     * checkLegacy
     *
     * @param path
     * @param prefix
     * @return
     */
    private static boolean checkLegacy(String path, String prefix) {
        return path.startsWith(prefix.replaceAll("-", "_"));
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.storage.cache.FileCacheStorage#getForUser(int, int, int)
     */
    @Override
    public List<FileCacheItem> getForUser(int userId, int cid, int offset, int maxCount) throws OXException {
        List<FileCacheItem> ret = new ArrayList<FileCacheItem>();
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(FileCacheSql.GET_FOR_USER_WITH_PAGING);
            stmt.setInt(1, userId);
            stmt.setInt(2, maxCount);
            stmt.setInt(3, offset);
            resultSet = stmt.executeQuery();

            String filePrefix = getStoragePrefix(userId, cid);
            while (resultSet.next()) {
                String path = resultSet.getString("location");
                if (path.startsWith(filePrefix) || checkOldFormat(path) || checkLegacy(path, filePrefix)) {
                    ret.add(new FileCacheItem(resultSet.getString(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getDate(4)));
                }

            }
            return ret;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    @Override
    public void insert(String itemId, int userId, String path) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(FileCacheSql.ADD_STMT);
            stmt.setString(1, itemId);
            stmt.setInt(2, userId);
            stmt.setString(3, path);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }
    }

    @Override
    public void updateLastDate(String itemId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(FileCacheSql.UPDATE_LAST_DATE_STMT);
            stmt.setString(1, itemId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }
    }

    @Override
    public List<FileCacheItem> findOld(int days) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        ArrayList<FileCacheItem> ret = new ArrayList<FileCacheItem>();

        try {
            stmt = connection.prepareStatement(FileCacheSql.SELECT_BY_LAST_DATE_STMT);
            stmt.setInt(1, days);
            resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                ret.add(new FileCacheItem(resultSet.getString(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getDate(4)));
            }
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
        return ret;
    }

    @Override
    public void delete(String itemId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(FileCacheSql.DELETE_STMT);
            stmt.setString(1, itemId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.storage.cache.FileCacheStorage#findAllForUser(int)
     */
    @Override
    public List<FileCacheItem> findAllForUser(int userId, int cid) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        ArrayList<FileCacheItem> ret = new ArrayList<FileCacheItem>();

        try {
            stmt = connection.prepareStatement(FileCacheSql.SELECT_BY_USER);
            stmt.setInt(1, userId);
            resultSet = stmt.executeQuery();
            String filePrefix = getStoragePrefix(userId, cid);
            while (resultSet.next()) {
                String path = resultSet.getString("location");
                if (path.startsWith(filePrefix) || checkLegacy(path, filePrefix)) {
                    ret.add(new FileCacheItem(resultSet.getString(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getDate(4)));
                }
            }
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
        return ret;
    }
}
