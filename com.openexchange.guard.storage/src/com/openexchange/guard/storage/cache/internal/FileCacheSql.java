/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.storage.cache.internal;

public class FileCacheSql {

    public static final String GET_BY_ID_STMT = "SELECT itemID,userid, location, lastDate FROM FileCache WHERE itemID = ?";
    public static final String GET_BY_ID_FOR_USER = "SELECT itemID,userid, location, lastDate FROM FileCache WHERE itemID = ? AND userId = ?";
    public static final String GET_FOR_USER_WITH_PAGING = "SELECT itemId, userId, location, lastDate FROM FileCache where userId = ? ORDER BY lastDate DESC LIMIT ? OFFSET ?";
    public static final String ADD_STMT = "INSERT INTO FileCache VALUES (?, ?, ?, NOW())";
    public static final String UPDATE_LAST_DATE_STMT = "UPDATE FileCache SET lastDate = NOW() WHERE itemID = ?";
    public static final String SELECT_BY_LAST_DATE_STMT = "SELECT itemID,userid, location, lastDate FROM FileCache WHERE lastDate < DATE_SUB(NOW(), INTERVAL ? DAY);";
    public static final String SELECT_BY_USER = "SELECT itemID, userid, location, lastDate FROM FileCache WHERE userid = ?";
    public static final String DELETE_STMT = "DELETE FROM FileCache WHERE itemID = ?;";
}
