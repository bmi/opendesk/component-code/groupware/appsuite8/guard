/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.storage.osgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.GuardShardingService;
import com.openexchange.guard.storage.Storage;
import com.openexchange.guard.storage.cache.FileCacheStorage;
import com.openexchange.guard.storage.cache.internal.FileCacheStorageImpl;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link GuardStorageActivator}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class GuardStorageActivator extends HousekeepingActivator {

    /**
     * Initialises a new {@link GuardStorageActivator}.
     */
    public GuardStorageActivator() {
        super();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { GuardDatabaseService.class };
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() throws Exception {
        final Logger logger = LoggerFactory.getLogger(GuardStorageActivator.class);
        logger.info("Starting bundle {}", context.getBundle().getSymbolicName());
        Services.setServiceLookup(this);
        registerService(FileCacheStorage.class, new FileCacheStorageImpl(this));
        trackService(FileCacheStorage.class);
        trackService(Storage.class);
        trackService(GuardShardingService.class);
        openTrackers();
        logger.info("Guard FileCacheStorage registered.");
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.HousekeepingActivator#stopBundle()
     */
    @Override
    protected void stopBundle() throws Exception {
        final Logger logger = LoggerFactory.getLogger(GuardStorageActivator.class);
        logger.info("Stopping bundle {}", context.getBundle().getSymbolicName());
        unregisterService(FileCacheStorage.class);
        Services.setServiceLookup(null);
        logger.info("Guard FileCacheStorage registered.");
        super.stopBundle();
    }
}
