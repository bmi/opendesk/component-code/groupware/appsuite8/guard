/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.session.internal;

import com.openexchange.exception.OXException;

/**
 * {@link GuardSessionStorage} provides access to stored OX Guard session information
 */
public interface GuardSessionStorage {

    /**
     * Creates or updates a new session
     * 
     * @param sessionId the session to create
     * @param token the authentication token
     * @param userId the userId
     * @param contextId the contextId
     */
    public void InsertOrUpdate(String sessionId, String token, int userId, int contextId) throws OXException;

    /**
     * Get's a session by id
     * 
     * @param sessionId the session id
     * @return the session for the given sessionId or null if no such session was found or an error occurred
     */
    public GuardSession getById(String sessionId) throws OXException;

    /**
     * Deletes a session
     * 
     * @param sessionId the id of the session to delete
     */
    public void deleteById(String sessionId) throws OXException;

    /**
     * Deletes sessions which are older than the given amount of milliseconds
     * 
     * @param milliSeconds the milliseconds
     * @return The amount of session which have been removed 
     */
    public int deleteOldSessions(long milliSeconds) throws OXException;
}
