/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.secondfactor;

import com.openexchange.exception.OXException;

/**
 * {@link SecondFactorService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public interface SecondFactorService {

    /**
     * Check if the user has a second factor associated with the account
     *
     * @param oxUserid
     * @param oxCid
     * @param guardUserId
     * @param guardContextId
     * @return True if has a second factor
     * @throws OXException
     */
    public boolean hasSecondFactor(int oxUserid, int oxCid, int guardUserId, int guardContextId) throws OXException;

    /**
     * Associated a second factor token with a user
     * 
     * @param oxUserid
     * @param oxCid
     * @param guardUserId
     * @param guardContextId
     * @param token
     * @return true if successful
     * @throws OXException
     */
    public boolean addSecondFactor(int oxUserid, int oxCid, int guardUserId, int guardContextId, String token) throws OXException;

    /**
     * Verify if the token matches/valid for the second factor
     *
     * @param oxUserid
     * @param oxCid
     * @param guardUserId
     * @param guardContextId
     * @param token
     * @throws OXException if not valid with details
     */
    public void verifySecondFactor(int oxUserid, int oxCid, int guardUserId, int guardContextId, String token) throws OXException;

    /**
     * Remove the second factor token associated with the user
     * @param userid
     * @param cid
     * @throws OXException if fails for some reason
     */
    public void removeSecondFactor (int userid, int cid) throws OXException;

    /**
     * Remove the second factor token associated with the user if it is a single use token
     * @param userid
     * @param cid
     * @throws OXException if fails for some reason
     */
    public void removeSecondFactorIfSingleUse (int userid, int cid) throws OXException;

}
