/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.keysources;

import java.io.IOException;
import java.io.StringReader;
import java.util.Date;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import org.bouncycastle.openpgp.PGPKeyPair;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.operator.bc.BcPGPKeyPair;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.common.java.Strings;
import com.openexchange.guard.keymanagement.keysources.exceptions.KeySourcesExceptionCodes;
import com.openexchange.guard.keymanagement.services.KeyPairSource;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.keymanagement.storage.KeyCacheStorage;
import com.openexchange.server.ServiceLookup;

/**
 * {@link DbCachingKeyPairSource} defines a {@link KeyPairSource} which gets pre generated key pairs from the {@link KeyCacheStorage}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class DbCachingKeyPairSource implements KeyPairSource {

    private ServiceLookup services;
    private static final Logger logger = LoggerFactory.getLogger(DbCachingKeyPairSource.class);

    /**
     *
     * Initializes a new {@link DbCachingKeyPairSource}.
     *
     * @param guardCipherService The service used for decrypting pre-generated keys
     * @param guardMasterKeyService The service used for getting the master key in order to decrypt pre-generated keys
     */
    public DbCachingKeyPairSource(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Internal method to fetch pre generated key data from the storage
     *
     * @return The pre generated key data, or null if no such data is available
     * @throws OXException
     */
    private synchronized String getKeyData() throws OXException {
        KeyCacheStorage keyCacheStorage = services.getServiceSafe(KeyCacheStorage.class);
        return keyCacheStorage.popNext();
    }

    /**
     * Internal method to decrypt the given keyData
     *
     * @param keyData The encrypted key data
     * @return The decrypted key data
     * @throws OXException
     */
    private String decryptKeyData(String keyData) throws OXException {
        GuardCipherService guardCipherService = services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.AES_CBC);
        MasterKeyService mkeyService = services.getServiceSafe(MasterKeyService.class);
        if (!Strings.isEmpty(keyData)) {
            String decryptedKeyData = guardCipherService.decrypt(keyData, mkeyService.getMasterKey(0, false).getRC(), "key");
            return decryptedKeyData.isEmpty() ? null : decryptedKeyData;
        }
        else {
            logger.info("No pre-generated key pair available");
        }
        return null;
    }

    /**
     * Internal method for parsing key data
     *
     * @param keyData The key data to parse
     * @return The parsed key data as {@link PEMKeyPair} object
     * @throws IOException
     */
    private PEMKeyPair parseKeyData(String keyData) throws IOException {
        try (StringReader dataReader = new StringReader(keyData); PEMParser parser = new PEMParser(dataReader)) {
            return (PEMKeyPair) parser.readObject();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keymanagement.services.KeyPairCreationStrategy#createKeyPair()
     */
    @Override
    public PGPKeyPair get() throws OXException {

        //Get pre generated key data from storage and decrypt it for further processing
        String keyData = decryptKeyData(getKeyData());
        if (keyData != null) {
            try {
                //Create a new key pair from the pre generated decrypted key data
                PEMKeyPair pemKeyPair = parseKeyData(keyData);
                if (pemKeyPair == null) {
                    throw KeySourcesExceptionCodes.KEY_CREATION_ERROR.create("Unable to parse key data");
                }
                AsymmetricCipherKeyPair keyPair =
                    new AsymmetricCipherKeyPair(PublicKeyFactory.createKey(pemKeyPair.getPublicKeyInfo()),
                                                PrivateKeyFactory.createKey(pemKeyPair.getPrivateKeyInfo()));
                return new BcPGPKeyPair(PGPPublicKey.RSA_GENERAL, keyPair, new Date());
            } catch (Exception e) {
                throw KeySourcesExceptionCodes.KEY_CREATION_ERROR.create(e, e.getMessage());
            }
        }
        return null;
    }
}
