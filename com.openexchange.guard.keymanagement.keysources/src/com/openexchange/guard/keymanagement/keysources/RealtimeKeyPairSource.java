/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.keysources;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPKeyPair;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.operator.bc.BcPGPKeyPair;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.keymanagement.keysources.exceptions.KeySourcesExceptionCodes;
import com.openexchange.guard.keymanagement.services.KeyPairSource;

/**
 * {@link RealtimeKeyPairSource} implements a {@link KeyPairSource} which will create a {@link PGPKeyPair} on demand in realtime.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class RealtimeKeyPairSource implements KeyPairSource {

    private final int rsaKeyLength;
    private final int rsaCertainty;

    /**
     * Initializes a new {@link RealtimeKeyPairSource}.
     *
     * @param rsaKeyLength The RSA key length to use for new keys
     * @param rsaCertainty The RSA key certainty to use for new keys
     */
    public RealtimeKeyPairSource(int rsaKeyLength, int rsaCertainty) {
        this.rsaKeyLength = rsaKeyLength;
        this.rsaCertainty = rsaCertainty;
    }

    /**
     * Initializes a new {@link RealtimeKeyPairSource}.
     *
     * @param guardConfigService The {@link GuardConfigurationService} to get the required RSA configurations from.
     */
    public RealtimeKeyPairSource(GuardConfigurationService guardConfigService) {
        this(guardConfigService.getIntProperty(GuardProperty.rsaKeyLength),
             guardConfigService.getIntProperty(GuardProperty.rsaCertainty));
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keymanagement.services.KeyPairCreationStrategy#createKeyPair()
     */
    @Override
    public PGPKeyPair get() throws OXException {

        try {
            RSAKeyPairGenerator rsaKeyPairGenerator = new RSAKeyPairGenerator();
            rsaKeyPairGenerator.init(new RSAKeyGenerationParameters(BigInteger.valueOf(0x10001), new SecureRandom(), rsaKeyLength, rsaCertainty));
            return new BcPGPKeyPair(PGPPublicKey.RSA_GENERAL, rsaKeyPairGenerator.generateKeyPair(), new Date());
        } catch (PGPException e) {
            throw KeySourcesExceptionCodes.KEY_CREATION_ERROR.create(e, e.getMessage());
        }
    }
}
