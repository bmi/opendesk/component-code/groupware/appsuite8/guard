/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.hkp.internal;

import org.bouncycastle.openpgp.PGPPublicKeyRing;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.LongUtil;
import com.openexchange.guard.hkp.osgi.Services;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.export.KeyExportUtil;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.ratifier.GuardRatifierService;

/**
 *
 * {@link HKPGet} - The HKP "get" operation requests keys from the keyserver.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v.2.40
 */
public class HKPGet {

    /**
     * The "get" operation requests keys from the keyserver.
     *
     * @param search specifies which key to return
     * @return The key, or null, if no such key was found
     * @throws OXException
     */
    public String get(String search) throws OXException {
        if (search != null && !search.isEmpty()) {
            HKPSearch hkpSearch = new HKPSearch();
            if (search.contains("@")) {  // If email search

                String email = search.trim();
                GuardRatifierService validatorService = Services.getService(GuardRatifierService.class);
                validatorService.validate(email);
                GuardKeyService keyService = Services.getService(GuardKeyService.class);
                GuardKeys keys = keyService.getKeys(email);
                if (keys != null) {
                    return KeyExportUtil.export(keys.getPGPPublicKeyRing());
                }

            } else { // Otherwise keyid search

                search = normalizeSearch(search);

                PGPPublicKeyRing resultKeyRing = null;
                if (search.length() > 8) {
                    Long req_id = LongUtil.stringToLong(search);
                    resultKeyRing = hkpSearch.getPublicKeyRingById(req_id, 0, 0);
                } else {
                    resultKeyRing = hkpSearch.getPublicKeyRingById(normalizeSearch(search), 0, 0);
                }

                if (resultKeyRing != null) {
                    return KeyExportUtil.export(resultKeyRing);
                }
            }
        }
        return null;
    }

    /**
     * Normalize search parameter. If more than the 16 character ID (Fingerprint), then shorten
     *
     * @param search The search parameter to normalize
     * @return The normalized search parameter
     */
    private String normalizeSearch(String search) {
        search = search.replace(" ", "");  // remove padding from a fingerprint
        search = search.replace("0x", "").toLowerCase();
        if (search.length() > 16) {  // If more than 16, we'll just look at the last 16
            search = search.substring(search.length() - 16);
        }
        return search;
    }
}
