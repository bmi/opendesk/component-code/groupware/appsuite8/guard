/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.hkp.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.LongUtil;
import com.openexchange.guard.hkp.osgi.Services;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.OGPGPKey;
import com.openexchange.guard.keymanagement.commons.PGPKeys;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.storage.OGPGPKeysStorage;
import com.openexchange.guard.keymanagement.storage.PGPKeysStorage;

/**
 * {@link HKPSearch} - Contains internal logic for looking up keys serverd via HKP
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.2
 */
class HKPSearch {

    private static final Logger LOG = LoggerFactory.getLogger(HKPSearch.class);

    public PGPPublicKeyRing getPublicKeyRingById(long reqId, int userid, int cid) throws OXException {
        try {
            PGPKeysStorage pgpKeysStorage = Services.getService(PGPKeysStorage.class);
            PGPKeys keyMapping = pgpKeysStorage.getById(reqId);
            if (keyMapping != null) {
                if (keyMapping.isLocal()) {
                    GuardKeyService keyService = Services.getService(GuardKeyService.class);
                    GuardKeys key = keyService.getKeys(keyMapping.getEmail(), keyMapping.getKeyId());
                    if (key == null) {
                        return null;
                    }
                    return key.getPGPPublicKeyRing();
                }
            }

            String hexid = LongUtil.longToHexStringTruncated(reqId);
            return (getUploadedPublicKeyRingById(hexid, userid, cid));
        } catch (Exception ex) {
            LOG.error("Error while retrieving public key ring for " + reqId, ex);
            return (null);
        }
    }

    public PGPPublicKeyRing getPublicKeyRingById(String hexId, int userid, int cid) throws OXException {
        try {
            PGPKeysStorage pgpKeysStorage = Services.getService(PGPKeysStorage.class);
            PGPKeys keyMapping = pgpKeysStorage.getByHexId(hexId);
            if (keyMapping != null) {
                if (keyMapping.isLocal()) {
                    GuardKeyService keyService = Services.getService(GuardKeyService.class);
                    GuardKeys key = keyService.getKeys(keyMapping.getEmail(), keyMapping.getKeyId());
                    if (key == null) {
                        return null;
                    }
                    return key.getPGPPublicKeyRing();
                }
            }
            return (getUploadedPublicKeyRingById(hexId, userid, cid));

        } catch (Exception ex) {
            LOG.error("Error while retrieving public key ring for " + hexId, ex);
            return (null);
        }
    }

    public List<PGPPublicKeyRing> getPublicKeyRing(String email) throws OXException {
        GuardKeyService keyService = Services.getService(GuardKeyService.class);
        PGPKeysStorage pgpKeysStorage = Services.getService(PGPKeysStorage.class);
        List<PGPKeys> internalKeyMappings = pgpKeysStorage.getInternalByEmail(email);

        //remove duplicate mappings
        Set<PGPKeys> duplicatesRemoved = new TreeSet<PGPKeys>(new Comparator<PGPKeys>() {

            @Override
            public int compare(PGPKeys key1, PGPKeys key2) {
                if (key1.getKeyId() == key2.getKeyId()) {
                    return 0;
                }
                return key1.getKeyId() < key2.getKeyId() ? -1 : 1;
            }
        });
        duplicatesRemoved.addAll(internalKeyMappings);
        internalKeyMappings = new ArrayList<PGPKeys>(duplicatesRemoved);

        List<PGPPublicKeyRing> ret = new ArrayList<PGPPublicKeyRing>();
        for (PGPKeys internalMapping : internalKeyMappings) {
            if (internalMapping.isLocal()) {
                GuardKeys key = keyService.getKeys(email,internalMapping.getKeyId());
                if (key != null) {
                    if (key.isCurrent()) {
                        ret.add(0, key.getPGPPublicKeyRing());  // always add current key as the first
                    } else {
                        ret.add(key.getPGPPublicKeyRing());
                    }
                }
            }
        }
        return ret;
    }


    ////////////////////////////////// HELPERS /////////////////////////////////////////

    /**
     * Get the upload public key ring with the specified identifier
     *
     * @param pgpId The public key ring identifier
     * @param userid The user identifier
     * @param cid The context identifier
     * @return The PGP public key ring
     * @throws Exception
     */
    private PGPPublicKeyRing getUploadedPublicKeyRingById(String pgpId, int userid, int cid) throws Exception {
        if (cid == 0) {
            return null;
        }
        OGPGPKeysStorage ogpgpKeysStorage = Services.getService(OGPGPKeysStorage.class);
        List<OGPGPKey> pgpKeys = ogpgpKeysStorage.getForUserByIds(userid, cid, Arrays.asList(new String[] { pgpId }));
        if (pgpKeys.size() > 0) {
            GuardKeys key = new GuardKeys();
            key.setPGPKeyRingFromAsc(pgpKeys.get(0).getPublicPGPAscData());
            return key.getPGPPublicKeyRing();
        }
        return null;
    }
}
