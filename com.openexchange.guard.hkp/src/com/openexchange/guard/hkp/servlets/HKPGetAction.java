/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.hkp.servlets;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.hkp.internal.HKPGet;

/**
 *
 * {@link HKPGetAction} Handles incoming HKP get actions
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class HKPGetAction {

    private static final Logger logger = LoggerFactory.getLogger(HKPGetAction.class);
    private final HttpServletResponse response;
    private final String search;

    /**
     * Initializes a new {@link HKPGetAction}.
     * 
     * @param response
     * @param search
     */
    public HKPGetAction(HttpServletResponse response, String search) {
        this.response = response;
        this.search = search;
    }

    /**
     * Performs the get action
     * 
     * @throws OXException
     * @throws IOException
     */
    public void doAction() throws OXException, IOException {
        String keyContent = new HKPGet().get(search);
        if (keyContent != null) {
            response.setContentType("application/pgp-keys");
            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().println(keyContent);
            logger.debug("Found key, sending");
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            logger.debug("Key not found");
        }
    }
}
