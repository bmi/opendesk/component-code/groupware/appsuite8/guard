/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.activity.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.activity.ActivityTrackingService;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.guard.user.OXGuardUser;

/**
 * {@link ActivityTrackingServiceImpl}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class ActivityTrackingServiceImpl implements ActivityTrackingService {

    private final GuardDatabaseService guardDatabaseService;
    final Logger logger = LoggerFactory.getLogger(ActivityTrackingServiceImpl.class);

    public ActivityTrackingServiceImpl(GuardDatabaseService guardDatabaseService) {
        this.guardDatabaseService = guardDatabaseService;
    }
    /* (non-Javadoc)
     * @see com.openexchange.guard.activity.ActivityTrackingService#updateActivity(int, int)
     */
    @Override
    public void updateActivity(int userId, int contextId) {
        Connection connection = null;
        PreparedStatement stmt = null;
        try {
            connection = guardDatabaseService.getWritableForGuard();
            stmt = connection.prepareStatement(ActivityTrackingSql.UPDATE_ACTIVITY);
            stmt.setInt(1, userId);
            stmt.setInt(2, contextId);
            stmt.execute();
        } catch (OXException | SQLException ex) {
            logger.error("Problem saving activity", ex);
        } finally {
            DBUtils.closeSQLStuff(stmt);
            if (connection != null) {
                guardDatabaseService.backWritableForGuard(connection);
            }
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.activity.ActivityTrackingService#removeActivityRecord(int, int)
     */
    @Override
    public void removeActivityRecord(int userId, int contextId) {
        Connection connection = null;
        PreparedStatement stmt = null;
        try {
            connection = guardDatabaseService.getWritableForGuard();
            stmt = connection.prepareStatement(ActivityTrackingSql.REMOVE_ACTIVITY);
            stmt.setInt(1, userId);
            stmt.setInt(2, contextId);
            stmt.execute();
        } catch (OXException | SQLException ex) {
            logger.error("Problem saving activity", ex);
        } finally {
            DBUtils.closeSQLStuff(stmt);
            if (connection != null) {
                guardDatabaseService.backWritableForGuard(connection);
            }
        }

    }
    /* (non-Javadoc)
     * @see com.openexchange.guard.activity.ActivityTrackingService#getExpiredGuests()
     */
    @Override
    public List<OXGuardUser> getExpiredGuests(int days) {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ArrayList<OXGuardUser> users = new ArrayList<OXGuardUser>();
        try {
            connection = guardDatabaseService.getReadOnlyForGuard();
            stmt = connection.prepareStatement(ActivityTrackingSql.GET_OLD_ITEMS);
            stmt.setInt(1, days);
            rs = stmt.executeQuery();
            while (rs.next()) {
                int userId = rs.getInt(1);
                int contextId = rs.getInt(2);
                users.add(new OXGuardUser(userId, contextId));
            }
            return users;
        } catch (OXException | SQLException ex) {
            logger.error("Problem saving activity", ex);
            return users;
        } finally {
            DBUtils.closeSQLStuff(rs);
            DBUtils.closeSQLStuff(stmt);
            if (connection != null) {
                guardDatabaseService.backReadOnlyForGuard(connection);
            }
        }
    }

}
