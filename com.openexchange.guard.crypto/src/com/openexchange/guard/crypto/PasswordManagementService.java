/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.crypto;

import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.auth.PasswordChangedResult;
import com.openexchange.guard.common.session.GuardUserSession;

/**
 * {@link PasswordManagementService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public interface PasswordManagementService {

    /**
     * {@link ResetPasswordDestination} - represents the destination where a new password was sent to during a password-reset
     */
    public static enum ResetPasswordDestination {
        /**
         * The new password was sent to the primary email address
         */
        PRIMARY,

        /**
         * The new password was sent to the secondary email address
         */
        SECONDARY
    }

    /**
     * Returns the {@link CryptoType} associated with the implementation of this this interface
     *
     * @return The related {@link CryptoType}
     */
    CryptoType.PROTOCOL getCryptoType();

    /**
     * Changes the password associated with the "current" key
     *
     * @param userSession The {@link GuardUserSession} related to the user who wants to change the password
     * @param guestEmail The email of the guest in case the password should be changed for a guest's key, or null if the password is changed for a regular user
     * @param oldpass The current password required in order to change the password
     * @param newpass The new password to set
     * @param question Prior Guard 2.10: The question set for a guest account, or <code>null</code> otherwise
     * @param answer Prior Guard 2.10: The answer set for a guest accoun, or <code>null</code> otherwise
     * @param pin Prior Guard 2.10: The guest's second factor PIN if enabled and required, <code>null</code> otherwise
     * @return The result of the password changed operation
     * @throws OXException if the password was not changed successfully
     */
    PasswordChangedResult changePassword(GuardUserSession userSession, String guestEmail, String oldpass, String newpass, String question, String answer, String pin) throws OXException;

    /**
     * Get the secondary email address associated with the key/account
     *
     * @param userSession The {@link GuardUserSession} to get the secondary email for
     * @return String The secondary email for the given session, or null if not available
     * @throws OXException
     */
    String getSecondaryEmail(GuardUserSession userSession) throws OXException;

    /**
     * Change the secondary/recovery email address associated with the key/account
     *
     * @param userSession GuardUserSession
     * @param password Password for the key
     * @param email New email address to use
     * @throws OXException
     */
    void changeSecondary(GuardUserSession userSession, String password, String email) throws OXException;

    /**
     * Delete password recovery for the user
     *
     * @param userSession The {@link GuardUserSession} related to the user
     * @param password Password to verify authentication
     * @throws OXException
     */
    void deleteRecover(GuardUserSession userSession, String password) throws OXException;

    /**
     * Do password reset
     *
     * @param email Email address to reset
     * @return The destination to which the new password was sent
     * @throws OXException
     */
    default ResetPasswordDestination resetPass(String email) throws OXException {
        return resetPass(null, null, null, null, false, email);
    }

    /**
     * Do password reset
     *
     * @param userSession The {@link GuardUserSession} of the user who triggered the password reset, or <code>null</code> if the reset was triggered by administrative/maintainace/CLT/etc
     * @param lang User language to be used for creating email
     * @param hostname Hostname for template/product name
     * @param senderIp Sender IP
     * @param web True if from web
     * @param email Email address to reset
     * @return The destination to which the new password was sent
     * @throws OXException
     */
    ResetPasswordDestination resetPass(GuardUserSession userSession, String lang, String hostname, String senderIp, boolean web, String email) throws OXException;
}
