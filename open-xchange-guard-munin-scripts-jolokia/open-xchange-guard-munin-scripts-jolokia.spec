
Name:          open-xchange-guard-munin-scripts-jolokia
BuildArch:     noarch
#!BuildIgnore: post-build-checks
BuildRequires: ant
Version:       @OXVERSION@
%define        ox_release 4
Release:       %{ox_release}_<CI_CNT>.<B_CNT>
Group:         Applications/Productivity
License:       GNU General Public License (GPL)
BuildRoot:     %{_tmppath}/%{name}-%{version}-build
URL:           http://www.open-xchange.com/
Source:        %{name}_%{version}.orig.tar.bz2
Summary:       Open-Xchange Guard Munin scripts
Autoreqprov:   no
Requires:      munin-node, perl-JSON, perl-libwww-perl
Conflicts:     open-xchange-guard-munin-scripts

%description
Munin is a highly flexible and powerful solution used to create graphs of
virtually everything imaginable throughout your network, while still
maintaining a rattling ease of installation and configuration.

This package contains Open-Xchange plugins for the Munin node.

Munin is written in Perl, and relies heavily on Tobi Oetiker's excellent
RRDtool. To see a real example of Munin in action, you can follow a link
from <http://munin.projects.linpro.no/> to a live installation.

Authors:
--------
    Open-Xchange


%prep
%setup -q

%build

%install
export NO_BRP_CHECK_BYTECODE_VERSION=true
ant -lib build/lib -Dbasedir=build -DdestDir=%{buildroot} -DpackageName=%{name} -f build/build.xml clean build

%post
TMPFILE=`mktemp /tmp/munin-node.configure.XXXXXXXXXX`
munin-node-configure --libdir /usr/share/munin/plugins/ --shell > $TMPFILE || :
if [ -f $TMPFILE ] ; then
  sh < $TMPFILE
  rm -f $TMPFILE
fi
find -L /etc/munin/plugins -name 'ox_*' -type l -delete
/etc/init.d/munin-node restart || :
exit 0


%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root)
%dir /usr/share/munin
/usr/share/munin/plugins/
%dir /etc/munin/
%dir /etc/munin/plugin-conf.d/
%config(noreplace) /etc/munin/plugin-conf.d/*

%changelog
* Thu Aug 11 2022 Martin Schneider <martin.schneider@open-xchange.com>
First preview of 2.10.7 release
* Thu Aug 11 2022 Martin Schneider <martin.schneider@open-xchange.com>
prepare for 2.10.7 release
* Mon Jul 04 2022 Martin Schneider <martin.schneider@open-xchange.com>
Build for patch 2022-07-11 (6145)
* Thu Jun 02 2022 Martin Schneider <martin.schneider@open-xchange.com>
Build for patch 2022-05-30 (6138)
* Wed Mar 16 2022 Martin Schneider <martin.schneider@open-xchange.com>
Build for patch 2022-03-21 (6116)
* Mon Feb 21 2022 Martin Schneider <martin.schneider@open-xchange.com>
Build for patch 2022-02-22 (6107)
* Mon Feb 14 2022 Martin Schneider <martin.schneider@open-xchange.com>
Build for patch 2022-02-15 (6099)
* Mon Jan 17 2022 Martin Schneider <martin.schneider@open-xchange.com>
Build for patch 2022-01-24
* Mon Nov 29 2021 Martin Schneider <martin.schneider@open-xchange.com>
First candidate for 2.10.6 release
* Fri Oct 22 2021 Martin Schneider <martin.schneider@open-xchange.com>
First preview of 2.10.6 release
* Wed Jun 09 2021 Martin Schneider <martin.schneider@open-xchange.com>
prepare for 2.10.6 release
* Wed Apr 21 2021 Martin Schneider <martin.schneider@open-xchange.com>
Build for patch 2021-04-27 (5984)
* Tue Mar 23 2021 Martin Schneider <martin.schneider@open-xchange.com>
Build for patch 2021-03-29 (5979)
* Thu Feb 11 2021 Martin Schneider <martin.schneider@open-xchange.com>
Build for patch 2021-02-22 (5954)
* Mon Feb 01 2021 Martin Schneider <martin.schneider@open-xchange.com>
Second candidate for 2.10.5 release
* Fri Jan 15 2021 Martin Schneider <martin.schneider@open-xchange.com>
First candidate for 2.10.5 release
* Thu Dec 17 2020 Martin Schneider <martin.schneider@open-xchange.com>
Second preview of 2.10.5 release
* Mon Nov 30 2020 Martin Schneider <martin.schneider@open-xchange.com>
First preview of 2.10.5 release
* Tue Oct 06 2020 Martin Schneider <martin.schneider@open-xchange.com>
prepare for 2.10.5 release
* Tue Jul 28 2020 Martin Schneider <martin.schneider@open-xchange.com>
First candidate for 2.10.4 release
* Tue Jun 30 2020 Martin Schneider <martin.schneider@open-xchange.com>
Second preview of 2.10.4 release
* Wed May 20 2020 Martin Schneider <martin.schneider@open-xchange.com>
First preview of 2.10.4 release
* Mon Feb 03 2020 Martin Schneider <martin.schneider@open-xchange.com>
prepare for 2.10.4
* Thu Nov 28 2019 Martin Schneider <martin.schneider@open-xchange.com>
Second candidate of 2.10.3 release
* Thu Nov 21 2019 Martin Schneider <martin.schneider@open-xchange.com>
First candidate of 2.10.3 release
* Thu Oct 17 2019 Martin Schneider <martin.schneider@open-xchange.com>
First preview of 2.10.3 release
* Wed Jun 19 2019 Martin Schneider <martin.schneider@open-xchange.com>
prepare for 2.10.3 release
* Fri May 10 2019 Martin Schneider <martin.schneider@open-xchange.com>
First candidate of 2.10.2 release
* Wed May 01 2019 Martin Schneider <martin.schneider@open-xchange.com>
Second preview of 2.10.2 release
* Thu Mar 28 2019 Martin Schneider <martin.schneider@open-xchange.com>
First preview of 2.10.2 release
* Mon Mar 11 2019 Martin Schneider <martin.schneider@open-xchange.com>
prepare for 2.10.2
* Fri Nov 23 2018 Martin Schneider <martin.schneider@open-xchange.com>
RC 1 for 2.10.1 release
* Mon Nov 05 2018 Martin Schneider <martin.schneider@open-xchange.com>
Second preview for 2.10.1 release
* Fri Oct 12 2018 Martin Schneider <martin.schneider@open-xchange.com>
First preview for 2.10.1 release
* Mon Sep 10 2018 Martin Schneider <martin.schneider@open-xchange.com>
prepare for 2.10.1
* Mon Jun 25 2018 Martin Schneider <martin.schneider@open-xchange.com>
Second candidate for 2.10.0 release
* Mon Jun 11 2018 Martin Schneider <martin.schneider@open-xchange.com>
First candidate for 2.10.0 release
* Fri May 18 2018 Martin Schneider <martin.schneider@open-xchange.com>
Fifth preview of 2.10.0 release
* Thu Apr 19 2018 Martin Schneider <martin.schneider@open-xchange.com>
Fourth preview of 2.10.0 release
* Tue Apr 03 2018 Martin Schneider <martin.schneider@open-xchange.com>
Third preview of 2.10.0 release
* Tue Feb 20 2018 Martin Schneider <martin.schneider@open-xchange.com>
Second preview of 2.10.0 release
* Fri Feb 02 2018 Martin Schneider <martin.schneider@open-xchange.com>
First preview of 2.10.0 release
* Wed Jan 10 2018 Martin Schneider <martin.schneider@open-xchange.com>
prepare for 2.10.0 release
* Tue May 16 2017 Martin Schneider <martin.schneider@open-xchange.com>
First candidate for 2.8.0 release
* Thu May 04 2017 Martin Schneider <martin.schneider@open-xchange.com>
Second preview of 2.8.0 release
* Mon Apr 03 2017 Martin Schneider <martin.schneider@open-xchange.com>
First preview of 2.8.0 release
* Fri Dec 02 2016 Martin Schneider <martin.schneider@open-xchange.com>
prepare for 2.8.0 release
* Fri Nov 25 2016 Martin Schneider <martin.schneider@open-xchange.com>
Second release candidate for 2.6.0 release
* Thu Nov 24 2016 Martin Schneider <martin.schneider@open-xchange.com>
First release candidate for 2.6.0 release
* Tue Nov 15 2016 Martin Schneider <martin.schneider@open-xchange.com>
Third preview of 2.6.0 release
* Sat Oct 29 2016 Martin Schneider <martin.schneider@open-xchange.com>
Second preview of 2.6.0 release
* Fri Oct 14 2016 Martin Schneider <martin.schneider@open-xchange.com>
First preview of 2.6.0 release
* Wed Oct 12 2016 Martin Schneider <martin.schneider@open-xchange.com>
prepare for 2.6.0 release
* Tue Jul 12 2016 Martin Schneider <martin.schneider@open-xchange.com>
Second candidate for 2.4.2 release
* Wed Jul 06 2016 Martin Schneider <martin.schneider@open-xchange.com>
First candidate for 2.4.2 release
* Wed Jun 29 2016 Martin Schneider <martin.schneider@open-xchange.com>
Second preview for 2.4.2 release
* Thu Jun 16 2016 Martin Schneider <martin.schneider@open-xchange.com>
First candidate for 2.4.2 release
* Thu Jun 16 2016 Martin Schneider <martin.schneider@open-xchange.com>
prepare for 2.4.2
* Wed Mar 30 2016 Martin Schneider <martin.schneider@open-xchange.com>
Second candidate for 2.4.0 release
* Thu Mar 24 2016 Martin Schneider <martin.schneider@open-xchange.com>
First candidate for 2.4.0 release
* Wed Mar 16 2016 Martin Schneider <martin.schneider@open-xchange.com>
Fifth preview of 2.4.0 release
* Fri Mar 04 2016 Martin Schneider <martin.schneider@open-xchange.com>
Fourth preview of 2.4.0 release
* Sat Feb 20 2016 Martin Schneider <martin.schneider@open-xchange.com>
Third candidate for 2.4.0 release
* Fri Feb 05 2016 Martin Schneider <martin.schneider@open-xchange.com>
Second candidate for 2.4.0 release
* Fri Feb 05 2016 Martin Schneider <martin.schneider@open-xchange.com>
First candidate for 2.4.0 release
* Sat Apr 11 2015 Martin Schneider <martin.schneider@open-xchange.com>
prepare for 2.4.0
