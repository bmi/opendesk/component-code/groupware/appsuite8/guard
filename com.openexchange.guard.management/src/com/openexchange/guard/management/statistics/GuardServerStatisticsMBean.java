/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.management.statistics;

import javax.management.MBeanException;

/**
 * Marker interface for defining a statistics MBean
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @since v2.4.0
 */
public interface GuardServerStatisticsMBean {

    public static final String DOMAIN = "com.openexchange.guard";

    public static final String KEY = "name";

    public static final String VALUE = "GuardServerStats";

    public static final String DESCRIPTION = "Management Bean for the OX Guard base statistics";

    String getPoolInfo() throws MBeanException;

    String getMemoryInfo() throws MBeanException;

    int getPoolCount() throws MBeanException;

    int getPoolActive() throws MBeanException;

    int getPoolWaiting() throws MBeanException;

    long getUsedMemory() throws MBeanException;

    long getFreeMemory() throws MBeanException;

    long getMaxMemory() throws MBeanException;

    long getTotalMemory() throws MBeanException;

    int getThreadCount() throws MBeanException;

}
