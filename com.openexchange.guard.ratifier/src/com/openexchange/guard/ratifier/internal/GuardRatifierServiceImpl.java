/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.ratifier.internal;

import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.idn.IDNA;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.java.Strings;
import com.openexchange.guard.oxapi.Normalizer;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.guard.ratifier.exceptions.GuardRatifierExceptionCodes;

/**
 * {@link GuardRatifierServiceImpl}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class GuardRatifierServiceImpl implements GuardRatifierService {

    private static final Logger logger = LoggerFactory.getLogger(GuardRatifierServiceImpl.class);
    private static final String ip4Regex = "\\b([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})\\b";
    private static final String ip6Regex = "\\A(?:" +               //Anchor address
        "(?:" +                                                     // Mixed
        "(?:[A-F0-9]{1,4}:){6}" +                                   // Non-compressed
        "|(?=(?:[A-F0-9]{0,4}:){2,6}" +                             // Compressed with 2 to 6 colons
        "(?:[0-9]{1,3}\\.){3}[0-9]{1,3}" +                          //    and 4 bytes
        "\\z)" +                                                    //    and anchored
        "(([0-9A-F]{1,4}:){1,5}|:)((:[0-9A-F]{1,4}){1,5}:|:)" +     //    and at most 1 double colon
        "|::(?:[A-F0-9]{1,4}:){5}" +                                // Compressed with 7 colons and 5 numbers
        ")" +
        "(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.){3}" +  // 255.255.255.
        "(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])" +          // 255
        "|" +                                                       // Standard\n" +
        "(?:[A-F0-9]{1,4}:){7}[A-F0-9]{1,4}" +                      // Standard
        "|" +                                                       // Compressed\n" +
        "(?=(?:[A-F0-9]{0,4}:){0,7}[A-F0-9]{0,4}" +                 // Compressed with at most 7 colons
        "\\z)" +                                                    //    and anchored
        "(([0-9A-F]{1,4}:){1,7}|:)((:[0-9A-F]{1,4}){1,7}|:)" +      //    and at most 1 double colon
        "|(?:[A-F0-9]{1,4}:){7}:|:(:[A-F0-9]{1,4}){7}" +            // Compressed with 8 colons
        ")\\z";                                                     // Anchor address

    /**
     * Initialises a new {@link GuardRatifierServiceImpl}.
     */
    public GuardRatifierServiceImpl() {
        super();
    }

    @Override
    public boolean isValid(String email) {
        if (Strings.isEmpty(email)) {
            return false;
        }
        if (!email.contains("@")) {
            return false;
        }
        try {
            String aceConformMailAddress = IDNA.toACE(new String(email));
            InternetAddress e = new InternetAddress(aceConformMailAddress);
            e.validate();
        } catch (AddressException e) {
            logger.warn("{}", e);
            return false;
        }
        return true;
    }

    @Override
    public void validate(String email) throws OXException {
        if (!isValid(email)) {
            throw GuardRatifierExceptionCodes.INVALID_EMAIL_ADDRESS.create(email);
        }
    }

    @Override
    public String normalise(String data) throws OXException {
        return new Normalizer().normalizeString(data);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.ratifier.GuardRatifierService#verifyRemoteIP(java.lang.String)
     */
    @Override
    public String verifyRemoteIP(String ipAddress) {
        return verifyIP(ipAddress, false);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.ratifier.GuardRatifierService#verifyIPAddress(java.lang.String)
     */
    @Override
    public String verifyIPAddress(String ipAddress) {
        return verifyIP(ipAddress, true);
    }

    private String verifyIP(String ipAddress, boolean allowLocal) {
        if (ipAddress == null) return null;
        if (ipAddress.matches(ip4Regex)) {
            // ip4 address
            if (allowLocal) return ipAddress;
            // Check if local IP
            Pattern p = Pattern.compile(ip4Regex);
            Matcher m = p.matcher(ipAddress);
            if (!m.find()) return null;
            String first = m.group(1);
            String second = m.group(2);
            int secondInt = 0;
            if (second == null) {
                return null;
            }
            secondInt = Integer.parseInt(second);
            if (ipAddress.equals("127.0.0.1")) return null;  // Localhost
            // Check IP4 local addresses
            if (first.equals("10") ||
                (first.equals("192") && second.equals("168")) ||
                (first.equals("172") && (secondInt >= 16) && (secondInt <= 31))) {
                return null;
            }
            return ipAddress;
        }
        ipAddress = ipAddress.toUpperCase();
        if (ipAddress.matches(ip6Regex)) {
            if (allowLocal) return ipAddress;
            // Check if local address
            if (ipAddress.equals("0:0:0:0:0:0:0:1") || ipAddress.equals("::1")) return null;  // Localhost
            if (ipAddress.startsWith("FD")) return null;  // IP6 local address
            return (ipAddress);
        }
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.ratifier.GuardRatifierService#validateReasonableDate(java.lang.Long)
     */
    @Override
    public void validateReasonablePastDate(Long ticks) throws OXException {
        Date date = new Date(ticks);
        Calendar oneDayInFuture = Calendar.getInstance();
        oneDayInFuture.add(Calendar.HOUR, 24);
        Calendar tenYearsInPast = Calendar.getInstance();
        tenYearsInPast.add(Calendar.YEAR, -10);
        if (date.after(oneDayInFuture.getTime()) || date.before(tenYearsInPast.getTime())) {
            throw GuardRatifierExceptionCodes.INVALID_DATE_RANGE.create();
        }


    }
}
