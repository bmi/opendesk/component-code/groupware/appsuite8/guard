# Jacob Sparre Andersen <jacob@jacob-sparre.dk>, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: OX Guard templates\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-24 10:44-0400\n"
"PO-Revision-Date: 2019-05-07 13:06+0200\n"
"Last-Translator: Nicky Thomassen <nicky@workshop-chapina.com>\n"
"Language-Team: Danish <da@workshop-chapina.com>\n"
"Language: da_DK\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.8.4\n"

#. Autocrypt Setup message for transferring keys
#: ../templates/autocryptTransfer.html:2
msgid "Autocrypt Setup Message"
msgstr "Installationsbesked fra Autocrypt"

#: ../templates/autocryptTransfer.html:8
msgid "Your $productName key transfer email"
msgstr "Din e-mail til at overføre nøgler for $productName"

#: ../templates/autocryptTransfer.html:12
msgid ""
"This email contains the information required to transfer your keys securely "
"to another email client."
msgstr ""
"Denne e-mail indeholder de oplysninger du skal bruge, for sikkert at flytte "
"dine nøgler til en anden e-mail klient."

#: ../templates/autocryptTransfer.html:13
msgid ""
"To begin, open this email in the new client and follow the instructions.  "
"You will need the multi-digit passcode provided to you when this email was "
"created."
msgstr ""
"For at begynde, skal du åbne denne e-mail i den nye klient og følge "
"instruktionerne. Du får brug for den PIN-kode, du fik oplyst da denne e-mail "
"blev lavet."

#: ../templates/guestresettempl.html:1
msgid "$productName Account Reset"
msgstr "$productName nulstil konto"

#: ../templates/guestresettempl.html:7 ../templates/guesttempl.html:5
msgid "$productName - Your secure email link"
msgstr "$productName - Dit sikre e-mail-link"

#: ../templates/guestresettempl.html:318
msgid "Request received to reset your <strong>$productName</strong> account"
msgstr ""
"Ønske om at nulstille din <strong>$productName</strong>-konto modtaget."

#: ../templates/guestresettempl.html:319
msgid ""
"Clicking the link below will result in your account being reset, allowing "
"you to choose a password for your new account.  This will not reset the "
"password for previous emails, rather only apply to future emails secured "
"using $productName."
msgstr ""
"Hvis du klikker på henvisningen nedenfor, vil din konto blive nulstillet, og "
"du vil få mulighed for at vælge en adgangskode til din nye konto. Dette vil "
"ikke nulstille adgangskoden for tidligere modtagne e-mails. Det vil kun "
"påvirke fremtidige modtagne e-mails sikret med $productName."

#: ../templates/guestresettempl.html:320
msgid ""
"If you did not send this request, ignore this email and no changes will "
"occur to your account."
msgstr ""
"Hvis det ikke var dig der sendte denne forespørgsel, så ignorér venligst "
"denne e-mail, og der vil ikke ske nogle ændringer i din konto."

#: ../templates/guestresettempl.html:327
msgid "RESET ACCOUNT"
msgstr "NULSTIL KONTO"

#: ../templates/guestresettempl.html:334 ../templates/guesttempl.html:351
msgid ""
"If this does not work you can copy and paste the link below into your "
"browser."
msgstr ""
"Hvis dette ikke virker, så kan du kopiere linket nedenunder ind i din "
"browser."

#: ../templates/guestresettempl.html:359 ../templates/guesttempl.html:376
#: ../templates/passwordtempl.html:355 ../templates/resettempl.html:324
msgid ""
"Please do not reply to this email. If you want to contact us please click"
msgstr "Besvar venligst ikke denne e-mail. Hvis du vil kontakte os, så klik"

#: ../templates/guestresettempl.html:359 ../templates/guesttempl.html:376
#: ../templates/passwordtempl.html:355 ../templates/resettempl.html:324
msgid "here"
msgstr "her"

#: ../templates/guesttempl.html:336
msgid "You have received a secure <strong>$productName</strong> email"
msgstr "Du har modtaget en sikker <strong>$productName</strong> e-mail"

#: ../templates/guesttempl.html:337
msgid "Please click the button below to read the secure $productName email:"
msgstr ""
"Tryk venligst på knappen nedenunder for at læse den sikre $productName-e-"
"mail:"

#: ../templates/guesttempl.html:344
msgid "READ MESSAGE"
msgstr "LÆS BESKED"

#: ../templates/passwordtempl.html:1
msgid "Welcome to $productName"
msgstr "Velkommen til $productName"

#: ../templates/passwordtempl.html:7
msgid "$productName - Your secure email password"
msgstr "$productName - Din sikre e-mail-adgangskode"

#: ../templates/passwordtempl.html:303
msgid "Welcome to <strong>$productName</strong>"
msgstr "Velkommen til <strong>$productName</strong>"

#: ../templates/passwordtempl.html:305
msgid ""
"You have received this email because $from has sent you a secure email "
"message with $productName. You will receive a link to the secure message in "
"a separate email. $from added a personalized message to this invite:"
msgstr ""
"Du har modtaget denne e-mail, fordi $from har sendt dig en sikker e-mail "
"besked med $productName. Du vil modtage et link til den sikre besked i en "
"separat e-mail. $from tilføjede en personlig besked til denne invitation:"

#: ../templates/passwordtempl.html:308
msgid ""
"You have received this email because $from has sent you a secure email "
"message with $productName. You will receive a link to the secure message in "
"a separate email."
msgstr ""
"Du har modtaget denne e-mail, fordi $from har sendt dig en sikker e-mail med "
"$productName. Du vil modtage et link til den sikre besked i en separat e-"
"mail."

#: ../templates/passwordtempl.html:325
msgid ""
"$productName created the following temporary password in order for you to "
"access the secure email message:"
msgstr ""
"$productName oprettede den følgende midlertidige adgangskode, så du kan få "
"adgang til den sikre e-mail:"

#: ../templates/passwordtempl.html:337
msgid ""
"For extra security you will be asked to change your temporary password once "
"you access your secure email message for the first time."
msgstr ""
"For ekstra sikkerhed vil du blive bedt om, at skifte dit midlertidige "
"kodeord, når du åbner den sikre e-mail besked for første gang."

#. Password reset in subject line
#: ../templates/resettempl.html:2
msgid "Password Reset"
msgstr "Nulstil kodeord"

#: ../templates/resettempl.html:8
msgid "$productName - Your $productName password has been reset"
msgstr "$productName - Din $productName-adgangskode er blevet nulstillet"

#: ../templates/resettempl.html:292
msgid "<strong>$productName</strong> password reset"
msgstr "<strong>$productName</strong> nulstil kodeord"

#: ../templates/resettempl.html:293
msgid "Your $productName password has been reset. Your new password is:"
msgstr ""
"Din $productName-adgangskode er blevet nulstillet. Din nye adgangskode er:"

#: ../templates/resettempl.html:306
msgid ""
"For extra security please change this password in your $productName account."
msgstr ""
"For ekstra sikkerhed bedes du ændre denne adgangskode fra din $productName-"
"konto."

#: ../templates/upsell_example.html:3
msgid ""
"You have received an email protected with Guard Security.  If you would like "
"to find out how you can send fully protected emails, click on the following "
"link:"
msgstr ""
"Du har modtaget en e-mail beskyttet med Guard Security. Hvis du gerne vil "
"vide, hvordan du kan sende fuldt ud beskyttet e-mails, så tryk på det "
"følgende link:"

#: ../templates/upsell_example.html:5
msgid "Upgrade"
msgstr "Opgradér"

#~ msgid "OX Guard - Your secure email link"
#~ msgstr "OX Guard - Dit sikre e-mail-link"

#~ msgid "You have received a secure <strong>OX Guard</strong> email"
#~ msgstr "Du har modtaget en sikker <strong>OX Guard</strong> e-mail"

#~ msgid "Please click the button below to read the secure OX Guard email:"
#~ msgstr ""
#~ "Tryk venligst på knappen nedenunder for at læse den sikre OX Guard e-mail:"

#~ msgid "Welcome to OX Guard"
#~ msgstr "Velkommen til OX Guard"

#~ msgid "OX Guard - Your secure email password"
#~ msgstr "OX Guard - Dit sikre e-mail kodeord"

#~ msgid "Welcome to"
#~ msgstr "Velkommen til"

#~ msgid "OX Guard"
#~ msgstr "OX Guard"

#~ msgid ""
#~ "You have received this email because $from has sent you a secure email "
#~ "message with OX Guard. You will receive a link to the secure message in a "
#~ "separate email."
#~ msgstr ""
#~ "Du har modtaget denne e-mail, fordi $from har sendt dig en sikker e-mail "
#~ "med OX Guard. Du vil modtage et link til den sikre besked i en separat e-"
#~ "mail."

#~ msgid ""
#~ "OX Guard created the following temporary password in order for you to "
#~ "access the secure email message:"
#~ msgstr ""
#~ "OX Guard oprettede det følgende midlertidige kodeord, så du kan få adgang "
#~ "til den sikre e-mail besked:"

#~ msgid ""
#~ "Please click the link below to read the secure OX Guard email using your "
#~ "webmail:"
#~ msgstr ""
#~ "Tryk venligst på linket nedenunder for at læse den sikre OX Guard e-mail "
#~ "med din web-mail:"

#~ msgid ""
#~ "If this does not work you can copy and paste the link below into your "
#~ "browser or log into your webmail directly"
#~ msgstr ""
#~ "Hvis dette ikke virker, så kan du kopiere linket nedenunder ind i din "
#~ "browser, eller logge direkte ind i din web-mail"

#~ msgid ""
#~ "Please click the link below to read the secure $productName email using "
#~ "your webmail:"
#~ msgstr ""
#~ "Tryk venligst på linket nedenunder for at læse den sikre $productName-e-"
#~ "mail med din web-mail:"

#~ msgid ""
#~ "You have received this email because $from has sent you a secure email "
#~ "message with OX Guard. You will receive a link to the secure message in a "
#~ "separate email. $from added a personalized message to this invite:"
#~ msgstr ""
#~ "Du har modtaget denne e-mail, fordi $from har sendt dig en sikker e-mail "
#~ "besked med OX Guard. Du vil modtage et link til den sikre besked i en "
#~ "separat e-mail. $from tilføjede en personlig besked til denne invitation:"

#~ msgid "OX Guard - Your OX Guard password has been reset"
#~ msgstr "OX Guard - Dit OX Guard kodeord er blevet nulstillet"

#~ msgid "<strong>OX Guard </strong> password reset"
#~ msgstr "<strong>OX Guard </strong> nulstil kodeord"

#~ msgid "Your OX Guard password has been reset. Your new password is:"
#~ msgstr "Dit OX Guard kodeord er blevet nulstillet. Dit nye kodeord er:"

#~ msgid ""
#~ "For extra security please change this password in your OX Guard account."
#~ msgstr ""
#~ "For ekstra sikkerhed bedes du ændre dette kodeord fra din OX Guard konto."
