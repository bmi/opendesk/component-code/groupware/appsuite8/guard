/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.cron;

import java.util.List;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.activity.ActivityTrackingService;
import com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService;
import com.openexchange.guard.guest.metadata.storage.GuardGuestEmailMetadata;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.storage.Storage;
import com.openexchange.guard.storage.cache.FileCacheItem;
import com.openexchange.guard.storage.cache.FileCacheStorage;
import com.openexchange.guard.user.OXGuardUser;

/**
 * {@link GuestCleanupTask}
 *
 * Cleanup task for Guests created by Guard.  Cleans the fileCache and MetaData, removing all "shares" for a Guest.
 * This allows periodic Guest cleanup to delete the Guest user
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class GuestCleanupTask implements Runnable {

    private static Logger logger = LoggerFactory.getLogger(GuestCleanupTask.class);
    private final ActivityTrackingService activityTracker;
    private final FileCacheStorage fileCacheStorage;
    private final GuardGuestEMailMetadataService metaDataService;
    private final int daysToKeep;

    public GuestCleanupTask(int daysToKeep, ActivityTrackingService activityTracker, FileCacheStorage fileCacheStorage, GuardGuestEMailMetadataService metaDataService) {
        this.daysToKeep = daysToKeep;
        this.activityTracker = activityTracker;
        this.fileCacheStorage = Objects.requireNonNull(fileCacheStorage, "FileCacheStorage service missing");
        this.metaDataService = Objects.requireNonNull(metaDataService, "MetaDataService missing");
    }

    private String getDirectoryPrefix(String fullLocation) {
        if(fullLocation.contains(Storage.DELIMITER)) {
            return fullLocation.substring(0, fullLocation.indexOf(Storage.DELIMITER));
        }
        return null;
    }

    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        if (daysToKeep == 0) {
            return;
        }
        if (activityTracker == null) {
            return;
        }
        logger.info("Running Scheduled cleanup of Guests");

        //Getting unused fileCache items
        List<OXGuardUser> users = activityTracker.getExpiredGuests(daysToKeep);
        int failedItems = 0;
        int deletedItems = 0;

        //Remove the unused fileCache items from the storages
        for (OXGuardUser user : users) {
            try {
                List<FileCacheItem> items = fileCacheStorage.findAllForUser(user.getId(), user.getContextId());
                for (FileCacheItem fileCacheItem: items) {
                    //Remove from the file storage
                    Storage storage = Services.getService(Storage.class);
                    storage.deleteEncrObj(fileCacheItem.getLocation());
                    //Remove from the persistent storage (usually the DB)
                    fileCacheStorage.delete(fileCacheItem.getItemId());
                    Integer shardId = storage.getGuestShardIdFromPrefix(getDirectoryPrefix(fileCacheItem.getLocation()));
                    if (shardId != null) {
                        metaDataService.delete(shardId, new GuardGuestEmailMetadata(fileCacheItem.getItemId()));
                    }
                    deletedItems++;
                }
                activityTracker.removeActivityRecord(user.getId(), user.getContextId());
            } catch (OXException ex) {
                logger.error("Error delete object ", ex);
                failedItems++;
            }
        }
        logger.info("Expired Guests cleanup.  Deleted " + deletedItems + " files for " + users.size() + " users");
        if (failedItems > 0) {
            logger.error("Failed deleting " + failedItems + " files");
        }


    }

}
