/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.cron;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;

/**
 * {@link DailyTaskScheduler} schedules background tasks once a day.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public class DailyTaskScheduler {

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DailyTaskScheduler.class);
    private static long MINUTE_IN_MS = 60000;
    private static long DAY_IN_MS = MINUTE_IN_MS * 60 * 24;
    private final int hourOfTheDay;
    private final ArrayList<ScheduledTimerTask> taskList = new ArrayList<ScheduledTimerTask>();

    /**
     * Initializes a new {@link DailyTaskScheduler}.
     * 
     * @param hourOfTheDay Defines at what hour of the day the registered tasks should get executed.
     * @throws OXException if needed services {@link GuardConfigurationService}, {@link TimerService} are absent
     */
    public DailyTaskScheduler(int hourOfTheDay) throws OXException {
        this.hourOfTheDay = hourOfTheDay;
    }

    /**
     * Gets the time at which the jobs should get executed today
     * 
     * @return The time at which the jobs should get executed today
     * @throws OXException
     */
    private Date getExecutionDayTime() throws OXException {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfTheDay);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date time = calendar.getTime();
        return time;
    }

    /**
     * Gets the time until the next daily run of jobs will be performed.
     * If the configured time of the daily run has been passed for the current day, 0 will be returned.
     * 
     * @return The amount of time in ms to wait for the next daily run of jobs, or 0 if the configured hour of day is in the past for the current day.
     * @throws OXException
     */
    private long getTimeToNextExecution() throws OXException {
        Date dayExecutionTime = getExecutionDayTime();
        Date now = new Date();
        if (now.compareTo(dayExecutionTime) > 0) {
            ///The configured time for execution has already passed for today
            return 0;
        }
        else {
            return dayExecutionTime.getTime() - now.getTime();
        }
    }

    /**
     * Schedules the given tasks for being executed once a day.
     * 
     * @param task The tasks to schedule.
     * @throws OXException
     */
    public void register(Runnable... tasks) throws OXException {
        for (Runnable task : tasks) {
            long timeToExecution = getTimeToNextExecution();
            logger.info("Scheduling daily background task {} for running at hour {} each day and first execution at {}",
                task,
                hourOfTheDay,
                timeToExecution <= 0 ? "NOW" : new Date(new Date().getTime() + timeToExecution));
            TimerService timer = Services.getService(TimerService.class);
            taskList.add(timer.scheduleAtFixedRate(task, timeToExecution, DAY_IN_MS, TimeUnit.MILLISECONDS));
        }
    }

    /**
     * Cancels scheduling of all registered tasks.
     * If a task is executed while this method is called the task will not get canceled.
     */
    public void unregisterAll() {
        for (ScheduledTimerTask task : taskList) {
            task.cancel();
        }
    }
}
