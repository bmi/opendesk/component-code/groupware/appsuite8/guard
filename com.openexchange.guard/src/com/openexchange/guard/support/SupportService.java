/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.support;

import java.net.URI;
import com.openexchange.exception.OXException;
import com.openexchange.guard.support.PasswordReseter.EmailTarget;

/**
 * Interface for OX Guard Support functionality
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public interface SupportService {

    /**
     * Marks a deleted key as "exposed", which allows a user to to download a key
     * @param contextId the context ID of the user
     * @param email the email of the user
     * @return an URI to access the deleted key for downloading it
     * @throws OXException
     */
    public URI exposeKey(int contextId, String email) throws OXException;

    /**
     * Resets a user's password, a new password will be sent to the user's primary or secondary email address
     * @param contextId the context ID
     * @param email the email
     * @param language to use for creating the reset email
     * @throws OXException
     */
    public EmailTarget resetPassword(String email, String language) throws OXException;

    /**
     * Deletes a user and all of his keys. Deleted keys are backed up and can still be accessed as download by using {@link #ExposeKey}
     * @param contextId the context id
     * @param userId the user's id
     * @throws OXException
     */
    public void deleteUser(int contextId, int userId) throws OXException;

    /**
     * Deletes a user and all of his keys. Deleted keys are backed up and can still be accessed as download by using {@link #ExposeKey}
     * @param email the email address of the user which should be deleted
     * @throws OXException
     */
    public void deleteUser(String email) throws OXException;

    /**
     * Removes the secret PIN from a guest key
     * @param email the email address of the guest to remove the pin for
     * @throws OXException
     */
    public void removePin(String email) throws OXException;

    /**
     * Upgrade a guest account to a regular user
     * @param email the email addres sof the guest which should be upgraded to a regular user
     * @throws OXException
     */
    public void upgradeGuestAccount(String email, String userid, String cid) throws OXException;
}
