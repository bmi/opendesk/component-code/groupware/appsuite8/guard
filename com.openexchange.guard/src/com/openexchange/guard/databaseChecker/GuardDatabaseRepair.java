/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.databaseChecker;

import static com.openexchange.java.Autoboxing.i;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import com.openexchange.database.DatabaseService;
import com.openexchange.exception.OXException;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.guard.email.storage.ogEmail.Email;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.PGPKeys;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.storage.PGPKeysStorage;
import com.openexchange.guard.osgi.Services;

/**
 * Methods to verify the integrity of the Guard main lookup tables
 * Checks for missing keys in the keytables and populates the tables as needed
 *
 * {@link GuardDatabaseRepair}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class GuardDatabaseRepair {

    private final GuardDatabaseService guardDatabaseService;
    private final DatabaseService databaseService;
    private final StringBuilder writer;

    private static final String getOgKeyTableEntries = "SELECT id, cid, email, keyid FROM og_KeyTable";
    private static final String getShardCount = "SELECT current_shard FROM sharding";
    private static final String NEWLINE = System.lineSeparator();

    public GuardDatabaseRepair () throws OXException {
        this.writer = new StringBuilder();
        this.guardDatabaseService = Services.getService(GuardDatabaseService.class);
        this.databaseService = guardDatabaseService.getDatabaseService();
    }

    /**
     * Primary function.
     * Checks the og_KeyTables in the user shards and Guest shards
     * @throws OXException
     */
    public StringBuilder doRepair (boolean dryRun) throws OXException {
        Connection configCon = null;
        try {
            // First, get a list of db schemas
            configCon = databaseService.getReadOnly();
            Map<String, Integer> schema = databaseService.getAllSchemata(configCon);
            databaseService.backReadOnly(configCon);
            Iterator<Entry<String, Integer>> it = schema.entrySet().iterator();
            // For each schema, let's check to og_KeyTables
            while (it.hasNext()) {
                Map.Entry<String, Integer> scheme = it.next();
                Connection schemaConnection = databaseService.get(i(scheme.getValue()), scheme.getKey());  // Get connection for specific schema
                writer.append("Checking schema: " + scheme.getKey());
                writer.append(NEWLINE);
                checkKeysInMasterTable(schemaConnection, 0, dryRun);  // Do the check
                databaseService.back(i(scheme.getValue()), schemaConnection);
            }
            // Now for Guests
            Connection guardCon = null;
            Statement stmt = null;
            ResultSet rs = null;
            int count = 0;
            try {
                // Need to get count of total shards
                guardCon = guardDatabaseService.getReadOnlyForGuard();
                stmt = guardCon.createStatement();
                rs = stmt.executeQuery(getShardCount);
                if (rs.next()) {
                    count = rs.getInt(1);
                }
            } catch (SQLException e) {
                writer.append(e);
                writer.append(NEWLINE);
            } finally {
                // close before updating to avoid timeout errors
                DBUtils.closeSQLStuff(rs, stmt);
                guardDatabaseService.backReadOnlyForGuard(guardCon);
            }
            // OK, have count.  Now, check each
            if (count > 0) {
                for (int i = 1; i <= count; i++) {
                    try {
                        writer.append("Checking Guest Schema " + i);
                        writer.append(NEWLINE);
                        Connection shardCon = guardDatabaseService.getReadOnlyForShard(i); // Get connection for each shard
                        checkKeysInMasterTable(shardCon, i, dryRun); // Do check
                        guardDatabaseService.backWritableForShard(i, shardCon);
                    } catch (OXException ex) {
                        writer.append("Problem checking Guest Schema " + i);
                        writer.append(NEWLINE);
                        writer.append(ex);
                    }
                }
            }
            return writer;
        } finally {
            try {
                if (configCon != null && !configCon.isClosed()) {
                    configCon.close();
                }
            } catch (SQLException e) {
                // Silent fail.  Transient likely already closed
            }
        }
    }

    /**
     * Function to check the og_email and PGPKeys for each database
     * @param con
     * @param db
     * @throws OXException
     */
    private void checkKeysInMasterTable (Connection con, int db, boolean dryRun) throws OXException {
        Statement stmt = null;
        ResultSet rs = null;
        EmailStorage storage = Services.getService(EmailStorage.class);
        PGPKeysStorage pgpStorage = Services.getService(PGPKeysStorage.class);
        GuardKeyService keyService = Services.getService(GuardKeyService.class);
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(getOgKeyTableEntries);
            // For each key entry in og_KeyTable
            while (rs.next()) {
                int id = rs.getInt(1);
                int cid = rs.getInt(2);
                String email = rs.getString(3);
                long keyid = rs.getLong(4);
                // Check og_email table first
                List<Email> emails = storage.getById(cid, id);
                if (emails.isEmpty()) { // If empty, then let's insert it
                    writer.append("Missing email " + email + " in og_email. " + (dryRun ? "": "Restoring"));
                    writer.append(NEWLINE);
                    if (dryRun) {
                        writer.append("To add email " + email + " to og_email");
                        writer.append(NEWLINE);
                    } else {
                        storage.insertOrUpdate(email, cid, id, db);
                    }
                }
                // Next, check PGPKey storage to make sure lookup by pgp key will work
                PGPKeys key = pgpStorage.getById(keyid);
                if (key == null) {  // If not found, missing.  Add
                    writer.append("Missing public key lookup for keyid " + keyid + ". " + (dryRun ? "": "Restoring"));
                    writer.append(NEWLINE);
                    if (!dryRun) {
                        Collection <GuardKeys> keys = keyService.getAllKeys(id, cid);
                        if (!keys.isEmpty()) {
                            Iterator<GuardKeys> it = keys.iterator();
                            while (it.hasNext()) {  // May be multiple keys.  Only add back the one that is missing
                                final GuardKeys toAdd = it.next();
                                if (toAdd.getKeyid() == keyid) {  // The missing keyID
                                    pgpStorage.addPublicKeyIndex(cid, toAdd.getKeyid(), toAdd.getEmail(), toAdd.getPGPPublicKeyRing());
                                    writer.append("added");
                                    writer.append(NEWLINE);
                                }
                            }
                        }
                    }

                }
            }
        } catch (SQLException e) {
            writer.append(String.format("Error during %s repair: %s", dryRun ? "dry" : "", e.getMessage()));
            e.printStackTrace();
        } finally {
            DBUtils.closeSQLStuff(rs, stmt);
        }
    }

}
