/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.internal;

import java.io.ByteArrayInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonObject;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.GuardMaintenanceService;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.crypto.CryptoManager;
import com.openexchange.guard.crypto.PasswordManagementService;
import com.openexchange.guard.databaseChecker.GuardDatabaseRepair;
import com.openexchange.guard.email.storage.ogEmail.Email;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.guest.GuestLookupService;
import com.openexchange.guard.guestreset.GuestResetService;
import com.openexchange.guard.guestupgrade.GuestUpgradeService;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.ImportSystemPublicKey;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.keymanagement.storage.KeyCacheStorage;
import com.openexchange.guard.keymanagement.storage.RemoteKeyCacheStorage;
import com.openexchange.guard.mailcreator.MailCreatorService;
import com.openexchange.guard.notification.GuardNotificationService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.oxapi.MailResolver;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.guard.secondfactor.SecondFactorService;
import com.openexchange.guard.smime.SmimeKeyService;
import com.openexchange.guard.smime.storage.CaCertStorage;
import com.openexchange.guard.support.SupportService;
import com.openexchange.guard.support.SupportServiceImpl;
import com.openexchange.guard.support.UserChanger;
import com.openexchange.server.ServiceLookup;

/**
 * {@link GuardMaintenanceServiceImpl}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class GuardMaintenanceServiceImpl implements GuardMaintenanceService {

    private static final Logger LOG = LoggerFactory.getLogger(GuardMaintenanceServiceImpl.class);
    private final ServiceLookup services;

    /**
     * Initialises a new {@link GuardMaintenanceServiceImpl}.
     */
    public GuardMaintenanceServiceImpl(ServiceLookup services) {
        super();
        this.services = services;
    }

    @Override
    public JsonObject test(String mail) throws OXException {
        final MailResolver mailResolver = new MailResolver();
        JsonObject userdat;
        try {
            userdat = mailResolver.resolveUser(mail);
            if (userdat != null && userdat.has(mail)) {
                return userdat;
            }
            throw GuardMaintenanceExceptionCodes.UNABLE_TO_RESOLVE_MAIL_ADDRESS.create(mail);
        } catch (final Exception e) {
            LOG.error("Error getting email from REST API", e);
            throw GuardMaintenanceExceptionCodes.UNABLE_TO_RESOLVE_MAIL_ADDRESS.create(e, mail);
        }
    }

    @Override
    public void reset(String mail, CryptoType.PROTOCOL type) throws OXException {
        GuardRatifierService validatorService = services.getServiceSafe(GuardRatifierService.class);
        validatorService.validate(mail);
        try {
            this.resetPass(mail, type);
        } catch (final Exception e) {
            throw GuardMaintenanceExceptionCodes.FAILED_RESETTING_PASSWORD.create(e, mail);
        }
    }

    /*
     * Reset password. Used from command line tools only
     */
    private void resetPass(String email, CryptoType.PROTOCOL type) throws Exception {
        try {
            //TODO-SMIME: This if statement seems unnecessary. Resolve
            CryptoManager cryptoManager = services.getServiceSafe(CryptoManager.class);
            PasswordManagementService keyService = cryptoManager.getPasswordManagementService(type);
            if (type == CryptoType.PROTOCOL.SMIME) {
                if (keyService != null) {
                    keyService.resetPass(email);
                    return;
                }
                throw GuardMaintenanceExceptionCodes.FAILED_RESETTING_PASSWORD.create();
            }
            GuardKeyService guardKeyService = services.getServiceSafe(GuardKeyService.class);
            GuardKeys keys = guardKeyService.getKeys(email);
            if (keys == null) {
                throw GuardMaintenanceExceptionCodes.UNABLE_TO_RESOLVE_MAIL_ADDRESS.create(email);
            }
            if (keys.getContextid() < 0) {// Guest
                int templid = 0;
                GuestLookupService guestService = services.getServiceSafe(GuestLookupService.class);
                int userId = guestService.lookupOxUserId(email, keys.getContextid());
                int cid = keys.getContextid() * -1;
                try {
                    GuardConfigurationService guardConfigService = services.getServiceSafe(GuardConfigurationService.class);
                    templid = guardConfigService.getIntProperty(GuardProperty.templateID, userId, cid);
                } catch (Exception e) {
                    LOG.error("problem getting template id for reset password email", e);
                }
                GuardConfigurationService configService = services.getServiceSafe(GuardConfigurationService.class);
                String defaultLanguage = configService.getProperty(GuardProperty.defaultLanguage, userId, cid);

                reset(email, guardKeyService, userId, cid, keys, templid, defaultLanguage);
                LOG.info("Reset sent for email {}", email);
            } else {
                services.getServiceSafe(PasswordManagementService.class);
                //@formatter:off
                keyService.resetPass(
                    new GuardUserSession(keys.getContextid(), keys.getUserid(), keys.getContextid(), keys.getUserid()),
                    keys.getLanguage(),
                    null,
                    null,
                    false,
                    email);
                //@formatter:on
            }
        } catch (Exception e) {
            LOG.error("Error resetting password for " + email, e);
            throw e;
        }
    }

    protected void reset(String email, GuardKeyService keyService, int userId, int cid, GuardKeys keys, int templid, String defaultLanguage) throws OXException {
        boolean recoveryEnabled = keyService.isRecoveryEnabled(keys.getUserid(), keys.getContextid());
        if (!recoveryEnabled) {
            LOG.info("Failed to reset password for email address {} as recovery is not enabled/available.", email);
            throw GuardCoreExceptionCodes.DISABLED_ERROR.create("reset password");
        }

        String newpass = keyService.generatePassword(userId, cid);
        MailCreatorService mailCreatorService = services.getServiceSafe(MailCreatorService.class);
        JsonObject mail = mailCreatorService.getResetEmail(email, mailCreatorService.getFromAddress(email, email, userId, cid), newpass, defaultLanguage, templid, "", 0, 0);

        GuardNotificationService guardNotificationService = services.getServiceSafe(GuardNotificationService.class);
        guardNotificationService.send(mail, userId, cid, null);

        keyService.resetPassword(email, newpass);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.GuardMaintenanceService#removePIN(java.lang.String)
     */
    @Override
    public void removePIN(String mail) throws OXException {
        // Pre 2.10, can be removed soon
        GuardKeyService keyService = services.getServiceSafe(GuardKeyService.class);
        keyService.removePin(mail);
        // 2.10 and post
        EmailStorage ogEmailStorage = services.getServiceSafe(EmailStorage.class);
        Email ogEmail = ogEmailStorage.getByEmail(mail);
        if (ogEmail == null) {
            throw GuardMaintenanceExceptionCodes.ACCOUNT_NOT_FOUND.create(mail);
        }
        SecondFactorService secondFactor = services.getServiceSafe(SecondFactorService.class);
        secondFactor.removeSecondFactor(ogEmail.getUserId(), ogEmail.getContextId());
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.GuardMaintenanceService#upgrade(java.lang.String)
     */
    @Override
    public void upgrade(String mail, String userid, String cid) throws OXException {
        GuardRatifierService validatorService = services.getServiceSafe(GuardRatifierService.class);
        validatorService.validate(mail);

        try {
            GuestUpgradeService upgradeService = services.getServiceSafe(GuestUpgradeService.class);
            upgradeService.upgrade(mail, userid, cid);
        } catch (final Exception e) {
            throw GuardMaintenanceExceptionCodes.FAILED_UPGRADING_TO_OX_ACCOUNT.create(e, mail);
        }
    }

    @Override
    public void delete(String email) throws OXException {
        GuardRatifierService validatorService = services.getServiceSafe(GuardRatifierService.class);
        validatorService.validate(email);
        SupportService svc = new SupportServiceImpl();
        svc.deleteUser(email);
    }

    @Override
    public void resetGuest(String email, String fromEmail, int fromId, int fromCid) throws OXException {
        GuestResetService resetService = Services.getService(GuestResetService.class);
        resetService.reset(email, fromEmail, fromId, fromCid);
    }

    @Override
    public void importPublic(String file) throws OXException {
        ImportSystemPublicKey pubImport = services.getServiceSafe(ImportSystemPublicKey.class);
        pubImport.importPublic(file);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.GuardMaintenanceService#repair()
     */
    @Override
    public StringBuilder repair(boolean dryRun) throws OXException {
        GuardDatabaseRepair repairService = new GuardDatabaseRepair();
        return repairService.doRepair(dryRun);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.GuardMaintenanceService#changeUser(java.lang.String, java.lang.String, int, int)
     */
    @Override
    public void changeUser(String email, String newEmail) throws OXException {
        UserChanger changer = new UserChanger();
        changer.changeUser(email, newEmail);
    }

    @Override
    public int createMasterKeys() throws OXException {
        final MasterKeyService masterKeyService = services.getServiceSafe(MasterKeyService.class);
        return masterKeyService.createKey().getIndex();
    }

    @Override

    public void clean() throws OXException {
        KeyCacheStorage keyCacheStorage = Services.getService(KeyCacheStorage.class);
        keyCacheStorage.wipe();
        RemoteKeyCacheStorage remoteKeys = Services.getService(RemoteKeyCacheStorage.class);
        remoteKeys.wipe();
    }

    @Override
    public boolean importCa(byte[] file, int grpId) throws OXException {
        final CaCertStorage certStorage = services.getServiceSafe(CaCertStorage.class);
        String data = new String(file);
        return certStorage.addCertificate(data, grpId);
    }

    @Override
    public void importKey(String type, byte[] file, int userId, int contextId, String oldPass, String newPass) throws OXException {
        services.getServiceSafe(SmimeKeyService.class).importKey(new ByteArrayInputStream(file), userId, contextId, oldPass, newPass);
    }
}
