/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.user.GuardCapabilities;
import com.openexchange.guard.user.OXGuardUser;
import com.openexchange.guard.user.OXUser;
import com.openexchange.guard.user.OXUserService;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link AuthUtils}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class AuthUtils {

    private static Logger LOG = LoggerFactory.getLogger(AuthUtils.class);

    /**
     * Get the {@link GuardProperty.secureReply} 'com.openexchange.guard.SecureReply' property for the specified user in the specified context
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The value of the {@link GuardProperty.secureReply} 'com.openexchange.guard.SecureReply' property
     */
    private static boolean secureReply(int userId, int contextId) {
        try {
            GuardConfigurationService guardConfigurationService = Services.getService(GuardConfigurationService.class);
            return guardConfigurationService.getBooleanProperty(GuardProperty.secureReply, userId, contextId);
        } catch (OXException e) {
            LOG.error("{}", e);
        }
        return GuardProperty.secureReply.getDefaultValue(Boolean.class);
    }

    /**
     * Gets a {@link UserIdentity} from the given {@link GuardUserSession}
     *
     * @param guardUserSession The user session
     * @return The {@link UserIdentity} from the given {@link GuardUserSession}
     * @throws OXException
     */
    public static UserIdentity getUserIdentityFrom(GuardUserSession guardUserSession) throws OXException {
        return getUserIdentityFrom(guardUserSession.getGuardUserId(),
            guardUserSession.getGuardContextId(),
            guardUserSession.getUserId(),
            guardUserSession.getContextId());
    }

    public static UserIdentity getUserIdentityFrom(int guardUserId, int guardContextId) throws OXException {
        return getUserIdentityFrom(guardUserId, guardContextId, 0, 0);
    }

    /**
     * Gets a {@link UserIdentity} from the given ids.
     *
     * @param guardUserId The ID of the guard user
     * @param guardContextId The ID of guard user's context
     * @return The {@link UserIdentity} from the given ids.
     * @throws OXException
     */
    public static UserIdentity getUserIdentityFrom(int guardUserId, int guardContextId, int oxUserId, int oxCid) throws OXException {
        //new user identity
        UserIdentity userIdentity = new UserIdentity(null);

        //add ox guard user information
        OXGuardUser oxGuardUser = new OXGuardUser( guardUserId, guardContextId);
        userIdentity.setOXGuardUser(oxGuardUser);

        //get OX appsuite user information if guest key info was passed or absent and add them
        OXUserService userService = Services.getService(OXUserService.class);
        //..get it by guard-id and guard-cid
        OXUser oxUser = (oxUserId == 0 || oxCid < 0) ? userService.getUser(guardContextId, guardUserId) : new OXUser(null, oxUserId, oxCid, null, false, null);
        if (oxUser != null) {
            userIdentity.setOXUser(oxUser);
            userIdentity.setIdentity(oxUser.getEmail());
        }
        return userIdentity;

    }

    /**
     * Create JSON that gets encrypted and returned as the auth code
     *
     * @param json
     * @param userid
     * @param cid
     * @param email
     * @return
     */
    public static JsonObject createAuthJson(int userid, int cid, int oxUserId, int oxCid, String email, String authenticationFieldName, String authenticationFieldValue, String language) {
        JsonObject auth = new JsonObject();
        auth.addProperty(authenticationFieldName, authenticationFieldValue);
        auth.addProperty("email", email);
        auth.addProperty("user_id", userid);
        auth.addProperty("cid", cid);
        auth.addProperty("oxUserId", oxUserId);
        auth.addProperty("oxCid", oxCid);
        auth.addProperty("language", language);
        return (auth);
    }

    /**
     * Create a json of settings, include the individuals settings, plus global
     *
     * @param settings
     * @return
     * @throws OXException
     */
    public static JsonObject getSettings(String settings, GuardCapabilities setting, int id, int cid) throws OXException {
        StringBuilder allSettings = new StringBuilder();
        if (settings == null) {
            settings = "";
        }

        // If settings string loaded with key, convert to JSON
        if (!settings.startsWith("{")) {
            allSettings.append("{");
        }
        if (settings != null) {
            allSettings.append(settings);
        }
        if (!settings.endsWith("}")) {
            allSettings.append("}");
        }

        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);

        // Convert to Json, then check if global defaults present and add. Individual settings override
        Gson gson = new GsonBuilder().create();
        JsonObject json = gson.fromJson(allSettings.toString(), JsonObject.class);
        if (!json.has("Private")) {// If not defined at user level database, use global settings
            if (setting.isPrivatepass() == false) {// If config cascade value set as false, then use
                json.add("ChangePrivate", new JsonPrimitive("false"));
            } else {
                json.add("ChangePrivate", new JsonPrimitive(configService.getBooleanProperty(GuardProperty.okPrivate, id, cid) ? "true" : "false"));
            }
        }

        if (!json.has("oxguard")) {
            json.add("oxguard", new JsonPrimitive(setting.hasPermission(GuardCapabilities.Permissions.MAIL)));
        }

        // If either the global setting is set for no delete keys, or the config cascade, then use
        json.add("noDeletePrivate", new JsonPrimitive(setting.isNoDeletePrivate()));
        json.add("noDeleteRecovery", new JsonPrimitive(setting.isNoDeleteRecovery()));

        json.add("noRecovery", new JsonPrimitive(setting.isNorecovery()));

        if (!configService.getBooleanProperty(GuardProperty.showStatus, id, cid)) {
            json.addProperty("status", false);
        }
        json.addProperty("min_password_length", configService.getIntProperty(GuardProperty.minPasswordLength, id, cid));
        json.addProperty("password_length", configService.getIntProperty(GuardProperty.newPassLength, id, cid));

        json.addProperty("secureReply", secureReply(id, cid));
        if (cid > 0) {
            json.addProperty("autoCryptEnabled", configService.getBooleanProperty(GuardProperty.autoCryptEnabled, id, cid));
        }

        return (json);
    }

}
