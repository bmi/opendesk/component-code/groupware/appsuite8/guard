/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.auth;

import static com.openexchange.java.Autoboxing.B;
import static com.openexchange.java.Autoboxing.I;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.openexchange.context.ContextService;
import com.openexchange.crypto.CryptoType.PROTOCOL;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.impl.ContextExceptionCodes;
import com.openexchange.guard.activity.ActivityTrackingService;
import com.openexchange.guard.antiabuse.GuardAntiAbuseService;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.common.java.Strings;
import com.openexchange.guard.common.servlets.utils.AntiAbuseUtils;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.common.util.JsonUtil;
import com.openexchange.guard.crypto.AuthCryptoType;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.inputvalidation.EmailValidator;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.util.PGPUtil;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.oxuser.GetUserService;
import com.openexchange.guard.oxuser.GetUserServiceImpl;
import com.openexchange.guard.oxuser.OxUserResult;
import com.openexchange.guard.secondfactor.SecondFactorService;
import com.openexchange.guard.session.GuardSessionService;
import com.openexchange.guard.translation.GuardTranslationService;
import com.openexchange.guard.update.internal.GuardKeysUpdater;
import com.openexchange.guard.user.GuardCapabilities;
import com.openexchange.guard.user.OXUserService;

/**
 * {@link PGPAuthenticationService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class PGPAuthenticationService implements AuthenticationService {

    private static Logger LOG = LoggerFactory.getLogger(PGPAuthenticationService.class);
    private final static String ENCR_PASSWORD_AUTH_FIELD_NAME = "encr_password";
    private final static String PGP_SESSION_KEY_AUTH_FIELD_NAME = "pgp_session_key";

    //Other values than the actual auth-token which can be returned in the "auth" response field
    private static String AUTH_RET_LOCKOUT = "Lockout";
    private static String AUTH_RET_UPDATING = "Updating";
    private static String AUTH_RET_NO_KEY = "No Key";
    private static String AUTH_RET_PASSWORD_NEEDED = "Password Needed";
    private static String AUTH_RET_ERROR = "Error: Problem with login";

    private AuthenticationResult login(GuardAuthenticationParameters authenticationParameters, GuardUserSession userSession, GuardCapabilities settings) throws Exception {
        int userid = userSession.getUserId();
        int cid = userSession.getContextId();
        String lang = authenticationParameters.getStringParameter("lang");
        JsonObject returnJson = CommonAuth.initJson(userid, cid);
        returnJson.add("lang", getAvailableLanguagesJson(lang));
        GuardAntiAbuseService antiabuse = Services.getService(GuardAntiAbuseService.class);
        // Get public RSA keys for password encryption
        try {
            GuardKeyService keyService = Services.getService(GuardKeyService.class);
            String email = authenticationParameters.getStringParameter("email");
            //@formatter:on
            if (email != null) {
                email = new EmailValidator().assertInput(email, "email");
            }
            String encrpass = authenticationParameters.getStringParameter("encr_password");
            if (encrpass == null) {
                encrpass = "";
            }
            // Check lockout of bad attempts.  If excessive, don't give encrypted auth key
            if (antiabuse.blockLogin(AntiAbuseUtils.getAllowParameters(authenticationParameters.getClientIP(), authenticationParameters.getUsetAgent(), authenticationParameters.getScheme(), userid, cid, encrpass))) {
                returnJson.addProperty("auth", AUTH_RET_LOCKOUT);
                returnJson.add("settings", AuthUtils.getSettings(null, settings, userid, cid));
                LOG.info("Lockout auth due to bad attempts");
                return new AuthenticationResult(returnJson);
            }

            //Verify and UPDATE the context; i.e. triggering update tasks.
            try {
                ContextService contextService = Services.getService(ContextService.class);
                contextService.getContext(userSession.getContextId());
            } catch (OXException ex) {
                if (ContextExceptionCodes.UPDATE.equals(ex)) {
                    returnJson.addProperty("auth", AUTH_RET_UPDATING);
                    returnJson.addProperty("error", ex.getDisplayMessage(new Locale(lang)));
                    LOG.debug("The server is currently down for maintenance. cid = " + cid + " userid = " + userid);
                    return new AuthenticationResult(returnJson);
                }
                throw ex;
            }

            returnJson.addProperty("cid", I(userSession.getGuardContextId()));
            returnJson.addProperty("userid", I(userSession.getGuardUserId()));
            GuardKeys keys = null;
            String keyid = authenticationParameters.getStringParameter("keyid");
            if (keyid != null) {
                try {
                    long kid = Long.parseLong(keyid);
                    keys = keyService.getKeys(userSession.getGuardUserId(), userSession.getGuardContextId(), kid);
                } catch (Exception ex) {
                    LOG.error("Problem with login for Key ID " + keyid, ex);
                }
            } else {
                keys = keyService.getKeys(userSession.getGuardUserId(), userSession.getGuardContextId());
            }

            if (keys == null) {
                if (email != null) {
                    keys = keyService.getKeys(email);
                    if (keys != null) {
                        // Check if this is a Guest OX User account
                        GetUserService getUser = new GetUserServiceImpl(authenticationParameters.getCookies(), authenticationParameters.getStringParameter("session"), authenticationParameters.getUsetAgent());
                        OxUserResult guestUser = getUser.getUser(userid);
                        if (guestUser != null && !guestUser.isGuest()) {
                            keys = null;
                            LOG.error("Mismatch between email address and userid/cid for Guard login " + email);
                        }
                    }
                }
            }

            if (keys == null) {
                returnJson.addProperty("auth", AUTH_RET_NO_KEY);
                returnJson.add("settings", AuthUtils.getSettings(null, settings, userid, cid));
                LOG.debug("Auth sent with No Key for cid = " + cid + " userid = " + userid);
                return new AuthenticationResult(returnJson);
            }
            // Keys found, log the login activity
            logActivity(keys.getUserid(), keys.getContextid());

            // Check language not updated
            try {
                if (keys.getLocale() == null || !keys.getLocale().equals(userSession.getLocale())) {
                    updateLocale(keys, userSession.getLocale());
                }
            } catch (IllegalArgumentException e) {
                LOG.error("Invalid locale {} for user keys: {}", keys.getLanguage(), keys.getEmail());
            }

            returnJson.addProperty("primaryEmail", keys.getEmail());
            returnJson.add("settings", AuthUtils.getSettings(keys.getSettings().toString(), settings, userid, cid));
            returnJson.add("recoveryAvail", new JsonPrimitive(B(keys.isRecoveryAvail())));
            // Check for initial setup, not ready for authentication
            boolean proceed = true;
            if (keys.isPasswordNeeded() == true) {
                returnJson.addProperty("auth", AUTH_RET_PASSWORD_NEEDED);
                if ((keys.getQuestion() == null) && (keys.isRecoveryAvail())) {  // New, first time use
                    returnJson.addProperty("new", Boolean.TRUE);
                }
                proceed = false;
            }
            SecondFactorService secondFactor = Services.getService(SecondFactorService.class);
            if (secondFactor.hasSecondFactor(userSession.getUserId(), userSession.getContextId(), keys.getUserid(), keys.getContextid())) {
                returnJson.addProperty("pin", Boolean.TRUE);
                proceed = false;
            }

            if (!proceed) {  // Initial password or pin, don't authenticate
                LOG.debug("Auth sent with Password Needed for cid = " + cid + " userid = " + userid);
                return new AuthenticationResult(returnJson);
            }

            // OK, let's authenticate
            GuardCipherService cipherService = Services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.AES_GCM);
            try {
                String returndata = "Bad Password";

                if (encrpass.length() > 1) {// If provided with encryption password, check
                    try {
                        VerificationResult passwordVerification = verifyPassword(keys, userid, cid, encrpass);
                        if (passwordVerification.isValidated()) {
                            GuardKeysUpdater updater = Services.getService(GuardKeysUpdater.class);
                            if (updater != null) {
                                updater.checkUpgrades(passwordVerification.getKey(), encrpass);
                            }
                            returndata = AuthCryptoType.PGP.getValue();
                            //@formatter:off
                            returndata += cipherService.encrypt(
                                AuthUtils.createAuthJson(keys.getUserid(), 
                                                         keys.getContextid(), 
                                                         userSession.getUserId(), 
                                                         userSession.getContextId(), 
                                                         keys.getEmail(), 
                                                         ENCR_PASSWORD_AUTH_FIELD_NAME, 
                                                         authenticationParameters.hasParameter("encr_password") ? authenticationParameters.getStringParameter("encr_password") : "",
                                                         authenticationParameters.hasParameter("language") ? authenticationParameters.getStringParameter("language") : ""
                                                         ).toString(),
                                getToken(userSession));

                            antiabuse.report(AntiAbuseUtils.getReportParameter(
                                true, 
                                authenticationParameters.getClientIP(), 
                                authenticationParameters.getUsetAgent(), 
                                authenticationParameters.getScheme(), 
                                userid, cid, 
                                encrpass));
                            //@formatter:on

                            LOG.debug("Auth with proper password for cid = " + cid + " userid = " + userid);
                        } else {
                            //@formatter:off
                            antiabuse.report(AntiAbuseUtils.getReportParameter(false, 
                                authenticationParameters.getClientIP(),
                                authenticationParameters.getUsetAgent(),
                                authenticationParameters.getScheme(),
                                userid, cid, 
                                encrpass));
                            //@formatter:on
                            LOG.debug("Auth, OG user, settings only for cid = " + cid + " userid = " + userid);
                        }
                    } catch (OXException e) {
                        if (GuardAuthExceptionCodes.LOCKOUT.equals(e)) {
                            returndata = "lockout";
                        } else {
                            if (GuardAuthExceptionCodes.BAD_PASSWORD.equals(e)) {
                                //@formatter:off
                                antiabuse.report(AntiAbuseUtils.getReportParameter(false, 
                                    authenticationParameters.getClientIP(),
                                    authenticationParameters.getUsetAgent(),
                                    authenticationParameters.getScheme(),
                                    userid, cid, 
                                    encrpass));
                                //@formatter:on
                                LOG.debug("Auth, OG user, settings only for cid = " + cid + " userid = " + userid);
                            } else {
                                throw e;
                            }
                        }
                    }
                } else {
                    String pgpSessionPassword = authenticationParameters.getStringParameter(PGP_SESSION_KEY_AUTH_FIELD_NAME);
                    if (pgpSessionPassword != null && !pgpSessionPassword.isEmpty()) {
                        returndata = AuthCryptoType.PGP.getValue();
                        //@formatter:off
                        returndata = cipherService.encrypt(
                            AuthUtils.createAuthJson(keys.getUserid(), 
                                                     keys.getContextid(), 
                                                     userSession.getUserId(), 
                                                     userSession.getContextId(), 
                                                     keys.getEmail(), 
                                                     PGP_SESSION_KEY_AUTH_FIELD_NAME, 
                                                     pgpSessionPassword, 
                                                     authenticationParameters.hasParameter("language") ? authenticationParameters.getStringParameter("language") : ""
                                                    ).toString(),
                            getToken(userSession));
                        //@formatter:on
                    }
                }
                returnJson.addProperty("auth", returndata);
                return new AuthenticationResult(returnJson);
            } catch (Exception e) {
                LOG.error("Error during login", e);
                throw e;
            }

        } catch (Exception e) {
            if ((e.getMessage() != null) && (e.getMessage().contains("doesn't exist"))) {
                returnJson.addProperty("auth", AUTH_RET_NO_KEY);
                returnJson.add("settings", AuthUtils.getSettings(null, settings, 0, 0));
                return new AuthenticationResult(returnJson);
            } else {
                JsonObject errorJson = new JsonObject();
                errorJson.addProperty("auth", AUTH_RET_ERROR);
                errorJson.addProperty("lang", getAvailableLanguagesJson().toString());
                LOG.error("Error during login", e);
                return new AuthenticationResult(errorJson);
            }
        }
    }

    private void loginOld(JsonObject json, HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession, GuardCapabilities settings) throws Exception {
        response.setContentType("application/json");
        // response.addHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_OK);
        response.addHeader("Connection", "close");
        int userid = userSession.getUserId();
        int cid = userSession.getContextId();
        String lang = JsonUtil.getStringFromJson(json, "lang");
        JsonObject returnJson = CommonAuth.initJson(userid, cid);
        returnJson.add("lang", getAvailableLanguagesJson(lang));
        GuardAntiAbuseService antiabuse = Services.getService(GuardAntiAbuseService.class);
        // Get public RSA keys for password encryption
        try {
            GuardKeyService keyService = Services.getService(GuardKeyService.class);
            String email = ((json == null) || (json.get("email") == null) || json.get("email").isJsonNull() || json.get("email").getAsString().isEmpty()) ? null : json.get("email").getAsString();
            if (email != null) {
                email = new EmailValidator().assertInput(email, "email");
            }
            String encrpass = JsonUtil.getStringFromJson(json, "encr_password");
            if (encrpass == null) {
                encrpass = "";
            }
            // Check lockout of bad attempts.  If excessive, don't give encrypted auth key
            if (antiabuse.blockLogin(AntiAbuseUtils.getAllowParameters(request, userid, cid, encrpass))) {
                returnJson.addProperty("auth", AUTH_RET_LOCKOUT);
                returnJson.add("settings", AuthUtils.getSettings(null, settings, userid, cid));
                ServletUtils.sendJsonOK(response, returnJson);
                LOG.info("Lockout auth due to bad attempts");
                return;
            }

            //Verify and UPDATE the context; i.e. triggering update tasks.
            try {
                ContextService contextService = Services.getService(ContextService.class);
                contextService.getContext(userSession.getContextId());
            } catch (OXException ex) {
                if (ContextExceptionCodes.UPDATE.equals(ex)) {
                    returnJson.addProperty("auth", AUTH_RET_UPDATING);
                    returnJson.addProperty("error", ex.getDisplayMessage(new Locale(lang)));
                    ServletUtils.sendJsonOK(response, returnJson);
                    LOG.debug("The server is currently down for maintenance. cid = " + cid + " userid = " + userid);
                    return;
                }
                throw ex;
            }

            returnJson.addProperty("cid", I(userSession.getGuardContextId()));
            returnJson.addProperty("userid", I(userSession.getGuardUserId()));
            GuardKeys keys = null;
            String keyid = ServletUtils.getStringParameter(request, "keyid");
            if (keyid != null) {
                try {
                    long kid = Long.parseLong(keyid);
                    keys = keyService.getKeys(userSession.getGuardUserId(), userSession.getGuardContextId(), kid);
                } catch (Exception ex) {
                    LOG.error("Problem with login for Key ID " + keyid, ex);
                }
            } else {
                keys = keyService.getKeys(userSession.getGuardUserId(), userSession.getGuardContextId());
            }

            if (keys == null) {
                if (email != null) {
                    keys = keyService.getKeys(email);
                    if (keys != null) {
                        // Check if this is a Guest OX User account
                        GetUserService getUser = new GetUserServiceImpl(request);
                        OxUserResult guestUser = getUser.getUser(userid);
                        if (guestUser != null && !guestUser.isGuest()) {
                            keys = null;
                            LOG.error("Mismatch between email address and userid/cid for Guard login " + email);
                        }
                    }
                }
            }

            if (keys == null) {
                returnJson.addProperty("auth", AUTH_RET_NO_KEY);
                returnJson.add("settings", AuthUtils.getSettings(null, settings, userid, cid));
                ServletUtils.sendJsonOK(response, returnJson);
                LOG.debug("Auth sent with No Key for cid = " + cid + " userid = " + userid);
                return;
            }
            // Keys found, log the login activity
            logActivity(keys.getUserid(), keys.getContextid());

            // Check language not updated
            try {
                if (keys.getLocale() == null || !keys.getLocale().equals(userSession.getLocale())) {
                    updateLocale(keys, userSession.getLocale());
                }
            } catch (IllegalArgumentException e) {
                LOG.error("Invalid locale {} for user keys \"{}\":  ", keys.getLanguage(), keys.getEmail(), e.getMessage());
            }

            returnJson.addProperty("primaryEmail", keys.getEmail());
            returnJson.add("settings", AuthUtils.getSettings(keys.getSettings().toString(), settings, userid, cid));
            returnJson.add("recoveryAvail", new JsonPrimitive(B(keys.isRecoveryAvail())));
            // Check for initial setup, not ready for authentication
            boolean proceed = true;
            if (keys.isPasswordNeeded() == true) {
                returnJson.addProperty("auth", AUTH_RET_PASSWORD_NEEDED);
                if ((keys.getQuestion() == null) && (keys.isRecoveryAvail())) {  // New, first time use
                    returnJson.addProperty("new", Boolean.TRUE);
                }
                proceed = false;
            }
            SecondFactorService secondFactor = Services.getService(SecondFactorService.class);
            if (secondFactor.hasSecondFactor(userSession.getUserId(), userSession.getContextId(), keys.getUserid(), keys.getContextid())) {
                returnJson.addProperty("pin", Boolean.TRUE);
                proceed = false;
            }

            if (!proceed) {  // Initial password or pin, don't authenticate
                ServletUtils.sendJson(response, returnJson);
                LOG.debug("Auth sent with Password Needed for cid = " + cid + " userid = " + userid);
                return;
            }

            // OK, let's authenticate
            GuardCipherService cipherService = Services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.AES_GCM);
            try {
                String returndata = "Bad Password";

                if (encrpass.length() > 1) {// If provided with encryption password, check
                    try {
                        VerificationResult passwordVerification = verifyPassword(keys, userid, cid, encrpass);
                        if (passwordVerification.isValidated()) {
                            GuardKeysUpdater updater = Services.getService(GuardKeysUpdater.class);
                            if (updater != null) {
                                updater.checkUpgrades(passwordVerification.getKey(), encrpass);
                            }
                            returndata = AuthCryptoType.PGP.getValue();
                            returndata += cipherService.encrypt(AuthUtils.createAuthJson(keys.getUserid(), keys.getContextid(), userSession.getUserId(), userSession.getContextId(), keys.getEmail(), ENCR_PASSWORD_AUTH_FIELD_NAME, (json != null && json.has("encr_password")) ? json.get("encr_password").getAsString() : "", (json != null && json.has("language")) ? json.get("language").getAsString() : "").toString(), getToken(userSession));
                            antiabuse.report(AntiAbuseUtils.getReportParameter(true, request, userid, cid, encrpass));
                            LOG.debug("Auth with proper password for cid = " + cid + " userid = " + userid);
                        } else {
                            antiabuse.report(AntiAbuseUtils.getReportParameter(false, request, userid, cid, encrpass));
                            LOG.debug("Auth, OG user, settings only for cid = " + cid + " userid = " + userid);
                        }
                    } catch (OXException e) {
                        if (GuardAuthExceptionCodes.LOCKOUT.equals(e)) {
                            returndata = "lockout";
                        } else {
                            if (GuardAuthExceptionCodes.BAD_PASSWORD.equals(e)) {
                                antiabuse.report(AntiAbuseUtils.getReportParameter(false, request, userid, cid, encrpass));
                                LOG.debug("Auth, OG user, settings only for cid = " + cid + " userid = " + userid);
                            } else {
                                throw e;
                            }
                        }
                    }
                } else {
                    String pgpSessionPassword = JsonUtil.getStringFromJson(json, PGP_SESSION_KEY_AUTH_FIELD_NAME);
                    if (pgpSessionPassword != null && !pgpSessionPassword.isEmpty()) {
                        returndata = AuthCryptoType.PGP.getValue();
                        returndata = cipherService.encrypt(AuthUtils.createAuthJson(keys.getUserid(), keys.getContextid(), userSession.getUserId(), userSession.getContextId(), keys.getEmail(), PGP_SESSION_KEY_AUTH_FIELD_NAME, pgpSessionPassword, json.has("language") ? json.get("language").getAsString() : "").toString(), getToken(userSession));
                    }
                }
                returnJson.addProperty("auth", returndata);
                ServletUtils.sendJson(response, returnJson);
            } catch (Exception e) {
                LOG.error("Error during login", e);
                throw e;
            }

        } catch (Exception e) {
            if ((e.getMessage() != null) && (e.getMessage().contains("doesn't exist"))) {
                returnJson.addProperty("auth", AUTH_RET_NO_KEY);
                returnJson.add("settings", AuthUtils.getSettings(null, settings, 0, 0));
                ServletUtils.sendJson(response, returnJson);
            } else {
                JsonObject errorJson = new JsonObject();
                errorJson.addProperty("auth", AUTH_RET_ERROR);
                errorJson.addProperty("lang", getAvailableLanguagesJson().toString());
                ServletUtils.sendJsonOK(response, errorJson);
                LOG.error("Error during login", e);
            }
        }
    }

    /**
     * If the key locale is different from the UI login, update the key data
     * 
     * @param key
     * @param locale
     * @throws OXException
     */
    private void updateLocale(GuardKeys key, Locale locale) throws OXException {
        key.setLocale(locale);
        KeyTableStorage keyService = Services.getService(KeyTableStorage.class);
        keyService.updateLocale(key);
    }

    /**
     * Obtain a session token for this session
     * 
     * @param GuardUserSession
     * @return String of the token for encryption
     * @throws OXException
     */
    private String getToken(GuardUserSession userSession) throws OXException {
        GuardSessionService sessionService = Services.getService(GuardSessionService.class);
        String token = sessionService.getToken(userSession.getGuardSession());
        if (token == null) {
            token = sessionService.newToken(userSession.getGuardSession(), userSession.getGuardUserId(), userSession.getGuardContextId());
        }
        if (token == null) {
            LOG.error("Auth unable to get token from database for cid = " + userSession.getGuardContextId() + " userid = " + userSession.getGuardUserId());
            throw GuardAuthExceptionCodes.TOKEN_ERROR.create();
        }
        return token;
    }

    /**
     * First, verify the current password. If fails, then check if one of the other keys
     *
     * @param key
     * @param userid
     * @param cid
     * @param password
     * @return
     * @throws Exception
     */
    private VerificationResult verifyPassword(GuardKeys key, int userid, int cid, String password) throws Exception {

        if (key.getPGPSecretKeyRing() != null && PGPUtil.verifyPassword(key.getPGPSecretKeyRing(), password, key.getSalt())) {
            return (new VerificationResult(key, true));
        }
        KeyTableStorage ogKeyTableStorage = Services.getService(KeyTableStorage.class);
        List<GuardKeys> userKeys = ogKeyTableStorage.getKeysForUser(userid, cid);
        if (userKeys.size() > 1) {
            for (GuardKeys userKey : userKeys) {
                if (!userKey.isCurrent()) {// Don't recheck the current key
                    if (userKey.getPGPSecretKeyRing() != null && PGPUtil.verifyPassword(userKey.getPGPSecretKeyRing(), password, userKey.getSalt())) {
                        return (new VerificationResult(userKey, true));
                    }
                }
            }
        }
        return (new VerificationResult());
    }

    private JsonObject getAvailableLanguagesJson() throws OXException {
        return getAvailableLanguagesJson(null);
    }

    /**
     * Get a list of the available languages that have been translated for the Guest users
     * 
     * @param code Local code for translations
     * @return
     * @throws OXException
     */
    private JsonObject getAvailableLanguagesJson(String code) throws OXException {
        GuardTranslationService translationService = Services.getService(GuardTranslationService.class);
        if (Strings.isEmpty(code)) {
            code = translationService.getAvailableCode("NON_EXISTENT");
        }
        Map<String, String> available = translationService.getAvailableLanguages(code);
        JsonObject jsonAvailable = new JsonObject();
        for (final java.util.Iterator<Entry<String, String>> it = available.entrySet().iterator(); it.hasNext();) {
            String key = it.next().getKey();
            jsonAvailable.addProperty(key, available.get(key));
        }
        return jsonAvailable;
    }

    /**
     * Log the recent login activity. Used to track Guest user usage
     * 
     * @param userId
     * @param cid
     */
    private void logActivity(int userId, int cid) {
        ActivityTrackingService activityTracker = Services.getServiceLookup().getOptionalService(ActivityTrackingService.class);
        if (activityTracker != null) {
            activityTracker.updateActivity(userId, cid);
        }
    }

    @Override
    public PROTOCOL getCryptoType() {
        return PROTOCOL.PGP;
    }

    @Override
    public AuthenticationResult authenticate(GuardUserSession userSession, GuardAuthenticationParameters parameter) throws OXException {
        try {
            GuardCapabilities settings = Services.getService(OXUserService.class).getGuardCapabilieties(userSession.getContextId(), userSession.getUserId());
            return login(parameter, userSession, settings);
        } catch (Exception ex) {
            LOG.error("Error during login", ex);
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(ex, ex.getMessage());
        }
    }
}
