/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.auth;

import java.util.Map;
import com.openexchange.guard.oxapi.OxCookie;

/**
 * {@link GuardAuthenticationParameters} - represents a bunch of information required for a Guard login
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public class GuardAuthenticationParameters {

    private final Map<String, Object> parameters;
    private final String clientIp;
    private final String userAgent;
    private final String scheme;
    private final OxCookie cookies;

    /**
     * Initializes a new {@link GuardAuthenticationParameters}.
     *
     * @param clientIp The IP of the authenticating client
     * @param userAgent The user agent of the authenticating client
     * @param schema The authenticating schema ("https", "http", etc)
     * @param cookies The cookies provided by the client
     * @param parameters The parameters provided by the client
     */

    public GuardAuthenticationParameters(//@formatter:off
        String clientIp, 
        String userAgent, 
        String schema, 
        OxCookie cookies, 
        Map<String, Object> parameters) { //@formatter:on

        this.clientIp = clientIp;
        this.userAgent = userAgent;
        this.scheme = schema;
        this.parameters = parameters;
        this.cookies = cookies;
    }

    /**
     * Gets the IP address of the authenticating client
     *
     * @return The IP address of the authenticating client
     */
    public String getClientIP() {
        return clientIp;
    }

    /**
     * Gets the user agent of the authenticating client
     *
     * @return The user agent of the authenticating client
     */
    public String getUsetAgent() {
        return userAgent;
    }

    /**
     * Gets the authentication scheme
     *
     * @return The scheme used for the authentication request
     */
    public String getScheme() {
        return scheme;
    }

    /**
     * Gets the cookies provided by the authenticating client
     *
     * @return The cookies provided by the authenticating client
     */
    public OxCookie getCookies() {
        return this.cookies;
    }

    /**
     * Checks if a given parameter is present
     *
     * @param name The name of the parameter
     * @return <code>True</code> if the parameter is present, <code>False</code> if the parameter is not present or it's value is <code>null</code>
     */
    public boolean hasParameter(String name) {
        return parameters.containsKey(name) && parameters.get(name) != null;
    }

    /**
     * Get the value of the given parameter as string
     *
     * @param name The name of the parameter to get
     * @return The parameter's value as String, or <code>null</code> if the parameter is not present
     */
    public String getStringParameter(String name) {
        return getStringParameter(name, null);
    }

    /**
     * Gets the value of the given parameter as string
     *
     * @param name The name of the parameter to get
     * @param defaultValue The default value to be returned in case the given parameter is not present
     * @return The parameters's value as String, or the given default value, in case the parameter is not present
     */
    public String getStringParameter(String name, String defaultValue) {
        return hasParameter(name) ? (String) parameters.get(name) : defaultValue;
    }
}