/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.servlets.certificatemanagement.responses;

import java.util.Date;
import com.openexchange.guard.certificatemanagement.commons.RecipCertificate;

/**
 * {@link RecipCertificateResponse} - A Response wrapping a {@link RecipCertificate} object
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public class RecipCertificateResponse {

    private final RecipCertificate recipCertificate;

    /**
     * 
     * Initializes a new {@link RecipCertificateResponse}.
     *
     * @param recipCertificate The {@link RecipCertificate} to return
     */
    public RecipCertificateResponse(RecipCertificate recipCertificate) {
        this.recipCertificate = recipCertificate;
    }

    /**
     * Gets the certificate's date of expiration
     *
     * @return The date of expiration
     */
    public Date getExpires() {
        return recipCertificate != null ? recipCertificate.getSmimeKeys().getExpires() : null;
    }
    
    /**
     * Gets whether the certifate is expired or not
     *
     * @return <code>True</code>, if the given certificate is expired, <code>False</code> if not.
     *         Returns <code>Null</code> if no certificate is available
     */
    public Boolean getExpired() {
        return recipCertificate != null ? Boolean.valueOf(recipCertificate.getSmimeKeys().isExpired()) : null;
    }

    /**
     * Return the serial number of the certificate
     *
     * @return The unique serial number
     */
    public String getSerial() {
        return recipCertificate != null ? recipCertificate.getSmimeKeys().getSerial().toString() : null;
    }

    /**
     * Gets the name of certificate's issuer
     *
     * @return The issuer name of the certificate
     */
    public String getCertifier() {
        return recipCertificate != null ? recipCertificate.getSmimeKeys().getCertifier() : null;
    }
}
