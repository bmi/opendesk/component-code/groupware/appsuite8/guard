/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.guard.servlets.certificatemanagement.actions;

import static com.openexchange.guard.servlets.certificatemanagement.actions.Parameters.PARAM_NEW_PASSWORD;
import static com.openexchange.guard.servlets.certificatemanagement.actions.Parameters.PARAM_PASSWORD;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.certificatemanagement.responses.CertificatesResponse;
import com.openexchange.guard.servlets.fileupload.FileUploadHandler;
import com.openexchange.guard.smime.SmimeKeyService;
import com.openexchange.java.Streams;

/**
 * {@link UploadCertificateAction} allows to upload a S/MIME certificate and keys
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public class UploadCertificateAction extends GuardServletAction {

    private static final Logger LOG = LoggerFactory.getLogger(UploadCertificateAction.class);

    private static final int MAX_KEY_FILE_UPLOAD_SIZE = 1000000; /* 1 MB */

    @SuppressWarnings("resource")
    public SmimeKeys importCertificates(Collection<FileItem> items, GuardUserSession userSession) throws OXException {
        InputStream certificateInputStream = null;
        try {
            String password = null;
            String newPassword = null;
            for (final FileItem item : items) {
                if (item.getFieldName().equals(PARAM_PASSWORD)) {
                    password = item.getString(StandardCharsets.UTF_8.displayName());
                } else if (item.getFieldName().equals(PARAM_NEW_PASSWORD)) {
                    newPassword = item.getString(StandardCharsets.UTF_8.displayName());
                } else {
                    certificateInputStream = item.getInputStream();
                }
            }
            if (certificateInputStream == null) {
                throw GuardCoreExceptionCodes.MULTIPART_UPLOAD_MISSING.create();
            }
            if (password == null) {
                throw GuardCoreExceptionCodes.PARAMETER_MISSING.create(PARAM_PASSWORD);
            }
            if (newPassword == null) {
                throw GuardCoreExceptionCodes.PARAMETER_MISSING.create(PARAM_NEW_PASSWORD);
            }
            SmimeKeyService keyService = Services.getService(SmimeKeyService.class);
            return keyService.importKey(certificateInputStream, userSession.getUserId(), userSession.getContextId(), password, newPassword);
        } catch (OXException e) {
            LOG.error("Error importing", e);
            throw e;
        } catch (final Exception e) {
            LOG.error("Error importing", e);
            throw OXException.general("Problem parsing request", e);
        } finally {
            Streams.close(certificateInputStream);
        }
    }
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        FileUploadHandler fileUploadHandler = new FileUploadHandler(Services.getService(GuardConfigurationService.class));
        SmimeKeys importedCertificate = importCertificates(fileUploadHandler.parseItems(request, MAX_KEY_FILE_UPLOAD_SIZE), userSession);
        ServletUtils.sendObject(response, new CertificatesResponse(importedCertificate));
    }

}
