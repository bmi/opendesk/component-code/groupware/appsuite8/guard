/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keyretrieval;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.guard.common.servlets.utils.AntiAbuseUtils;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.keymanagement.commons.antiabuse.AntiAbuseAction;
import com.openexchange.guard.keymanagement.commons.antiabuse.AntiAbuseWrapper;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.authentication.AuthorizationHeaderParser;


/**
 * {@link RetrievalAllExposedKeysAction}
 *
 * Allows retrieving deleted keys for a user, which have been exposed
 * using the support API ({@link com.openexchange.guard.servlets.supportApi.ExposeKeyAction})
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.0
 */
public class RetrievalAllExposedKeysAction extends GuardServletAction {

    private static final String BASIC_AUTH_PREFIX = "Basic";
    private static final String CONTENT_TYPE = "application/pgp-keys";
    private static final boolean MANDATORY = true;

    /* (non-Javadoc)
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {

        String email = null;
        String password = null;
        final int userId = ServletUtils.getIntParameter(request, "userId", MANDATORY);
        final int contextId = ServletUtils.getIntParameter(request, "contextId", MANDATORY);

        //Get the auth header if available
        String authHeader = new AuthorizationHeaderParser().parse(BASIC_AUTH_PREFIX, request);
        if(authHeader != null) {

            //email & password from auth-header
            if(authHeader != null && authHeader.contains(":")) {
                String[] credentials = authHeader.split(":");
                if (credentials != null && credentials.length >= 2) {
                    email = credentials[0];
                    password = credentials[1];
                }
            }

            //Export
            final String closureEmail = email;
            final String closurePassword = password;
            AntiAbuseWrapper antiAbuseWrapper = getAntiAbuseWrapper(AntiAbuseUtils.getAllowParameters(request, email, password));
            final String exportData = antiAbuseWrapper.doAction(new AntiAbuseAction<String>() {
                @Override
                public String doAction() throws Exception {
                    return new KeyRetriever().exportAllKeys(closureEmail, userId, contextId, closurePassword);
                }

            });

            if(exportData != null) {
                response.setContentLength(exportData.length());
                response.setHeader("Content-Disposition", "attachment; filename=\"export.asc\"");
                ServletUtils.sendOK(response, CONTENT_TYPE, exportData);
            }
            else {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "No exposed keys found for user.");
            }
        }
        else {
            //Client did not provide credentials
            response.setHeader("WWW-Authenticate", "Basic");
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}
