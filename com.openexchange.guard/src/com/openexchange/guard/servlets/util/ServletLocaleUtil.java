/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.util;

import static com.openexchange.guard.common.util.LocaleUtil.getLocalFor;
import java.util.Locale;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.osgi.Services;

/**
 * {@link ServletLocaleUtil}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public class ServletLocaleUtil {

    private static final String LANGUAGE = "locale";

    /**
     * Gets the default locale
     *
     * @return The default locale
     * @throws OXException
     */
    private static Locale getDefaultLocale() throws OXException {
        GuardConfigurationService configurationService = Services.getService(GuardConfigurationService.class);
        return getLocalFor(configurationService.getProperty(GuardProperty.defaultLanguage));
    }

    /**
     * Extracts the locale from the user's language cookie
     *
     * @param request The request
     * @return The locale extracted from the request's language cookie
     */
    private static Locale getLocaleFromCookie(HttpServletRequest request) {
        for (Cookie cookie : request.getCookies()) {
            if (LANGUAGE.equals(cookie.getName())) {
                return getLocalFor(cookie.getValue());
            }
        }
        return null;
    }


    /**
     * Gets the user's locale for a given request
     *
     * @param request The request containing the user's locale
     * @return The locale of the given request
     * @throws OXException
     */
    public static Locale getLocaleFor(HttpServletRequest request) throws OXException {
        Locale locale = getLocaleFromCookie(request);
        if (locale == null) {
            locale = getDefaultLocale();
        }
        return locale;
    }

    /**
     * Gets the user's locale for a given request
     *
     * @param request The request containing the user's locale
     * @return The locale of the given request
     */
    public static Locale getLocaleSaveFor(HttpServletRequest request) {
        try {
            return getLocaleFor(request);
        } catch (OXException e) {
            return Locale.getDefault();
        }
    }
}
