/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.crypto;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.mime.services.GuardParsedMimeMessage;
import com.openexchange.guard.servlets.GuardServletAction;

/**
 * {@link AbstractCryptoServletAction} Contains methods shared with different actions in the crypto servlet
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public abstract class AbstractCryptoServletAction extends GuardServletAction {

    protected static GuardParsedMimeMessage parse(HttpServletRequest request, JSONObject json, InputStream message, int userId, int contextId, String fromName, String senderEmail, boolean senderIsGuest)
        throws MessagingException, JSONException, OXException, IOException {
        request = Objects.requireNonNull(request, "request must not be null");
        message = Objects.requireNonNull(message, "message must not be null");

        final boolean draft = ServletUtils.getBooleanParameter(request, "draft", false);
        String host = null;
        if (request.getHeader("X-Host-Name") != null) {
            host = request.getHeader("X-Host-Name");
        }
        GuardParsedMimeMessage msg = new GuardParsedMimeMessage(message, json, userId, contextId, fromName, draft, host, senderEmail, senderIsGuest);
        return msg;
    }

}
