/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets;

import java.io.IOException;
import java.util.HashMap;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.logging.GuardLogProperties;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.oxapi.exceptions.OXApiException;
import com.openexchange.guard.servlets.authentication.BasicAuthServletAuthenticationHandler;

/**
 * Represents a OX Guard Servlet
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public abstract class AbstractGuardServlet extends HttpServlet {

    private static final long serialVersionUID = 8327090398980830760L;
    private static final Logger LOG = LoggerFactory.getLogger(AbstractGuardServlet.class);
    private static final String ACTION = "action";
    private final GuardErrorResponseRenderer errorResponseRenderer;

    /**
     * Initializes a new {@link AbstractGuardServlet}.
     */
    public AbstractGuardServlet() {
        //Default OXException errors will be sent as SC_OK according to the OX HTTP API
        errorResponseRenderer = new GuardErrorResponseRenderer(HttpServletResponse.SC_OK);
    }

    /**
     * Initializes a new {@link AbstractGuardServlet}.
     *
     * @param errorResponseRenderer The error renderer to use in case of an OXException
     */
    public AbstractGuardServlet(GuardErrorResponseRenderer errorResponseRenderer) {
        this.errorResponseRenderer = errorResponseRenderer;
    }


     /**
      * Creates a new BASIC-AUTH authentication handler
      *
      * @return A new {@link BasicAuthServletAuthenticationHandler}
      * @throws OXException
      */
    protected BasicAuthServletAuthenticationHandler createBasicAuthHandler() throws OXException {
        //Protecting the servlet's actions with BASIC-AUTH
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        String restApiUsername = configService.getProperty(GuardProperty.restApiUsername);
        String restApiPassword = configService.getProperty(GuardProperty.restApiPassword);
        return new BasicAuthServletAuthenticationHandler(restApiUsername, restApiPassword);
    }

    /**
     * Performs the an action requested by the "action" parameter in the given request if the action is an allowed action
     *
     * @param request the request containing the action parameter
     * @param response the response
     * @param allowedActions a set of allowed actions to perform
     */
    protected void doAction(HttpServletRequest request, HttpServletResponse response, HashMap<String, GuardServletAction> allowedActions) {
        String actionName = ServletUtils.getStringParameter(request, ACTION);
        GuardServletAction action = null;
        if (allowedActions != null && actionName != null) {
            action = allowedActions.get(actionName);
        }

        if (action != null) {
            doAction(request, response, actionName, action);
        } else {
            ServletUtils.sendError(response, "unknown action");
        }
    }

    /**
     * Performs the given action
     *
     * @param request request object
     * @param response response object
     * @param action the action to perform
     */
    protected void doAction(HttpServletRequest request, HttpServletResponse response, String actionName, GuardServletAction action) {
        try {
            action.doAction(request, response);
        } catch (OXApiException e) {
            //Passing the middleware exception on to the client
            LOG.error(String.format("OX API Error while processing command \"%s\": %s", actionName, e.getError()), e);
            ServletUtils.sendOK(response, e.getContentTypeHader().getValue(), e.getBackendErrorData());
        } catch (OXException e) {
            //Creating error object for the client
            LOG.error(String.format("Error processing command \"%s\": %s", actionName, e.getMessage()), e);
            try {
                GuardErrorResponseRenderer errorRenderer = action.getErrorResponseRenderer();
                if(errorRenderer == null) {
                    errorRenderer = this.errorResponseRenderer;
                }
                errorRenderer.renderError(request, response, actionName, e);
            } catch (IOException e1) {
                LOG.error(String.format("Error writing response: %s", e.getMessage()), e1);
                ServletUtils.sendError(response, e);
            }
        } catch (Exception e) {
            //Unexpected exception
            LOG.error(String.format("Error processing command \"%s\": %s", actionName, e.getMessage()), e);
            ServletUtils.sendError(response, e);
        } finally {
            GuardLogProperties.removeLogProperties();
        }
    }
}
