/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.guest.exceptions;

import com.openexchange.i18n.LocalizableStrings;

/**
 * {@link GuestServletExceptionMessages}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v7.8.3
 */
public class GuestServletExceptionMessages implements LocalizableStrings {

    // Unable to find the pgp keys for the specified user
    public static final String UNABLE_TO_FIND = "Unable to find the key for user %1$s";
    // A PIN is required for opening the email.  The PIN was configured/specified by the sender
    public static final String PIN_REQUIRED = "A PIN is required to open this email.  The PIN was chosen by the sender.";
    // The account is not a Guest account
    public static final String NOT_GUEST = "This is not a guest account";
    // The account is locked out due to excessive bad login attempts.  This is just a brief error message
    public static final String LOCKOUT = "Lockout";
    // Service unavailable
    public static final String SERVICE_UNAVAILABLE = "Service unavailable, check configuration";
    // The ID of the email is invalid
    public static final String BAD_EMAIL_ID = "Invalid email ID";
    // The token used for authentication is not valid
    public static final String BAD_AUTHENTICATION_MSG = "Authentication token is not valid";

    /**
     * Prevent instantiation.
     */
    private GuestServletExceptionMessages() {}
}
