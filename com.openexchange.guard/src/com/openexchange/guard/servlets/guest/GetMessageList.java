/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.guest;

import java.util.Collection;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.json.JSONObject;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.guest.GuardGuestService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.fileupload.FileUploadHandler;
import com.openexchange.guard.servlets.fileupload.JsonFileUploadHandler;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link GetMessageList}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GetMessageList extends GuardServletAction {

    private static final String JSON_DATA_FIELD_NAME = "json";

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {

        //guest identity in the JSON body
        FileUploadHandler fileUploadHandler = new FileUploadHandler(Services.getService(GuardConfigurationService.class));
        JsonFileUploadHandler jsonFileUploadHandler = new JsonFileUploadHandler(fileUploadHandler);
        Collection<FileItem> items = fileUploadHandler.parseItems(request);
        JSONObject json = jsonFileUploadHandler.getJsonFrom(items, JSON_DATA_FIELD_NAME, true);
        final UserIdentity guestIdentity = getUserIdentityFrom(json);
        if (guestIdentity == null) {
            throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
        }

        GuardGuestService guestService = Services.getService(GuardGuestService.class);
        List<String> emailItemIds = guestService.getEmailItemIds(guestIdentity);
        ServletUtils.sendObject(response, emailItemIds);
    }
}
