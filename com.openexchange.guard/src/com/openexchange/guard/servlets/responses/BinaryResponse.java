/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.responses;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.ContentTypeDetectionUtil;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;

/**
 * {@link BinaryResponse} serves binary content to a client.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class BinaryResponse {

    private final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(BinaryResponse.class);
    private final HttpServletResponse response;
    private static final List<String> INLINE_WHITE_LIST;
    private static final String APPLICATION_OCTETSTREAM = "application/octet-stream";

    static {
        //A list of content-types for which we allow inline content-disposition
        INLINE_WHITE_LIST = Arrays.asList(new String[] {
                "image/jpeg",
                "image/png",
                "image/gif",
        });
    }

    /**
     * Initializes a new {@link BinaryResponse}.
     *
     * @param response The response to use
     */
    public BinaryResponse(HttpServletResponse response) {
        this.response = response;
    }

    /**
     * Internal method to check if we send the binary data as disposition "attachment" for security reason.
     *
     * @param contentType The content type of the binary data
     * @return true, if the binary data must be served as disposition "attachment", false otherwise
     */
    private boolean forceServeAsDownload(String contentType) {
        if (contentType != null) {
            return !INLINE_WHITE_LIST.contains(contentType);
        }
        return true;
    }

    /**
     * Internal method to guess the content type of the binary data
     *
     * @param data The data to guess the content-type for
     * @return The guessed content-type, or "application/octet-stream" if the content-type could not be specified.
     * @throws OXException
     */
    private String guessContentType(InputStream data) {
        try {
            final String contentType = ContentTypeDetectionUtil.guessContentType(data);
            if (contentType != null) {
                return contentType;
            }
            logger.debug("Unknown content type for binary data");
        } catch (final Exception e) {
            logger.error("Error while detecting content-type for binary data: {} ", e.getMessage());
        }
        return APPLICATION_OCTETSTREAM;
    }

    /**
     * Serves binary data
     *
     * @param data The binary data
     * @param serveAsDownload True to force content-disposition being set to "attachment", false for auto-detection
     * @param fileName The filename associated with the data
     * @throws OXException
     */
    public void send(InputStream data, boolean serveAsDownload, String fileName) throws OXException {
        try {
            data = Objects.requireNonNull(data, "data must not be null");
            data = data.markSupported() ? data : new BufferedInputStream(data, ContentTypeDetectionUtil.READ_SIZE);
            final String contentType = guessContentType(data);

            //Setting content-disposition to "attachment" if desired by the caller, or if forced
            if (serveAsDownload || forceServeAsDownload(contentType)) {
                fileName = Objects.requireNonNull(fileName, "fileName must not be null");
                response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            }

            //Setting the guessed contentType
            response.setContentType(contentType);
            //instruct the client to respect our contentType and not to sniff it in order to prevent
            //XSS if content-disposition is not 'attachment'
            response.setHeader("X-Content-Type-Options", "nosniff");

            //serve the data
            IOUtils.copy(data, response.getOutputStream());
        } catch (final IOException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }

}
