/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.responses;

import java.util.Date;
import org.bouncycastle.openpgp.PGPException;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.openexchange.guard.keymanagement.commons.GuardKeys;

/**
 * {@link GuardKeyInfoResponse}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class GuardKeyInfoResponse {

    private final GuardKeys keys;
    private final String password;
    private final boolean omitPublicKeyRing;

    /**
     * Initializes a new {@link GuardKeyInfoResponse}.
     *
     * @param keys The keys
     */
    public GuardKeyInfoResponse(GuardKeys keys) {
        this(keys,null,false);
    }

    /**
     * Initializes a new {@link GuardKeyInfoResponse}.
     *
     * @param keys The keys
     * @param password The private key password
     */
    public GuardKeyInfoResponse(GuardKeys keys, String password) {
        this(keys,password,false);
    }

    public GuardKeyInfoResponse(GuardKeys keys, String password, boolean ommitPublicKeyRing) {
        this.keys = keys;
        this.password = password;
        this.omitPublicKeyRing = ommitPublicKeyRing;
    }

    @JsonInclude(Include.NON_NULL)
    public PublicKeyRingInfoResponse getPublicRing() {
        return omitPublicKeyRing ?
            null :
            new PublicKeyRingInfoResponse(keys.getPGPPublicKeyRing(),keys.getPGPSecretKeyRing());
    }

    @JsonInclude(Include.NON_NULL)
    public SecretKeyRingInfoResponse getPrivateRing() throws PGPException {
        return this.password != null && this.keys.getPGPSecretKeyRing() != null ?
            new SecretKeyRingInfoResponse(keys.getPGPSecretKeyRing(), password, keys.getSalt()) :
            null;
    }

    public int getUserId() {
        return keys.getUserid();
    }

    public int getContextId() {
        return keys.getContextid();
    }

    public boolean isCurrent() {
        return keys.isCurrent();
    }

    public Date getPasswordModificationTimestamp() {
      return keys.getLastup();
    }
}
