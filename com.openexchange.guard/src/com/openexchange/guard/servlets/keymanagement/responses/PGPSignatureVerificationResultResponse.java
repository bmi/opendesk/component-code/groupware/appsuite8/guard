/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.responses;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.openexchange.guard.servlets.keymanagement.strings.KeyManagementStrings;
import com.openexchange.guard.translation.GuardTranslator;
import com.openexchange.i18n.Translator;
import com.openexchange.pgp.core.PGPSignatureVerificationResult;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;

/**
 * {@link PGPSignatureVerificationResultResponse}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class PGPSignatureVerificationResultResponse {

    private final PGPSignatureVerificationResult pgpSignatureVerificationResult;
    private final GuardTranslator translator;

    /**
     * Initializes a new {@link PGPSignatureVerificationResultResponse}.
     *
     * @param pgpSignatureVerificationResult The verification result
     * @param translator The {@link Translator} used for translating textual values
     */
    public PGPSignatureVerificationResultResponse(PGPSignatureVerificationResult pgpSignatureVerificationResult, GuardTranslator translator) {
        this.translator = Objects.requireNonNull(translator,"translator must not be null");
        this.pgpSignatureVerificationResult = Objects.requireNonNull(pgpSignatureVerificationResult,"pgpSignatureVerificationResult must not be null");
    }

    /**
     * Returns if OX Guard was able to verify the signature
     *
     * @return True, if the signature has been verified, false otherwise
     */
    public boolean isVerified() {
        return pgpSignatureVerificationResult.isVerified();
    }

    /**
     * Checks if OX Guard was able to fetch the verifying public PGP key
     *
     * @return True, if OX Guard was not able to fetch the verifying public PGP key, false otherwise.
     */
    public boolean isKeyMissing() {
        return pgpSignatureVerificationResult.isMissing();
    }

    /**
     * Gets the int value of the signature's type.
     *
     * @return the int value of the signature's type.
     */
    public int getSignatureType() {
        return pgpSignatureVerificationResult.getSignature().getSignatureType();
    }

    /**
     * A textual description of the signature's type.
     *
     * @return An textual description of the signature type, translated for the user's locale
     */
    public String getDescription() {
        switch (getSignatureType()) {
            case 0x10:

               return translator.getTranslation(KeyManagementStrings.SIGNATURE_TYPE_GENERIC);
            case 0x11:
                return translator.getTranslation(KeyManagementStrings.SIGNATURE_TYPE_PERSONA);
            case 0x12:
                return translator.getTranslation(KeyManagementStrings.SIGNATURE_TYPE_CASUAL);
            case 0x13:
                return translator.getTranslation(KeyManagementStrings.SIGNATURE_TYPE_POSITIVE);
            case 0x18:
                return translator.getTranslation(KeyManagementStrings.SIGNATURE_TYPE_SUBKEY);
            case 0x19:
                return translator.getTranslation(KeyManagementStrings.SIGNATURE_TYPE_PRIMARY);
            case 0x1f:
                return translator.getTranslation(KeyManagementStrings.SIGNATURE_TYPE_DIRECT);
            case 0x20:
                return translator.getTranslation(KeyManagementStrings.SIGNATURE_TYPE_REVOKE);
            case 0x28:
                return translator.getTranslation(KeyManagementStrings.SIGNATURE_TYPE_SUBKEY_REVOKE);
            case 0x30:
                return translator.getTranslation(KeyManagementStrings.SIGNATURE_TYPE_CERT_REVOKE);
            default:
                return translator.getTranslation(KeyManagementStrings.SIGNATURE_TYPE_UNKNOWN);
        }
    }

    /**
     * The creation time of the signature as UNIX timestamp.
     *
     * @return The creation time of the signature
     */
    public long getCreationTime() {
        return pgpSignatureVerificationResult.getSignature().getCreationTime().getTime();
    }

    /**
     * The user ID of the signature, if present
     *
     * @return The user-ID related to the signature verification, or null if no user-id is related to the signature.
     */
    @JsonInclude(Include.NON_NULL)
    public String getUserId() {
        return pgpSignatureVerificationResult.getUserId();
    }

    /**
     * The ID of the key which created the signature.
     *
     * @return The issuer's key id
     */
    public String getIssuerKeyId() {
        return Long.toString(pgpSignatureVerificationResult.getSignature().getKeyID());
    }

    /**
     * The finger print of the key which created the signature
     * @return The issuer key's fingerprint
     */
    @JsonInclude(Include.NON_NULL)
    public String getIssuerFingerPrint() {
       return pgpSignatureVerificationResult.getIssuerKey() != null ?
           PGPKeysUtil.getFingerPrint(pgpSignatureVerificationResult.getIssuerKey().getFingerprint()) :
           null;
    }

    /**
     * The user ID of the issuer
     *
     * @return The user ID of the issuer
     */
    @JsonInclude(Include.NON_NULL)
    public String getIssuer() {
        if (pgpSignatureVerificationResult.getIssuerKey() != null &&
            pgpSignatureVerificationResult.getIssuerKey().getUserIDs().hasNext()) {
            return pgpSignatureVerificationResult.getIssuerKey().getUserIDs().next();
        }
        return null;
    }

    /**
     * The int value of the used hash algorithm
     *
     * @return The hash algorithm used to create the signature.
     */
    public int getHashAlgorithmId() {
        return pgpSignatureVerificationResult.getSignature().getHashAlgorithm();
    }

    /**
     * The OpenPGP version number for this signature
     *
     * @returhe The OpenPGP version number for this signature
     */
    public int getVersion() {
        return pgpSignatureVerificationResult.getSignature().getVersion();
    }

    /**
     * The id of the public key for which the signature was created, or null if the signature is not related to a key.
     *
     * @return The id of the public key for which the signature was issued
     */
    @JsonInclude(Include.NON_NULL)
    public String getKeyId() {
        return pgpSignatureVerificationResult.getPublicKey() != null ?
               Long.toString(pgpSignatureVerificationResult.getPublicKey().getKeyID()) :
               null;
    }

    /**
     * The fingerprint of the public key for which the signature was created, or null if the signature is not related to a key.
     *
     * @return The fingerprint of the key for which the signature was issued
     */
    @JsonInclude(Include.NON_NULL)
    public String getFingerPrint() {
        return pgpSignatureVerificationResult.getPublicKey() != null ?
               PGPKeysUtil.getFingerPrint(pgpSignatureVerificationResult.getPublicKey().getFingerprint()) :
               null;
    }

    /**
     * The image related to the signature, or null if the signature is not related to an image.
     *
     * @return  The image realted to the signature, or null if the signature is not related to an image.
     */
    @JsonInclude(Include.NON_NULL)
    public PublicKeyImageResponse getImage() {
        return pgpSignatureVerificationResult.getUserAttributes() != null &&
               pgpSignatureVerificationResult.getUserAttributes().getImageAttribute() != null ?
                   new PublicKeyImageResponse(pgpSignatureVerificationResult.getUserAttributes().getImageAttribute()) :
                   null;
    }
}
