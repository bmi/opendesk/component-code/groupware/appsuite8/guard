/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.responses;

import java.util.ArrayList;
import java.util.Iterator;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.export.KeyExportUtil;

/**
 * {@link SecretKeyRingInfoResponse}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class SecretKeyRingInfoResponse {

    private final PGPSecretKeyRing keyRing;
    private final String password;
    private final String salt;

    /**
     * Initializes a new {@link SecretKeyRingInfoResponse}.
     * @param pgpSecretKeyRing
     * @param password
     */
    public SecretKeyRingInfoResponse(PGPSecretKeyRing pgpSecretKeyRing, String password, String salt) {
        this.keyRing = pgpSecretKeyRing;
        this.password = password;
        this.salt = salt;
    }

    @JsonInclude(Include.NON_NULL)
    public String getRing() throws OXException {
        return password != null && salt != null ?
            KeyExportUtil.exportPGPPrivateKey(keyRing, password, salt) :
            null;
    }

    public SecretKeyInfoResponse[] getKeys() {
       ArrayList<SecretKeyInfoResponse> list = new ArrayList<SecretKeyInfoResponse>();
       Iterator<PGPSecretKey> iter = this.keyRing.getSecretKeys();
       while(iter.hasNext()) {
           list.add(new SecretKeyInfoResponse(iter.next()));
       }
       return list.toArray(new SecretKeyInfoResponse[list.size()]);
    }

}
