/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.JsonObject;
import com.openexchange.guard.autocrypt.AutoCryptService;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.handler.ResponseHandler;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.util.PGPUtil;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;
/**
 * {@link TransferAutoCryptKeyAction}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.2
 */
public class TransferAutoCryptKeyAction extends GuardServletAction {

    private static final String PASSWORD_PARAMETER = "password";

    /* (non-Javadoc)
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        JsonObject json = Services.getService(ResponseHandler.class).getJsonAndDecodeEncryptedPasswords(request, userSession.getUserId(), userSession.getContextId());
        if (!json.has(PASSWORD_PARAMETER)) {
            throw new IllegalArgumentException("Missing password");
        }
        String password = json.get(PASSWORD_PARAMETER).getAsString();
        GuardKeyService guardKeyService = Services.getService(GuardKeyService.class);
        GuardKeys keys = guardKeyService.getKeys(userSession.getUserId(), userSession.getContextId());
        if (keys == null || keys.getPGPSecretKeyRing() == null) {
            throw GuardCoreExceptionCodes.KEY_NOT_FOUND.create();
        }
        if (!PGPUtil.verifyPassword(keys.getPGPSecretKeyRing(), password, keys.getSalt())) {
            throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
        }
        AutoCryptService autoCryptService = Services.getService(AutoCryptService.class);
        ServletUtils.sendObject(response, autoCryptService.sendTransferEmail(keys, password, request.getRemoteHost(), request.getRemoteAddr()));
    }
}
