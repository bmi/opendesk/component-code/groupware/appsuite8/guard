/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.actions;

import static com.openexchange.java.Autoboxing.l;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.common.util.LongUtil;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.OGPGPKeyRing;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.PublicExternalKeyService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.pgpcore.services.PGPKeySigningService;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.keymanagement.responses.PGPSignatureVerificationResultCollectionResponse;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.core.PGPSignatureVerificationResult;

/**
 * {@link GetSignaturesAction}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GetSignaturesAction extends GuardServletAction {

    @FunctionalInterface
    private interface KeyVerificationStrategy {
        Collection<PGPSignatureVerificationResult> verifyKey(UserIdentity verifier, long keyId, boolean verifySubkeys) throws OXException;
    }

    private List<KeyVerificationStrategy> verificationStrategies = null;

    /**
     * Initializes a new {@link GetSignaturesAction}.
     *
     */
    public GetSignaturesAction() {

        //Define a list of strategies for searching the provided key (by id) and verify it
        verificationStrategies = Arrays.asList(

            //User's own keys
            new KeyVerificationStrategy() {

                @Override
                public Collection<PGPSignatureVerificationResult> verifyKey(UserIdentity verifier, long keyId, boolean verifySubkeys) throws OXException {
                    Collection<PGPSignatureVerificationResult> verificationResults = Collections.emptyList();
                    final GuardKeyService keyService = Services.getService(GuardKeyService.class);
                    final PGPKeySigningService pgpKeySigningService = Services.getService(PGPKeySigningService.class);

                    final GuardKeys key = keyService.getKeys(keyId);
                    if (key != null) {
                        if (verifySubkeys) {
                            verificationResults = pgpKeySigningService.verifyKey(verifier, key);
                        } else {
                            verificationResults = verifySubKey(pgpKeySigningService, key.getPGPPublicKeyRing(), verifier, keyId);
                        }
                    }
                    return verificationResults;
                }
            },

            //pgp keys of external recipients uploaded by the user
            new KeyVerificationStrategy() {

                @Override
                public Collection<PGPSignatureVerificationResult> verifyKey(UserIdentity verifier, long keyId, boolean verifySubkeys) throws OXException {
                    Collection<PGPSignatureVerificationResult> verificationResults = Collections.emptyList();
                    final PGPKeySigningService pgpKeySigningService = Services.getService(PGPKeySigningService.class);
                    PublicExternalKeyService publicExternalKeyService = Services.getService(PublicExternalKeyService.class);
                    OGPGPKeyRing pgpKeyRing = publicExternalKeyService.get(verifier.getOXUser().getId(), verifier.getOXUser().getContextId(), LongUtil.longToHexStringTruncated(keyId));
                    if (pgpKeyRing != null) {
                        if (verifySubkeys) {
                            verificationResults = pgpKeySigningService.verifyKey(verifier, pgpKeyRing.getKeyRing());
                        } else {
                            verificationResults = verifySubKey(pgpKeySigningService, pgpKeyRing.getKeyRing(), verifier, keyId);
                        }
                    }

                    return verificationResults;
                }
            }

        );
    }

    private Collection<PGPSignatureVerificationResult> verifySubKey(PGPKeySigningService pgpKeySigningService, PGPPublicKeyRing pgpKeyRing, UserIdentity verifier, long keyId) throws OXException {
        PGPPublicKey publicKey = pgpKeyRing.getPublicKey(keyId);
        if (publicKey != null) {
            return pgpKeySigningService.verifyKey(verifier, publicKey);
        }
        throw GuardCoreExceptionCodes.KEY_NOT_FOUND.create();
    }

    private Collection<PGPSignatureVerificationResult> verifyKey(UserIdentity verifier, long keyId, boolean verifySubkeys) throws OXException{
        Collection<PGPSignatureVerificationResult> verificationResults = Collections.emptyList();
        for (KeyVerificationStrategy keyVerificationStrategy : verificationStrategies) {
           verificationResults = keyVerificationStrategy.verifyKey(verifier, keyId, verifySubkeys);
           if(verificationResults.size() > 0) {
               break;
           }
        }
        return verificationResults;
    }

    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        final boolean mandatory = true;
        Long keyId = ServletUtils.getLongParameter(request, Parameters.PARAM_KEYID, mandatory);
        boolean verifySubkeys = ServletUtils.getBooleanParameter(request, Parameters.PARAM_SUBKEYS);

        UserIdentity user = getUserIdentityFrom(userSession);

        Collection<PGPSignatureVerificationResult> verificationResults = verifyKey(user, l(keyId), verifySubkeys);
        if(!verificationResults.isEmpty()) {
            ServletUtils.sendObject(response, new PGPSignatureVerificationResultCollectionResponse(verificationResults, getTranslatorFrom(userSession)));
        }
        else {
            throw GuardCoreExceptionCodes.KEY_NOT_FOUND.create();
        }
    }
}
