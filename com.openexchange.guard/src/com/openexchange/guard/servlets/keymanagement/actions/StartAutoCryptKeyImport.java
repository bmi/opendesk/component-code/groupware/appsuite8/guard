/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.actions;

import java.nio.charset.StandardCharsets;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.guard.autocrypt.AutoCryptService;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.oxapi.Api;
import com.openexchange.guard.oxapi.ApiResponse;
import com.openexchange.guard.oxapi.OxCookie;
import com.openexchange.guard.servlets.GuardServletAction;

/**
 * {@link StartAutoCryptKeyImport}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.2
 */
public class StartAutoCryptKeyImport extends GuardServletAction {

    private static final String ID_PARAM = "id";
    private static final String FOLDER_PARAM = "folder";
    private static final String ATTACHMENT_PARAMT = "attachment";


    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        Api api = new Api(new OxCookie(request.getCookies()), request);
        String id = ServletUtils.getStringParameter(request, ID_PARAM, true);
        String folder = ServletUtils.getStringParameter(request, FOLDER_PARAM, true);
        String att = ServletUtils.getStringParameter(request, ATTACHMENT_PARAMT, true);
        ApiResponse resp = api.getPlainAttachment(id, att, folder);
        if (resp == null) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create("Unable to get attachment");
        }
        try {
            String content = new String(resp.readContent(), StandardCharsets.UTF_8);
            AutoCryptService autoCryptService = Services.getService(AutoCryptService.class);
            ServletUtils.sendObject(response, autoCryptService.getStartData(content));
        } finally {
            resp.close();
        }

    }


}
