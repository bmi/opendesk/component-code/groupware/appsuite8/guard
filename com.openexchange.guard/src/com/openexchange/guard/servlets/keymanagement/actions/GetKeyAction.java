/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.actions;

import static com.openexchange.guard.common.util.JsonUtil.emptyJsonString;
import static com.openexchange.guard.servlets.keymanagement.actions.Parameters.PARAM_KEYID;
import static com.openexchange.guard.servlets.keymanagement.actions.Parameters.PARAM_KEY_TYPE;
import static com.openexchange.java.Autoboxing.l;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.servlets.utils.AntiAbuseUtils;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.antiabuse.AntiAbuseAction;
import com.openexchange.guard.keymanagement.commons.antiabuse.AntiAbuseWrapper;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.keymanagement.responses.GuardKeyInfoResponse;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link GetKeyAction} returns a specific key for a user, or the "current" key-
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class GetKeyAction extends GuardServletAction {

    protected enum KeyType {
        PUBLIC,
        PRIVATE,
        PUBLIC_PRIVATE
    }

    /**
     * Internal method to create a {@link GuardKeyInfoResponse} in a brute force protected way
     *
     * @param request The request
     * @param userSession The session
     * @param keys The key
     * @param omitPublicKey true to omit the public key, false to include it.
     * @return {@link GuardKeyInfoResponse}
     * @throws OXException if the user provided a bad password or the account is locked due.
     */
    private GuardKeyInfoResponse createProtectedResponse(final HttpServletRequest request, final GuardUserSession userSession, final GuardKeys keys, final boolean omitPublicKey) throws Exception {
        final UserIdentity userIdentity = requireUserIdentityFrom(request, userSession.getUserId(), userSession.getContextId());
        AntiAbuseWrapper antiAbuseProtection = getAntiAbuseWrapper(keys, AntiAbuseUtils.getAllowParameters(request, userSession.getUserId(),  userSession.getContextId(), new String(userIdentity.getPassword())));
        return antiAbuseProtection.doAction(new AntiAbuseAction<GuardKeyInfoResponse>() {

            @Override
            public GuardKeyInfoResponse doAction() throws Exception {
                return new GuardKeyInfoResponse(keys, new String(userIdentity.getPassword()), omitPublicKey);
            }
        });
    }

    /**
     * Internal method to return the specified key, or the current key.
     *
     * @param userSession The user's session
     * @param keyId The ID of the key to return, or null in order to return the "current" key
     * @return The Key specified by the ID, "current" key, or null.
     * @throws OXException
     */
    protected GuardKeys getKey(GuardUserSession userSession, Long keyId) throws OXException {
        GuardKeyService keyService = Services.getService(GuardKeyService.class);
        GuardKeys key = keyId == null ?
               keyService.getKeys(userSession.getGuardUserId(), userSession.getGuardContextId()) :
               keyService.getKeys(l(keyId));
        //Only return the queried key if it belongs to the user
        return (key != null && key.getContextid() == userSession.getGuardContextId() && key.getUserid() == userSession.getGuardUserId()) ? key : null;
    }

    /**
     * Parses the {@link KeyType} parameter from the given string
     *
     * @param typeParameter The string to parse the {@link KeyType} from
     * @return The {@link KeyType} parsed from the given string.
     * @throws OXException
     */
    protected KeyType parseKeyType(String typeParameter) throws OXException {
        if (typeParameter == null) {
            return KeyType.PUBLIC_PRIVATE;
        }
        try {
            return KeyType.valueOf(typeParameter.toUpperCase());
        } catch (Exception e) {
            throw GuardCoreExceptionCodes.INVALID_PARAMETER_VALUE.create(PARAM_KEY_TYPE);
        }
    }

    /**
     * Determines which {@link KeyType} will be served to the client
     *
     * @param requestedMode The requested {@link KeyType} by the client
     * @param key The key to server
     * @return The real {@link KeyType} to server
     */
    protected KeyType getKeyType(KeyType requestedMode, GuardKeys key) {
        if (requestedMode == KeyType.PUBLIC_PRIVATE && key.hasPrivateKey()) {
            return KeyType.PUBLIC_PRIVATE;
        }
        else if (requestedMode == KeyType.PUBLIC_PRIVATE && !key.hasPrivateKey()) {
            return KeyType.PUBLIC;
        }
        else if (requestedMode == KeyType.PRIVATE && key.hasPrivateKey()) {
            return KeyType.PRIVATE;
        }
        else if (requestedMode == KeyType.PUBLIC) {
            return KeyType.PUBLIC;
        }
        return null;
    }

    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        if (userSession == null) {
            throw GuardAuthExceptionCodes.MISSING_SESSION.create();
        }
        final Long keyId = ServletUtils.getLongParameter(request, PARAM_KEYID);
        if (keyId == null && ServletUtils.hasParameter(request, PARAM_KEYID)) {
            throw GuardCoreExceptionCodes.INVALID_PARAMETER_VALUE.create(PARAM_KEYID);
        }

        //Getting the requested key
        final GuardKeys keys = getKey(userSession, keyId);
        if (keys != null) {

            //The keytype defines what parts of the key will be serverd to the client
            final KeyType mode = getKeyType(parseKeyType(ServletUtils.getStringParameter(request, PARAM_KEY_TYPE)), keys);

            //Serve key content and meta data
            if (mode == KeyType.PUBLIC) {
                ServletUtils.sendObject(response, new GuardKeyInfoResponse(keys));
            } else if (mode == KeyType.PRIVATE) {
                final boolean omitPublicKey = true;
                ServletUtils.sendObject(response, createProtectedResponse(request, userSession, keys, omitPublicKey));
            }
            else if(mode == KeyType.PUBLIC_PRIVATE){
                //export public and private key
                final boolean omitPublicKey = false;
                ServletUtils.sendObject(response, createProtectedResponse(request, userSession, keys, omitPublicKey));
            }
            else {
                ServletUtils.sendOK(response, "application/json", emptyJsonString());
            }
        } else {
            throw GuardCoreExceptionCodes.KEY_NOT_FOUND.create();
        }
    }

}
