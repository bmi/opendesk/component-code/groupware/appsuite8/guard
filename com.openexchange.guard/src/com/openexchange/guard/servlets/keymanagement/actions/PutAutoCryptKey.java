/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.autocrypt.AutoCryptService;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.handler.ResponseHandler;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;

/**
 * {@link PutAutoCryptKey}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.2
 */
public class PutAutoCryptKey extends GuardServletAction {

    private static final String HEADER_PARAM = "header";
    private static final String ADD_IF_NEW = "add";
    private static final String CONFIRMED = "confirmed";
    private static final String SENT_DATE = "date";

    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        JsonObject json = Services.getService(ResponseHandler.class).getJson(request);
        if (json == null || !json.has(HEADER_PARAM)) {
            throw OXException.mandatoryField("Missing header data");
        }
        String header = json.get(HEADER_PARAM).getAsString();
        long sentDate = json.get(SENT_DATE).getAsLong();
        boolean add = ServletUtils.getBooleanParameter(request, ADD_IF_NEW);
        boolean confirmed = ServletUtils.getBooleanParameter(request, CONFIRMED);
        AutoCryptService autoCryptService = Services.getService(AutoCryptService.class);
        Object resp = autoCryptService.importKey(userSession.getUserId(), userSession.getContextId(), header, sentDate, add, confirmed);
        ServletUtils.sendObject(response, resp);

    }


}
