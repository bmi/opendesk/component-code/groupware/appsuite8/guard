/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.actions;

import static com.openexchange.java.Autoboxing.l;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.servlets.utils.AntiAbuseUtils;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.inputvalidation.EmailValidator;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.antiabuse.AntiAbuseAction;
import com.openexchange.guard.keymanagement.commons.antiabuse.AntiAbuseWrapper;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link AddUserIDAction} adds a new ID to a user's key
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class AddUserIDAction extends GuardServletAction {

    /* (non-Javadoc)
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {

        final boolean mandatory = true;
        final Long keyId = ServletUtils.getLongParameter(request, Parameters.PARAM_KEYID, mandatory);
        final String name = ServletUtils.getStringParameter(request, Parameters.PARAM_NAME, mandatory);
        final String email = new EmailValidator().assertInput(ServletUtils.getStringParameter(request, Parameters.PARAM_EMAIL, mandatory), Parameters.PARAM_EMAIL);
        final UserIdentity userIdentity = requireUserIdentityFrom(request, userSession.getUserId(), userSession.getContextId());
        final String newUserId = name + " <" + email + ">";
        final GuardKeyService keyService = Services.getService(GuardKeyService.class);
        final GuardKeys keys = keyService.getKeys(userSession.getGuardUserId(), userSession.getGuardContextId(), l(keyId));

        if(keys != null) {
            //Adding the user ID in a Brute Force protected way
            AntiAbuseWrapper bruteForceProtection = getAntiAbuseWrapper(AntiAbuseUtils.getAllowParameters(request, userSession.getUserId(), userSession.getContextId(), new String(userIdentity.getPassword())));
            bruteForceProtection.doAction(new AntiAbuseAction<Void>() {

                @Override
                public Void doAction() throws OXException {
                    keyService.addUserId(keys, newUserId, new String(userIdentity.getPassword()));
                    return null;
                }});
        }
        else {
            throw GuardCoreExceptionCodes.KEY_NOT_FOUND.create();
        }
    }
}
