/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.login;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.crypto.CryptoManager;
import com.openexchange.guard.crypto.PasswordManagementService;
import com.openexchange.guard.crypto.PasswordManagementService.ResetPasswordDestination;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.antiabuse.AntiAbuseAction;
import com.openexchange.guard.keymanagement.commons.antiabuse.AntiAbuseWrapper;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.crypto.Exceptions.CryptoServletExceptionCodes;

/**
 * Resets a user's password for the current key
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class ResetPasswordAction extends GuardServletAction {

    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        CryptoManager cryptoManager = Services.getService(CryptoManager.class);
        Map<String, String[]> params = getHttpParameters(request);
        PasswordManagementService passwordManangementService = cryptoManager.getPasswordManagementService(getCryptoType(request));

        if (passwordManangementService != null) {
            getJsonVerifyLogin(request, userSession, true);
            AntiAbuseWrapper antiAbuse = getAntiAbuseWrapper(request, userSession, "");

            final String lang = getParameter(params, "lang");
            final String hostname = request.getServerName();
            final String senderIp = request.getRemoteAddr();

            try {
                ResetPasswordDestination usedDestination = antiAbuse.doAction(new AntiAbuseAction<ResetPasswordDestination>() {

                    @Override
                    public ResetPasswordDestination doAction() throws Exception {
                        return passwordManangementService.resetPass(userSession, lang, hostname, senderIp, true, null);
                    }
                });
                final String responseString = usedDestination == ResetPasswordDestination.PRIMARY ? "primary" : "ok";
                ServletUtils.sendObject(response, responseString);
                return;
            } catch (Exception e) {
                if (e instanceof OXException) {
                    throw (OXException) e;
                }
                throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e.getMessage());
            }
        }
        throw CryptoServletExceptionCodes.UNKNOWN_CRYPTO_TYPE.create();
    }
}
