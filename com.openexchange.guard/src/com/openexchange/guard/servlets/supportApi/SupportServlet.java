
package com.openexchange.guard.servlets.supportApi;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.java.Strings;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.AbstractGuardServlet;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.authentication.BasicAuthServletAuthenticationHandler;
import com.openexchange.guard.servlets.authentication.GuardAuthenticationHandler;

/**
 *
 * Servlet for providing access to the Guard's support API
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class SupportServlet extends AbstractGuardServlet {

    private static final Logger logger = LoggerFactory.getLogger(SupportServlet.class);
    private static final long serialVersionUID = -8809364881638293438L;
    private static final String EXPOSE_KEY_ACTION = "expose_key";
    private static final String RESET_PASSWORD_ACTION = "reset_password";
    private static final String DELETE_USER_ACTION = "delete_user";
    private static final String UPGRADE_GUEST = "upgrade_guest";
    private transient final HashMap<String, GuardServletAction> postActions;


    /**
     * Initializes a new {@link SupportServlet}.
     *
     * @throws OXException
     */
    public SupportServlet() throws OXException {
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);

        String supportApiUsername = configService.getProperty(GuardProperty.supportApiUsername);
        String supportApiPassword = configService.getProperty(GuardProperty.supportApiPassword);
        //Protecting the servlet's actions with BASIC-AUTH
        GuardAuthenticationHandler basicAuthServletAuthentication = new BasicAuthServletAuthenticationHandler(supportApiUsername, supportApiPassword);

        postActions = new HashMap<String, GuardServletAction>();
        if(!Strings.isEmpty(supportApiUsername) && !Strings.isEmpty(supportApiPassword)){
            //Setting up the servlet's actions
            postActions.put(EXPOSE_KEY_ACTION, new ExposeKeyAction().setAuthenticationHandler(basicAuthServletAuthentication));
            postActions.put(RESET_PASSWORD_ACTION, new ResetPasswordAction().setAuthenticationHandler(basicAuthServletAuthentication));
            postActions.put(DELETE_USER_ACTION, new DeleteUserAction().setAuthenticationHandler(basicAuthServletAuthentication));
            postActions.put(UPGRADE_GUEST, new UpgradeGuestAction().setAuthenticationHandler(basicAuthServletAuthentication));
        }
        else{
            logger.error("Denied OX Guard support servlet initialization due to unset Basic-Auth configuration. Please set properties 'com.openexchange.guard.supportapiusername' and 'com.openexchange.guard.supportapipassword' appropriately.");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        if (postActions.size() == 0) {
            logger.error("OX Guard support servlet not initialized.  Please check there is a configured support api username and password");
        }
        super.doAction(request, response, postActions);
    }
}
