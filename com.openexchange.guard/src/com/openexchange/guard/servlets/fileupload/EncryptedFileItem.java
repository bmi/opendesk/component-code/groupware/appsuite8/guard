/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.fileupload;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.codec.binary.Base64OutputStream;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemHeaders;
import org.apache.commons.fileupload.disk.DiskFileItem;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;

/**
 * {@link EncryptedFileItem} - Enhances {@link FileItem} in order to provide symmetric encryption
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.3
 */
public class EncryptedFileItem implements FileItem {

    private final GuardCipherAlgorithm algorithm;
    private final FileItem delegate;
    private final SecretKey secretKey;
    private Cipher decryptionCipher;
    private byte[] iv;

    /**
     * Initializes a new {@link EncryptedFileItem}.
     *
     * @param delegate The {@link FileItem} to encrypt
     * @param algorithm The algorithm to use for encryption
     * @param secretKey The {@link SecretKey} to use for encryption
     */
    public EncryptedFileItem(FileItem delegate, GuardCipherAlgorithm algorithm, SecretKey secretKey) {
        this.delegate = delegate;
        this.algorithm = algorithm;
        this.secretKey = secretKey;
    }

    /**
     * Internal getter for the encryption Cipher. Also the IV will get created.
     *
     * @return The encryption Cipher
     * @throws IOException
     */
    private Cipher getEncryptionCipher() throws IOException {
        try {
            Cipher encryptionCipher = Cipher.getInstance(algorithm.getTransformation(), algorithm.getProvider());
            encryptionCipher.init(Cipher.ENCRYPT_MODE, secretKey);
            this.iv = encryptionCipher.getIV();
            return encryptionCipher;
        } catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException e) {
            throw new IOException(e.getMessage(), e);
        }
    }

    /**
     * Internal getter for the decryption Cipher
     *
     * @return The decryption Cipher initialized with the IV
     * @throws IOException
     */
    private Cipher getDecryptionCipher() throws IOException {
        try {
            if (decryptionCipher == null) {
                decryptionCipher = Cipher.getInstance(algorithm.getTransformation(), algorithm.getProvider());
                if (iv == null) {
                    throw new IllegalArgumentException("IV must not be null");
                }
                decryptionCipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
            }
            return decryptionCipher;
        } catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | NoSuchProviderException e) {
            throw new IOException(e.getMessage(), e);
        }
    }

    /**
     * Returns the {@link File} object for the FileItem's data's temporary location on the disk.
     * Note that for FileItems that have their data stored in memory, this method will return null.
     *
     * @return The {@link File} related to the stored data on disk, or <code>null</code> if the item is store in memory.
     */
    public File getStoreLocation() {
        if (delegate != null) {
            if (delegate instanceof DiskFileItem) {
                return ((DiskFileItem) delegate).getStoreLocation();
            }
        }
        return null;
    }

    /**
     * Returns the {@link EncryptedSharedFileInputStream} object for the FileItem's data's temporary location on the disk.
     * Note that for FileItems that have their data stored in memory, this method will return null.
     *
     * @return The {@link EncryptedSharedFileInputStream} related to the stored data on disk, or <code>null</code> if the item is store in memory.
     * @throws IOException
     */
    public EncryptedSharedFileInputStream getSharedInputStream() throws IOException {
        File file = getStoreLocation();
        if (file != null) {
            if (iv == null) {
                throw new IllegalArgumentException("IV must not be null");
            }
            return new EncryptedSharedFileInputStream(algorithm, secretKey, iv, file);
        }
        return null;
    }

    @Override
    public FileItemHeaders getHeaders() {
        return delegate.getHeaders();
    }

    @Override
    public void setHeaders(FileItemHeaders headers) {
        delegate.setHeaders(headers);
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new Base64InputStream(new CipherInputStream(delegate.getInputStream(), getDecryptionCipher()));
    }

    @Override
    public String getContentType() {
        return delegate.getContentType();
    }

    @Override
    public String getName() {
        return delegate.getName();
    }

    @Override
    public boolean isInMemory() {
        return delegate.isInMemory();
    }

    @Override
    public long getSize() {
        return delegate.getSize();
    }

    @Override
    public byte[] get() {
        return delegate.get();
    }

    @Override
    public String getString(String encoding) throws UnsupportedEncodingException {
        return delegate.getString(encoding);
    }

    @Override
    public String getString() {
        return delegate.getString();
    }

    @Override
    public void write(File file) throws Exception {
        delegate.write(file);
    }

    @Override
    public void delete() {
        delegate.delete();
    }

    @Override
    public String getFieldName() {
        return delegate.getFieldName();
    }

    @Override
    public void setFieldName(String name) {
        delegate.setFieldName(name);

    }

    @Override
    public boolean isFormField() {
        return delegate.isFormField();
    }

    @Override
    public void setFormField(boolean state) {
        delegate.setFormField(state);
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        return new Base64OutputStream(new CipherOutputStream(delegate.getOutputStream(), getEncryptionCipher()));
    }
}
