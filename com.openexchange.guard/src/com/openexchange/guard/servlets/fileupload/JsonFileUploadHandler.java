/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.fileupload;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;

/**
 * {@link JsonFileUploadHandler} adds various JSON handling to a {@link FileUploadHandler}.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class JsonFileUploadHandler {

    private final FileUploadHandler fileUploadFactory;

    /**
     * Initializes a new {@link JsonFileUploadHandler}.
     *
     * @param fileUploadHandler The {@link FileUploadHandler} to add the JSON handling for.
     */
    public JsonFileUploadHandler(FileUploadHandler fileUploadHandler) {
        this.fileUploadFactory = fileUploadHandler;
    }

    /**
     * Parses the JSON part from the incoming input stream.
     *
     * @param jsonInputStream the input stream to parse the JSON from
     * @return The json object parse from the input stream
     * @throws IOException
     * @throws JSONException
     */
    public JSONObject getJsonFrom(InputStream jsonInputStream) throws IOException, JSONException {
        ByteArrayOutputStream jsonData = new ByteArrayOutputStream();
        IOUtils.copy(jsonInputStream, jsonData);
        return new JSONObject(new String(jsonData.toByteArray(), StandardCharsets.UTF_8));
    }

    /**
     * Gets the JSON object from a parsed list of fileItems.
     *
     * @param fileItems The parsed list of FileItems
     * @param name The name of the JSON item
     * @return The JSON object, or null if the list does not contain a JSON object
     * @throws IOException
     * @throws JSONException
     * @throws OXException If mandatory is set to true and the given FileItem was not found.
     */
    public JSONObject getJsonFrom(Collection<FileItem> fileItems, String name) throws IOException, JSONException {
        try {
            return getJsonFrom(fileItems, name, false);
        } catch (OXException e) {
            return null;
        }
    }

    /**
     * Gets the JSON object from a parsed list of fileItems.
     *
     * @param fileItems The parsed list of FileItems
     * @param name The name of the JSON item
     * @param mandatory True to throw an OXException if the JSON is not present in the given fileItems, False to return null.
     * @return The JSON object, or null if the list does not contain a JSON object and mandatory is set to false
     * @throws IOException
     * @throws OXException If mandatory is set to true and the given FileItem was not found.
     * @throws JSONException
     */
    public JSONObject getJsonFrom(Collection<FileItem> fileItems, String name, boolean mandatory) throws IOException, OXException, JSONException {
        try(InputStream jsonStream = fileUploadFactory.getFileItemStreamFrom(fileItems, name, mandatory);){
            if (jsonStream != null) {
                return getJsonFrom(jsonStream);
            }
            return null;
        }
    }
}
