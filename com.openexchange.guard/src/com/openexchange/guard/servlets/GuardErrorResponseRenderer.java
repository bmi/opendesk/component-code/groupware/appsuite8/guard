
package com.openexchange.guard.servlets;

import static com.openexchange.guard.servlets.util.ServletLocaleUtil.getLocaleSaveFor;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.openexchange.ajax.AJAXUtility;
import com.openexchange.exception.Category;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.java.Strings;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.translation.GuardTranslationService;

public class GuardErrorResponseRenderer {

    private static final String REQUEST_PARAM_INCLUDE_STACKTRACE = "includeStackTraceOnError";
    private static final String MULTIPART = "multipart/";
    private static final String CALLBACK = "callback";
    private static final String JSON_ERROR = "error";
    private static final String JSON_ERROR_PARAMS = "error_params";
    private static final String JSON_CATEGORIES = "categories";
    private static final String JSON_CATEGORY = "category";
    private static final String JSON_CODE = "code";
    private static final String JSON_ERROR_ID = "error_id";
    private static final String JSON_ERROR_DESC = "error_desc";
    private static final String JSON_ERROR_STACK = "error_stack";
    private static final Object JSON_STACK_NATIVE_METHOD = "(Native Method)";
    private static final Object JSON_STACK_UNKNOWN_SOURCE = "(Unknown Source)";

    private static final char[] JS_FRAGMENT_PART1 = ("<!DOCTYPE html><html><head>" + "<META http-equiv=\"Content-Type\" " + "content=\"text/html; charset=UTF-8\">" + "<script type=\"text/javascript\">" + "(parent[\"callback_").toCharArray();

    private static final char[] JS_FRAGMENT_PART2 = "\"] || window.opener && window.opener[\"callback_".toCharArray();

    private static final char[] JS_FRAGMENT_PART3 = ")</script></head></html>".toCharArray();

    private static final Pattern PATTERN_QUOTE = Pattern.compile("(^|[^\\\\])\"");
    private static final Pattern PATTERN_CONTROL = Pattern.compile("[\\x00-\\x1F\\x7F]");
    private static final Pattern PATTERN_DSLASH = Pattern.compile("(?://+)");
    private final int errorResponseCode;
    private final boolean wrapErrorInArray;

    /**
     * Initializes a new {@link GuardErrorResponseRenderer}.
     *
     * @param errorResponseCode The response code which will be send to the client in case of an error
     */
    public GuardErrorResponseRenderer(int errorResponseCode) {
        this(errorResponseCode, false);
    }

    /**
     * Initializes a new {@link GuardErrorResponseRenderer}.
     * 
     * @param errorResponseCode The response code which will be send to the client in case of an error
     * @param wrapErrorInArray True in order to wrap the error in an array (legacy API), false otherwise
     */
    public GuardErrorResponseRenderer(int errorResponseCode, boolean wrapErrorInArray) {
        this.errorResponseCode = errorResponseCode;
        this.wrapErrorInArray = wrapErrorInArray;
    }

    /**
     * Internal method to check whether the request contains multipart content or not
     *
     * @param request The request to be evaluated.
     * @return true if the request is multipart, false otherwise
     */
    private final boolean isMultipartContent(HttpServletRequest request) {
        final String contentType = request.getContentType();
        if (contentType == null) {
            return false;
        }
        if (contentType.toLowerCase().startsWith(MULTIPART)) {
            return true;
        }
        return false;
    }

    /**
     * Internal method to check if the client requested HTML response
     *
     * @param request the client's request
     * @return true, if the client requested a HTML response, false otherwise
     */
    private boolean isRespondWithHTML(final HttpServletRequest request) {
        return Boolean.parseBoolean(request.getParameter("respondWithHTML"));
    }

    /**
     * Internal method to check if the client requested JSON response
     *
     * @param request the client's request
     * @return true, if the client requested a JSON response, false otherwise
     */
    protected boolean isRespondJSON(final HttpServletRequest request) {
        return Boolean.parseBoolean(request.getParameter("respondWithJSON"));
    }

    /**
     * Checks whether the client expects the error object wrapped in HTML/JS
     *
     * @param request The HTTP request to check
     * @return <code>true</code> if a JavaScript call-back is expected; otherwise <code>false</code>
     */
    private boolean expectsJsCallback(HttpServletRequest request) {
        return (!isRespondJSON(request) && (isMultipartContent(request) || isRespondWithHTML(request) || request.getParameter(CALLBACK) != null));
    }

    /**
     * Internal method to create a JSON error object
     *
     * @param e the exception to create the error object for
     * @param locale the request's locale
     * @param includeStackTrace true, if a stack trace should be included, false otherwise
     * @return The json error object for the given exception
     */
    protected JsonObject createErrorJson(OXException e, Locale locale, boolean includeStackTrace) {
        JsonObject errorObject = new JsonObject();
        String translatedDisplayMessage = getTranslatedDisplayMessage(e, locale);
        errorObject.addProperty(JSON_ERROR, translatedDisplayMessage);

        JsonArray errorParams = new JsonArray();
        Object[] logArgsArray = e.getLogArgs();
        if (logArgsArray != null) {
            for (Object logArgs : logArgsArray) {
                if (logArgs != null) {
                    JsonPrimitive displayArgParam = new JsonPrimitive(logArgs.toString());
                    errorParams.add(displayArgParam);
                }
            }
        }
        errorObject.add(JSON_ERROR_PARAMS, errorParams);

        if ((e.getCategories() != null) && (e.getCategories().size() == 1)) {
            errorObject.addProperty(JSON_CATEGORIES, e.getCategory().toString());
        } else {
            JsonArray categoryParams = new JsonArray();
            List<Category> categories = e.getCategories();
            for (Category c : categories) {
                JsonPrimitive categorieParam = new JsonPrimitive(c.toString());
                categoryParams.add(categorieParam);
            }
            errorObject.add(JSON_CATEGORIES, categoryParams);
        }

        errorObject.addProperty(JSON_CATEGORY, e.getCategory().toString());
        errorObject.addProperty(JSON_CODE, e.getErrorCode());
        errorObject.addProperty(JSON_ERROR_ID, e.getExceptionId());
        errorObject.addProperty(JSON_ERROR_DESC, e.getSoleMessage());

        JsonArray errorStack = new JsonArray();
        if (includeStackTrace) {
            StackTraceElement[] traceElements = e.getStackTrace();
            if (traceElements != null && traceElements.length > 0) {
                final StringBuilder tmp = new StringBuilder(64);
                for (final StackTraceElement stackTraceElement : traceElements) {
                    tmp.setLength(0);
                    writeElementTo(stackTraceElement, tmp);
                    JsonPrimitive error = new JsonPrimitive(tmp.toString());
                    errorStack.add(error);
                }
            }
        }
        errorObject.add(JSON_ERROR_STACK, errorStack);
        return errorObject;
    }

    private String getTranslatedDisplayMessage(OXException e, Locale locale) {
        String displayMessage = e.getDisplayMessage(locale); // try to get translation from middleware (e. g. for OXExceptionStrings.MESSAGE)
        if (Strings.isEmpty(displayMessage) || displayMessage.equalsIgnoreCase(e.getDisplayMessage(null))) { // try guard translation
            GuardTranslationService translationService = Services.optService(GuardTranslationService.class);
            if (translationService != null) {
                return translationService.getTranslation(displayMessage, locale.toString());
            }
        }
        return displayMessage;
    }

    private static void writeElementTo(final StackTraceElement element, final StringBuilder sb) {
        sb.append(element.getClassName()).append('.').append(element.getMethodName());
        if (element.isNativeMethod()) {
            sb.append(JSON_STACK_NATIVE_METHOD);
        } else {
            final String fileName = element.getFileName();
            if (null == fileName) {
                sb.append(JSON_STACK_UNKNOWN_SOURCE);
            } else {
                sb.append('(').append(fileName);
                final int lineNumber = element.getLineNumber();
                if (lineNumber >= 0) {
                    sb.append(':').append(lineNumber);
                }
                sb.append(')');
            }
        }
    }

    /**
     * Renders the given exception to a response
     *
     * @param request the request
     * @param response the response
     * @param actionName
     * @param e the error to render
     * @throws IOException
     */
    public void renderError(HttpServletRequest request, HttpServletResponse response, String actionName, OXException e) throws IOException {
        response.setStatus(errorResponseCode);
        boolean includeStackTrace = ServletUtils.getBooleanParameter(request, REQUEST_PARAM_INCLUDE_STACKTRACE);
        JsonObject errorObject = createErrorJson(e, getLocaleSaveFor(request), includeStackTrace);
        if (expectsJsCallback(request)) {
            response.setContentType("text/html; charset=UTF-8");
            String callback = ServletUtils.getStringParameter(request, CALLBACK);
            if (callback != null) {
                if (callback.indexOf('"') >= 0) {
                    callback = PATTERN_QUOTE.matcher(callback).replaceAll("$1\\\\\"");
                }
            } else {
                callback = actionName;
            }
            callback = AJAXUtility.sanitizeParam(callback);

            PrintWriter writer = null;
            try {
                try {
                    writer = response.getWriter();
                } catch (IllegalStateException e1) {
                    //It's possible that the underlying logic did not use getWriter() but getOutputStream(); And only one is allowed.
                    writer = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), response.getCharacterEncoding()));
                } finally {
                    if (writer != null) {
                        writer.write(JS_FRAGMENT_PART1);
                        writer.write(callback);
                        writer.write(JS_FRAGMENT_PART2);
                        writer.write(callback);
                        writer.write("\"])(");
                        writer.write(errorObject.toString());
                        writer.write(JS_FRAGMENT_PART3);
                    }
                }
            } finally {
                if (writer != null) {
                    writer.close();
                }
            }
        } else {
            //Writing error as plain JSON
            JsonElement ret = null;
            if (this.wrapErrorInArray) {
                JsonArray errorObjects = new JsonArray();
                errorObjects.add(errorObject);
                ret = errorObjects;
            } else {
                ret = errorObject;
            }
            PrintWriter writer = null;
            try {
                try {
                    writer = response.getWriter();
                } catch (IllegalStateException e1) {
                    //It's possible that the underlying logic did not use getWriter() but getOutputStream(); And only one is allowed.
                    writer = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), response.getCharacterEncoding()));
                }
                response.setContentType("application/json; charset=UTF-8");
                // response.addHeader("Access-Control-Allow-Origin", "*");
                writer.write(ret.toString());
            } finally {
                if (writer != null) {
                    writer.close();
                }
            }
        }
    }
}
