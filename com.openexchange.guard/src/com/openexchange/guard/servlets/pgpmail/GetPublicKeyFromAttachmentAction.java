/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.pgpmail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Properties;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.servlets.utils.AntiAbuseUtils;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.crypto.CryptoManager;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.OGPGPKeyRing;
import com.openexchange.guard.keymanagement.commons.antiabuse.AntiAbuseAction;
import com.openexchange.guard.keymanagement.commons.antiabuse.AntiAbuseWrapper;
import com.openexchange.guard.keymanagement.services.PublicExternalKeyService;
import com.openexchange.guard.mime.services.Attachment;
import com.openexchange.guard.mime.services.MimeEncryptionService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.oxapi.Api;
import com.openexchange.guard.oxapi.ApiResponse;
import com.openexchange.guard.oxapi.OxCookie;
import com.openexchange.guard.pgpcore.services.PGPCryptoService;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.keymanagement.responses.ExternalPublicKeyRingCollectionResponse;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.keys.parsing.PGPPublicKeyRingFactory;

/**
 * Retrieves a public key sent as mail attachment
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class GetPublicKeyFromAttachmentAction extends GuardServletAction {

    private static final boolean MANDATORY           = true;
    private static final String  PARAM_FOLDER        = "folder";
    private static final String  PARAM_EMAIL_ID      = "emailid";
    private static final String  PARAM_ATTACKMENT_ID = "attach";
    private static final String  PARAM_AUTH_TOKEN    = "auth";
    private static final String  PARAM_FILE_NAME     = "filename";
    private static final String  PARAM_INLINE        = "inline";

    private Api createOXApi(HttpServletRequest request) {
        return new Api(new OxCookie(request.getCookies()), request);
    }

    private PGPPublicKeyRing createKeyRing(String asciiKeyData) throws IOException {
        return PGPPublicKeyRingFactory.create(asciiKeyData);
    }

    private PGPPublicKeyRing createKeyRing(InputStream inputStream) throws IOException {
        return createKeyRing(new String(IOUtils.toByteArray(inputStream), StandardCharsets.UTF_8));
    }

    private PGPPublicKeyRing parseKeyRingData(Api oxapi, String folder, String emailid, String attachmentId) throws IOException {
        try(ApiResponse plainAttachment = oxapi.getPlainAttachment(emailid, attachmentId, folder)){
            Attachment att = new Attachment();
            att.setContent(plainAttachment.readContent());
            return createKeyRing(new String(att.getContent(), StandardCharsets.UTF_8));
        }
    }

    private PGPPublicKeyRing parsePGPInlineKeyData(UserIdentity userIdentity, Api oxapi, String folder, String emailid, String attachmentId) throws IllegalStateException, IOException, OXException {
        PGPCryptoService cryptoService = Services.getService(PGPCryptoService.class);
        try(ApiResponse plainAttachment = oxapi.getPlainAttachment(emailid, attachmentId, folder)){
            if (plainAttachment == null) {
                throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create("Unable to get attachment");
            }
            InputStream encryptedKeyData = plainAttachment.getContent();
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            cryptoService.decrypt(encryptedKeyData, output, userIdentity);
            return createKeyRing(new String(output.toByteArray(), StandardCharsets.UTF_8));
        }
    }

    private PGPPublicKeyRing parsePGPMimeKeyData(UserIdentity userIdentity, Api oxapi, String folder, String emailid, String filename) throws OXException, MessagingException, IOException {
        MimeMessage mimeMessage = new MimeMessage(
            Session.getDefaultInstance(new Properties()),
            new ByteArrayInputStream(oxapi.getMime(emailid, folder).getBytes(StandardCharsets.UTF_8)));
        MimeEncryptionService mimeCryptoService = Services.getService(CryptoManager.class).getMimeEncryptionService(CryptoType.PROTOCOL.PGP);
        Part decryptedPart = mimeCryptoService.doDecryption(mimeMessage, filename, userIdentity);
        return decryptedPart == null ? null : createKeyRing(decryptedPart.getInputStream());
    }

    private List<OGPGPKeyRing> importExternalPublicKeyring(GuardUserSession userSession, PGPPublicKeyRing parsedKeyRingData) throws OXException {
        PublicExternalKeyService externalKeyService = Services.getService(PublicExternalKeyService.class);
        return externalKeyService.importPublicKeyRing(userSession.getUserId(), userSession.getContextId(), parsedKeyRingData);
    }


    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response,GuardUserSession userSession) throws Exception {

        PGPPublicKeyRing parsedKeyRingData = null;

        String folder = ServletUtils.getStringParameter(request, PARAM_FOLDER,MANDATORY);
        String emailid = ServletUtils.getStringParameter(request, PARAM_EMAIL_ID, MANDATORY);
        String attachmentId = ServletUtils.getStringParameter(request, PARAM_ATTACKMENT_ID, MANDATORY);
        String auth = ServletUtils.getStringParameter(request, PARAM_AUTH_TOKEN, !MANDATORY);

        if(auth == null) {
            //The client wants to import the keyring from a plaintext email object
            parsedKeyRingData = parseKeyRingData(createOXApi(request), folder, emailid, attachmentId);
        } else {
            //The client wants to import the key from an encrypted email object
            UserIdentity userIdentity = requireUserIdentityFrom(userSession, auth);
            AntiAbuseWrapper antiAbuseWrapper = getAntiAbuseWrapper(
                AntiAbuseUtils.getAllowParameters(request, userIdentity.getIdentity(), new String(userIdentity.getPassword())));
            final String filename = ServletUtils.getStringParameter(request, PARAM_FILE_NAME, MANDATORY);
            final boolean inline = ServletUtils.getBooleanParameter(request,PARAM_INLINE,!MANDATORY);
            parsedKeyRingData = antiAbuseWrapper.doAction(new AntiAbuseAction<PGPPublicKeyRing>() {

                @Override
                public PGPPublicKeyRing doAction() throws Exception {
                    return  inline ?
                        parsePGPInlineKeyData(userIdentity, createOXApi(request), folder, emailid, attachmentId ) :
                        parsePGPMimeKeyData(userIdentity, createOXApi(request), folder, emailid, filename);
                }
            });

        }

        //Import the parsed key data as external key ring
        List<OGPGPKeyRing> importedKeys = importExternalPublicKeyring(userSession, parsedKeyRingData);
        ServletUtils.sendObject(response, new ExternalPublicKeyRingCollectionResponse(importedKeys));
    }
}
