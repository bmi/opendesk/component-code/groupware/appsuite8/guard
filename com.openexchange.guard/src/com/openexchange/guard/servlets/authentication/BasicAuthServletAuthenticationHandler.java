/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.authentication;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.servlets.GuardServletAction;

/**
 * BASIC-AUTH authentication for OX Guard requests
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class BasicAuthServletAuthenticationHandler implements GuardAuthenticationHandler {

    private static final String WWW_AUTHENTICATE_BASIC_AUTH = "BASIC-AUTH";

    private static final Logger LOG = LoggerFactory.getLogger(BasicAuthServletAuthenticationHandler.class);

    private static final String AUTHORISATION_HEADER = "Authorization";
    private static final String BASIC_AUTH_PREFIX = "Basic";

    private final String userName;
    private final String password;

    /**
     * Initializes a new {@link BasicAuthServletAuthenticationHandler}.
     *
     * @param userName The user name to check
     * @param password The password to check
     */
    public BasicAuthServletAuthenticationHandler(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    /**
     * Checks the credentials
     *
     * @param credentials The credentials to check
     * @return true, if user name and password are correct, false otherwise
     */
    private boolean checkCredentials(String[] credentials) {
        return credentials[0].equals(userName) && credentials[1].equals(password);
    }

    @Override
    public boolean authenticate(GuardServletAction action, GuardUserSession userSession, HttpServletRequest request) throws Exception {
        String authHeader = new AuthorizationHeaderParser().parse(BASIC_AUTH_PREFIX, request);
        if(authHeader != null && authHeader.contains(":")) {
            String[] credentials = authHeader.split(":");
            if (credentials != null && credentials.length == 2) {
                boolean ret = checkCredentials(credentials);
                if (!ret) {
                    LOG.error(String.format("BASIC_AUTH authentication failed from IP %s for request %s", ServletUtils.getClientIP(request), request.toString()));
                }
                return ret;
            }
        }

        return false;
    }

    @Override
    public String getWWWAuthenticateHeader() {
        return WWW_AUTHENTICATE_BASIC_AUTH;
    }
}
