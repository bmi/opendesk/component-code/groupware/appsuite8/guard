/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.authentication;

import java.util.Objects;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.pgpcore.services.TokenAuthenticationService;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link UserIdentityJsonParser}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.3
 */
public class UserIdentityJsonParser implements UserIdentityParser {

    private static final Logger logger = LoggerFactory.getLogger(UserIdentityJsonParser.class);
    private final JSONObject json;
    private final TokenAuthenticationService tokenAuthenticationService;


    /**
     * Initializes a new {@link UserIdentityJsonParser}.
     * @param json The json to parse the auth-token from
     */
    public UserIdentityJsonParser(TokenAuthenticationService tokenAuthenticationService, JSONObject json) {
        this.tokenAuthenticationService = Objects.requireNonNull(tokenAuthenticationService, "The provided service must not be null.");
        this.json = Objects.requireNonNull(json, "The provided JSON object must not be null");
    }

    /**
     * Constructs an UserIdentity from a given authToken
     *
     * @param sessionIdentifier The client identifier
     * @param authToken The authToken
     * @return The decrypted UserIdentifier
     * @throws OXException
     */
    private UserIdentity getIdentityForToken(String sessionIdentifier, String authToken) throws OXException {
        return tokenAuthenticationService.decryptUserIdentity(sessionIdentifier, authToken);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.servlets.pgpcore.UserIdentityFactory#create()
     */
    @Override
    public UserIdentity parse() throws OXException {
        try {
            if (json.has("user")) {
                JSONObject signer = json.getJSONObject("user");
                if (signer.has("identity") && signer.has("password")) {
                    return new UserIdentity(signer.get("identity").toString(), signer.get("password").toString());
                } else if (signer.has("session") && signer.has("auth")) {
                    UserIdentity ret = getIdentityForToken(signer.get("session").toString(), signer.get("auth").toString());
                    if (ret == null) {
                        logger.error("Unable to decode user identity from auth-token");
                    }
                    return ret;
                }
            }
            return null;
        } catch (Exception e) {
            throw GuardCoreExceptionCodes.JSON_ERROR.create(e.getMessage());
        }
    }

    @Override
    public UserIdentity parse(int userId, int cid) throws OXException {
        return parse();
    }
}
