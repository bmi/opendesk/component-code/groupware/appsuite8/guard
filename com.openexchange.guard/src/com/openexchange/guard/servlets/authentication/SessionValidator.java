package com.openexchange.guard.servlets.authentication;

import javax.servlet.http.HttpServletRequest;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.session.GuardUserSession;

/**
 * {@link SessionValidator} Validates a session ID
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4
 */
public interface SessionValidator {

    /**
     * Validates a session ID
     * @param the request to validate the session for
     * @return a session object if the session ID was valid, or null if the session ID was not valid
     * @throws OXException
     */
    GuardUserSession validateSession(HttpServletRequest request) throws OXException;
}
