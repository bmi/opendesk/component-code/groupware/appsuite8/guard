/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.pgpcore;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.fileupload.FileUploadHandler;
import com.openexchange.guard.servlets.fileupload.JsonFileUploadHandler;
import com.openexchange.guard.sharing.SharingService;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link ChangeShareAction}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class ChangeShareAction extends GuardServletAction {

    private static final String DATA_FIELD_NAME = "data";
    private static final String JSON_DATA_FIELD_NAME = "json";
    private static final String CONTENT_TYPE_OCTETSTREAM= "application/octet-stream";
    private static final String UPDATED_SHARES_JSON_ARRAY = "updatedShares";
    private static final String USER_ID = "id";

    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        SharingService sharing = Services.getService(SharingService.class);
        FileUploadHandler fileUploadHandler = new FileUploadHandler(Services.getService(GuardConfigurationService.class));
        JsonFileUploadHandler jsonFileUploadHandler = new JsonFileUploadHandler(fileUploadHandler);
        Collection<FileItem> items = fileUploadHandler.parseItems(request);
        try (InputStream dataStream = fileUploadHandler.getFileItemStreamFrom(items, DATA_FIELD_NAME,true)){

            JSONObject json = jsonFileUploadHandler.getJsonFrom(items, JSON_DATA_FIELD_NAME, true);
            if (json == null || !json.has(UPDATED_SHARES_JSON_ARRAY)) {
                throw OXException.mandatoryField(UPDATED_SHARES_JSON_ARRAY);
            }
            JSONArray newShares = json.getJSONArray(UPDATED_SHARES_JSON_ARRAY);
            UserIdentity userIdentity = getUserIdentityFrom(json);
            if (userIdentity == null) {
                throw OXException.mandatoryField("user identity information");
            }
            response.setContentType(CONTENT_TYPE_OCTETSTREAM);
            sharing.shareFile(dataStream, getList(newShares), userIdentity, response.getOutputStream());
        }

    }

    private List<Integer> getList (JSONArray json) throws OXException {
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (Object user: json) {
            JSONObject userJson = (JSONObject) user;
            if (!userJson.has(USER_ID)) {
                throw OXException.mandatoryField("id in User list for sharing");
            }
            try {
                list.add(userJson.getInt(USER_ID));
            } catch (JSONException e) {
                throw OXException.general("Problem adding userid for share, possibly mal-formed JSON");
            }
        }
        return list;
    }

}
