/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.pgpcore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.pgpcore.services.TokenAuthenticationService;
import com.openexchange.guard.servlets.GuardServletAction;

/**
 * {@link CreateAuthenticationTokenAction}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public class CreateAuthenticationTokenAction extends GuardServletAction {

    private static final String SESSION_IDENTIFIER_PARAMETER_NAME = "session";
    private static final String USER_IDENTITY_PARAMETER_NAME = "identity";
    private static final String PASSWORD_PARAMETER_NAME = "password";
    private static final String TYPE = "type";

    private JSONObject createResponseJson(String authToken) throws JSONException {
        JSONObject ret = new JSONObject();
        ret.put("auth", authToken);
        return ret;
    }

    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {

        TokenAuthenticationService tokenAuthService = Services.getService(TokenAuthenticationService.class);

        String sessionIdentifier = ServletUtils.getStringParameter(request, SESSION_IDENTIFIER_PARAMETER_NAME, true);
        String userIdentity = ServletUtils.getStringParameter(request, USER_IDENTITY_PARAMETER_NAME, true);
        String password = ServletUtils.getStringParameter(request, PASSWORD_PARAMETER_NAME, true);
        String type = ServletUtils.getStringParameter(request, TYPE, false);
        if (type == null) {
            type = "pgp";  // fallback
        }

        String authToken = tokenAuthService.createAuthenticationToken(sessionIdentifier, userIdentity, password.toCharArray(), type);
        if (authToken != null) {
            ServletUtils.sendOK(response, "application/json", createResponseJson(authToken).toString());
        }
        else {
            ServletUtils.sendResponse(response, HttpStatus.SC_UNAUTHORIZED, "Unauthorized");
        }
    }
}
