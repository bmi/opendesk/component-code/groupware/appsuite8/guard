/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.pgpcore;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileItem;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.pgpcore.services.PGPPacketService;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.fileupload.FileUploadHandler;


/**
 * {@link ExtractEncryptedSessionKeysAction}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.0
 */
public class ExtractEncryptedSessionKeysAction extends GuardServletAction {

    private static final String DATA_FIELD_NAME = "data";

    /* (non-Javadoc)
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        final PGPPacketService pgpPacketService = Services.getService(PGPPacketService.class);
        FileUploadHandler fileUploadHandler = new FileUploadHandler(Services.getService(GuardConfigurationService.class));
        Collection<FileItem> items = fileUploadHandler.parseItems(request);
        final boolean mandatory = true;
        try(InputStream dataStream = fileUploadHandler.getFileItemStreamFrom(items, DATA_FIELD_NAME, mandatory)){
            final Collection<byte[]> encrytedSessions = pgpPacketService.getEncrytedSessions(dataStream);

            ServletUtils.sendObject(response, new Object(){

                public String[] getEncryptedSessionKeyPackets() {
                    ArrayList<String> ret = new ArrayList<String>();
                    for(byte[] session : encrytedSessions) {
                       ret.add(new String(Base64.encodeBase64(session), StandardCharsets.UTF_8));
                    }
                    return ret.toArray(new String[ret.size()]);
                }
            });
        }
    }
}
