/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.pgpcore;

import java.io.IOException;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.AbstractGuardServlet;
import com.openexchange.guard.servlets.GuardErrorResponseRenderer;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.authentication.BasicAuthServletAuthenticationHandler;
import com.openexchange.guard.servlets.authentication.GuardAuthenticationHandler;

/**
 * {@link PGPCoreServlet}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public class PGPCoreServlet extends AbstractGuardServlet {

    private static final long serialVersionUID = -7751294336216449509L;
    private static final String POST_ENCRYPT_ACTION = "encrypt";
    private static final String POST_DECRYPT_ACTION = "decrypt";
    private static final String POST_SIGN_ACTION = "sign";
    private static final String POST_VERIFY_ACTION = "verify";
    private static final String POST_CREATE_AUTH_ACTION = "createauthtoken";
    private static final String POST_DELETE_AUTH_ACTION = "deleteauthtoken";
    private static final String POST_EXTRACT_ENCRYPTED_SESSION_KEY_PACKETS = "extractsessionkeys";
    private static final String POST_CHANGE_SHARING = "changeshare";
    private static final String GET_AUTOCRYPT = "getautocrypt";
    private transient final HashMap<String, GuardServletAction> postActions;
    private transient final HashMap<String, GuardServletAction> putActions;
    private transient final HashMap<String, GuardServletAction> getActions;

    /**
     * Internal method to create an authentication handler which protects the API
     *
     * @return An authentication handler to use
     * @throws OXException
     */
    private GuardAuthenticationHandler createAuthenticationHandler() throws OXException {
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        return new BasicAuthServletAuthenticationHandler(configService.getProperty(GuardProperty.restApiUsername),
            configService.getProperty(GuardProperty.restApiPassword));
    }

    /**
     * Initializes a new {@link PGPCoreServlet}.
     *
     * @throws OXException
     */
    public PGPCoreServlet() throws OXException {
         //Using 500 as default error code for OXExceptions for the core PGP API
        super(new GuardErrorResponseRenderer(HttpServletResponse.SC_INTERNAL_SERVER_ERROR));

        GuardAuthenticationHandler authHandler = createAuthenticationHandler();

        //GET
        getActions = new HashMap<String, GuardServletAction>();
        getActions.put(GET_AUTOCRYPT, new GetAutocryptAction().setAuthenticationHandler(authHandler));

        //POST
        postActions = new HashMap<String, GuardServletAction>();
        postActions.put(POST_CREATE_AUTH_ACTION, new CreateAuthenticationTokenAction().setAuthenticationHandler(authHandler));
        postActions.put(POST_DELETE_AUTH_ACTION, new DeleteAuthenticationTokenAction().setAuthenticationHandler(authHandler));
        postActions.put(POST_ENCRYPT_ACTION, new EncryptAction().setAuthenticationHandler(authHandler));
        postActions.put(POST_DECRYPT_ACTION, new DecryptAction().setAuthenticationHandler(authHandler));
        postActions.put(POST_SIGN_ACTION, new SignAction().setAuthenticationHandler(authHandler));
        postActions.put(POST_VERIFY_ACTION, new VerifyAction().setAuthenticationHandler(authHandler));
        postActions.put(POST_EXTRACT_ENCRYPTED_SESSION_KEY_PACKETS, new ExtractEncryptedSessionKeysAction().setAuthenticationHandler(authHandler));
        postActions.put(POST_CHANGE_SHARING, new ChangeShareAction().setAuthenticationHandler(authHandler));

        //PUT
        putActions = new HashMap<String, GuardServletAction>();
        //Also making the delete action available via PUT for backward compatibility reason
        putActions.put(POST_DELETE_AUTH_ACTION, new DeleteAuthenticationTokenAction().setAuthenticationHandler(authHandler));
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        super.doAction(request, response, postActions);
    }

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doPut(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       super.doAction(request, response, putActions);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doAction(request, response, getActions);
     }
}
