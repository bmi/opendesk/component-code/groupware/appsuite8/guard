/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.pgpcore;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.DeferredFileOutputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.common.util.JsonUtil;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.pgpcore.services.PGPCryptoService;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.fileupload.FileUploadHandler;
import com.openexchange.guard.servlets.fileupload.JsonFileUploadHandler;
import com.openexchange.guard.sharing.SharingService;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link EncryptAction}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public class EncryptAction extends GuardServletAction {

    private static final Logger log = LoggerFactory.getLogger(GuardServletAction.class);
    private static final String CONTENT_TYPE_PGP_ENCRYPTED = "application/pgp-encrypted";
    private static final String JSON_DATA_FIELD_NAME = "json";
    private static final String DATA_FIELD_NAME = "data";
    private static final String OPTION_ARMORED = "armored";
    private static final String RECIPIENT_ARRAY_NAME = "recipients";
    private static final String SHARING_ARRAY_NAME = "sharing";

    /**
     * Parses the recipient list from the given JSON object
     *
     * @param json the JSON object to parse the recipient list from
     * @return A list of recipients parsed from the given JSON object
     * @throws OXException
     * @throws JSONException
     */
    private List<String> getRecipientFromJson(JSONObject json) throws OXException, JSONException {

        ArrayList<String> ret = new ArrayList<>();
        if (json.has(RECIPIENT_ARRAY_NAME)) {
            JSONArray recipients = json.getJSONArray(RECIPIENT_ARRAY_NAME);
            for (int i = 0; i < recipients.length(); i++) {
                ret.add(recipients.getString(i));
            }
        }
        if (json.has(SHARING_ARRAY_NAME)) {
            JSONArray sharesArray = json.getJSONArray(SHARING_ARRAY_NAME);
            ArrayList<String> shares = new ArrayList<>();
            for (int i = 0; i < sharesArray.length(); i++) {
                if (!sharesArray.getJSONObject(i).has("email")) {
                    throw OXException.mandatoryField("email in sharing array");
                }
                shares.add(sharesArray.getJSONObject(i).getString("email"));
            }
            SharingService sharingService = Services.getService(SharingService.class);
            return sharingService.getAdditionalShareRecipients(ret, shares);
        }
        if (ret.isEmpty()) {
            throw GuardCoreExceptionCodes.JSON_PARAMETER_MISSING.create(RECIPIENT_ARRAY_NAME);
        }
        return ret;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {

        JSONObject json = null;
        InputStream dataStream = null;
        FileUploadHandler fileUploadHandler = new FileUploadHandler(Services.getService(GuardConfigurationService.class));
        JsonFileUploadHandler jsonFileUploadHandler = new JsonFileUploadHandler(fileUploadHandler);

        //Using streaming approach because we do not want large plaintext data to be buffered on disk for parsing.
        FileItemIterator itemIterator = fileUploadHandler.parseIterator(request);
        while (itemIterator.hasNext()) {
            FileItemStream fileItemStream = itemIterator.next();
            if (fileItemStream.getFieldName().equals(JSON_DATA_FIELD_NAME)) {
                try (InputStream jsonStream = fileItemStream.openStream()) {
                    json = jsonFileUploadHandler.getJsonFrom(jsonStream);
                }
            } else if (fileItemStream.getFieldName().equals(DATA_FIELD_NAME)) {
                dataStream = fileItemStream.openStream();
            }

            if (json != null && dataStream != null) {
                //we got all we need: prevent that hasNext() skips the dataStream.
                break;
            }
        }

        if (json != null) {
            if (dataStream != null) {

                //Parsing the provided Json
                boolean armored = JsonUtil.getBooleanFromJson(json, OPTION_ARMORED);
                List<String> recipients = getRecipientFromJson(json);
                UserIdentity signingIdentity = null;
                if (json.has("user")) {
                    signingIdentity = getUserIdentityFrom(json);
                    if (signingIdentity == null) {
                        throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
                    }
                }

                //Encrypting the data to memory or disk.
                DeferredFileOutputStream outputStream = fileUploadHandler.createOutputStream();
                try {
                    PGPCryptoService cryptoService = Services.getService(PGPCryptoService.class);
                    if (signingIdentity != null) {
                        cryptoService.encryptSigned(dataStream, outputStream, armored, signingIdentity, recipients);
                    } else {
                        cryptoService.encrypt(dataStream, outputStream, armored, recipients);
                    }

                    //Send the data back to the client
                    try (InputStream responseStream = outputStream.isInMemory() ? new ByteArrayInputStream(outputStream.getData()) : new FileInputStream(outputStream.getFile());) {
                        response.setContentType(CONTENT_TYPE_PGP_ENCRYPTED);
                        IOUtils.copy(responseStream, response.getOutputStream());
                    }
                } finally {
                    //Cleanup if encrypted data has been buffered to disk.
                    if (!outputStream.isInMemory()) {
                        try {
                            Files.delete(outputStream.getFile().toPath());
                        } catch (IOException e) {
                            //Just log the error
                            log.error("Error deleting tmp. file {}", outputStream.getFile().toPath());
                        }
                    }
                    IOUtils.closeQuietly(outputStream);
                    IOUtils.closeQuietly(dataStream);
                }

            } else {
                throw GuardCoreExceptionCodes.PARAMETER_MISSING.create(DATA_FIELD_NAME);
            }
        } else {
            throw GuardCoreExceptionCodes.PARAMETER_MISSING.create(JSON_DATA_FIELD_NAME);
        }
    }
}
