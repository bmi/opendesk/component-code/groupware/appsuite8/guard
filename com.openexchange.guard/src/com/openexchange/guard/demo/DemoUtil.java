/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.demo;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.antiabuse.GuardAntiAbuseService;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.demo.exceptions.DemoExceptionCodes;
import com.openexchange.guard.email.storage.ogEmail.Email;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.smime.SmimeKeyService;
import com.openexchange.guard.support.UserDeleter;
import com.openexchange.session.UserAndContext;

/**
 * This class provides functionalities which are useful for running OX Guard in
 * a development or demonstration environment; For accessing the class it is required
 * to set com.openexchange.guard.demo to true.
 *
 * @author @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
public class DemoUtil {

    private static final Logger LOG = LoggerFactory.getLogger(DemoUtil.class);

    /**
     * Throws an OXException if OX Guard is not in demo mode
     *
     * @throws OXException
     */
    protected void assertDemoMode() throws OXException {
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        if (!configService.getBooleanProperty(GuardProperty.demo)) {
            LOG.error("Attempted access demo functionality, but not configured for demo.  Add demo:true to config file is needed");
            throw DemoExceptionCodes.DEMO_MODE_DISABLED_ERROR.create();
        }
    }
    
    /**
     * Internal method to resolve a {@link UserAndContext} from the given email
     *
     * @param email The email to revolve the {@link UserAndContext} for
     * @return The {@link UserAndContext} for the given email, or null if no {@link UserAndContext} could be resolved
     * @throws OXException
     */
    private UserAndContext getUserAndContext(String email) throws OXException {
        //Check for PGP
        EmailStorage ogEmailStorage = Services.getService(EmailStorage.class);
        Email emailObject = ogEmailStorage.getByEmail(email);
        if (emailObject != null) {
            return UserAndContext.newInstance(emailObject.getUserId(), emailObject.getContextId());
        }

        //Check for S/MIME
        List<SmimeKeys> keys = Services.getService(SmimeKeyService.class).getKeys(email);
        Optional<SmimeKeys> smimeKey = keys.stream().filter(k -> k.isLocal()).findAny();
        if (smimeKey.isPresent()) {
            return UserAndContext.newInstance(smimeKey.get().getUserId(), smimeKey.get().getContextId());
        }

        return null;
    }
    
    /**
     * Internal method to get a list of {@link UserAndContext} representing all known OX Guard users
     *
     * @return A list of {@link UserAndContext} representing all known OX Guard users
     * @throws OXException
     */
    private Set<UserAndContext> getAllUserAndContext() throws OXException {
        final List<Email> allEmails = Services.getService(EmailStorage.class).getEmails();
        final List<SmimeKeys> allKeys = Services.getService(SmimeKeyService.class).getPublicKeys();
        final HashSet<UserAndContext> ret = new HashSet<UserAndContext>(allEmails.size() + allKeys.size());

        for (Email email : allEmails) {
            ret.add(UserAndContext.newInstance(email.getUserId(), email.getContextId()));
        }
        for (SmimeKeys key : allKeys) {
            ret.add(UserAndContext.newInstance(key.getUserId(), key.getContextId()));
        }
        return ret;
    }

    /**
     * Internal method to delete a user's Guard Key by ID
     *
     * @param userId The ID of the user
     * @param contextId The ID of the context
     * @throws OXException
     */
    private void deleteById(int userId, int contextId) throws OXException {
        new UserDeleter().delete(userId, contextId);
    }

    /**
     * Deletes all known OX Guard keys WITHOUT creating any backups
     * (com.openexchange.guard.demo has to be "true")
     * 
     * @throws OXException
     */
    public int deleteAllUserKeys() throws OXException {
        //ensure OX Guard is running in demo mode; or throw an exception
        assertDemoMode();

        final Set<UserAndContext> users = getAllUserAndContext();
        for (UserAndContext user : users) {
            deleteById(user.getUserId(), user.getContextId());
        }
        return users.size();
    }

    /**
     * Deletes all known OX Guard keys for a given email WITHOUT creating any backups
     * (com.openexchange.guard.demo has to be "true")
     *
     * @param email The email of the user to delete all keys for
     * @throws OXException
     */
    public void deleteUserKeys(String email) throws OXException {
        //ensure OX Guard is running in demo mode; or throw an exception
        assertDemoMode();

        UserAndContext user = getUserAndContext(email);
        if (user != null) {
            deleteById(user.getUserId(), user.getContextId());
        } else {
            throw DemoExceptionCodes.UNKNOWN_EMAIL_ERROR.create(email);
        }
    }

    /**
     * Resets the Anti-Abuse entry for a given email
     *
     * @param email The email of the user to reset the Anti-Abuse entry for
     * @throws OXException
     */
    public void resetBad(String email) throws OXException {
        //ensure OX Guard is running in demo mode; or throw an exception
        assertDemoMode();

        UserAndContext user = getUserAndContext(email);
        if (user != null) {
            GuardAntiAbuseService antiAbuseService = Services.getService(GuardAntiAbuseService.class);
            final String username = user.getUserId() + "-" + user.getContextId();
            antiAbuseService.removeBad(username);
        } else {
            throw DemoExceptionCodes.UNKNOWN_EMAIL_ERROR.create(email);
        }
    }

    /**
     * Resets the Anti-Abuse entry for a given user
     *
     * @param contextId The context ID
     * @param userId The user ID
     * @throws OXException
     */
    public void resetBad(int contextId, int userId) throws OXException {
        //ensure OX Guard is running in demo mode; or throw an exception
        assertDemoMode();

        GuardAntiAbuseService antiAbuseService = Services.getService(GuardAntiAbuseService.class);
        final String username = userId + "-" + contextId;
        antiAbuseService.removeBad(username);
    }
}
