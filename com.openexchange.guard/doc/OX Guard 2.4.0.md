# OX Guard

## Overview

OX Guard is a fully integrated security add-on to OX App Suite that provides end users with a flexible email and file encryption solution. OX Guard is a highly scalable, multi server, feature rich solution that is so simple-to-use that end users will actually use it. With a single click a user can take control of their security and send secure emails and share encrypted files. This can be done from any device to both OX App Suite and non-OX App Suite users.

OX Guard uses standard PGP encryption for the encryption of email and files. PGP has been around for a long time, yet has not really caught on with the masses. This is generally blamed on the confusion and complications of managing the keys, understanding trust, PGP format types, and lack of trusted central key repositories. Guard simplifies all of this, making PGP encryption as easy as a one click process, with no keys to keep track of, yet the options of advanced PGP management for those that know how.

This article will guide you through the installation of Guard and describes the basic configuration and software requirements. As it is intended as a quick walk-through it assumes an existing installation of the operating system including a single server App Suite setup as well as average system administration skills. This guide will also show you how to setup a basic installation with none of the typically used distributed environment settings. The objective of this guide is:

* To setup a single server installation
* To setup a single Guard instance on an existing Open-Xchange installation, no cluster
* To use the database service on the existing Open-Xchange installation for Guard, no replication
* To provide a basic configuration setup, no mail server configuration

### Key Features

* Simple security at the touch of a button
* Provides user based security - Separate from provider
* Supplementary security to Provider based security - Layered
* Powerful features yet simple to use and understand
* Security - Inside and outside of the OX environment
* Email and Drive integration
* Uses proven PGP security

### Availability

If an OX App Suite customer would like to evaluate OX Guard integration, the first step is to contact OX Sales. OX Sales will then work on the request and send prices and license/API (for the hosted infrastructure) key details to the customer.

### Requirements

Please review [OX Guard Requirements](https://oxpedia.org/wiki/index.php?title=AppSuite:OX_System_Requirements#OX_Guard) for a full list of requirements.

Since OX Guard is a Microservice it can either be added to an existing Open-Xchange installation or it can be deployed on a dedicated environment. OX App Suite v7.8.1 or later is required to operate this extension both in a single or multi server environments.

#### Prerequisites

* Open-Xchange REST API
* Grizzly HTTP connector (open-xchange-grizzly)
* A supported Java Virtual Machine (Java 7)
* An Open-Xchange App Suite installation v7.8.1 or later
* Please Note: To get access to the latest minor features and bug fixes, you need to have a valid license. The article [Updating OX-Packages](https://oxpedia.org/wiki/index.php?title=AppSuite:UpdatingOXPackages) explains how that can be done.

### Important Notes

#### Customisation

OX Guard version supports branding / theming using the configuration cascade, defining a templateID for a user or context. Check the [OX Guard Customisation](https://oxpedia.org/wiki/index.php?title=AppSuite:GuardCustomization) article for more details.

#### Mail Resolver

READ THIS VERY CAREFULLY; BEFORE PROCEEDING WITH GUARD INSTALLATION!

The Guard installation must be able to determine if an email recipient is a local OX user or if it should be a guest account. The default MailResolver uses the context domain name to do this. On many installations, domains may extend across multiple context and multiple database shards. In these cases, the default MailResolver won't work. In addition, if a custom authentication package is used, the Mail Resolver will likely not work.

Be sure to test the mail resolver using:

~~~bash
/opt/open-xchange/sbin/guard test email@domain
~~~

to see if the mail Resolver works.

If the test does not work, you will likely need a custom Mail Resolver. Please see [Mail Resolver](https://oxpedia.org/wiki/index.php?title=AppSuite:GuardMailResolver) page

This resolver software *depends heavily on your local deployment*.

## Download and Installation

### General

The installation of the `open-xchange-rest` package which is required for Guard and the main `open-xchange-guard` package in version 2.4.0 will eventually execute database update tasks if installed and activated. Please take this into account.

There are several components to the Guard service. They can be all installed on the same server as the OX middleware or on a separate server.

The components required for the OX middleware are: `open-xchange-rest`, `open-xchange-guard-backend-plugin`

The components required for the OX frontend are: `open-xchange-guard-ui` `open-xchange-guard-ui-static` and optionally `open-xchange-guard-help-en-us` (or preferred language for help files)

The components required for the Guard server `open-xchange-guard` and either `open-xchange-guard-file-storage` or `open-xchange-guard-s3-storage` depending on what storage you want to use. The examples below make use of the `open-xchange-guard-file-storage`. Adjust the commands accordingly to fit your needs. In addition `open-xchange` and `open-xchange-core` are required to run OX Guard.

### Debian Linux 7.0 (Wheezy)

If not already done, add the following repositories to your Open-Xchange apt configuration:

~~~bash
deb https://software.open-xchange.com/products/guard/stable/guard/DebianWheezy /
deb https://software.open-xchange.com/products/appsuite/stable/backend/DebianWheezy /
~~~

and then run for a single node installation:

~~~bash
$ apt-get update
$ apt-get install open-xchange-rest open-xchange-guard open-xchange-guard-file-storage open-xchange-guard-ui open-xchange-guard-ui-static open-xchange-guard-backend-plugin
~~~

or the following for a distributed installation:

~~~bash
$ apt-get update
$ apt-get install open-xchange-guard open-xchange-guard-file-storage open-xchange-guard-ui open-xchange-guard-ui-static
~~~

The packages `open-xchange-rest` and `open-xchange-guard-backend-plugin` missing in the distributed installation have to be installed on the node running the middleware.

### Debian Linux 8.0 (Jessie)

If not already done, add the following repositories to your Open-Xchange apt configuration:

~~~bash
deb https://software.open-xchange.com/products/guard/stable/guard/DebianJessie /
deb https://software.open-xchange.com/products/appsuite/stable/backend/DebianJessie /
~~~

and then run for a single node installation:

~~~bash
$ apt-get update
$ apt-get install open-xchange-rest open-xchange-guard open-xchange-guard-file-storage open-xchange-guard-ui open-xchange-guard-ui-static open-xchange-guard-backend-plugin
~~~

or the following for a distributed installation:

~~~bash
$ apt-get update
$ apt-get install open-xchange-guard open-xchange-guard-file-storage open-xchange-guard-ui open-xchange-guard-ui-static
~~~

The packages `open-xchange-rest` and `open-xchange-guard-backend-plugin` missing in the distributed installation have to be installed on the node running the middleware.

### RedHat Enterprise Linux 6 or CentOS 6

If not already done, add the following repositories to your Open-Xchange yum configuration:

~~~bash
[open-xchange-guard-stable-guard]
name=Open-Xchange-guard-stable-guard
baseurl=https://software.open-xchange.com/products/guard/stable/guard/RHEL6/
gpgkey=https://software.open-xchange.com/oxbuildkey.pub
enabled=1
gpgcheck=1
metadata_expire=0m

[ox-backend]
name=Open-Xchange-backend
baseurl=https://software.open-xchange.com/products/appsuite/stable/backend/RHEL6/
gpgkey=https://software.open-xchange.com/oxbuildkey.pub
enabled=1
gpgcheck=1
metadata_expire=0m
~~~

and then run for a single node installation:

~~~bash
$ yum update
$ yum install open-xchange-rest open-xchange-guard open-xchange-guard-file-storage open-xchange-guard-ui open-xchange-guard-ui-static open-xchange-guard-backend-plugin
~~~

or the following for a distributed installation:

~~~bash
$ yum update
$ yum install open-xchange-guard open-xchange-guard-file-storage open-xchange-guard-ui open-xchange-guard-ui-static 
~~~

The packages `open-xchange-rest` and `open-xchange-guard-backend-plugin` missing in the distributed installation have to be installed on the node running the middleware.

### Redhat Enterprise Linux 7 or CentOS 7

If not already done, add the following repositories to your Open-Xchange yum configuration:

~~~bash
[open-xchange-guard-stable-guard]
name=Open-Xchange-guard-stable-guard
baseurl=https://software.open-xchange.com/products/guard/stable/guard/RHEL7/
gpgkey=https://software.open-xchange.com/oxbuildkey.pub
enabled=1
gpgcheck=1
metadata_expire=0m

[ox-backend]
name=Open-Xchange-backend
baseurl=https://software.open-xchange.com/products/appsuite/stable/backend/RHEL7/
gpgkey=https://software.open-xchange.com/oxbuildkey.pub
enabled=1
gpgcheck=1
metadata_expire=0m
~~~

and then run for a single node installation:

~~~bash
$ yum update
$ yum install open-xchange-rest open-xchange-guard open-xchange-guard-file-storage open-xchange-guard-ui open-xchange-guard-ui-static open-xchange-guard-backend-plugin
~~~

or the following for a distributed installation:

~~~bash
$ yum update
$ yum install open-xchange-guard open-xchange-guard-file-storage open-xchange-guard-ui open-xchange-guard-ui-static 
~~~

The packages `open-xchange-rest` and `open-xchange-guard-backend-plugin` missing in the distributed installation have to be installed on the node running the middleware.

### SUSE Linux Enterprise Server 11

Add the package repository using zypper if not already present:

~~~bash
$ zypper ar https://software.open-xchange.com/products/guard/stable/guard/SLES11 guard-stable-guard
$ zypper ar https://software.open-xchange.com/products/appsuite/stable/backend/SLES11 ox-backend
~~~

and then run for a single node installation:

~~~bash
$ zypper ref
$ zypper in open-xchange-rest open-xchange-guard open-xchange-guard-file-storage open-xchange-guard-ui open-xchange-guard-ui-static open-xchange-guard-backend-plugin
~~~

or the following for a distributed installation:

~~~bash
$ zypper ref
$ zypper in open-xchange-guard open-xchange-guard-file-storage open-xchange-guard-ui open-xchange-guard-ui-static 
~~~

The packages `open-xchange-rest` and `open-xchange-guard-backend-plugin` missing in the distributed installation have to be installed on the node running the middleware.

Guard requires Java 1.7, which will be installed through the Guard packages, still SUSE Linux Enterprise Server 11 will not use Java 1.7 by default. Therefore we have to set Java 1.7 as the default instead of Java 1.6:

~~~bash
$ update-alternatives --config java
~~~

Now select the Java 1.7 JRE, example:

~~~bash
There are 2 alternatives which provide 'java'.

  Selection    Alternative
-----------------------------------------------
*         1    /usr/lib64/jvm/jre-1.6.0-ibm/bin/java
 +        2    /usr/lib64/jvm/jre-1.7.0-ibm/bin/java

Press enter to keep the default[*], or type selection number: 2
Using '/usr/lib64/jvm/jre-1.7.0-ibm/bin/java' to provide 'java'.
~~~

### SUSE Linux Enterprise Server 12

Add the package repository using zypper if not already present:

~~~bash
$ zypper ar https://software.open-xchange.com/products/guard/stable/guard/SLE_12 guard-stable-guard
$ zypper ar https://software.open-xchange.com/products/appsuite/stable/backend/SLE_12 ox-backend
~~~

and then run for a single node installation:

~~~bash
$ zypper ref
$ zypper in open-xchange-rest open-xchange-guard open-xchange-guard-file-storage open-xchange-guard-ui open-xchange-guard-ui-static
~~~

or the following for a distributed installation:

~~~bash
$ zypper ref
$ zypper in open-xchange-guard open-xchange-guard-file-storage open-xchange-guard-ui open-xchange-guard-ui-static 
~~~

The packages `open-xchange-rest` and `open-xchange-guard-backend-plugin` missing in the distributed installation have to be installed on the node running the middleware.

Guard requires Java 1.7, which will be installed through the Guard packages, still SUSE Linux Enterprise Server 11 will not use Java 1.7 by default. Therefor we have to set Java 1.7 as the default instead of Java 1.6:

~~~bash
$ update-alternatives --config java
~~~

Now select the Java 1.7 JRE, example:

~~~bash
There are 2 alternatives which provide 'java'.

  Selection    Alternative
-----------------------------------------------
*         1    /usr/lib64/jvm/jre-1.6.0-ibm/bin/java
 +        2    /usr/lib64/jvm/jre-1.7.0-ibm/bin/java

Press enter to keep the default[*], or type selection number: 2
Using '/usr/lib64/jvm/jre-1.7.0-ibm/bin/java' to provide 'java'.
~~~

### Univention Corporate Server

If you have purchased the OX App Suite for UCS, the OX Guard is part of the offering. OX Guard is available in the Univention App Center. Please check the UMC module App Center for the installation of the OX Guard at your already available environment.

Please note: By default, OX Guard generates the link to the secure content for external recipients on the basis of the local fully qualified domain name (FQDN). If the local FQDN is not reachable from the Internet, it has to be specified manually. This can be done by setting a UCR variable, e.g. via the UMC module "Univention Configuration Registry". The variable has to contain the external FQDN of the OX Guard system:

~~~bash
oxguard/cfg/guard.properties/com.openexchange.guard.externalEmailURL=HOSTNAME.DOMAINNAME
~~~

## Update OX Guard

This section contains information about updating a 2.4.0 version (e.g. for patch fixes). Upgrading from prior versions is discussed in different articles. You can find more information in the **Update OX Guard Versions <= 2.2.x** and **Update OX Guard Versions <= 2.0.x** sections below.

### Debian Linux 7.0 (Wheezy)

If not already done, add the following repositories to your Open-Xchange apt configuration:

~~~bash
deb https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/guard/stable/guard/updates/DebianWheezy /
deb https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/appsuite/stable/backend/DebianWheezy /
~~~

Then run:

~~~bash
$ apt-get update
$ apt-get dist-upgrade
~~~

If you want to see, what apt-get is going to do without actually doing it, you can run:

~~~bash
$ apt-get dist-upgrade -s
~~~

### Debian Linux 8.0 (Jessie)

If not already done, add the following repositories to your Open-Xchange apt configuration:

~~~bash
deb https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/guard/stable/guard/updates/DebianJessie /
deb https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/appsuite/stable/backend/DebianJessie /
~~~

Then run:

~~~bash
$ apt-get update
$ apt-get dist-upgrade
~~~

If you want to see, what apt-get is going to do without actually doing it, you can run:

~~~bash
$ apt-get dist-upgrade -s
~~~

### Redhat Enterprise Linux 6 or CentOS 6

If not already done, add the following repositories to your Open-Xchange yum configuration:

~~~bash
[open-xchange-guard-stable-guard-updates]
name=Open-Xchange-guard-stable-guard-updates
baseurl=https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/guard/stable/guard/updates/RHEL6/
gpgkey=https://software.open-xchange.com/oxbuildkey.pub
enabled=1
gpgcheck=1
metadata_expire=0m

[ox-backend]
name=Open-Xchange-backend
baseurl=https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/appsuite/stable/backend/updates/RHEL6/
gpgkey=https://software.open-xchange.com/oxbuildkey.pub
enabled=1
gpgcheck=1
metadata_expire=0m
~~~

and then run:

~~~bash
$ yum update
$ yum upgrade
~~~

### Redhat Enterprise Linux 7 or CentOS 7

If not already done, add the following repositories to your Open-Xchange yum configuration:

~~~bash
[open-xchange-guard-stable-guard-updates]
name=Open-Xchange-guard-stable-guard-updates
baseurl=https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/guard/stable/guard/updates/RHEL7/
gpgkey=https://software.open-xchange.com/oxbuildkey.pub
enabled=1
gpgcheck=1
metadata_expire=0m

[ox-backend]
name=Open-Xchange-backend
baseurl=https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/appsuite/stable/backend/updates/RHEL7/
gpgkey=https://software.open-xchange.com/oxbuildkey.pub
enabled=1
gpgcheck=1
metadata_expire=0m
~~~

and then run:

~~~bash
$ yum update
$ yum upgrade
~~~

### SUSE Linux Enterprise Server 11

Add the package repository using `zypper` if not already present:

~~~bash
$ zypper ar https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/guard/stable/guard/updates/SLES11 guard-stable-guard-updates
$ zypper ar https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/appsuite/stable/backend/updates/SLES11 ox-backend
~~~

and then run:

~~~bash
$ zypper dup -r guard-stable-guard-backend-updates
$ zypper dup -r guard-stable-guard-ui-updates
~~~

You might need to run:

~~~bash
$ zypper ref
~~~

to update the repository metadata before running `zypper` up.

### SUSE Linux Enterprise Server 12

Add the package repository using `zypper` if not already present:

~~~bash
$ zypper ar https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/guard/stable/guard/updates/SLE_12 guard-stable-guard-updates
$ zypper ar https://LDBUSER:LDBPASSWORD@software.open-xchange.com/products/appsuite/stable/backend/updates/SLE_12 ox-backend
~~~

and then run:

~~~bash
$ zypper dup -r guard-stable-guard-backend-updates
$ zypper dup -r guard-stable-guard-ui-updates
~~~

You might need to run:

~~~bash
$ zypper ref
~~~

to update the repository metadata before running `zypper` up.

### Univention Corporate Server

If you have purchased the OX App Suite for UCS, the OX Guard is part of the offering. OX Guard is available in the Univention App Center. Please check the UMC module App Center for the update of the OX Guard.

### Update OX Guard Versions <= 2.2.x

If you are upgrading from a 2.2 version to 2.4, please read the [OX Guard 2.2.x to 2.4.0 Migration](ttps://oxpedia.org/wiki/index.php?title=AppSuite:something) article.

### Update OX Guard Versions <= 2.0.x

If you are upgrading from older versions of OX Guard (e.g. from 1.2 to 2.0.0), please read [this](https://oxpedia.org/wiki/index.php?title=AppSuite:OX_Guard#Update_OX_Guard_v1.2_to_OX_Guard_v2.0.0) first.

## Configuration

The following gives an overview of the most important settings to enable Guard for users on the Open-Xchange installation. Some of those settings have to be modified in order to establish the database and REST API access from the Guard service. All settings relating to the Guard backend component are located in the configuration file `guard-core.properties` located in `/opt/open-xchange/etc`. The default configuration should be sufficient for a basic "up-and-running" setup (with the exception of defining the database username and password). Please refer to the inline documentation of the configuration file for more advanced options. Additional information can be found in the [Guard Configuration](https://oxpedia.org/wiki/index.php?title=AppSuite:GuardConfiguration) article.

### Basic Configuration

~~~bash
$ vim /opt/open-xchange/etc/guard-core.properties
~~~

Guard database for storing Guard user information, main lookup tables:

~~~bash
com.openexchange.guard.oxguardDatabaseHostname=localhost
~~~

Guard database that stores keys for guest users. May be the same as above. New guest shards will be created on this database as needed. If not supplied, will use the `oxguardDatabaseHostname`:

~~~bash
com.openexchange.guard.oxguardShardDatabase=localhost
~~~

Username and Password for the databases above:

~~~bash
com.openexchange.guard.databaseUsername=openexchange
com.openexchange.guard.databasePassword=db_password
~~~

Open-Xchange REST API host:

~~~bash
com.openexchange.guard.restApiHostname=localhost
~~~

Open-Xchange REST API username and password (need to be defined in the OX backend in the "Configure services" below):

~~~bash
com.openexchange.guard.restApiUsername=apiusername
com.openexchange.guard.restApiPassword=apipassword
~~~

External URL for this Open-Xchange installation. This setting will be used to generate the link to the secure content for external recipients:

~~~bash
com.openexchange.guard.externalEmailURL=URL_TO_OX
~~~

### Middleware Configuration on OX Guard node

If you are installing OX Guard on a node that until yet did not host an Open-Xchange middleware you have to additionally configure some parts of the following properties files:

* `configdb.properties`: information about the existing configuration database.
* `server.properties`: information about the connections have to be set.
* `system.properties`: at least `SERVER_NAME` should be set. 

### Sevices Configuration

#### Apache

Configure the `mod_proxy_http` module by adding the Guard API.

##### Redhat Enterprise Linux 6 or CentOS 6

~~~bash
$ vim /etc/httpd/conf.d/proxy_http.conf
~~~

##### Debian GNU/Linux 7.0 and SUSE Linux Enterprise Server 11

~~~bash
$ vim /etc/apache2/conf.d/proxy_http.conf
~~~

~~~bash
<Proxy balancer://oxguard>
       Order deny,allow
       Allow from all

       BalancerMember http://localhost:8009/oxguard timeout=1800 smax=0 ttl=60 retry=60 loadfactor=100 route=OX1
       ProxySet stickysession=JSESSIONID|jsessionid scolonpathdelim=ON
       SetEnv proxy-initial-not-pooled
       SetEnv proxy-sendchunked
</Proxy>

ProxyPass /appsuite/api/oxguard balancer://oxguard
~~~

**Please Note**: The Guard API settings must be inserted ***before*** the existing `<Proxy /appsuite/api>` parameter.

After the configuration is done, restart the Apache webserver

~~~bash
$ apachectl restart
~~~

#### Open-Xchange

Edit the `guard-api.properties` configuration file for the OX backend which contain some general Guard settings. Please remove comments in front of the following settings to the configuration file `guard-api.properties` on the Open-Xchange backend servers:

~~~bash
$ vim /opt/open-xchange/etc/guard-api.properties
~~~
~~~bash
# OX GUard general permission, required to activate Guard in the AppSuite UI.
com.openexchange.capability.guard=true

# Default theme template id for all users that have no custom template id configured.
com.openexchange.guard.templateID=0
~~~

Configure the API username and password that you assigned to Guard in the `server.properties` file:

~~~bash
$ vim /opt/open-xchange/etc/server.properties
~~~
~~~bash
# Specify the user name used for HTTP basic auth by internal REST servlet
com.openexchange.rest.services.basic-auth.login=apiusername

# Specify the password used for HTTP basic auth by internal REST servlet
com.openexchange.rest.services.basic-auth.password=apipassword
~~~

Finally, the OX backend needs to know where the Guard server is located. This is used to notify the Guard server of changes in users, and to send emails marked for signature. The URL for the Guard server should include the URL suffix `/guardadmin`. In the event of a cluster setup, any Guard server can be referenced here, as it is not session specific, though ideally would have a HTTP load balancer/failover URL:

~~~bash
$ vim /opt/open-xchange/etc/guard-api.properties
~~~
~~~bash
# Specifies the URI to the OX Guard end-point; e.g. http://guard.host.invalid:8081/guardadmin
# Default is empty
com.openexchange.guard.endpoint=http://guardserver:8009/guardadmin
~~~

Restart the OX backend

~~~bash
$ /etc/init.d/open-xchange restart
~~~

#### SELinux

Running SELinux prohibits your local Open-Xchange backend service to connect to localhost:8009, which is where the Guard backend service listens to. In order to allow localhost connections to 8009 execute the following:

~~~bash
$ setsebool -P httpd_can_network_connect 1
~~~

### Generating the `oxguardpass`

Once the Guard configuration (database and backend configuration) and the service configuration has been applied, the Guard administration script needs to be executed in order to create the master password file in `/opt/open-xchange/etc/oxguardpass`. The initiation only needs to be done **once** for a multi server setup, for details please see the sections **Optional** and/or **Clustering**.

**Please Note**: If you run a cluster of OX / Guard nodes, only execute this command on **ONE** node. Not on all nodes! See [OX Guard Clustering](https://oxpedia.org/wiki/index.php?title=AppSuite:GuardCluster) for details.

~~~bash
$ /opt/open-xchange/sbin/guard --init
~~~

**Please Note**: It is important to understand that the master password file located at `/opt/open-xchange/etc/oxguardpass` is required to reset user passwords; without them the administrator will not be able to reset user passwords anymore in the future. The file contains the passwords used to encrypt the master database key, as well as passwords used to encrypt protected data in the users table. It must be the same on all Guard servers.

### Test Setup

Not required, but it is a good idea to test the Guard setup before starting the initialization. The test function will verify that Guard has a good connection to the OX backend, and that it can resolve email addresses to users.

To test, use an email address that exists on the OX backend (john@example.com for this example)

~~~bash
/opt/open-xchange/sbin/guard --test john@example.com
~~~

Guard should return information from the OX backend regarding the user associated with "john@example.com". Problems resolving information for the user should be resolved before using Guard. Check Rest API passwords and settings if errors returned.

### Enabling Guard for Users

Guard provides two capabilities for users in the environment as well as a basic "core" level:

* Guard: `com.openexchange.capability.guard`
* Guard Mail: `com.openexchange.capability.guard-mail`
* Guard Drive: `com.openexchange.capability.guard-drive`

The "core" Guard enabled a basic read functionality for Guard encrypted emails. We recommend enabling this for all users, as this allows all recipients to read Guard emails sent to them. Great opportunity for upsell. Recipients with only Guard enabled can then do a secure reply to the sender, but they can't start a new email or add recipients.

**Guard Mail** and **Guard Drive** are additional options for users. "Guard Mail" allows users the full functionality of Guard emails. "Guard Drive" allows for encryption and decryption of drive files.

Each of those two Guard components is enabled for all users that have the according capability configured. Please note that users need to have the Drive permission set to use Guard Drive. So the users that have Guard Drive enabled must be a subset of those users with OX Drive permission. Since v7.6.0 we enforce this via the default configuration. Those capabilities can be activated for specific user by using the Open-Xchange provisioning scripts:

#### Guard Mail:

~~~bash
$ /opt/open-xchange/sbin/changeuser -c 1 -A oxadmin -P admin_password -u testuser --config/com.openexchange.capability.guard-mail=true
~~~

#### Guard Drive:

~~~bash
$ /opt/open-xchange/sbin/changeuser -c 1 -A oxadmin -P admin_password -u testuser --config/com.openexchange.capability.guard-drive=true
~~~

**Please Note**: Guard Drive requires Guard Mail to be configured for the user as well. In addition, these capabilities may be configured globally by editing the `guard-api.properties` file on the OX bakend.

### Recipient key detection

#### Local
Guard needs to determine if an email recipients email address is an internal or external (non-ox) user.

To detect if the recipient is an account on the same OX Guard system there is a mechanism needed to map a recipient mail address to the correct local OX context. The default implementation delivered in the product achieves that by looking up the mail domain (@example.com) within the list of context mappings. That is at least not possible in case of ISPs where different users/contexts use the same mail domain. In case your OX system does not use mail domains in context mappings it is required to deploy an OX OSGi bundle implementing the `com.openexchange.mailmapping.MailResolver` class or by interfacing Guard with your mail resolver system. Please see [OX Guard Mail Resolver](https://oxpedia.org/wiki/index.php?title=AppSuite:GuardMailResolver) for details.

#### External

Starting with Guard 2.0, Guard will use public PGP Key servers if configured to find PGP Public keys. In addition, Guard will also look up SRV records for PGP Key servers for a recipients domain. This follows the standards [OpenPGP Draft](http://tools.ietf.org/html/draft-shaw-openpgp-hkp-00#page-9).

External PGP servers to use can be configured in the guard.properties file on the Guard servers.

~~~bash
com.openexchange.guard.publicPGPDirectory = hkp://keys.gnupg.net:11371, hkp://pgp.mit.edu:11371
~~~

If you would like this Guard installation discoverable by other Guard servers, then create an SRV record for each domain ("example.com" in this illustration):

~~~bash
_hkp._tcp.open-xchange.com. 28800 IN    SRV     10 1 80 appsuite.example.com.
~~~

You will also need to make additional entries in the apache `proxy_http.conf` file.

~~~bash
<Proxy balancer://oxguardpgp>
       Order deny,allow
       Allow from all, add
       BalancerMember http://guardserver:8009/pgp timeout=1800 smax=0 ttl=60 retry=60 loadfactor=50 route=OX3
       ProxySet stickysession=JSESSIONID|jsessionid scolonpathdelim=ON
       SetEnv proxy-initial-not-pooled
       SetEnv proxy-sendchunked
  </Proxy>
<Proxy /pks>
       ProxyPass balancer://oxguardpgp
</Proxy>
~~~

**Please Note** PGP Public key servers by default append use the URL server/pks when the record is obtained from an SRV record. The proxy above routes anything with the Apache domain/pks to the OX Guard PGP server.

### Clustering

You can run multiple OX Guard servers in your environment to ensure high availability or enhance scalability. OX Guard integrates seamlessly into the existing Open-Xchange infrastructure by using the existing interface standards and is therefor transparent to the environment. A couple of things have to be prepared in order to loosely couple OX Guard servers with Open-Xchange servers in a cluster.

#### MySQL

The MySQL servers need to be configured in order to allow access to the configdb of Open-Xchange. To do so you need to set the following configuration in the MySQL `my.cnf`:

~~~bash
bind = 0.0.0.0
~~~

This allows the Guard backend to bind to the MySQL host which is configured in the `guard-core.properties` file with `com.openexchange.guard.configdbHostname`. After the bind for the MySQL instance is configured and the OX Guard backend would be able to connect to the configured host, you have to grant access for the OX Guard service on the MySQL instance to manage the databases. Do so by connecting to the MySQL server via the MySQL client. Authenticate if necessary and execute the following, please note that you have to modify the hostname / IP address of the client who should be able to connect to this database, it should include all possible OX Guard servers:

~~~sql
GRANT ALL PRIVILEGES ON *.* TO 'openexchange'@'oxguard.example.com' IDENTIFIED BY ‘secret’;
~~~

#### Apache

OX Guard uses the Open-Xchange REST API to store and fetch data from the Open-Xchange databases. The REST API is a servlet running in the Grizzly container. By default it is not exposed as a servlet through Apache and is only accessibly via port 8009. In order to use Apache's load balancing via `mod_proxy` we need to add a servlet called "preliminary" to `proxy_http.conf`, example based on a clustered `mod_proxy `configuration:

~~~bash
<Location /preliminary>
     Order Deny,Allow
     Deny from all
     # Only allow access from Guard servers within the network. Do not expose this
     # location outside of your network. In case you use a load balancing service in front
     # of your Apache infrastructure you should make sure that access to /preliminary will
     # be blocked from the Internet / outside clients. Examples:
     # Allow from 192.168.0.1
     # Allow from 192.168.1.1 192.168.1.2
     # Allow from 192.168.0.
     ProxyPass /preliminary balancer://oxcluster/preliminary
</Location>
~~~

Make sure that the balancer is properly configured in the `mod_proxy` configuration. Examples on how to do so can be found in our clustering configuration for Open-Xchange AppSuite. Like explained in the example above, please make sure that this location is only available in your internal network, there is no need to expose `/preliminary` to the public, it is only used by Guard servers to connect to the OX backend. If you have a load balancer in front of the Apache cluster you should consider blocking access to `/preliminary` from WAN to restrict access to the servlet to internal network services only.

Now add the OX Guard `BalancerMembers` to the oxguard balancer configuration (also in `proxy_http.conf`) to address all your OX Guard nodes in the cluster in this balancer configuration. The configuration has to be applied to all Apache nodes within the cluster.

If the Apache server is a dedicated server `/` instance you also have to install the OX Guard UI-Static package on all Apache nodes in the cluster in order to provide static files like images or CSS to the OX Guard client. Example for Debian (the OX Guard repository has to be configured in the package management prior):

~~~bash
$ apt-get install open-xchange-guard-ui-static
~~~

#### Open-Xchange

Disable the Open-Xchange IPCheck for session verification. This is required because OX Guard will use the users session cookie to connect to the Open-Xchange REST API, but as a different IP address than the OX Guard server has been used during authentication the request would fail if you don't disable the IPCheck:

~~~bash
$ vim /opt/open-xchange/etc/server.properties
~~~

and set:

~~~bash
com.openexchange.IPCheck=false
~~~

The OX Guard UI package has to be installed on all Open-Xchange backend nodes as well, example for Debian (the OX Guard repository has to be configured in the package management prior):

~~~bash
$ apt-get install open-xchange-guard-ui
~~~

Restart the Open-Xchange service afterwards.

#### OX Guard

For details in clustering Guard servers, please see [OX Guard Clustering](https://oxpedia.org/wiki/index.php?title=AppSuite:GuardCluster). It is **critical** that all Guard servers have the same `oxguardpass` file. Please see the clustering link for details. Do not run `/opt/open-xchange/sbin/guard --init` on more than one server.

After all the services like MySQL, Apache and Open-Xchange have been configured you need to update the OX Guard backend configuration to point to the correct API endpoints. Set the REST API endpoint to an Apache server by setting the following value in `/opt/open-xchange/etc/guard-core.properties`:

~~~bash
com.openexchange.guard.restApiHostname=apache.example.com
~~~

Per default Guard will try to connect to port 8009 to this host, but as we configured the REST API to be proxies thorugh the servlet `/preliminary` on every Apache we now also need to change the target port for the REST API. You can do so by adding the following line into `/opt/open-xchange/etc/guard-core.properties`:

~~~bash
com.openexchange.guard.oxBackendPort=80
~~~

Please also change all settings in regards to MySQL like `com.openexchange.guard.configdbHostname`, `com.openexchange.guard.oxguardDatabaseHostname`, `com.openexchange.guard.databaseUsername` or `om.openexchange.guard.databasePassword`.

Afterwards restart the OX Guard service and check the log file if the OX Guard backend is able to connect to the configured REST API.

### Multi Node

If you have multiple OX and Guard installations, please see the following documentation [OX Guard Modular Setup](https://oxpedia.org/wiki/index.php?title=AppSuite:OX_Guard_Modular).

## Support API

The OX Guard Support API enables administrative access to various functions for maintaining OX Guard from a client in a role as a support employee. A client has to do a BASIC AUTH authentication in order to access the API. Username and password can be configured in the guard.properties file using the following settings:

~~~bash
# Specify the username and password for accessing the Support API of Guard
com.openexchange.guard.supportapiusername=
com.openexchange.guard.supportapipassword=
~~~

In contrast to the rest of the OX Guard requests, the OX Guard support API requests are accessible using: /guardsupport. This distinction allows more flexible configuration since the support API should not always be accessible from everywhere. But if you want to expose the Guard Support API using Apache a very basic Apache configuration could look like this:

**Warning**: Exposing the support API to the internet could be huge security risk. Only add to Apache if you know what you are doing:

~~~bash
<Location /appsuite/api/guardsupport>                                                               
    ProxyPass http://localhost:8009/guardsupport                                                    
    #...
</Location>
~~~

It might also be preferable to add a new balancer directive for `guardsupport`

~~~bash
<Location /appsuite/api/guardsupport>                                                               
    ProxyPass balancer://oxguardsupport                                                           
</Location>
<Proxy balancer://oxguardsupport>                                                                
    #...
    BalancerMember http://localhost:8009/guardsupport #...
    #...
</Proxy>
~~~

### Reset password

`POST /guardsupport/?action=reset_password`

Performs a password reset and sends a new random generated password to a specified email address by the user or a default address if the user did not specify an email address. The reset password function is currently not available for guest users. (*Since Guard 2.0*)

Parameters:

* `email` – The email address of the user to reset the password for
* `default` (optional) – The email address to send the new password to, if the user did not specify a secondary email address


### Expose key

`POST /guardsupport/?action=expose_key`

Marks a deleted user key temporary as “exposed” and creates a unique URL for downloading the exposed key. Automatic resetting of exposed keys to "not exposed" is scheduled once a day and resets all exposed keys which have been exposed before X hours, where X can be configured using com.openexchange.guard.exposedKeyDurationInHours in the guard.properties files. (*Since Guard 2.0*)

Parameters:

* `email` – The email address of the user to expose the deleted keys for
* `cid` – The context id

Response: A URL pointing to the downloadable exposed keys.

### Delete user

`POST /guardsupport/?action=delete_user`

Deletes all keys related to a certain user. The keys are backed up and can be exposed using the “expose_key” call. (*Since Guard 2.0*)

Parameters:

* `user_id` – The user's id
* `cid` - The context id

## Customisation

Guard's templates are customisable at the user and context level. Please see [Customisation](https://oxpedia.org/wiki/index.php?title=AppSuite:GuardCustomization) for details.

## Entropy

Guard requires entropy (randomness) to generate the private/public keys that are used. Depending on the server and it's environment, this may become a problem. Please see [Entropy](https://oxpedia.org/wiki/index.php?title=AppSuite:GuardEntropy) for a possible solution.
