# OX Guard 2.4.0 Configuration

There are two main files for configuring OX Guard: `guard-api.properties` and `guard-core.properties`. The first configuration file is part of the OX backend and contains properties, among others, that enable the OX Guard functionality for various modules such as Mail and Drive as well as some capabilities. The second configuration file is part of the OX Guard and contains properties that configures the behaviour of the product.

## Configuration File (`guard-api.properties`)

### Main Properties

~~~bash
com.openexchange.capability.guard = true
~~~
Enables Guard. If not set, no guard functions will be loaded in the UI. Needed if users should be able to do ANY Guard functions including reading encrypted emails. This level will allow users without "guard-mail" enabled to read emails sent to them, reply to those emails, but not create new emails. Recommended minimum level for all users.

~~~bash
com.openexchange.capability.guard-mail = true
~~~
Enables the user(s) ability to send encrypted emails. If False but guard enabled, they can read encrypted emails and reply to the original sender, but they cannot compose new emails

~~~bash
com.openexchange.capability.guard-drive = true
~~~
Enables the drive functionality. If false, user(s) will not be able to decode nor upload new encrypted files

### Optional Properties

~~~bash
com.openexchange.guard.templateID = 0
~~~
Define template customization ID for the Guest reader emails, the Guest reader, and system emails. See [Customization](https://oxpedia.org/wiki/index.php?title = AppSuite:GuardCustomization) for details.

~~~bash
com.openexchange.guard.endpoint =
~~~
Specifies the URI to the OX Guard end-point; e.g. http://guard.host.invalid:8081/some/path. By default is empty.

#### Capabilities

~~~bash
com.openexchange.capability.guard-pin = true
~~~
Enables optional PIN function when sending emails to non-ox users. Will provide an additional 4 digit pin that should be sent to the recipient. Extra protection during the time that the temporary password was assigned and sent.

~~~bash
com.openexchange.capability.guard-nodeleterecovery = true
~~~
(Guard 2.0) Disable the ability of the user to delete the recovery keys. Makes it impossible to reset password, but also adds level of protection/security

~~~bash
com.openexchange.capability.guard-nodeleteprivate = true
~~~
(Guard 2.0) Disable the ability of the user to delete their private key. They can revoke it, but not delete the key.

~~~bash
com.openexchange.capability.guard-nodeleteonrevoke = true
~~~
(Depreciated as of Guard 2.0)
Default when revoking an item is to delete the content key, making the item impossible to decode. If this option is true, then the item is merely expired and can later be retrieved for decoding in case of legal requirements, corporate requirements, etc

~~~bash
com.openexchange.capability.guard-noextra = true
~~~
(Depreciated as of Guard 2.0)
Disables the ability to add an extra password to encrypted items. May be required by some industry

## Configuration File (`guard-core.properties`)

### Database
~~~bash
com.openexchange.guard.oxguardDatabaseHostname = localhost
~~~
The address of the MySQL database for OX Guard data. May be the same as the OX MySQL database.

~~~bash
com.openexchange.guard.oxguardDatabaseRead
~~~
Optional read-only IP/name for the OX Guard database that might be used in Master-Slave setups.

~~~bash
com.openexchange.guard.oxguardShardDatabase
~~~
IP/Name for the location of the Guest database shards. Additional shards will be created on this database

~~~bash
com.openexchange.guard.oxguardShardRead
~~~
Optional read-only IP/name for Guest database shards that might be used in Master-Slave setups.

~~~bash
com.openexchange.guard.databaseUsername = username
~~~
The username to access the OX Backend and Guard database. This user needs to have select, create, lock, insert, update privileges. Guard database user also should have alter (for updates), drop, index.

~~~bash
com.openexchange.guard.databasePassword = password
~~~
The password for the databases

### OX API
~~~bash
com.openexchange.guard.restApiHostname = localhost
~~~
The address for the OX REST API. It would be the location of the OX Backend

~~~bash
com.openexchange.guard.OXBackendPort =  8009
~~~
The port for the OX Backend. Default is 8009 (which is direct communication with the backend). Could be 80, etc, if going through load balancers

~~~bash
com.openexchange.guard.restApiUsername = open-xchange com.openexchange.guard.restApiPassword = secret
~~~
Username and password for the REST API

~~~bash
com.openexchange.guard.externalEmailURL = example.com/appsuite/api/oxguard/reader/reader.html
~~~
When Guard sends an e-Mail to external recipients those recipients will be able to access the encrypted content by opening a link in that e-Mail. The description and the link of that e-Mail are not encrypted and always readable by the recipient. The link points to the Guard reader for external recipients, a servlet to decrypt and display the encrypted e-Mail content. Specify which domain and path should be used. The `https` link will be created dynamically by Guard. This value will be used as the default unless over-written by cascade value `com.openexchange.guard.externalReaderURL`.

### Support API
~~~bash
com.openexchange.guard.supportapiusername = xxxxx
com.openexchange.guard.supportapipassword = yyyyy
~~~
If the support API is to be used, a username and password should be configured.

~~~bash
com.openexchange.guard.exposedKeyDurationInHours = 168
~~~
When a user is deleted, the Private keys are saved in a temporary deleted Keys table (in case of accidental deletion). If support "exposes" the key, the user can then retrieve it using link generated. For security reasons, this link is only valid for a short period of time. This property defines that duration.

### File Storage
Local/remote storage is required for temporary caching of non-ox encrypted emails. This can be an attached local file store, or Amazon S3 compatible object store depending on which `open-xchange-guard-*-storage` package is installed (`file` or `S3`).

#### General Properties
~~~bash
com.openexchange.guard.cacheDays = 30
~~~
How many days emails are kept in file store before being deleted. Measured from time of sending, reset when someone reads the email

#### Storage Specific Properties
##### File-Storage
~~~bash
com.openexchange.guard.storage.file.uploadDirectory = /var/spool/open-xchange/guard/uploads
~~~
Defines the temporary upload directory for OX Guard Drive files for `open-xchange-guard-file-storage` package.

##### S3-Storage
~~~bash
com.openexchange.guard.storage.s3.endpoint =
com.openexchange.guard.storage.s3.bucketName =
com.openexchange.guard.storage.s3.region =
com.openexchange.guard.storage.s3.accessKey =
com.openexchange.guard.storage.s3.secretKey =
~~~
S3 configuration options if the package `open-xchange-guard-s3-storage` is selected.

### Crypto
~~~bash
com.openexchange.guard.aesKeyLength=256 (Depreciated)
~~~
AES Key length. 256 is preferred, but not supported on all systems. May need to have the [Java Cryptography Extension (JCE) Unlimited Strength Jurisdiction Policy Files](http://www.oracle.com/technetwork/java/javase/downloads/index.html) installed.

~~~bash
com.openexchange.guard.rsaKeyLength=2048
~~~
RSA key length. Used when creating PGP keys

### PGP
~~~bash
com.openexchange.guard.publicPGPDirectory = hkp://keys.gnupg.net:11371, hkp://pgp.mit.edu:11371
~~~
List of PGP Public key servers to query for public keys

~~~bash
com.openexchange.guard.publicKeyWhitelist
~~~
A list of IP addresses of TRUSTED Guard servers. When the public PGP key server is queried, it will normally only find Guard keys that have already been created. If on the whitelist, the Guard server will also query the OX backend to see if the email address exists on the OX system, and if so, will create new keys for the user.

~~~bash
com.openexchange.guard.keyValidDays = 3650
~~~
PGP keys created will only be valid for this number of days. Default is 10 years. Set to 0 if no expiration date.

~~~bash
com.openexchange.guard.pgpCacheDays=7
~~~
When looking up remote PGP keys, if found, the keys will be stored in a temporary cache. Set number of days until the cache item is expired and remote lookup is repeated.

~~~
com.openexchange.guard.usestarttls = true
~~~
Use TLS when delivering to the SMTP server when available

### Email
~~~bash
com.openexchange.guard.guestSMTPServer=smtp.example.com
com.openexchange.guard.guestSMTPPort=25
com.openexchange.guard.guestSMTPUsername=
com.openexchange.guard.guestSMTPPassword=
~~~
SMTP settings for outgoing emails from the guest reader. Emails sent from within the system use the OX Backend. The guest reader, however, sends replies through this SMTP. In addition, password emails (reset, initial) are sent through the SMTP server.

### Bad Attempts
~~~bash
com.openexchange.guard.badMinuteLock = 10
~~~
Defines how long someone will be locked out after bad attempts. Defaults to 10 minutes.

~~~bash
com.openexchange.guard.badPasswordCount = 5
~~~
Defines how many times a person can attempt to unlock an encrypted item before being locked out. Defaults to 5 times.

### RSA Key Generation
~~~bash
com.openexchange.guard.rsacache = true
~~~
RSA keys are pre-generated in the background, encrypted, and stored for future user keys. RSA key generation is the most time consuming function and the RSA cache significantly improves new user creation time.

~~~bash
com.openexchange.guard.rsacachecount = 100
~~~
Number of RSA keys to pre-generate

~~~bash
com.openexchange.guard.keycachecheckinterval = 30
~~~
Interval in seconds to check the RSA cache and re-populate if less than rsacachecount.

~~~bash
com.openexchange.guard.rsacertainty = 256
~~~
Bit certainty for RSA key generation. Higher numbers assure the number is in fact prime but time consuming. Lower is much faster. May need to be lower if not using cache.

### Passwords
~~~bash
com.openexchange.guard.newpasslength=8
~~~
Length of the randomly generated passwords when a user resets password.

~~~bash
com.openexchange.guard.minpasswordlength=6
~~~
Minimum password length.

### Backend
~~~bash
com.openexchange.guard.oxbackendpath = /ajax/
~~~
URL used to communicated directly with the OX backend.

~~~bash
com.openexchange.guard.oxbackendidletime = 60
~~~
HTTP connections to the backend are kept open for faster response. This is the timeout setting that will close idle connections.

### Guest Accounts
~~~bash
com.openexchange.guard.shardsize=1000
~~~
Guest users data are placed in databases oxguard_x. After set number of users, another database shard is created

~~~bash
com.openexchange.guard.externalreaderpath=/appsuite/api/oxguard/reader/reader.html
~~~
Full path after domain name for the external reader (if changed from default)

### Recovery
If you do not want password recovery available, you can disable by adding

~~~bash
com.openexchange.guard.noRecovery = true
~~~
Keep in mind, that a lost password will result in total loss of encrypted data

### Miscellaneous
~~~bash
com.openexchange.guard.secureReply = true
~~~
(Guard 2.2) Normally, when a person replies from an encrypted email, the reply is automatically encrypted. Set to false to disable this automatic encryption

## SSL
As of 2.4.0, OX Guard is running inside the OSGi container, meaning that all its servlets are being registered and served by Grizzly.

### API SSL
The communication between Guard and the OX Backend is by default set to HTTP. All items to be encrypted are already encrypted at this point, but other information (sender name, filename, etc) could appear in plain text here.

TODO

### Incoming SSL
The communication between the frontend load balancer (Apache or otherwise) to Guard is by default HTTP (if protected network). More information on how to enable SSL you can find [here](http://oxpedia.org/wiki/index.php?title=AppSuite:Grizzly#X-FORWARDED-PROTO_Header).
