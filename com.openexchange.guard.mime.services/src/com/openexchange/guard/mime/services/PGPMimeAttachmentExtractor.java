/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;


/**
 * {@link PGPMimeAttachmentExtractor}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class PGPMimeAttachmentExtractor {

    /**
     * Extracts an attachment from the given MIME message
     * @param mimeMessage the message to extract the attachment from
     * @param name the name (case IN-sensitive) of the attachment to extract from the MIME message
     * @return The extracted attachment, or null if no such attachment was found
     * @throws MessagingException
     * @throws IOException
     */
    public Part getAttachmentFromMessage(MimeMessage mimeMessage, String name) throws MessagingException, IOException {

    	if (mimeMessage == null) return null;
        Multipart multiPart = (Multipart)mimeMessage.getContent();

        //Iterating over all body parts and search for the part with the given name
        return parseMultipartForName (multiPart, name);

    }

    /**
     * Parse all multiparts of a message for a file name
     * @param multiPart
     * @param name
     * @return
     * @throws MessagingException
     * @throws IOException
     */
    private Part parseMultipartForName (Multipart multiPart, String name) throws MessagingException, IOException {
    	for (int i = 0; i < multiPart.getCount(); i++) {
            Part bodyPart = multiPart.getBodyPart(i);
            String type = bodyPart.getContentType();
            if (type != null && type.contains("multipart")) {
            	Part r = parseMultipartForName((Multipart) bodyPart.getContent(), name);
            	if (r != null) return (r);
            }
            String filename = bodyPart.getFileName();
            if (filename != null) {
                if (filename.toLowerCase().trim().equals(name.toLowerCase().trim())) {
                    return bodyPart;
                }
            }
        }

        return null;
    }

    /**
     * Extracts an attachment from the given InputStream containing a MIME Message
     * @param mimeMessageStream The InputStream containing the MIME Message
     * @param name the name (case IN-sensitive) of the attachment to extract from the given MIME stream
     * @return the extracted attachment, or null if no such attachment was found
     * @throws MessagingException
     * @throws IOException
     */
    public Part getAttachmentFromMessage(InputStream mimeMessageStream, String name) throws MessagingException, IOException {
        Session session = Session.getDefaultInstance(new Properties());
        return getAttachmentFromMessage(new MimeMessage(session,mimeMessageStream), name);
    }

    /**
     * Extracts an attachment from the given MIME message
     * @param mimeMessage the message to extract the attachment from
     * @param contentId The ID of the attachment to extract
     * @return The extracted attachment, or null if no such attachment was found
     * @throws MessagingException
     * @throws IOException
     */
    public Part getAttachmentFromMessageID(MimeMessage message, String contentId) throws IOException, MessagingException {

    	if (message == null) return null;
        Multipart mp = (Multipart) message.getContent();

        return parseMultipartForId(mp, contentId);
    }

    /**
     * Parse all multiparts for a contentID attachment
     * @param mp
     * @param contentId
     * @return
     * @throws IOException
     * @throws MessagingException
     */
    private Part parseMultipartForId (Multipart mp, String contentId) throws IOException, MessagingException {
    	for (int i = 0; i < mp.getCount(); i++) {
            Part p = mp.getBodyPart(i);
            String type = p.getContentType();
            if (type != null && type.contains("multipart")) {
            	Part r = parseMultipartForId ((Multipart) p.getContent(), contentId);
            	if (r != null) return (r);
            }
            String[] cids = p.getHeader("Content-ID");
            String cid = null;
            if (cids != null) {
                cid = cids[0].replace("<", "").replace(">", "");
                if (cid.toLowerCase().trim().equals(contentId.toLowerCase().trim())) {
                    return p;
                }
            }
        }
    	return null;
    }

    /**
     * Extracts an attachment from the given InputStream containing a MIME Message
     * @param mimeMessageStream The InputStream containing the MIME Message
     * @param contentId The ID of the attachment to extract
     * @return the extracted attachment, or null if no such attachment was found
     * @throws MessagingException
     * @throws IOException
     */
    public Part getAttachmentFromMessageID(InputStream mimeMessageStream, String contentId) throws MessagingException, IOException {
        Session session = Session.getDefaultInstance(new Properties());
        return getAttachmentFromMessageID(new MimeMessage(session,mimeMessageStream), contentId);
    }
}
