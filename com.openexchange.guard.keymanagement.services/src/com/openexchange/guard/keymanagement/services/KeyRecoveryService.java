/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services;

import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.GuardKeys;

/**
 * {@link KeyRecoveryService} offers methods for handling lost password of {@link GuardKeys}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public interface KeyRecoveryService {

    /**
     * Checks if a password recovery should be created for a given user or not.
     *
     * @param contextId The context of the user
     * @param userId The user
     * @return True, if OX Guard should create a password recovery for new keys, False otherwise
     * @throws OXException
     */
    boolean checkCreateRecovery(int contextId, int userId) throws OXException;

    /**
     * Creates password recovery data for a {@link GuardKeys} key pair.
     *
     * The recovery data is the encrypted password which can be stored along the key
     * in the key storage. It can be used for changing the keys password even if the original password was lost.
     *
     * @param keys The key pair to create the recovery for
     * @param password The original password for the given {@link GuardKeys}
     * @throws OXException
     */
    String createRecovery(GuardKeys keys, String password) throws OXException;

    /**
     * createRecovery
     *
     * @param masterKeyIndex Master key index for the user
     * @param password New password
     * @param salt User salt
     * @param extra Any extra unique to user key data to use encrypting this recovery
     * @param userId User ID of the user
     * @param cid Context Id of the user
     * @param email Email of the user (used for error messaging)
     * @return New encrypted passowrd recovery
     * @throws OXException
     */
    String createRecovery(int masterKeyIndex, String password, String salt, String extra, int userId, int cid, String email) throws OXException;

    /**
     * Get the hash from recovery
     *
     * @param recoveryString Stored recovery string
     * @param masterKeyIndex Master Key index
     * @param salt Salt for the key
     * @param extra Any extra unique to user key data to use encrypting this recovery
     * @param userId User Id of the user
     * @param cid Context Id of theuser
     * @return Passwordhash that can be used to decrypt the private key
     * @throws OXException
     */
    String getRecoveryHash(String recoveryString, int masterKeyIndex, String salt, String extra, int userId, int cid) throws OXException;
}
