/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services;

import java.security.PrivateKey;
import java.util.Collection;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.GuardKeys;

/**
 * {@link GuardKeyService}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
public interface GuardKeyService {

    /**
     * Gets a specific {@link GuardKeys} for a specified key ID
     *
     * @param keyId The key ID (master- or sub-key- ID)
     * @return The {@link GuardKeys} or null, if no such key was found
     * @throws OXException
     */
    GuardKeys getKeys(long keyId) throws OXException;

    /**
     * Gets the "current" {@link GuardKeys} for the specified user
     *
     * @param id The user identifier
     * @param cid The context identifier
     * @return The {@link GuardKeys}
     * @throws OXException
     */
    GuardKeys getKeys(int id, int cid) throws OXException;

    /**
     * Gets a specific {@link GuardKeys} for the specified user
     *
     * @param id The user identifier
     * @param cid The context identifier
     * @param masterKeyId The master key identifier. Sub key IDs are not considered.
     * @return The {@link GuardKeys}
     * @throws OXException
     */
    GuardKeys getKeys(int id, int cid, long masterKeyId) throws OXException;

    /**
     * Gets a specific {@link GuardKeys} by a master- or sub-key id
     *
     * @param id The user identifier
     * @param cid The context identifier
     * @param keyId The ID of the key. Sub key IDs are considered.
     * @return The {@link GuardKeys}
     * @throws OXException
     */
    GuardKeys getKeysBySubKeyId(int id, int cid, long keyId) throws OXException;

    /**
     * Gets a specific {@link GuardKeys} by a master- or sub-key id
     *
     * @param email The user's email
     * @param keyId The ID of the key. Sub key IDs are considered.
     * @return The {@link GuardKeys}
     * @throws OXException
     */
    GuardKeys getKeysBySubKeyId(String email, long keyId) throws OXException;

    /**
     * Gets the key for the user with the specified e-mail address
     *
     * @param email The e-mail address of the user
     * @return The key
     * @throws OXException
     */
    GuardKeys getKeys(String email) throws OXException;

    /**
     * Gets the key with the specified identifier for the user with the specified e-mail address
     *
     * @param email The e-mail address
     * @param keyId The key identifier
     * @return The key
     * @throws OXException
     */
    GuardKeys getKeys(String email, long keyId) throws OXException;

    /**
     * Retrieves all keys for a given user
     *
     * @param id The user identifier
     * @param cid The context identifier
     * @return A collection of {@link GuardKeys} for the given user
     * @throws OXException
     */
    Collection<GuardKeys> getAllKeys(int id, int cid) throws OXException;

    /**
     * Stores the specified key
     *
     * @param keys The key to store
     * @param userCreated flag indicating whether the user is created
     * @return The updated stored keys
     * @throws OXException
     */
    GuardKeys storeKeys(GuardKeys keys, boolean userCreated) throws OXException;

    /**
     * Updates the specified keys for the specified user
     *
     * @param id The user identifier
     * @param cid The context identifier
     * @param keys The keys to update
     * @param recovery The recovery password
     * @param reset flag indicating whether to reset the password
     * @throws OXException
     */
    void updateKeys(int id, int cid, GuardKeys keys, String recovery, boolean reset) throws OXException;

    /**
     * Updates the raw public and private key data
     *
     * @param key The key to update
     * @throws OXException
     */
    void updateKeyMaterial(GuardKeys key) throws OXException;

    /**
     * Adds the specified {@link GuardKeys} key
     *
     * @param key The key to add
     * @throws OXException
     */
    void addNewKey(GuardKeys key) throws OXException;

    /**
     * Updates the PIN for the specified guest user
     *
     * @param id The user identifier
     * @param cid The context identifier
     * @param pin The new PIN
     * @throws OXException
     */
    void updatePin(int id, int cid, String pin) throws OXException;

    /**
     * Removes the PIN from the specified guest
     *
     * @param email the email of the guest user / guest key to remove the PIN for
     * @throws OXExeption
     */
    void removePin(String email) throws OXException;

    /**
     * Recovers the password hash for the specified user
     *
     * @param id The user identifier
     * @param cid The context identifier
     * @return The password hash
     * @throws OXException
     */
    String recoverPasswordHash(int id, int cid) throws OXException;

    /**
     * Resets password for an account with new password, stores new keys, and stores new recovery
     *
     * @param email The e-mail address of the user
     * @param newpass The new password
     * @throws OXException
     */
    void resetPassword(String email, String newpass) throws OXException;

    /**
     * Store the security question and answer for the specified user
     *
     * @param id The user identifier
     * @param cid The context identifier
     * @param question The question
     * @param answer The answer
     * @return true if the question was successfully stored; false otherwise
     * @throws OXException
     */
    boolean storeQuestion(int id, int cid, String question, String answer) throws OXException;

    /**
     * Encrypt the private key with the specified password and salt
     *
     * @param privateKey The private key to encrypt
     * @param password The password to encrypt it with
     * @param salt The salt to use
     * @return The encrypted private key as string
     * @throws OXException
     */
    String encryptPrivateKey(PrivateKey privateKey, String password, String salt) throws OXException;

    /**
     * Decrypt the private key with the specified password and salt. In addition the Password will be extended by the salt.
     *
     * @param privateKey the encrypted private key
     * @param password The password to decrypt it with
     * @param salt The salt
     *
     * @return The decrypted {@link PrivateKey}
     * @throws OXException
     */
    PrivateKey decryptPrivateKey(String privateKey, String password, String salt) throws OXException;

    /**
     * Decrypt the private key with the specified password hash.
     *
     * @param privateKey the encrypted private key
     * @param password The password hash (This is the salted password)
     * @param salt The salt
     *
     * @return The decrypted {@link PrivateKey}
     * @throws OXException
     */
    PrivateKey decryptPrivateKeyWithHash(String privateKey, String hash, String salt) throws OXException;

    /**
     * Generates a password
     *
     * @param userId  the Id of the user
     * @param cid     the context of the user
     * @return String with a generated password
     * @throws OXException
     */
    String generatePassword(int userId, int cid) throws OXException;

    /**
     * Returns if password recovery is enabled for the given user/context id.
     *
     * @param userId The id of the user
     * @param contextId The id of the context
     * @return <code>true</code> if enabled, otherwise <code>false</code>
     * @throws OXException
     */
    boolean isRecoveryEnabled(int userId, int contextId) throws OXException;

    /**
     * Deletes a key ring
     *
     * @param key The key ring to delete
     * @param password The password for the key, or null if it is just a public key
     * @param userId The ID of the user deleting the key
     * @param contextId The context ID of the user deleting the key
     * @throws OXException
     */
    void delete(GuardKeys key, String password, int userId, int contextId) throws OXException;

    /**
     * Updates settings for ALL existing keys of a user based on the given key.
     *
     * @param key The key containing the settings to set for all existing keys.
     * @throws OXException
     */
    void updateSettingsForUser(GuardKeys key) throws OXException;

    /**
     * Updates the question and answer for ALL existing keys of a user based on the given key
     *
     * @param key The key containing the question and answer to set for all existing keys.
     * @throws OXException
     */
    void updateAnswerQuestionForUser(GuardKeys key) throws OXException;

    /**
     * Sets the current key for a user.
     *
     * @param key The key to mark as current
     * @throws OXException
     */
    void setCurrentKey(GuardKeys key) throws OXException;

    /**
     * Adds a new user-ID to the key
     *
     * @param key The key
     * @param userId The new user ID to add
     * @throws OXException
     */
    void addUserId(GuardKeys key, String userId, String password) throws OXException;

    /**
     * Marks the given key a revoked
     *
     * @param key The key to be marked as revoked
     * @param keyId The ID of the key to revoke, or 0 to revoke all keys of the key
     * @param recovationReason The reason why the key is being revoked.
     * @param password The password for the key
     * @throws OXException
     */
    void revokeKey(GuardKeys key, long keyId, RevocationReason revocationReason, String password) throws OXException;
}
