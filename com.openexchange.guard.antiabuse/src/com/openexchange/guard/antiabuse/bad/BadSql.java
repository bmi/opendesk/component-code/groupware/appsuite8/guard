/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.antiabuse.bad;

public class BadSql {

    public static final String SELECT_BY_ID_STMT = "SELECT last, count, NOW() as cur FROM Bad WHERE Bad.id = ?";
    public static final String ADD_STMT = "INSERT INTO Bad VALUES (?, 1, NOW()) ON DUPLICATE KEY UPDATE count = count + 1, last = NOW()";
    public static final String DELETE_BY_ID_STMT = "DELETE FROM Bad WHERE id = ?";
    public static final String DELETE_BY_TIME_STMT = "DELETE FROM Bad WHERE last < DATE_SUB(NOW(), INTERVAL ? MINUTE);";
}
