/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.antiabuse.bad;

import com.openexchange.exception.OXException;

/**
 * {@link GuardBadStorageService} provides access to stored bad login attempts
 */
public interface GuardBadStorage {

    /**
     * Gets the bad login attempts for a given id/session
     *
     * @param id the session to the get the attempts for
     * @return the bad login attempts for the given id
     * @throws OXException due an error
     */
    public Bad getBad(String id) throws OXException;

    /**
     * Adds a bad login attempt for a given id/session
     *
     * @param id the session to add the bad login attempt for
     * @throws OXException due an error
     */
    public void addBad(String id) throws OXException;

    /**
     * Removes all bad login attempts for a given session
     *
     * @param id the session to remove the bad login attempts for
     * @throws OXException due an error
     */
    public void removeBad(String id) throws OXException;

    /**
     * Check if id is listed as bad.
     *
     * @param id Could be IP, itemID, or session
     * @param threshold Number of bad tries within time period acceptable
     * @return true, if the ID has been listed as bad, false otherwise
     * @throws OXException
     */
    boolean isBad(String id, int threshold) throws OXException;

    /**
     * Check if id is listed as bad while using the default threshold from configuration
     * @param id id could be IP, itemID or session
     * @return true, if the ID has been listed as bad, false otherwise
     * @throws OXExcpetion
     */
    boolean isBad(String id) throws OXException;
}
