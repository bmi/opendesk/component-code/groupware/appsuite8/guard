package com.openexchange.guard.antiabuse.weakforced;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.antiabuse.AllowParameters;
import com.openexchange.antiabuse.AntiAbuseService;
import com.openexchange.antiabuse.ReportParameters;
import com.openexchange.antiabuse.Status;
import com.openexchange.exception.OXException;
import com.openexchange.guard.antiabuse.osgi.Services;

public class WeakforcedImpl {


    private static final Logger LOG = LoggerFactory.getLogger(WeakforcedImpl.class);

    private static Map<String, String> properties;

    private AntiAbuseService getWeakForced () {
        AntiAbuseService wf;
        properties = new HashMap<String, String> ();
        properties.put("app", "oxguard");
        wf = Services.getServiceLookup().getOptionalService(AntiAbuseService.class);

        return (wf);
    }


    /**
     * Perform allow call to weakforced
     * If service unavailable, return OK for login
     * @param login
     * @param password
     * @param remoteAddress
     * @return
     */
    private Status allow (AllowParameters allowParams) {
        AntiAbuseService wf = getWeakForced();
        if (wf != null) {
            try {
                Status status = wf.allow(allowParams);
                return (status);
            } catch (OXException e) {
                LOG.error("Problem checking weakforced ", e);
            }
        }
        return null;
    }

    /**
     * Send weakforced report
     * @param reportValue
     * @param login
     * @param password
     * @param remoteAddress
     */
    public void report (ReportParameters reportParams) {
        AntiAbuseService wf = getWeakForced();
        if (wf != null) {
            try {
                wf.report(reportParams);
            } catch (OXException e) {
                LOG.error("Problem reporting weakforced ", e);
            }
        }
        return;
    }

    /**
     * Check if login is to be allowed
     * @param login
     * @param password
     * @param remoteAddress
     * @return
     */
    public boolean isAllowed (AllowParameters allowParam) {
        Status status = allow (allowParam);
        if (status == null) {
            return true;
        }
        if (!status.isOk()) {
            LOG.error(String.format("Login blocked for %s at %s by weakforced", allowParam.getLogin() ,allowParam.getRemoteAddress()));
        }
        return (status.isOk());
    }

    /**
     * Check if login is blocked
     * @param login
     * @param password
     * @param remoteAddress
     * @return
     */
    public boolean isBlocked (AllowParameters allowParam) {
        Status status = allow (allowParam);
        if (status == null) {
            return false;
        }
        if (status.isBlocked()) {
            LOG.error(String.format("Login blocked for %s at %s by weakforced", allowParam.getLogin() ,allowParam.getRemoteAddress()));
        }
        return (status.isBlocked());
    }


}
