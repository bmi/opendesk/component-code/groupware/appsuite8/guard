/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.user;

public class GuardCapabilities {

    public enum Permissions {
        NONE, GUARD, MAIL, FILE, GUARD_MAIL_FILE
    };

    private Permissions perm = Permissions.NONE;
    private boolean privatepass = true;
    private boolean noDeleteRecovery = false;
    private boolean noDeletePrivate = false;
    private boolean norecovery = false;

    /**
     * Initialises a new {@link GuardCapabilities}.
     */
    public GuardCapabilities() {
        super();
    }

    /**
     * Gets the perm
     *
     * @return The perm
     */
    public Permissions getPerm() {
        return perm;
    }

    /**
     * Sets the perm
     *
     * @param perm The perm to set
     */
    public void setPerm(Permissions perm) {
        this.perm = perm;
    }

    /**
     * Gets the privatepass
     *
     * @return The privatepass
     */
    public boolean isPrivatepass() {
        return privatepass;
    }

    /**
     * Sets the privatepass
     *
     * @param privatepass The privatepass to set
     */
    public void setPrivatepass(boolean privatepass) {
        this.privatepass = privatepass;
    }

    /**
     * Gets the noDeleteRecovery
     *
     * @return The noDeleteRecovery
     */
    public boolean isNoDeleteRecovery() {
        return noDeleteRecovery;
    }

    /**
     * Sets the noDeleteRecovery
     *
     * @param noDeleteRecovery The noDeleteRecovery to set
     */
    public void setNoDeleteRecovery(boolean noDeleteRecovery) {
        this.noDeleteRecovery = noDeleteRecovery;
    }

    /**
     * Gets the noDeletePrivate
     *
     * @return The noDeletePrivate
     */
    public boolean isNoDeletePrivate() {
        return noDeletePrivate;
    }

    /**
     * Sets the noDeletePrivate
     *
     * @param noDeletePrivate The noDeletePrivate to set
     */
    public void setNoDeletePrivate(boolean noDeletePrivate) {
        this.noDeletePrivate = noDeletePrivate;
    }

    /**
     * Gets the norecovery
     *
     * @return The norecovery
     */
    public boolean isNorecovery() {
        return norecovery;
    }

    /**
     * Sets the norecovery
     *
     * @param norecovery The norecovery to set
     */
    public void setNorecovery(boolean norecovery) {
        this.norecovery = norecovery;
    }

    public boolean hasPermission(GuardCapabilities.Permissions permission) {
        return this.perm == permission ||
               this.perm == Permissions.GUARD_MAIL_FILE /*highest available*/;
    }

    public boolean hasMinimumPermission() {
        return this.perm == GuardCapabilities.Permissions.GUARD ||
               this.perm == GuardCapabilities.Permissions.FILE ||
               this.perm == GuardCapabilities.Permissions.GUARD_MAIL_FILE ||
               this.perm == GuardCapabilities.Permissions.MAIL;
    }

}
