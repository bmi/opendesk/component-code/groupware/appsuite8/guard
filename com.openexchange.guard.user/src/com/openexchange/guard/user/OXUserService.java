/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.user;

import com.openexchange.exception.OXException;

/**
 * {@link OXUserService} gets information about regular OX Appsuite users
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public interface OXUserService {

    /**
     * Tries to retrieve the user associated with the given email address.
     *
     * @param email The address to get the user for
     * @return The {@link OXUser} if existing. Otherwise <code>null</code>.
     * @throws OXException If an error occurs while reading from the persistent storage
     */
    OXUser getUser(String email) throws OXException;

    /**
     * Tries to retrieve the user associated with the given context- and user user-ID
     *
     * @param contextId The context-ID
     * @param userId The user-ID
     * @return The {@link OXUser} if existing. Otherwise <code>null</code>.
     * @throws OXException If an error occurs.
     */
    OXUser getUser(int contextId, int userId) throws OXException;

    /**
     * Attempts to retrieve an OX Guard guest user from email
     * @param email
     * @return
     * @throws OXException
     */
    OXGuardUser getGuestUser(String email) throws OXException;

    /**
     * Gets the guard capabilities for a given user.
     *
     * @param contextId The context ID of the user
     * @param userId The ID of the user
     * @return The capabilities for the given user
     * @throws OXException
     */
    GuardCapabilities getGuardCapabilieties(int contextId, int userId) throws OXException;

    /**
     * Checks if user is OX Guest user
     * @param userId
     * @param contextId
     * @return
     * @throws OXException
     */
    boolean isGuest(int userId, int contextId) throws OXException;
}
