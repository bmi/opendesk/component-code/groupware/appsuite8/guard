/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.pgp.keys.common.PGPSymmetricKey;

/**
 * {@link UserIdentity} represents a generic PGP user.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public class UserIdentity {

    private char[]          password;
    private String          identity;
    private PGPSymmetricKey symmetricKey;
    private OXUser          oxUser;
    private OXGuardUser     oxGuardUser;
    private static Logger LOG = LoggerFactory.getLogger(UserIdentity.class);

    /**
     * Initializes a new {@link UserIdentity}.
     *
     * @param identity The identity
     * @param password The password
     */
    public UserIdentity(String identity, String password) {
        this.identity = identity;
        this.password = password != null ? password.toCharArray() : null;
    }

    /**
     * Initializes a new {@link UserIdentity}.
     *
     * @param identity the identity
     */
    public UserIdentity(String identity) {
        this.identity = identity;
        this.password = new char[] {};
    }

    /**
     * Gets the identity
     *
     * @return The identity
     */
    public String getIdentity() {
        return identity;
    }

    /**
     * Sets the identity
     *
     * @param identity The identity to set
     * @return this
     */
    public UserIdentity setIdentity(String identity) {
        this.identity = identity;
        return this;
    }

    /**
     * Gets the password
     *
     * @return The password
     */
    public char[] getPassword() {
        return password;
    }

    /**
     * Sets the password
     * @param password The password
     * @return this
     */
    public UserIdentity setPassword(String password) {
        this.password = password.toCharArray();
        return this;
    }

    /**
     * Gets the user's symmetric PGP session key which can be used alternative to the password for decryptingn PGP data.
     *
     * @return A PGP symmetric session key.
     */
    public PGPSymmetricKey getPGPSessionKey() {
        return symmetricKey;
    }

    /**
     * Sets a symmetric PGP session key to use for decryption.
     *
     * @param pgpSessionKey A symmetric session key used to decryption.
     * @return this
     */
    public UserIdentity setPGPSession(PGPSymmetricKey symmetricKey) {
        this.symmetricKey = symmetricKey;
        return this;
    }

    /**
     * Gets the related OX user for this PGP user entity, or null if this entity is not related to an regular OX Appsuite account.
     *
     * @return The OX Appsuite user, or null, if this entity is not related to a regular OX Appsuite account.
     */
    public OXUser getOXUser() {
        if (this.oxUser == null) {
            if (this.oxGuardUser != null) {
                LOG.error("Call for OX User which is null.  Using Guard user.  Please report");
                // Fallback to Guard user
                return new OXUser(this.identity, this.oxGuardUser.getId(), this.oxGuardUser.getContextId(), null, this.oxGuardUser.getContextId() < 0, null);
            }
        }
        return this.oxUser;
    }

    /**
     * Sets the OX Appsuite user related to this PGP entity.
     *
     * @param oxUser the related OX User
     * @return this, for a fluent like method call
     */
    public UserIdentity setOXUser(OXUser oxUser) {
        this.oxUser = oxUser;
        return this;
    }

    /**
     * Returns if this PGP entity is related to an OX account or not.
     *
     * @return True if this PGP entity is related to an OX account, false otherwise.
     */
    public boolean isOXUser() {
        return this.oxUser != null;
    }

    /**
     * Gets the related OX Guard user information for this PGP user entity, or null if this entity is not related to an OX Guard account.
     *
     * @return The OX Guard user, or null, if this entity is not related to an OX Guard account.
     */
    public OXGuardUser getOXGuardUser() {
        return this.oxGuardUser;
    }

    /**
     * Sets the OX Guard user related to this PGP entity.
     *
     * @param oxGuardUser The related OX Guard user
     * @return This, for a fluent like method call
     */
    public UserIdentity setOXGuardUser(OXGuardUser oxGuardUser) {
        this.oxGuardUser = oxGuardUser;
        return this;
    }

    /**
     * Returns if this PGP entity is related to an, non guest, OX Guard user.
     *
     * @return True if the PGP entity is related to a regular OX Guard account which is not a guest.
     */
    public boolean isOXGuardUser() {
        return this.oxGuardUser != null && !this.oxGuardUser.isGuest();
    }

    /**
     * Returns if this PGP entity is related to an OX Guard Guest Account or not.
     * @return True, if the PGP entity is related to an OX Guard Guest account, false otherwise
     */
    public boolean isOXGuardGuest() {
       return this.oxGuardUser != null && this.oxGuardUser.isGuest();
    }
}
