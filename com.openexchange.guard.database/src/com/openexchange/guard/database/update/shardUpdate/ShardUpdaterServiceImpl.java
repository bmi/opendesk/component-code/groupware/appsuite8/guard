/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.database.update.shardUpdate;

import com.openexchange.exception.OXException;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.GuardShardingService;
import com.openexchange.guard.database.exception.GuardDatabaseExceptionCodes;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.server.ServiceLookup;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * {@link ShardUpdaterServiceImpl}  Performs any updates required and registered on Guest shards
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v7.10.3
 */
public class ShardUpdaterServiceImpl implements ShardUpdaterService {

    ServiceLookup serviceLookup;
    GuardDatabaseService guardDatabaseService = null;
    ShardUpdateTask[] tasks;

    private static final String SELECT_TASK = "SELECT `success` FROM updates WHERE id = ?";
    private static final String UPDATE_TASK = "INSERT INTO updates (`id`, `success`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `success` = ?";

    public ShardUpdaterServiceImpl (ServiceLookup serviceLookup, GuardDatabaseService guardDatabaseService, ShardUpdateTask...shardUpdateTasks) {
        this.serviceLookup = serviceLookup;
        this.guardDatabaseService = guardDatabaseService;
        this.tasks = shardUpdateTasks;
    }

    /**
     * Check updates table to see if task has been executed.
     *
     * @param shardId
     * @param name
     * @return
     * @throws OXException
     */
    private boolean checkDone (int shardId, String name) throws OXException {
        Connection connection = guardDatabaseService.getReadOnlyForShard(shardId);

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(SELECT_TASK);
            stmt.setString(1, name);
            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return resultSet.getBoolean("success");
            }
            return false;
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForShard(shardId, connection);;
        }
    }

    /**
     * Mark the task as success or failure
     *
     * @param shardId
     * @param name
     * @param success
     * @throws OXException
     */
    private void markDone (int shardId, String name, boolean success) throws OXException {
        Connection connection = guardDatabaseService.getWritableForShard(shardId);

        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(UPDATE_TASK);
            stmt.setString(1, name);
            stmt.setBoolean(2, success);
            stmt.setBoolean(3, success);
            stmt.execute();
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForShard(shardId, connection);
        }
    }

    @Override
    public void doUpdates() throws OXException {
        GuardShardingService shardingService = serviceLookup.getServiceSafe(GuardShardingService.class);

        List<Integer> shards = shardingService.getAllShards();
        for (Integer shard : shards) {
            doUpdates(shard);
        }
    }

    @Override
    public void doUpdates(int shard) throws OXException {
        for (ShardUpdateTask task: tasks) {
            if (!checkDone(shard, task.getName())) {
                markDone(shard, task.getName(), task.update(shard));
            }
        }

    }

}
