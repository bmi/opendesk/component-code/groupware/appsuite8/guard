/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.database.update.shardUpdate;

import com.openexchange.exception.OXException;

/**
 * {@link ShardUpdateTask}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v7.10.3
 */
public abstract class ShardUpdateTask {

    protected static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ShardUpdateTask.class);

    protected String name;  // Name of the update

    public String getName() {
        return this.name;
    }

    public boolean update(int shardId) throws OXException {
        LOG.info("Performing shard update \" {} \" for shardId {}", name, shardId);
        boolean result = doUpdate(shardId);
        if (result) {
            LOG.info("Success shard update \" {} \" for shardId {}", name, shardId);
        } else {
            LOG.error("Problem with shard update \" {} \" for shardId {}", name, shardId);
        }
        return result;
    }

    /**
     * Perform an update on specific shard
     *
     * @param shardId
     * @return true if success
     * @throws OXException
     */
    protected abstract boolean doUpdate(int shardId) throws OXException;

}
