/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.database.internal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import com.openexchange.database.Assignment;
import com.openexchange.database.AssignmentFactory;
import com.openexchange.database.DBPoolingExceptionCodes;
import com.openexchange.database.DatabaseService;
import com.openexchange.database.JdbcProperties;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.GuardShardingService;
import com.openexchange.guard.database.exception.GuardDatabaseExceptionCodes;
import com.openexchange.guard.database.utils.GuardConnectionWrapper;
import com.openexchange.server.ServiceExceptionCode;

/**
 * {@link GuardDatabaseServiceImpl}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 2.4.0
 */
public class GuardDatabaseServiceImpl implements GuardDatabaseService {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(GuardDatabaseServiceImpl.class);

    private final DatabaseService databaseService;
    private final GuardConfigurationService guardConfigurationService;
    private final AssignmentFactory assignmentFactory;

    public GuardDatabaseServiceImpl(DatabaseService databaseService, GuardConfigurationService guardConfigurationService, AssignmentFactory assignmentFactory) {
        this.databaseService = databaseService;
        this.guardConfigurationService = guardConfigurationService;
        this.assignmentFactory = assignmentFactory;
    }

    @Override
    public void reloadAssignments() throws OXException {
        this.assignmentFactory.reload();
    }

    //----------- read/write ox guard table -------------
    @Override
    public Connection getWritableForGuardNoTimeout() throws OXException {
        String guardSchemaName = this.guardConfigurationService.getProperty(GuardProperty.dbSchemaBaseName);
        Assignment assignment = assignmentFactory.get(guardSchemaName);
        if (assignment == null) {
            throw GuardDatabaseExceptionCodes.ASSIGNMENT_NOT_AVAILABLE_FOR_SCHEMA.create(guardSchemaName);
        }
        return databaseService.getWritable(assignment, true);
    }

    @Override
    public Connection getWritableForGuard() throws OXException {
        String guardSchemaName = this.guardConfigurationService.getProperty(GuardProperty.dbSchemaBaseName);
        Assignment assignment = assignmentFactory.get(guardSchemaName);
        if (assignment == null) {
            throw GuardDatabaseExceptionCodes.ASSIGNMENT_NOT_AVAILABLE_FOR_SCHEMA.create(guardSchemaName);
        }
        return databaseService.getWritable(assignment, false);
    }

    @Override
    public Connection getReadOnlyForGuard() throws OXException {
        String guardSchemaName = this.guardConfigurationService.getProperty(GuardProperty.dbSchemaBaseName);
        Assignment assignment = assignmentFactory.get(guardSchemaName);
        if (assignment == null) {
            throw GuardDatabaseExceptionCodes.ASSIGNMENT_NOT_AVAILABLE_FOR_SCHEMA.create(guardSchemaName);
        }
        return databaseService.getReadOnly(assignment, false);
    }

    @Override
    public void backWritableForGuardNoTimeout(Connection connection) {
        back(connection);
    }

    @Override
    public void backWritableAfterReadingForGuardNoTimeout(Connection connection) {
        databaseService.backWritableAfterReading(connection);
    }

    @Override
    public void backReadOnlyForGuard(Connection connection) {
        back(connection);
    }

    @Override
    public void backWritableForGuard(Connection connection) {
        back(connection);
    }

    //----------- read/write shard table -------------

    @Override
    public Connection getWritableForShardNoTimeout(int shardId) throws OXException {
        if (shardId == 0) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create("Shard with id 0 unknown!");
        }
        String shardName = getShardName(shardId);
        Assignment assignment = assignmentFactory.get(shardName);
        if (assignment == null) {
            throw GuardDatabaseExceptionCodes.ASSIGNMENT_NOT_AVAILABLE_FOR_SCHEMA.create(shardName);
        }
        return databaseService.getWritable(assignment, true);
    }

    @Override
    public Connection getWritableForShard(int shardId) throws OXException {
        if (shardId == 0) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create("Shard with id 0 unknown!");
        }
        String shardName = getShardName(shardId);
        Assignment assignment = assignmentFactory.get(shardName);
        if (assignment == null) {
            throw GuardDatabaseExceptionCodes.ASSIGNMENT_NOT_AVAILABLE_FOR_SCHEMA.create(shardName);
        }
        return databaseService.getWritable(assignment, false);
    }

    @Override
    public Connection getReadOnlyForShard(int shardId) throws OXException {
        if (shardId == 0) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create("Shard with id 0 unknown!");
        }
        String shardName = getShardName(shardId);
        Assignment assignment = assignmentFactory.get(shardName);
        if (assignment == null) {
            throw GuardDatabaseExceptionCodes.ASSIGNMENT_NOT_AVAILABLE_FOR_SCHEMA.create(shardName);
        }
        return databaseService.getReadOnly(assignment, false);
    }

    @Override
    public void backWritableForShardNoTimeout(int shardId, Connection connection) {
        back(connection);
    }

    @Override
    public void backWritableAfterReadingForShardNoTimeout(int shardId, Connection connection) {
        databaseService.backWritableAfterReading(connection);
    }

    @Override
    public void backReadOnlyForShard(int shardId, Connection connection) {
        back(connection);
    }

    @Override
    public void backWritableForShard(int shardId, Connection connection) {
        back(connection);
    }

    @Override
    public String getShardName(int shardId) {
        return guardConfigurationService.getProperty(GuardProperty.dbSchemaBaseName) + "_" + shardId;
    }

    @Override
    public String getConnectionUrl(String hostname) {
        return "jdbc:mysql://" + hostname;
    }

    private void back(Connection connection) {
        if (null == connection) {
            LOG.error("", DBPoolingExceptionCodes.NULL_CONNECTION.create());
            return;
        }
        try {
            connection.close();
        } catch (SQLException e) {
            LOG.error("", DBPoolingExceptionCodes.SQL_ERROR.create(e, e.getMessage()));
        }
    }

    //----------- generic connection retrieval -------------

    @Override
    public GuardConnectionWrapper getWritable(int userId, int contextId, int shard) throws OXException {
        if (contextId >= 0) {
            if (userId < 0) {
                return new GuardConnectionWrapperImpl(this.getWritableForGuard(), userId, contextId, shard);
            }
            return new GuardConnectionWrapperImpl(this.getWritable(contextId), userId, contextId, shard);
        } else {
            if (shard == 0) {
                shard = Services.getService(GuardShardingService.class).getShard(userId, contextId);
            }
            return new GuardConnectionWrapperImpl(this.getWritableForShard(shard), userId, contextId, shard);
        }
    }

    @Override
    public void backWritable(GuardConnectionWrapper connectionWrapper) {
        if (connectionWrapper.getContextId() >= 0) {
            if (connectionWrapper.getUserId() < 0) {
                this.backWritableForGuard(connectionWrapper.getConnection());
            } else {
                this.backWritable(connectionWrapper.getContextId(), connectionWrapper.getConnection());
            }
        } else {
            this.backWritableForShard(connectionWrapper.getShardId(), connectionWrapper.getConnection());
        }
    }

    @Override
    public GuardConnectionWrapper getReadOnly(int userId, int contextId, int shard) throws OXException {
        if (contextId >= 0) {
            if (userId < 0) {
                return new GuardConnectionWrapperImpl(this.getReadOnlyForGuard(), userId, contextId, shard);
            }
            return new GuardConnectionWrapperImpl(this.getReadOnly(contextId), userId, contextId, shard);
        } else {
            if (shard == 0) {
                shard = Services.getService(GuardShardingService.class).getShard(userId, contextId);
            }
            return new GuardConnectionWrapperImpl(this.getReadOnlyForShard(shard), userId, contextId, shard);
        }
    }

    @Override
    public void backReadOnly(GuardConnectionWrapper connectionWrapper) {
        if (connectionWrapper.getContextId() >= 0) {
            if (connectionWrapper.getUserId() < 0) {
                this.backReadOnlyForGuard(connectionWrapper.getConnection());
            } else {
                this.backReadOnly(connectionWrapper.getContextId(), connectionWrapper.getConnection());
            }
        } else {
            this.backReadOnlyForShard(connectionWrapper.getShardId(), connectionWrapper.getConnection());
        }
    }

    //----------- methods for INIT -------------

    @Override
    public Connection getWritableForShardInit() throws OXException {
        Properties properties = new Properties();
        int conntectionTimeout = this.guardConfigurationService.getIntProperty(GuardProperty.oxGuardShardDatabaseInitConnectionTimeout);
        properties.setProperty("connectTimeout", Integer.toString(conntectionTimeout));

        return getWritableForInit(this.guardConfigurationService.getProperty(GuardProperty.oxGuardShardDatabase), properties);
    }

    @Override
    public Connection getWritableForGuardInit() throws OXException {
        Properties properties = new Properties();
        int conntectionTimeout = this.guardConfigurationService.getIntProperty(GuardProperty.oxGuardDatabaseInitConnectionTimeout);
        properties.setProperty("connectTimeout", Integer.toString(conntectionTimeout));

        return getWritableForInit(this.guardConfigurationService.getProperty(GuardProperty.oxGuardDatabaseHostname), properties);
    }

    private Connection getWritableForInit(String host, Properties propertyOptions) throws OXException {
        JdbcProperties jdbcProperties = Services.getService(JdbcProperties.class);
        if (jdbcProperties == null) {
            throw ServiceExceptionCode.absentService(JdbcProperties.class);
        }

        String connectionString = "jdbc:mysql://" + host;
        Properties properties = new Properties();
        properties.putAll(jdbcProperties.getJdbcPropertiesRaw());  // Load defaults
        properties.putAll(propertyOptions);
        properties.setProperty("user", this.guardConfigurationService.getProperty(GuardProperty.databaseUsername));
        properties.setProperty("password", this.guardConfigurationService.getProperty(GuardProperty.databasePassword));
        try {
            return DriverManager.getConnection(connectionString, properties);
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.CONNECTION_RETRIEVAL_ERROR.create(e, connectionString);
        }
    }

    @Override
    public void backWritableForInit(Connection connection) {
        back(connection);
    }

    //----------- delegates are ok -------------
    @Override
    public Connection getWritable(int contextId) throws OXException {
        return databaseService.getWritable(contextId);
    }

    @Override
    public Connection getReadOnly(int contextId) throws OXException {
        return databaseService.getReadOnly(contextId);
    }

    @Override
    public void backReadOnly(int contextId, Connection connection) {
        databaseService.backReadOnly(contextId, connection);
    }

    @Override
    public void backWritable(int contextId, Connection connection) {
        databaseService.backWritable(contextId, connection);
    }

    @Override
    public Connection getWritableForConfigDB() throws OXException {
        return databaseService.getWritable();
    }

    @Override
    public Connection getReadOnlyForConfigDB() throws OXException {
        return databaseService.getReadOnly();
    }

    @Override
    public void backReadOnlyForConfigDB(Connection connection) {
        databaseService.backReadOnly(connection);
    }

    @Override
    public void backWritableForConfigDB(Connection connection) {
        databaseService.backWritable(connection);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.database.GuardDatabaseService#getDatabaseService()
     */
    @Override
    public DatabaseService getDatabaseService() {
        return databaseService;
    }
}
