/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.database.internal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import org.apache.commons.lang.StringUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.database.Databases;
import com.openexchange.database.migration.DBMigration;
import com.openexchange.database.migration.DBMigrationConnectionProvider;
import com.openexchange.database.migration.DBMigrationExceptionCodes;
import com.openexchange.database.migration.DBMigrationExecutorService;
import com.openexchange.database.migration.DBMigrationState;
import com.openexchange.database.migration.resource.accessor.BundleResourceAccessor;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.database.DatabaseMaintenanceService;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.GuardShardingService;
import com.openexchange.guard.database.dao.SchemaCandidate;
import com.openexchange.guard.database.exception.GuardDatabaseExceptionCodes;
import com.openexchange.guard.database.osgi.DBMigrationServiceTracker;
import com.openexchange.guard.database.update.shardUpdate.ShardUpdaterService;
import com.openexchange.guard.database.utils.DBUtils;

/**
 * {@link GuardShardingServiceImpl}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class GuardShardingServiceImpl implements GuardShardingService {

    private static final Logger LOG = LoggerFactory.getLogger(GuardShardingServiceImpl.class);

    private static final String SELECT_ONE_BY_USERID_STMT = "SELECT db from og_email WHERE id = ? AND cid = ? LIMIT 1";
    private static final String SELECT_ONE_BY_EMAIL_STMT = "SELECT db FROM og_email WHERE email = ?;";
    private static final String SELECT_EXISTS_STMT = "SELECT COUNT(id) AS \"COUNT\" from og_email where id = ? AND cid = ?;";

    private final DBMigrationExecutorService dbMigrationExecutorService;

    private final GuardDatabaseService guardDatabaseService;

    private final DatabaseMaintenanceService databaseMaintenanceService;

    private final ShardUpdaterService updateService;

    /**
     * Initialises a new {@link GuardShardingServiceImpl}.
     *
     * @param databaseMaintenanceService
     */
    public GuardShardingServiceImpl(GuardDatabaseService guardDatabaseService, DBMigrationExecutorService dbMigrationExecutorService, DatabaseMaintenanceService databaseMaintenanceService, ShardUpdaterService updateService) {
        this.dbMigrationExecutorService = dbMigrationExecutorService;
        this.guardDatabaseService = guardDatabaseService;
        this.databaseMaintenanceService = databaseMaintenanceService;
        this.updateService = updateService;
    }

    @Override
    public int getNextShard() throws OXException {
        int shard = 0;

        Connection writableForGuard = guardDatabaseService.getWritableForGuard();

        ResultSet result = null;
        PreparedStatement stm = null;
        try {
            writableForGuard.setAutoCommit(false);

            GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
            int shardSize = configService.getIntProperty(GuardProperty.shardSize);

            synchronized (this) {
                stm = writableForGuard.prepareStatement("SELECT count, current_shard FROM sharding;");
                result = stm.executeQuery();

                if (result.next()) {
                    int currentCount = result.getInt("count");
                    int currentShard = result.getInt("current_shard");

                    if (currentCount >= shardSize) {
                        int newShardId = currentShard + 1;
                        stm.execute("UPDATE sharding set current_shard = " + newShardId + ", count = " + 0);

                        createRegisterAndUpdateShard(newShardId);
                        shard = newShardId;
                    } else {
                        String update = "UPDATE sharding set current_shard = " + currentShard + ", count = " + ++currentCount;
                        stm.execute(update);
                        shard = currentShard;
                    }
                    writableForGuard.commit();
                } else {
                    LOG.error("Unable to retrieve any entry from 'sharding' table.");
                    throw GuardDatabaseExceptionCodes.DB_ERROR.create("Cannot find any entry for shard. Unable to retrieve next shard and persist current status.");
                }
            }
        } catch (SQLException ex) {
            LOG.error("Error getting next shard", ex);
            DBUtils.rollback(writableForGuard);
        } finally {
            DBUtils.autocommit(writableForGuard);
            DBUtils.closeSQLStuff(result, stm);
            guardDatabaseService.backWritableForGuard(writableForGuard);
        }
        return shard;
    }


    /* (non-Javadoc)
     * @see com.openexchange.guard.database.GuardShardingService#hasShard(int, int)
     */
    @Override
    public boolean hasShard(int userId, int cid) throws OXException {
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(SELECT_EXISTS_STMT);
            stmt.setInt(1, userId);
            stmt.setInt(2, cid);
            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("COUNT") > 0;
            }
            return false;
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    @Override
    public int getShard(int userId, int cid) throws OXException {

        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(SELECT_ONE_BY_USERID_STMT);
            stmt.setInt(1, userId);
            stmt.setInt(2, cid);
            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("db");
            }
            throw GuardDatabaseExceptionCodes.GUEST_WITH_IDS_NOT_ASSIGNED_TO_SHARD.create(userId, cid);
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    @Override
    public int getShard(String email) throws OXException {
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(SELECT_ONE_BY_EMAIL_STMT);
            stmt.setString(1, email);
            resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt("db");
            }
            throw GuardDatabaseExceptionCodes.GUEST_NOT_ASSIGNED_TO_SHARD.create(email);
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    @Override
    public void updateShardSchema(final int shardId) throws OXException {
        Bundle guardDatabaseServiceBundle = Objects.requireNonNull(FrameworkUtil.getBundle(GuardDatabaseServiceImpl.class), "Missing required GuardDatabaseService bundle");
        BundleResourceAccessor bundleResourceAccessor = new BundleResourceAccessor(guardDatabaseServiceBundle);
        DBMigration dbMigration = new DBMigration(new DBMigrationConnectionProvider() {

            @SuppressWarnings("synthetic-access")
            @Override
            public Connection get() throws OXException {
                return guardDatabaseService.getWritableForShardNoTimeout(shardId);
            }

            @SuppressWarnings("synthetic-access")
            @Override
            public void back(Connection connection) {
                DBUtils.autocommit(connection);
                guardDatabaseService.backWritableForShardNoTimeout(shardId, connection);
            }

            @SuppressWarnings("synthetic-access")
            @Override
            public void backAfterReading(Connection connection) {
                DBUtils.autocommit(connection);
                guardDatabaseService.backWritableAfterReadingForShardNoTimeout(shardId, connection);
            }
        }, DBMigrationServiceTracker.SHARDDB_CHANGE_LOG, bundleResourceAccessor, guardDatabaseService.getShardName(shardId));
        executeAndAwaitCompletion(dbMigration);
    }

    private void executeAndAwaitCompletion(DBMigration dbMigration) throws OXException {
        DBMigrationState state = dbMigrationExecutorService.scheduleDBMigration(dbMigration);
        try {
            state.awaitCompletion();
        } catch (ExecutionException e) {
            LOG.error("", e.getCause());
            throw DBMigrationExceptionCodes.DB_MIGRATION_ERROR.create(e.getCause());
        } catch (InterruptedException e) {
            LOG.error("", e);
            Thread.currentThread().interrupt();
            throw DBMigrationExceptionCodes.DB_MIGRATION_ERROR.create(e);
        }
    }

    @Override
    public void updateAllShardSchemas() throws OXException {
        List<Integer> shards = getAllShards();
        for (Integer shard : shards) {
            updateShardSchema(shard);
        }
    }


    @Override
    public List<Integer> getAllShards() throws OXException {
        List<Integer> shardIds = new ArrayList<>();

        String shardBaseName = Services.getService(GuardConfigurationService.class).getProperty(GuardProperty.dbSchemaBaseName);
        String shardPrefix = shardBaseName + "_";
        ResultSet resultSet = null;
        Connection shardInitConnection = null;
        try {
            shardInitConnection = guardDatabaseService.getWritableForShardInit();

            Class.forName(com.openexchange.database.DatabaseConstants.DRIVER);

            resultSet = shardInitConnection.getMetaData().getCatalogs();

            while (resultSet.next()) {
                String databaseName = resultSet.getString(1);
                if (databaseName.startsWith(shardPrefix)) {
                    String shardId = StringUtils.substring(databaseName, shardPrefix.length());
                    try {
                        shardIds.add(Integer.parseInt(shardId));
                    } catch (NumberFormatException ex) {
                        LOG.warn("Error while retrieving shard identifier. Found database name '{}' does not match configured shard base name '{}'. This might happen due to a change of the guard database schema base name after the first time in production.", databaseName, shardBaseName, ex);
                    }
                }
            }
        } catch (SQLException | ClassNotFoundException e) {
            LOG.error("An error occurred while trying to get shard identifiers.", e);
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(resultSet);
            if (shardInitConnection != null) {
                guardDatabaseService.backWritableForInit(shardInitConnection);
            }
        }
        return shardIds;
    }

    private void createRegisterAndUpdateShard(int shard) throws OXException {
        String shardDbSchema = guardDatabaseService.getShardName(shard);
        Connection shardInitConnection = guardDatabaseService.getWritableForShardInit();
        String shardSlaveHost = Services.getService(GuardConfigurationService.class).getProperty(GuardProperty.oxGuardShardRead);

        SchemaCandidate candidate = new SchemaCandidate(guardDatabaseService, shardInitConnection, shardDbSchema, shardSlaveHost);
        try {
            candidate.start();
            databaseMaintenanceService.createAndRegisterSchemaIfNotExistent(candidate);
            candidate.commit();
        } finally {
            candidate.finish();
        }
        updateShardSchema(shard);
        // Perform any updates in the updateService
        updateService.doUpdates(shard);
    }


}
