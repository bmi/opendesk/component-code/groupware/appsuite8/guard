/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.database.internal;

import java.sql.Connection;
import com.openexchange.guard.database.utils.GuardConnectionWrapper;

/**
 * {@link GuardConnectionWrapperImpl}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public class GuardConnectionWrapperImpl implements GuardConnectionWrapper{

    private final Connection connection;

    private final int userId;

    private final int contextId;

    private final int shardId;

    public GuardConnectionWrapperImpl(Connection connection, int userId, int contextId, int shardId) {
        this.connection = connection;
        this.userId = userId;
        this.contextId = contextId;
        this.shardId = shardId;
    }

    @Override
    public int getShardId() {
        return shardId;
    }

    @Override
    public Connection getConnection() {
        return connection;
    }

    @Override
    public int getUserId() {
        return userId;
    }

    @Override
    public int getContextId() {
        return contextId;
    }
}
