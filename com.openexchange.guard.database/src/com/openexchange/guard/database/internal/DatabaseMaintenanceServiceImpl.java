/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.database.internal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.openexchange.database.AssignmentFactory;
import com.openexchange.database.Databases;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.impl.IDGenerator;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.database.DatabaseMaintenanceService;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.dao.SchemaCandidate;
import com.openexchange.guard.database.exception.GuardDatabaseExceptionCodes;
import com.openexchange.guard.database.utils.DBUtils;

/**
 * {@link DatabaseMaintenanceServiceImpl}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 2.4.0
 */
public class DatabaseMaintenanceServiceImpl implements DatabaseMaintenanceService {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(DatabaseMaintenanceServiceImpl.class);

    protected static final String CREATE_DB = "CREATE DATABASE `?` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;";

    private final GuardDatabaseService guardDatabaseService;

    private final GuardConfigurationService guardConfigurationService;

    private final AssignmentFactory assignmentFactory;

    public DatabaseMaintenanceServiceImpl(GuardDatabaseService guardDatabaseService, GuardConfigurationService guardConfigurationService, AssignmentFactory assignmentFactory) {
        this.guardConfigurationService = guardConfigurationService;
        this.guardDatabaseService = guardDatabaseService;
        this.assignmentFactory = assignmentFactory;
    }

    @Override
    public void register(SchemaCandidate candidate) throws OXException {
        Connection connection = null;
        try {
            connection = guardDatabaseService.getWritableForConfigDB();
            connection.setAutoCommit(false);

            int writePoolId = addToDbPool(connection, candidate.getSchemaName(), candidate.getMasterConnectionUrl());
            int readPoolId = 0;
            if (candidate.hasSlave()) {
                readPoolId = addToDbPool(connection, candidate.getSchemaName(), candidate.getSlaveConnectionUrl());
            }

            addToDbCluster(connection, readPoolId, writePoolId);

            connection.commit();
        } catch (SQLException e) {
            LOG.error("Error while adding database to pool or cluster. Candidate {} will be skipped.", candidate.toString(), e);
            throw GuardDatabaseExceptionCodes.DB_REGISTER_ERROR.create(e, candidate.getSchemaName());
        } finally {
            DBUtils.autocommit(connection);
            guardDatabaseService.backWritableForConfigDB(connection);
        }
        guardDatabaseService.reloadAssignments();
    }

    private int addToDbPool(Connection configdbConnection, String schemaName, String connectionUrl) throws SQLException {
        String mysqlPassword = this.guardConfigurationService.getProperty(GuardProperty.databasePassword);
        String mysqlUsername = this.guardConfigurationService.getProperty(GuardProperty.databaseUsername);

        PreparedStatement prep = null;
        int db_id = -1;
        try {
            db_id = nextId(configdbConnection);

            prep = configdbConnection.prepareStatement("INSERT INTO db_pool VALUES (?,?,?,?,?,?,?,?,?);");
            prep.setInt(1, db_id);
            prep.setString(2, connectionUrl);
            prep.setString(3, com.openexchange.database.DatabaseConstants.DRIVER);
            prep.setString(4, mysqlUsername);
            prep.setString(5, mysqlPassword);
            prep.setInt(6, 1);
            prep.setInt(7, 100);
            prep.setInt(8, 0);
            prep.setString(9, schemaName);
            prep.executeUpdate();
            prep.close();
        } finally {
            Databases.closeSQLStuff(prep);
        }
        return db_id;
    }

    private void addToDbCluster(Connection configdbConnection, int readPoolId, int writePoolId) throws SQLException {
        PreparedStatement prep = null;
        try {
            final int c_id = nextId(configdbConnection);
            prep = configdbConnection.prepareStatement("INSERT INTO db_cluster VALUES (?,?,?,?,?);");
            prep.setInt(1, c_id);
            prep.setInt(2, readPoolId);
            prep.setInt(3, writePoolId);
            prep.setInt(4, 100);
            prep.setInt(5, 0);
            prep.executeUpdate();
            prep.close();
        } finally {
            Databases.closeSQLStuff(prep);
        }
    }

    private int nextId(final Connection con) throws SQLException {
        boolean rollback = false;
        try {
            // BEGIN
            con.setAutoCommit(false);
            rollback = true;
            // Acquire next available identifier
            final int id = IDGenerator.getId(con);
            // COMMIT
            con.commit();
            rollback = false;
            return id;
        } finally {
            if (rollback) {
                Databases.rollback(con);
            }
        }
    }

    private boolean assignmentExists(String schemaName) {
        return assignmentFactory.get(schemaName) != null;
    }

    @Override
    public void createSchema(SchemaCandidate candidate) throws OXException {
        PreparedStatement statement = null;
        try {
            // the mysql user requires appropriate privileges on the desired database
            statement = candidate.getMasterConnection().prepareStatement(CREATE_DB.replace("?", candidate.getSchemaName()));
            statement.execute();
            LOG.info("Created master schema {} with connection param {}.", candidate.getSchemaName(), candidate.getMasterConnectionUrl());
        } catch (final SQLException sqlException) {
            if (sqlException.getMessage().contains("Access denied for user")) {
                String dbUser = Services.getService(GuardConfigurationService.class).getProperty(GuardProperty.databaseUsername);
                LOG.error("Open-Xchange server is unable to create the new database {} for user {} due to missing privileges. Add required privileges to the mysql user and start again. For more information have a look at http://oxpedia.org/wiki/index.php?title=AppSuite:DB_user_privileges", candidate.getSchemaName(), dbUser, sqlException);
                throw GuardDatabaseExceptionCodes.MISSING_PRIVILEGES_ERROR.create(candidate.getSchemaName(), dbUser, sqlException);
            }
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(sqlException, sqlException.getMessage());
        } finally {
            Databases.closeSQLStuff(statement);
        }
    }

    @Override
    public boolean exists(Connection connection, String schemaName) throws OXException {
        LOG.debug("Check existence of guard db named {}.", schemaName);

        ResultSet resultSet = null;
        try {
            Class.forName(com.openexchange.database.DatabaseConstants.DRIVER);

            resultSet = connection.getMetaData().getCatalogs();

            while (resultSet.next()) {
                String databaseName = resultSet.getString(1);
                if (databaseName.equals(schemaName)) {
                    return true;
                }
            }
        } catch (SQLException | ClassNotFoundException e) {
            LOG.error("An error occurred while checking the guard db '{}'", schemaName, e);
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            Databases.closeSQLStuff(resultSet);
        }
        return false;
    }

    @Override
    public void createAndRegisterSchemaIfNotExistent(SchemaCandidate... infos) throws OXException {
        for (SchemaCandidate info : infos) {
            String schemaName = info.getSchemaName();
            Connection connection = info.getMasterConnection(); // use master connection to check the existence
            if (!exists(connection, schemaName)) {
                createSchema(info);
                if(!assignmentExists(schemaName)) {
                    register(info);
                }
                else {
                    LOG.info("Guard DB pool assignment for '{}' already exists.", schemaName);
                }
            } else {
                LOG.info("Guard db '{}' already exists.", schemaName);
            }
        }
    }
}
