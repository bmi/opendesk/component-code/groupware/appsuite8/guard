/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.database;

import java.util.List;
import com.openexchange.exception.OXException;

/**
 * {@link GuardShardingService}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public interface GuardShardingService {

    /**
     * Get the next shard number. If there is no next shard a new one will be created. The implementation should lock the tables to ensure an atomic operation
     *
     * @return the next shard number
     * @throws OXExcption
     */
    int getNextShard() throws OXException;

    /**
     * Checks if a shard exists for the given userid and contextid
     * @param userId the user ID
     * @param cid the context ID
     * @return True, if a shard exists for the given IDs, false otherwise
     * @throws OXException
     */
    boolean hasShard(int userId, int cid) throws OXException;

    /**
     * Get the shard for the specified user identifier for the specified context
     *
     * @param userId The user identifier
     * @param cid The context identifier
     * @return the shard identifier
     * @throws OXException
     */
    int getShard(int userId, int cid) throws OXException;

    /**
     * Get the shard for the specified email address
     *
     * @param email The email address
     * @return The shard identifier
     * @throws OXException
     */
    int getShard(String email) throws OXException;

    /**
     * Updates the given shard based on the definition in sharddbChangeLog.xml
     *
     * @param shardId The identifier of the shard that should be created
     * @throws OXException
     */
    void updateShardSchema(int shardId) throws OXException;

    /**
     * Updates all currently existing shard schemas based on the definition in sharddbChangeLog.xml
     *
     * @throws OXException
     */
    void updateAllShardSchemas() throws OXException;

    /**
     * Returns all guard shards that are registered within the configdb.
     *
     * @return {@link List} with the shard identifiers
     * @throws OXException
     */
    List<Integer> getAllShards() throws OXException;
}
