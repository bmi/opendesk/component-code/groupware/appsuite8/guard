/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.database.exception;

import com.openexchange.exception.Category;
import com.openexchange.exception.DisplayableOXExceptionCode;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXExceptionFactory;
import com.openexchange.exception.OXExceptionStrings;

/**
 *
 * Exception codes for guard database purposes.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 2.4.0
 */
public enum GuardDatabaseExceptionCodes implements DisplayableOXExceptionCode {

    /** Open-Xchange server is unable to create the new database \"%1$s\" for user \"%2$s\" due to missing privileges. Add required privileges to the mysql user and start again. */
    MISSING_PRIVILEGES_ERROR("Open-Xchange server is unable to create the new database \"%1$s\" for user \"%2$s\" due to missing privileges. Add required privileges to the mysql user for the given database and start again.", GuardDatabaseExceptionMessages.MISSING_PRIVILEGES_ERROR_MSG, Category.CATEGORY_ERROR, 1),

    /** Unexpected database error: \"%1$s\" */
    DB_ERROR("Unexpected database error: \"%1$s\"",GuardDatabaseExceptionMessages.DB_ERROR_MSG,  Category.CATEGORY_ERROR, 2),

    /** Unable to register new database \"%1$s\" */
    DB_REGISTER_ERROR("Unable to register new database \"%1$s\"", GuardDatabaseExceptionMessages.DB_REGISTER_ERROR_MSG, Category.CATEGORY_ERROR, 3),

    /** Unable to retrieve a database connection to url \"%1$s\". Please check configured database host, user and password. */
    CONNECTION_RETRIEVAL_ERROR("Unable to retrieve a database connection to url '%1$s'. Please check configured database host, user and password.", GuardDatabaseExceptionMessages.CONNECTION_RETRIEVAL_ERROR_MSG, Category.CATEGORY_ERROR, 4),

    /** No database assignment available for the schema with name '%1$s' */
    ASSIGNMENT_NOT_AVAILABLE_FOR_SCHEMA("No database assignment available for the schema with name '%1$s'", GuardDatabaseExceptionMessages.ASSIGNMENT_NOT_AVAILABLE_FOR_SCHEMA_MSG, Category.CATEGORY_ERROR, 5),

    /** Guest with email address '%1$s' is not assigned to a shard. */
    GUEST_NOT_ASSIGNED_TO_SHARD("Guest with email address '%1$s' is not assigned to a shard.", GuardDatabaseExceptionMessages.GUEST_NOT_ASSIGNED_TO_SHARD_MSG, Category.CATEGORY_ERROR, 6),

    /** Guest with user id '%1$s' in context '%2$s' is not assigned to a shard. */
    GUEST_WITH_IDS_NOT_ASSIGNED_TO_SHARD("Guest with user id '%1$s' in context '%2$s' is not assigned to a shard.", GuardDatabaseExceptionMessages.GUEST_WITH_IDS_NOT_ASSIGNED_TO_SHARD_MSG, Category.CATEGORY_ERROR, 7),

    ;

    private final String message;
    private final String displayMessage;
    private final Category category;
    private final int number;

    /**
     * Default constructor.
     *
     * @param message message.
     * @param category category.
     * @param number detail number.
     */
    private GuardDatabaseExceptionCodes(String message, Category category, int number) {
        this(message, null, category, number);
    }

    private GuardDatabaseExceptionCodes(String message, String displayMessage, Category category, int number) {
        this.message = message;
        this.displayMessage = displayMessage != null ? displayMessage : OXExceptionStrings.MESSAGE;
        this.category = category;
        this.number = number;
    }

    @Override
    public String getPrefix() {
        return "GRD-DB";
    }

    /**
     * @return the message
     */
    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getDisplayMessage() {
        return displayMessage;
    }

    /**
     * @return the category
     */
    @Override
    public Category getCategory() {
        return category;
    }

    /**
     * @return the number
     */
    @Override
    public int getNumber() {
        return number;
    }

    @Override
    public boolean equals(final OXException e) {
        return OXExceptionFactory.getInstance().equals(this, e);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @return The newly created {@link OXException} instance
     */
    public OXException create() {
        return OXExceptionFactory.getInstance().create(this, new Object[0]);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(final Object... args) {
        return OXExceptionFactory.getInstance().create(this, (Throwable) null, args);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param cause The optional initial cause
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(final Throwable cause, final Object... args) {
        return OXExceptionFactory.getInstance().create(this, cause, args);
    }
}
