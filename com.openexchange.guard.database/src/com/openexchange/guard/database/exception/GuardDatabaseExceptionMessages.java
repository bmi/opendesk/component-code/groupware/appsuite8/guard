/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.database.exception;

import com.openexchange.i18n.LocalizableStrings;

/**
 * {@link GuardDatabaseExceptionMessages}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardDatabaseExceptionMessages  implements LocalizableStrings {

    // Technical error.  Unlikely to be displayed
    public static final String MISSING_PRIVILEGES_ERROR_MSG = "Open-Xchange server is unable to create the new database \"%1$s\" for user \"%2$s\" due to missing privileges. Add required privileges to the mysql user for the given database and start again.";
    // Unexpected database error followed by error message
    public static final String DB_ERROR_MSG = "Unexpected database error: \"%1$s\"";
    // Technical error
    public static final String DB_REGISTER_ERROR_MSG = "Unable to register new database \"%1$s\"";
    // Technical error. Unable to retrieve a database connection
    public static final String CONNECTION_RETRIEVAL_ERROR_MSG = "Unable to retrieve a database connection to url '%1$s'. Please check configured database host, user and password.";
    // Technical error.  No database assigned for the schema named %1
    public static final String ASSIGNMENT_NOT_AVAILABLE_FOR_SCHEMA_MSG = "No database assignment available for the schema with name '%1$s'";
    // Administrative error. The Guest does not have a database shard assigned
    public static final String GUEST_NOT_ASSIGNED_TO_SHARD_MSG = "Guest with email address '%1$s' is not assigned to a shard.";
    // Administrative error.  The guest with the userid and context does not have a database shard assigned
    public static final String GUEST_WITH_IDS_NOT_ASSIGNED_TO_SHARD_MSG = "Guest with user id '%1$s' in context '%2$s' is not assigned to a shard.";

    private GuardDatabaseExceptionMessages() {}
}
