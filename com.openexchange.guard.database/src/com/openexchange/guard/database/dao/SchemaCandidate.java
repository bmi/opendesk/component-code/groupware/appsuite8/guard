/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.database.dao;

import java.sql.Connection;
import java.sql.SQLException;
import org.apache.commons.lang.Validate;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.java.Strings;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.exception.GuardDatabaseExceptionCodes;
import com.openexchange.guard.database.utils.DBUtils;

/**
 * The {@link SchemaCandidate} defines serves as a container for schemas that should be created. In addition to a master connection the {@link SchemaCandidate} can also create related slave schema with providing its parameters.
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public class SchemaCandidate {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(SchemaCandidate.class);

    /**
     * The connection to create the schema for
     */
    private Connection masterConnection;
    /**
     * The connection URL that should be registered
     */
    private String masterConnectionUrl;

    /**
     * The connection URL that should be registered for related slave schema
     */
    private final String slaveConnectionUrl;

    /**
     * The schema name that should be registered
     */
    private String schemaName;

    private boolean committed = false;

    private final GuardDatabaseService guardDatabaseService;

    /**
     * Initializes a new {@link SchemaCandidate}.
     * 
     * @param masterConnection Connection to create the schema
     * @param schemaName Name of the schema to create
     * @param slaveHost (Optional) the host of an optional slave. Only if also a slave schema should get created.
     */
    public SchemaCandidate(GuardDatabaseService guardDatabaseService, Connection masterConnection, String schemaName, String slaveHost) {
        Validate.notNull(guardDatabaseService, "GuardDatabaseService cannot be null!");
        Validate.notNull(masterConnection, "Connection for master cannot be null!");
        Validate.notNull(schemaName, "Schema name cannot be null!");

        this.guardDatabaseService = guardDatabaseService;
        if (Strings.isEmpty(slaveHost)) {
            this.slaveConnectionUrl = null;
        } else {
            String guardSlaveReadUrl = guardDatabaseService.getConnectionUrl(slaveHost);
            this.slaveConnectionUrl = guardSlaveReadUrl;
        }
        try {
            this.masterConnection = masterConnection;
            this.masterConnectionUrl = masterConnection.getMetaData().getURL();
            this.schemaName = schemaName;
        } catch (SQLException e) {
            // should not happen ... but nevertheless log it
            LOG.error("Error while trying to receive connection url...", e);
        }
    }

    public Connection getMasterConnection() {
        return masterConnection;
    }

    public String getMasterConnectionUrl() {
        return masterConnectionUrl;
    }

    public String getSlaveConnectionUrl() {
        return slaveConnectionUrl;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public boolean hasSlave() {
        return this.slaveConnectionUrl != null;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Master connection: " + this.masterConnectionUrl + ", ");
        builder.append("Master schema name: " + this.schemaName + ", ");
        builder.append("hasSlave: " + this.hasSlave());
        if (this.hasSlave()) {
            builder.append(", Slave connection url: " + this.slaveConnectionUrl);
        }
        return builder.toString();
    }

    /**
     * Starts the transaction on the underlying connection in case the connection is owned by this instance.
     */
    public void start() throws OXException {
        try {
            DBUtils.startTransaction(masterConnection);
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Commits the transaction on the underlying connection in case the connection is owned by this instance.
     */
    public void commit() throws OXException {
        try {
            masterConnection.commit();
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        }
        committed = true;
    }

    /**
     * Backs the underlying connection in case the connection is owned by this instance, rolling back automatically if not yet committed.
     */
    public void finish() {
        if (false == committed) {
            DBUtils.rollback(masterConnection);
        }
        DBUtils.autocommit(masterConnection);

        guardDatabaseService.backWritableForInit(masterConnection);
    }
}
