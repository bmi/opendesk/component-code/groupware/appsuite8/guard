/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.updater;

import java.sql.Connection;
import java.sql.SQLException;
import com.openexchange.database.DatabaseService;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.update.PerformParameters;
import com.openexchange.groupware.update.UpdateExceptionCodes;
import com.openexchange.groupware.update.UpdateTaskAdapter;
import com.openexchange.guard.database.update.GuardCreateTableTask;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.tools.update.Tools;

/**
 * {@link ChangeOGKeyTableSizes}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 2.4.0
 */
public class ChangeOGKeyTableSizes extends UpdateTaskAdapter {

    public ChangeOGKeyTableSizes() {
        super();
    }

    @Override
    public void perform(PerformParameters params) throws OXException {
        final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ChangeOGKeyTableSizes.class);
        log.info("Performing update task {}", ChangeOGKeyTableSizes.class.getSimpleName());

        Connection con = params.getConnection();
        boolean rollback = false;
        try {
            con.setAutoCommit(false);
            rollback = true;
            
            Tools.changeVarcharColumnSize("RSAPrivate", 6000, "og_KeyTable", con);
            Tools.changeVarcharColumnSize("RSAPublic", 2000, "og_KeyTable", con);
            Tools.changeVarcharColumnSize("Recovery", 2000, "og_KeyTable", con);

            con.commit();
            rollback = false;
        } catch (final SQLException e) {
            throw UpdateExceptionCodes.SQL_PROBLEM.create(e, e.getMessage());
        } catch (final Exception e) {
            throw UpdateExceptionCodes.OTHER_PROBLEM.create(e, e.getMessage());
        } finally {
            if (rollback) {
                DBUtils.rollback(con);                
            }
            DBUtils.autocommit(con);
        }
        log.info("{} successfully performed.", ChangeOGKeyTableSizes.class.getSimpleName());
    }

    @Override
    public String[] getDependencies() {
        return new String[] { GuardCreateTableTask.class.getName() };
    }
}
