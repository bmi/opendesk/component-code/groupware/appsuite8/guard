/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.cipher.dataobject;

/**
 * {@link DecodedObject}
 */
public class DecodedObject {

    private String decoded64;

    private byte[] decodedBytes;

    private String type;

    /**
     * Initialises a new {@link DecodedObject}.
     */
    public DecodedObject() {
        super();
    }

    /**
     * Gets the decoded64
     *
     * @return The decoded64
     */
    public String getDecoded64() {
        return decoded64;
    }

    /**
     * Sets the decoded64
     *
     * @param decoded64 The decoded64 to set
     */
    public void setDecoded64(String decoded64) {
        this.decoded64 = decoded64;
    }

    /**
     * Gets the decodedBytes
     *
     * @return The decodedBytes
     */
    public byte[] getDecodedBytes() {
        return decodedBytes;
    }

    /**
     * Sets the decodedBytes
     *
     * @param decodedBytes The decodedBytes to set
     */
    public void setDecodedBytes(byte[] decodedBytes) {
        this.decodedBytes = decodedBytes;
    }

    /**
     * Gets the type
     *
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type
     *
     * @param type The type to set
     */
    public void setType(String type) {
        this.type = type;
    }
}
