/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.cipher.internal;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.cipher.exceptions.GuardCipherExceptionCodes;
import com.openexchange.guard.cipher.osgi.Services;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;

/**
 * {@link AbstractGuardCipherService}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
abstract class AbstractGuardCipherService implements GuardCipherService {

    protected final GuardCipherAlgorithm cipherAlgorithm;

    /**
     * Characters for random password. Excluding 'O' and 'I' for now
     */
    private static final String passChars = "0123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghjklmnpqrstuvwxyz";

    /**
     * Initialises a new {@link AbstractGuardCipherService}.
     *
     * @param cipherAlgorithm The cipher algorithm
     * @throws OXException If the specified cipher algorithm does not exist, or if the key length of the specified algorithm exceeds the maximum defined
     */
    public AbstractGuardCipherService(GuardCipherAlgorithm cipherAlgorithm) throws OXException {
        super();

        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        int keyLength = configService.getIntProperty(cipherAlgorithm.getKeyLengthPropertyName());
        try {
            int maxAllowedKeyLength = Cipher.getMaxAllowedKeyLength(cipherAlgorithm.name());
            if (keyLength > maxAllowedKeyLength) {

                org.slf4j.LoggerFactory.getLogger(AbstractGuardCipherService.class).error(
                    "The configured key length of {} for algorithm \"{}\" exceeds the maximum allowed keylength of {} for this system and OX Guard will not function properly. " +
                    "This might be due to legal restrictions in your country. Please check the JCE policy file.",
                    Integer.valueOf(keyLength),
                    cipherAlgorithm.getAlgorithmName(),
                    Integer.valueOf(maxAllowedKeyLength));
                throw GuardCipherExceptionCodes.INVALID_KEY_LENGTH.create(cipherAlgorithm.getTransformation(), Integer.valueOf(keyLength), Integer.valueOf(maxAllowedKeyLength));
            }
        } catch (NoSuchAlgorithmException e) {
            throw GuardCipherExceptionCodes.UNKNOWN_CIPHER_ALGORITHM.create(e, cipherAlgorithm.getTransformation());
        }

        this.cipherAlgorithm = cipherAlgorithm;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.crypto.internal.GuardCryptoService#generateRandomPassword()
     */
    @Override
    public String generateRandomPassword(int userId, int cid) throws OXException {
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        int length = configService.getIntProperty(GuardProperty.newPassLength, userId, cid);

        SecureRandom random = new SecureRandom();
        StringBuilder newPassword = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            newPassword.append(passChars.charAt(random.nextInt(passChars.length())));
        }

        return newPassword.toString();
    }

    /**
     * Get the Cipher
     *
     * @return The cipher
     * @throws OXException
     */
    protected Cipher getCipher() throws OXException {
        try {
            return Cipher.getInstance(cipherAlgorithm.getTransformation(), cipherAlgorithm.getProvider());
        } catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException e) {
            throw GuardCipherExceptionCodes.UNKNOWN_CIPHER_ALGORITHM.create(e, cipherAlgorithm.getTransformation());
        }
    }

    /**
     * Get the type of cipher from the AES string
     * data String expected #int#IV!data where int specifies GuardCipherAlg
     * getDecrCipher
     *
     * @param data AES data string with
     * @return Cipher
     * @throws OXException
     */
    protected Cipher getDecrCipher(String data) throws OXException {
        if (data != null && data.startsWith("#")) {
            Integer type = null;
            try {
                type = Integer.valueOf(data.substring(1, 2));
                for (GuardCipherAlgorithm alg : GuardCipherAlgorithm.values()) {
                    if (type.intValue() == alg.getValue()) {
                        return Cipher.getInstance(alg.getTransformation(), alg.getProvider());
                    }
                }
            } catch (Exception e) {
                if(type == null) {
                    throw GuardCipherExceptionCodes.UNKNOWN_CIPHER_ALGORITHM.create(e, "unknown");
                }
                throw GuardCipherExceptionCodes.UNKNOWN_CIPHER_ALGORITHM.create(e, type);
            }
        }
        return getCipher();
    }
}
