package com.openexchange.guard.pgpcore.services.osgi;

import com.openexchange.osgi.HousekeepingActivator;

public class PGPCoreServicesActivator extends HousekeepingActivator {

    /* (non-Javadoc)
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] {};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void startBundle() throws Exception {
        org.slf4j.LoggerFactory.getLogger(PGPCoreServicesActivator.class).info("Starting bundle: {}", context.getBundle().getSymbolicName());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void stopBundle() throws Exception {
        org.slf4j.LoggerFactory.getLogger(PGPCoreServicesActivator.class).info("Stopping bundle: {}", this.context.getBundle().getSymbolicName());
        super.stopBundle();
    }
    
}