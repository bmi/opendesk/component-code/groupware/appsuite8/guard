/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services.exceptions;

import com.openexchange.i18n.LocalizableStrings;

/**
 * {@link PGPCoreServicesExceptionMessages}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public final class PGPCoreServicesExceptionMessages implements LocalizableStrings {

    // Unable to retrieve recipients key, with specified key id(s)
    public static final String RECIPIENT_KEY_NOT_FOUND = "Unable to retrieve recipient's key (%1$s)";
    // Unable to retrieve signing key, with key Ids or email addresses in variable
    public static final String SIGNING_KEY_NOT_FOUND = "Unable to retrieve signing key (%1$s)";
    // Encryption error with error appended
    public static final String ENCRYPTION_ERROR = "An PGP encryption error occured: %1$s";
    // Decryption error with error appended
    public static final String DECRYPTION_ERROR = "An PGP decryption error occured: %1$s";
    // Signing error with error appended
    public static final String SIGNING_ERROR = "An PGP signing error occured: %1$s";
    // Signature verification error with error appended
    public static final String SIGNATURE_VERIFICATION_ERROR = "An PGP signature verification error occured: %1$s";
    // Unexpected error with error appended
    public static final String UNEXPECTED_ERROR = "Unexpected error occured: %1$s";
    // Unable to parse recipient address(es)
    public static final String RECIPIENT_PARSING_ERROR = "Unable to parse the given recipient (%1$s)";
    // The email message is not a proper multipart message (type of email)
    public static final String NOT_A_MULTIPART_MSG_ERROR = "The provided message is not a proper multipart message";
    // Signature found, but the data it was signing wasn't found
    public static final String SIGNATURE_ERROR_NO_SIGNED_DATA_MSG  = "No signed data found in email";
    // Unable to find a signature in the email
    public static final String SIGNATURE_ERROR_NO_SIGNATURE_MSG  = "No signature found in email";

    private PGPCoreServicesExceptionMessages() {}
}
