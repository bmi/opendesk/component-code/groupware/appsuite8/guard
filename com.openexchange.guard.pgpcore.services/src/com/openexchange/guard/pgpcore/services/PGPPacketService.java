/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services;

import java.io.InputStream;
import java.util.Collection;
import com.openexchange.exception.OXException;

/**
 * {@link PGPPacketService} is a high level service for working with Open PGP packets as described in RFC 4880 - "OpenPGP Message Format".
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.0
 * @see <a>https://tools.ietf.org/html/rfc4880</a>
 */
public interface PGPPacketService {

    /**
     * Extracts the encrypted session packets (5.1. Public-Key Encrypted Session Key Packets, Tag 1) from a given PGP Stream.
     *
     * @param pgpStream A pgp stream to extract the session key packets from.
     * @return A collection of encrypted session packets in byte representation.
     * @throws OXException
     */
    Collection<byte[]> getEncrytedSessions(InputStream pgpStream) throws OXException;
}
