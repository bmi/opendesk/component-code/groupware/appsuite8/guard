/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services;

import java.util.Collection;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.core.PGPSignatureVerificationResult;

/**
 * {@link PGPKeySigningService}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public interface PGPKeySigningService {

    /**
     * Verifies all signatures of the given key ring.
     *
     * @param verifier The {@link UserIdentiy} which wants to verify the given key
     * @param keys The key ring containing the signatures to verify.
     * @return A collection of verification results.
     * @throws OXException
     */
    Collection<PGPSignatureVerificationResult> verifyKey(UserIdentity verifier, GuardKeys keys) throws OXException;

    /**
     * Verifies all signatures of the given key ring.
     *
     * @param verifier The {@link UserIdentiy} which wants to verify the given key
     * @param keyRing The key ring containing the signatures to verify.
     * @return A collection of verification results.
     * @throws OXException
     */
    Collection<PGPSignatureVerificationResult> verifyKey(UserIdentity verifier, PGPPublicKeyRing keyRing) throws OXException;

    /**
     * Verifies all signatures of the given public key.
     *
     * @param verifier The {@link UserIdentiy} which wants to verify the given key
     * @param key The containing the signatures to verify.
     * @return A collection of verification results.
     * @throws OXException
     */
    Collection<PGPSignatureVerificationResult> verifyKey(UserIdentity verifier, PGPPublicKey key) throws OXException;
}
