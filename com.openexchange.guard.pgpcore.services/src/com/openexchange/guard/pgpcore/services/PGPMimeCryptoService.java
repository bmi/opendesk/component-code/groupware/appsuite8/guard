/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services;

import javax.mail.internet.MimeMessage;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link PGPMimeCryptoService} an convenience Service which provides PGP/Mime encryptiona and decryption.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public interface PGPMimeCryptoService {

    /**
     * Decrypts the given MimeMessage.
     *
     * @param mimeMessage The MimeMessage to decrypt.
     * @param user The user who wants to decrypt the message.
     * @return The decrypted {@link PGPMimeDecryptionResult}
     */
    PGPMimeDecryptionResult decrypt(MimeMessage mimeMessage, UserIdentity user) throws OXException;

    /**
     * Decrypts the given MimeMessage
     *
     * @param mimeMessage The MimeMessge to decrypt
     * @param key The key to use for decrypting the MimeMessage
     * @param password The password associated with the given key
     * @return The decrypted {@link PGPMimeDecryptionResult}
     * @throws OXException
     */
    PGPMimeDecryptionResult decrypt(MimeMessage mimeMessage, GuardKeys key, String password) throws OXException;

}
