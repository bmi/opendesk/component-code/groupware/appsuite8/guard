/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.impl.comparators;

import java.util.HashMap;
import java.util.Map;
import javax.mail.internet.MimeMessage.RecipientType;
import com.openexchange.guard.guest.MailSortField;

/**
 * {@link ComparatorRegistry} manages a collection of available {@link GuardGuestEmailComparator} instances.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class ComparatorRegistry {

    private static final Map<MailSortField, GuardGuestEmailComparator> COMPARATORS;

    /**
     * A default {@link MailSortField} to use
     */
    public static final MailSortField DEFAULT_SORT_FIELD = MailSortField.SENT_DATE;

    static {
        COMPARATORS = new HashMap<MailSortField, GuardGuestEmailComparator>();
        COMPARATORS.put(MailSortField.RECEIVED_DATE, new ReceivedDateComparator());
        COMPARATORS.put(MailSortField.FROM, new FromComparator());
        COMPARATORS.put(MailSortField.TO, new RecipientComparator(RecipientType.TO));
        COMPARATORS.put(MailSortField.CC, new RecipientComparator(RecipientType.CC));
        COMPARATORS.put(MailSortField.SUBJECT, new SubjectComparator());
        COMPARATORS.put(MailSortField.SIZE, new SizeComparator());
        COMPARATORS.put(MailSortField.SENT_DATE, new SentDateComparator());
        COMPARATORS.put(MailSortField.FLAG_SEEN, new SeenComparator());
        COMPARATORS.put(MailSortField.COLOR_LABEL, new ColorLabelComparator());
    }

    /**
     * Gets a registered comparator for the given {@link MailSortField}
     *
     * @param sortField The {@link MailSortField} to get a comparator for
     * @return The comparator registered for the given sort field, or null if no comparator has been registered for the given sort field.
     */
    public static GuardGuestEmailComparator getComparatorFor(MailSortField sortField) {
        return COMPARATORS.get(sortField);
    }

}
