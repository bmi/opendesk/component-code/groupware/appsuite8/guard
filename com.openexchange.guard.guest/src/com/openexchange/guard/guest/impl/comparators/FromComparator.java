/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.impl.comparators;

import com.openexchange.guard.guest.GuardGuestEmail;

/**
 * {@link FromComparator} compares the from addresses of the given E-Mails.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class FromComparator extends GuardGuestEmailComparator {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 5651347330791940619L;

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.impl.comparators.GuardGuestEmailComparator#compareInternal(com.openexchange.guard.guest.GuardGuestEmail, com.openexchange.guard.guest.GuardGuestEmail)
     */
    @Override
    public int compareEmails(GuardGuestEmail arg0, GuardGuestEmail arg1) {
        String[] from0 = arg0.getMetaData().getFrom();
        String[] from1 = arg1.getMetaData().getFrom();

        if (checkForNull(from0, from1)) {
            return compareNull(from0, from1);
        }

        if (from0.length == 0 && from1.length == 0) {
            return 0;
        } else if (from0.length == 0) {
            return 1;
        } else if (from1.length == 0) {
            return -1;
        } else {
            //Only considering the first sender
            return from0[0].compareTo(from1[0]);
        }
    }
}
