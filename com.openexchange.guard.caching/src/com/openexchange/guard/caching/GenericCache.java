/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.caching;

import java.io.Serializable;
import com.openexchange.exception.OXException;

/**
 * {@link GenericCache} - a generic caching service
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 * @param <T> The type of the objects to cache
 * @see {@link GenericCacheFactory} for creating {@link GenericCache} instances.
 */
public interface GenericCache<T extends Serializable> {

    /**
     * Helper method to create a concatenated key of multiple key-items
     *
     * @param keyItems The items to concatenate for a key
     * @return A concatenated key
     */
    default public Serializable createKey(Serializable... keyItems) {
        StringBuilder key = new StringBuilder();
        for (Serializable item : keyItems) {
            key.append(item);
            key.append("_");
        }
        return key.toString();
    }

    /**
     * Gets a specific item from the cache
     *
     * @key The key related to the item to get from the cache
     * @return The item with the given ID, or null if no such item exists within the cache
     * @throws OXException
     */
    public T get(Serializable key) throws OXException;

    /**
     * Gets a specific array from the cache
     *
     * @key The key related to the item to get from the cache
     * @return The array with the given ID, or null if no such array exists within the cache
     * @throws OXException
     */
    public T[] getArray(Serializable key) throws OXException;

    /**
     * Caches an item
     *
     * @key The key related to the item to put into the cache
     * @param object The item to cache
     * @throws OXException
     */
    public void put(Serializable key, T object) throws OXException;

    /**
     * Caches an array
     *
     * @key The key related to the array to put into the cache
     * @param array The array to cache
     * @throws OXException
     */
    public void putArray(Serializable key, T[] array) throws OXException;

    /**
     * Removes a specific item from the cache
     *
     * @key The key related to the item to remove from the cache
     * @throws OXException
     */
    public void remove(Serializable key) throws OXException;

    /**
     * Clears the whole cache
     *
     * @throws OXException
     */
    public void clear() throws OXException;
}
