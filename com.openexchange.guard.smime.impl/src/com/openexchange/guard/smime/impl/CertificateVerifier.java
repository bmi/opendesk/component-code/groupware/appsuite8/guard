/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl;

import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.util.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.smime.CertificateService;
import com.openexchange.guard.smime.CertificateVerificationResult;
import com.openexchange.guard.smime.CertificateVerificationResult.Result;
import com.openexchange.guard.smime.SmimeProperty;
import com.openexchange.guard.smime.exceptions.SmimeExceptionCodes;
import com.openexchange.guard.smime.storage.CaCertStorage;
import com.openexchange.server.ServiceLookup;

/**
 * {@link CertificateVerifier}
 * Verifies certificates against chain to confirm valid
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class CertificateVerifier implements CertificateService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CertificateVerifier.class);

    private final ServiceLookup services;

    /**
     * Initializes a new {@link CertificateVerifier}.
     *
     * @param services The service lookup
     */
    public CertificateVerifier(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Check revocation lists of the certificates.
     * Throw an error if any are revoked
     *
     * @param cert The primary user certs
     * @param additionalCerts Chain
     */
    private void checkRevokation(X509Certificate cert, Set<X509Certificate> additionalCerts) throws OXException {
        final CRLChecker crlChecker = CRLChecker.getInstance();
        if (crlChecker.checkCertificateRevoked(cert, additionalCerts)) {
            throw SmimeExceptionCodes.CERTIFICATE_REVOKED.create();
        }
        final Set<X509Certificate> addCerts = new HashSet<X509Certificate>(additionalCerts);
        for (final X509Certificate addCert : additionalCerts) {
            addCerts.remove(addCert);
            if (crlChecker.checkCertificateRevoked(addCert, addCerts)) {
                throw SmimeExceptionCodes.CERTIFICATE_REVOKED.create();
            }
        }
    }

    @Override
    public CertificateVerificationResult verify(Store<X509CertificateHolder> certificatesStore, X509CertificateHolder certFromSignedData, Date signDate, int userId, int contextId) throws Exception {
        Collection<X509CertificateHolder> certificateHolders = certificatesStore.getMatches(null);
        Set<X509Certificate> additionalCerts = new HashSet<>();
        JcaX509CertificateConverter certificateConverter = new JcaX509CertificateConverter();
        X509Certificate signingCert = certificateConverter.getCertificate(certFromSignedData);
        for (X509CertificateHolder certHolder : certificateHolders) {
            X509Certificate certificate = certificateConverter.getCertificate(certHolder);
            if (null != certificate && !certificate.equals(signingCert)) {
                additionalCerts.add(certificate);
            }
        }

        SmimeKeys keys = new SmimeKeys(certificateConverter.getCertificate(certFromSignedData), new ArrayList<X509Certificate>(additionalCerts), null, userId, contextId, signDate.getTime());
        GuardConfigurationService configService = services.getServiceSafe(GuardConfigurationService.class);
        // Check against revoke lists if configured to do so
        if (configService.getBooleanProperty(SmimeProperty.checkCRL, userId, contextId)) {
            checkRevokation(keys.getCertificate(), additionalCerts);
        }
        Result result = verify(keys.getCertificate(), additionalCerts, signDate, userId, contextId);
        return new CertificateVerificationResult(keys, result);
    }

    @Override
    public Result verify(X509Certificate certToCheck, Set<X509Certificate> additionalCerts, Date date, int userId, int cid) throws Exception {
        final Iterator<X509Certificate> it = additionalCerts.iterator();
        certToCheck.checkValidity(date);
        while (it.hasNext()) {
            final X509Certificate additCert = it.next();
            // Certificate signs self.  Possible root CA
            if (certToCheck.getIssuerX500Principal().equals(certToCheck.getSubjectX500Principal())) {
                LOGGER.debug("Found self-signed certificate. Going to check if this is a root CA");
                certToCheck.verify(certToCheck.getPublicKey());
                if (checkRoot(certToCheck, userId, cid)) {
                    LOGGER.debug("Self-signed certificate is a root CA");
                    return Result.VERIFIED;
                }
                LOGGER.debug("Found self-signed, non root CA certificate.");
                return Result.NOT_TRUSTED;
            }
            // Signed by another cert.  Verify it up the chain
            if (certToCheck.getIssuerX500Principal().equals(additCert.getSubjectX500Principal())) {
                certToCheck.verify(additCert.getPublicKey());
                return verify(additCert, additionalCerts, date, userId, cid);
            }

        }
        // Ran out of certificates to check against.  See if this is a root signed CA
        if (checkRoot(certToCheck, userId, cid)) {
            return Result.VERIFIED;
        }
        return Result.INCOMPLETE;
    }

    /**
     *
     * Check cert to see if it is a trusted root certificate
     *
     * @param cert
     * @param userId id of the user
     * @param cid Context id of the user
     * @return True if valid
     * @throws NoSuchAlgorithmException
     * @throws KeyStoreException
     * @throws OXException
     */
    private boolean checkRoot(X509Certificate cert, int userId, int cid) throws NoSuchAlgorithmException, KeyStoreException, OXException {
        final TrustManagerFactory tmfactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmfactory.init((KeyStore) null);
        for (final TrustManager trustManager : tmfactory.getTrustManagers()) {
            if (trustManager instanceof X509TrustManager) {
                try {
                    ((X509TrustManager) trustManager).checkClientTrusted(new X509Certificate[] { cert }, cert.getSigAlgName());
                    return true;
                } catch (final CertificateException e) {
                    // Nothing to do.  Failed to verify
                    LOGGER.debug("Failed to verify", e);
                }
                final X509Certificate[] caCerts = ((X509TrustManager) trustManager).getAcceptedIssuers();
                for (final X509Certificate caCert : caCerts) {
                    if (cert.getIssuerX500Principal().equals(caCert.getSubjectX500Principal())) {
                        try {
                            cert.verify(caCert.getPublicKey());
                            return true;
                        } catch (InvalidKeyException | CertificateException | NoSuchAlgorithmException | NoSuchProviderException | SignatureException e) {
                            // Nothing to do. Failed to verify
                            LOGGER.debug("Failed to verify", e);
                        }

                    }
                }
            }
        }
        // Check against any configured CAs
        final GuardConfigurationService configService = services.getServiceSafe(GuardConfigurationService.class);
        final int groupId = configService.getIntProperty(SmimeProperty.caGroupId, userId, cid);
        final CaCertStorage certStorage = services.getOptionalService(CaCertStorage.class);
        if (certStorage != null) {
            final List<X509Certificate> certs = certStorage.getCAs(groupId);
            for (final X509Certificate caCert : certs) {
                try {
                    cert.verify(caCert.getPublicKey());
                    return true;
                } catch (Exception e) {
                    // ignore
                    LOGGER.debug("Failed to verify", e);
                }
            }
        }
        return false;
    }

}
