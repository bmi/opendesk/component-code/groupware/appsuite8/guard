/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl;

import java.security.cert.X509Certificate;
import java.util.ArrayList;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.smime.SMIMECapabilitiesAttribute;
import org.bouncycastle.asn1.smime.SMIMECapabilityVector;
import org.bouncycastle.asn1.smime.SMIMEEncryptionKeyPreferenceAttribute;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoGeneratorBuilder;
import org.bouncycastle.mail.smime.SMIMESignedGenerator;
import org.bouncycastle.util.Store;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.smime.SmimeCryptoKeyUtil;

/**
 * {@link SMimeSigner}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class SMimeSigner {

    /**
     * Signs MimeBodyPart using the passed keys
     *
     * @param msg The MimeBodyPart to be signed
     * @param keys Keys used for signing
     * @param password Password to decrypt private keys for signing
     * @return Signed MimeMultipart
     * @throws Exception
     */
    public static MimeMultipart signMime(MimeBodyPart msg, SmimeKeys keys, String password) throws Exception {

        // Create cert chain that includes the public certificate plus all chain certificates.
        ArrayList<X509Certificate> fullChain = new ArrayList<X509Certificate>(keys.getChain().size() + 1);
        fullChain.addAll(keys.getChain());
        fullChain.add(keys.getCertificate());
        Store certs = new JcaCertStore(fullChain);

        ASN1EncodableVector signedAttrs = new ASN1EncodableVector();
        SMIMECapabilityVector caps = new SMIMECapabilityVector();

        caps.addCapability(NISTObjectIdentifiers.id_aes256_GCM);
        caps.addCapability(NISTObjectIdentifiers.id_aes256_CBC);
        caps.addCapability(NISTObjectIdentifiers.id_aes192_CBC);
        caps.addCapability(NISTObjectIdentifiers.id_aes128_CBC);

        signedAttrs.add(new SMIMECapabilitiesAttribute(caps));

        IssuerAndSerialNumber issAndSer = new IssuerAndSerialNumber(new X509CertificateHolder(keys.getCertificate().getEncoded()).toASN1Structure());

        signedAttrs.add(new SMIMEEncryptionKeyPreferenceAttribute(issAndSer));

        SMIMESignedGenerator gen = new SMIMESignedGenerator();

        //@formatter:off
        gen.addSignerInfoGenerator(new JcaSimpleSignerInfoGeneratorBuilder()
                                    .setProvider("BC")
                                    .setSignedAttributeGenerator(new AttributeTable(signedAttrs))
                                    .build("SHA256withRSA",
                                          SmimeCryptoKeyUtil.decryptPrivateKey(keys, password),
                                          keys.getCertificate())
                                  );
        //@formatter:on

        gen.addCertificates(certs);

        MimeMultipart signedPart = gen.generate(msg);

        return signedPart;

    }

}
