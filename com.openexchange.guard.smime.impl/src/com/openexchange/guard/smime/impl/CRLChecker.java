/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyStore;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.slf4j.Logger;

/**
 * {@link CRLChecker} Checks if certificate has been revoked
 * CRL URLs are checked (http only)
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class CRLChecker {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(CRLChecker.class);
    private static CRLChecker INSTANCE;
    private final Map<String, CRLCacheItem> cache;
    private static final long CACHE_LIFE = 1000 * 60 * 60 * 24;  // If not specified, cache good for one dat
    private static final long MAX_CACHE_LIFE = 1000 * 60 * 60 * 48;  // Max cache time allowed is 2 days.

    private class CRLCacheItem {

        private final X509CRL crl;
        private final Date exp;

        // Cache item for keeping local copies of CRL lists with expiration dates
        public CRLCacheItem(X509CRL crl, Date expDate) {
            final Date now = new Date();
            if (expDate == null || (expDate.getTime() > now.getTime() + MAX_CACHE_LIFE)) {
                expDate = new Date(now.getTime() + CACHE_LIFE);
            }
            this.crl = crl;
            this.exp = expDate;
        }

        public X509CRL getCRL() {
            return this.crl;
        }

        public Date getExpDate() {
            return exp;
        }
    }

    // Cleanup of cache
    void cleanCache() {
        final Date now = new Date();
        for (final Map.Entry<String, CRLCacheItem> item : cache.entrySet()) {
            if (item.getValue().getExpDate().before(now)) {
                cache.remove(item.getKey());
            }
        }
    }

    public static CRLChecker getInstance() {
        CRLChecker tmp = INSTANCE;
        if (null == tmp) {
            synchronized (CRLChecker.class) {
                tmp = INSTANCE;
                if (tmp == null) {
                    INSTANCE = tmp = new CRLChecker();
                }
            }
        }
        return tmp;
    }

    private CRLChecker() {
        // Instantiate new cache
        cache = new ConcurrentHashMap<String, CRLCacheItem>();
        // Setup cache cleaner
        final TimerTask cleanup = new TimerTask() {

            @Override
            public void run() {
                cleanCache();
            }
        };
        final Timer timer = new Timer("CRLCacheCleanup");
        timer.scheduleAtFixedRate(cleanup, CACHE_LIFE, CACHE_LIFE);
    }

    /**
     * Checks if a certificate has been revoked
     * May be signed by other certificates in the additionalCerts chain
     *
     * @param cert Cert to check
     * @param additionalCerts Addit certs that may be used to sign the CRL
     * @return True if revoked
     */
    public boolean checkCertificateRevoked(X509Certificate cert, Set<X509Certificate> additionalCerts) {
        final X509CRL crl = getCRL(cert, additionalCerts);
        if (crl != null) {
            if (crl.isRevoked(cert)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the CRL list from cache or fetch if needed
     *
     * @param cert
     * @param additionalCerts
     * @return
     */
    private synchronized X509CRL getCRL(X509Certificate cert, Set<X509Certificate> additionalCerts) {
        final String url = getURL(cert);
        if (url == null) {
            return null;
        }
        if (cache.containsKey(url)) {
            final CRLCacheItem crl = cache.get(url);
            if (crl.getExpDate().after(new Date())) {
                return crl.getCRL();
            }
            // Expired, remove from cache
            cache.remove(url);
        }
        try {
            final CertificateFactory cf = CertificateFactory.getInstance("X509");
            final URL urlAddr = new URL(url);
            final URLConnection con = urlAddr.openConnection();
            con.setConnectTimeout(2000); // make configurable
            con.setReadTimeout(2000);
            try (DataInputStream instream = new DataInputStream(con.getInputStream())) {
                final X509CRL crl = (X509CRL) cf.generateCRL(instream);
                if (crl != null) {
                    if (verifyCRL(crl, additionalCerts)) {
                        cache.put(url, new CRLCacheItem(crl, crl.getNextUpdate()));
                        return crl;
                    }
                    // not verified
                }
            } catch (CRLException | IOException e) {
                LOG.error("Error getting certificate revocation list", e);
            }
        } catch (IOException | CertificateException e) {
            LOG.error("Error getting certificate revocation list", e);
        }
        return null;

    }

    /**
     * Get the http url for the CRL list
     *
     *
     * @param cert
     * @return
     */
    private String getURL(X509Certificate cert) {
        if (null == cert) {
            return null;
        }
        try {
            final byte[] crlDistributionPointDerEncodedArray = cert.getExtensionValue(Extension.cRLDistributionPoints.getId());
            if (crlDistributionPointDerEncodedArray == null) {
                return null;
            }
            final ASN1InputStream oAsnInStream = new ASN1InputStream(new ByteArrayInputStream(crlDistributionPointDerEncodedArray));
            final ASN1Primitive derObjCrlDP = oAsnInStream.readObject();
            final DEROctetString dosCrlDP = (DEROctetString) derObjCrlDP;

            oAsnInStream.close();

            final byte[] crldpExtOctets = dosCrlDP.getOctets();
            final ASN1InputStream oAsnInStream2 = new ASN1InputStream(new ByteArrayInputStream(crldpExtOctets));
            final ASN1Primitive derObj2 = oAsnInStream2.readObject();
            final CRLDistPoint distPoint = CRLDistPoint.getInstance(derObj2);

            oAsnInStream2.close();

            final List<String> crlUrls = new ArrayList<String>();
            for (final DistributionPoint dp : distPoint.getDistributionPoints()) {
                final DistributionPointName dpn = dp.getDistributionPoint();
                // Look for URIs in fullName
                if (dpn != null) {
                    if (dpn.getType() == DistributionPointName.FULL_NAME) {
                        final GeneralName[] genNames = GeneralNames.getInstance(dpn.getName()).getNames();
                        // Look for an URI
                        for (int j = 0; j < genNames.length; j++) {
                            if (genNames[j].getTagNo() == GeneralName.uniformResourceIdentifier) {
                                final String url = DERIA5String.getInstance(genNames[j].getName()).getString();
                                crlUrls.add(url);
                            }
                        }
                    }
                }
            }
            for (final String url : crlUrls) {
                if (url.startsWith("http")) {
                    return url;
                }
            }
        } catch (final IOException e) {
            LOG.error("Error getting certificate revocation url", e);
        }
        return null;
    }

    /**
     * Verify that the crl is signed by either trusted root or in the additionalCerts list
     *
     *
     * @param crl
     * @param additionalCerts
     * @return
     */
    private boolean verifyCRL(X509CRL crl, Set<X509Certificate> additionalCerts) {
        if (null != additionalCerts) {
            for (X509Certificate add : additionalCerts) {
                try {
                    crl.verify(add.getPublicKey());
                    return true;  // Signed by one of the certs in our chain.
                } catch (Exception e) {
                    // Nothing to do, not verified.
                    LOG.debug("Verification failed", e);
                }
            }
        }
        final X500Principal principal = crl.getIssuerX500Principal();
        try {
            final TrustManagerFactory tmfactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmfactory.init((KeyStore) null);
            for (final TrustManager trustManager : tmfactory.getTrustManagers()) {
                if (trustManager instanceof X509TrustManager) {
                    final X509Certificate[] caCerts = ((X509TrustManager) trustManager).getAcceptedIssuers();
                    for (final X509Certificate caCert : caCerts) {
                        if (principal.equals(caCert.getSubjectX500Principal())) {
                            try {
                                crl.verify(caCert.getPublicKey());
                                return true;
                            } catch (final Exception e) {
                                // Nothing to do. Failed to verify
                            }
                        }
                    }
                }
            }
        } catch (final Exception e) {
            LOG.error("Error verifying certificate revocation list", e);
        }
        return false;
    }

}
