/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl.testService;

import java.io.ByteArrayInputStream;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.smime.impl.KeyImporter;
import com.openexchange.guard.smime.storage.CaCertStorage;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.encoding.Base64;

/**
 * {@link TestCAStorage}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class TestCAStorage implements CaCertStorage {

    //  THIS KEY IS FOR TESTING ONLY
    private static final String TEST_CA =
        "MIIKNgIBAzCCCfwGCSqGSIb3DQEHAaCCCe0EggnpMIIJ5TCCBH8GCSqGSIb3DQEHBqCCBHAwggRs\n"
            + "AgEAMIIEZQYJKoZIhvcNAQcBMBwGCiqGSIb3DQEMAQYwDgQIHTJIeEXkeXECAggAgIIEOGc1Yr+B\n"
            + "ivUukC2K+GIwriK5tWIefk7x9nKmTdwa+w3jYBG7t3zMVFWZhQaGDjehDgxxcqUo+lASPiTa2jeG\n"
            + "HeIHBNLieBdyiiFIeeJp/LKCedg0gvM0HAiQa829z9M4G+X955Wl3V2eeroqqQZvzo3wzymaWfGB\n"
            + "eVHENA+OboW5wGd18f8gQXuKw3aHnFToViI5MppgCjawJJd3RDIgZ61ZiWH3gQrAr4ttxzQrR562\n"
            + "qlqVzgKna8pzbISAWhXA49szUWxuYoBv2BE0RS7hywDoZSzk6kF/GAa7sAThlkd+E+l3wKxXNbPD\n"
            + "uKKE5zHmb8l7JJm0TYWL9k1igELEi8VzK7Crb/UwzrPRqzb783HypZFdiopTvJLfZGj4VMNaGGZW\n"
            + "jJ/vs2aPhJaGdMG4Y1MANcmXHui26IwckfM+s4FTN4M9g9yvWS3QDci9YpKVUVUljExJboLUnH/2\n"
            + "kcnbf7k07VY7F0q4HL/tTcBIpRo/FGVrpzo1JbKRcqCyN5YTb5oLA0AJ1GBqxtqfPt+OZxYO6Jmt\n"
            + "JAGUhhTKnTWEDP1/pmf9Gph8hh2ctwS9JnOomYU+z1+o1+afEXj0/CX6QARHow4k1il1aTaGODNT\n"
            + "e4lvGF66aT2G+a/yO6/7o3pOsVpYCl4tdXCBS3o9Ype73J2FNgVeAGFoS/aWmgcO/TK6qfsoUSh+\n"
            + "y+Awsig3PMkIganiuu83fFFobNLbLYQwS+NjgHgn666uYsJ/yz+wHXt3Ud2O6a+lLMI9Kpj6DW97\n"
            + "6g5PRBSAOs+u4n9JPnPY1OqvH+of99JEsSLRlWrQuL8rzNsQ2aeC8feyTpkQsg07qQmCPfWAR3nR\n"
            + "cRuRBrYWcLL2gW6xYE2WpZAAJW34tksGe2ShbhmWLqM8JLwZs0p5re4WcKlO+YbuibWfdE+UZx+Y\n"
            + "9BGBSu8n96C7lyAkCN0DbnBW0ICB9GucUGwWxzkxZS39FLzUabWxKyphxqbt5Kt/fAabctQPgI4I\n"
            + "F5ACUHFZ4ySZh9SxYxr6h6a0RW8c9ZLMyUY80WuuJbgtg9Wkzfmx03Y/z8rkaV2s3Fatga0f+QaV\n"
            + "fb8dd+pnzX+VbLPvSm9aT+Yh2GY7RLp0Gn8Pw/N8oUaaVTNy4a7CIj0bfhNMUBPgBMFMyQo1iFdU\n"
            + "U6SqU3Go1D+fNGGIVjOS8YlkNP6Q5SMK25f0b344kipIZrhecF4olnjuuITHviRVqH9+11ybX8f9\n"
            + "skXVr3IF1NRfx6YgW/p7cei5BNywXjFs1qnfconBMZj8kPatHlYdExnsrq8H2WB9hWsV+7D8Smxi\n"
            + "Ht8/+im3WrDDEdppJxcgHyhjBFuJrkgOkvBOY2t3gePGwgLkILH+dEmdeUN0vXZb+3KN9dFiAQcL\n"
            + "Q3k/yhAC2cE4FIPwLVHBaVB6btVrM1pyz+IjP2KzRhfp8bwmnQj3VFgEQ3V1KvSgNDCCBV4GCSqG\n"
            + "SIb3DQEHAaCCBU8EggVLMIIFRzCCBUMGCyqGSIb3DQEMCgECoIIE7jCCBOowHAYKKoZIhvcNAQwB\n"
            + "AzAOBAg+s+RbK8CG/AICCAAEggTIihV33XXzGyF6ORFgHMv61TGgjpcgbtV9++/hnipqi4t8jM/Y\n"
            + "WrpGE728GDQ7cjemMKVed6PUmBEkOkBTlfq9CNDdyGSOBCjENfONxleZv8DoEyND7PC1zaE7k9Pk\n"
            + "sUiJsmTf8u1XwDL4CNGxj03vOraPZSZvx4DfQgCN2OYDIeUDtIzBDbaShsHtdFmAex6DM2WFBfnz\n"
            + "wk8UlPrHVCFzbFq1lPq4fhQPh2dHsZewWNdwxRlFx8m75azJlZ17cbaAS6dTmNjTJUrTRsZyqVX+\n"
            + "C7NaVO1R4+d8hgfU8hO497JLCuXxgl3omxSDtu5tgl0T7XR6PaTNQx/opkrF0ApcwYLB5xPrcp3l\n"
            + "tugsGzdtwLz1DIjpfwxVB5UzwYa9npbsTz4Aa5A55JNkJ72z///zPAWNsVMeEwzYD3Y7pt9yzSQN\n"
            + "2S6YCaGNcfT6gTzqHdd0YuKEg0AYp51h0R9C6JKMdGOkrQXuCctMw3JCvNKDrncP8FwJhCDsysQr\n"
            + "WZFc9XZH3/6++Gj8tW+cYv7n6iL59p57WI/Gg4Ux4ECVoRDhkCfDm8ZIilQBNLGihvp1mvlOe41+\n"
            + "WF4Dy0nyisxryEEzQcp37Y2cTkvZWh3B86cdQyVpcXXDxmAaf7aV66F3P4RHsznBdCb3W+BTWkX3\n"
            + "yMqfgBYNvqTiEr1AszncCguOtGQRdjX9Rl97RDQRFusJ6W5Jrz5HwqVLZRNWrOD0ZFhbNKspy5b6\n"
            + "3PVNmLjWuD7w4jnFHo3PtgLYKDMOgz/3ROXji8NOavap38hT3b1KnMbX8tYgVcS1FcowR/SN0ZOH\n"
            + "bUIoQHJDpPZ51FgNtaFosZV9zNKlxuclw1SZCTAhFt4E7s8fjJS7jmGyTDZmDS2dc5kWrzwX0M9W\n"
            + "bYFeQeyOwxORS6USrJgYFBwbVa7uVtk9R1C8zb7J6nYmFHHMQmIAtP1WW+Miwr3BI+puXdUZpPKT\n"
            + "9S3i2MQsLOoEvDe954KQOFgnnoZBa5uZjY8yZeYU+UQ4Rwf/0NBjWNlUJIsVuPp24bHq+dPsAd2/\n"
            + "pOHr8eFOWQax+bgJJK3DfW/7exnIEiDbgQBfkGYUmHqPFzGWTog++ZdJI8BVytO8eDXCiiqwzvUH\n"
            + "mblgO+5+k9nfdhpxxYk6xmZFPsXUQC028XK7XAhg58R+vYUQmjUbZxE3QrRCxFLe30mlHBhGnr4m\n"
            + "51CbCjL4HJ4wyKjR7Rsiy8Hvg70p4LKSwUX+8V8KlM1dGMR4IVRPwvFURYNyK3I+fLgDc4TVATqV\n"
            + "GkVb7HQ6Hf8nxNfw6ia24Xrh/HYVIDrXrYIfk+AxWTKjgjKSHu4Oa9AqZzym71cnTZlChElzvTSQ\n"
            + "ppZV9UdZKYn/SUxEXo82F9Y6w6y0w43sTetobySrazM9KlXJ+gQKyrlzHHdfvoljHGKUjAbwJbUB\n"
            + "lHpTjEBb3V2DC7Ps9wthpapb3B8J1aA5emGq3qzCmhp0gv9cnjTZKmbGdpFPUe/LshJsUH07kaKj\n"
            + "DrKv83giGlCpe6bimsHgLz7CE7CqEtUN5SJ1yDaLXNbFq98owgnPc9QH2A90WYgPNpRD0u5oa6v5\n"
            + "KFtkRv/whEKkDkOclgeudD5hgXs0V/ptm94LMczQPsa7vwoqFPi0y2RMWaNlCIrvMUIwGwYJKoZI\n"
            + "hvcNAQkUMQ4eDAB0AGUAcwB0AEMAQTAjBgkqhkiG9w0BCRUxFgQUG3t8T5ZQALtRW25mOrOCA2wk\n"
            + "IxgwMTAhMAkGBSsOAwIaBQAEFPvBdqEJt6wjeEwEQXktUj5eHpDBBAgXdliCbTotwwICCAA=";

    private SmimeKeys caKeys;

    public TestCAStorage(ServiceLookup services) throws Exception {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64.decode(TEST_CA));
        caKeys = new KeyImporter(services).readPKCS12File(inputStream, "secret", 0, 0);
    }

    public SmimeKeys getCaKeys() {
        return caKeys;
    }

    @Override
    public List<X509Certificate> getCAs(int group) throws OXException {
        return Collections.singletonList(caKeys.getCertificate());
    }

    @Override
    public boolean addCertificate(String certString, int group) throws OXException {
        throw new UnsupportedOperationException("Importing certificates into the Test CA storage is not supported.");
    }
}
