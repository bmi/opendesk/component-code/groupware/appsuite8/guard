/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl.testService;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Collections;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.common.util.IDNUtil;
import com.openexchange.guard.servlets.authentication.GuestSessionValidator;
import com.openexchange.guard.servlets.authentication.RestSessionValidator;
import com.openexchange.guard.smime.SmimeCryptoKeyUtil;
import com.openexchange.guard.smime.SmimeKeyService;
import com.openexchange.server.ServiceLookup;
import com.openexchange.user.UserService;

/**
 * THIS IS A SERVLET USED FOR TESTING. IT IS NOT MEANT FOR PRODUCTION
 *
 * <p>
 * This servlet will add an S/MIME key to a user using the testing certificate authority
 * </p>
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class TestServlet extends HttpServlet {

    private static final long serialVersionUID = 7736766579172694518L;
    private ServiceLookup services;
    private TestCAStorage testCAStorage;
    private static final String ACTION = "action";
    private static Logger LOG = org.slf4j.LoggerFactory.getLogger(TestServlet.class);

    public TestServlet(ServiceLookup services, TestCAStorage testCAStorage) {
        this.services = services;
        this.testCAStorage = testCAStorage;
    }

    private GuardUserSession getSession(HttpServletRequest request) throws OXException {
        GuardUserSession guardUserSession = new GuestSessionValidator().validateSession(request);
        if (guardUserSession == null) {
            guardUserSession = new RestSessionValidator().validateSession(request);
        }
        return guardUserSession;
    }

    private boolean createAndStoreKey(GuardUserSession userSession) throws OXException, Exception {
        UserService userService = services.getServiceSafe(UserService.class);
        String email = userService.getUser(userSession.getUserId(), userSession.getContextId()).getMail();
        email = IDNUtil.aceEmail(email);
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(2048);
        KeyPair kp = kpg.generateKeyPair();
        SmimeKeys caKeys = testCAStorage.getCaKeys();
        PrivateKey decryptedPrivateKey = SmimeCryptoKeyUtil.decryptPrivateKey(caKeys, "secret");
        X509Certificate newCert = SmimeTestKeyCreator.generateV3Certificate(kp, decryptedPrivateKey, caKeys.getCertificate(), email);
        SmimeKeyService keyService = services.getServiceSafe(SmimeKeyService.class);
        SmimeKeys keys = keyService.createKey(newCert, Collections.emptyList(), kp.getPrivate(), "secret", userSession.getUserId(), userSession.getContextId(), true);
        keyService.storeKey(keys, userSession.getUserId(), userSession.getContextId(), false);
        return true;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String actionName = ServletUtils.getStringParameter(request, ACTION);
        try {
            if (!actionName.equals("test")) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "Unsupported action");
                return;
            }
            GuardUserSession userSession = getSession(request);
            if (userSession == null) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid session");
                return;
            }
            createAndStoreKey(userSession);
            JsonObject json = new JsonObject();
            json.addProperty("data", "OK");
            ServletUtils.sendJsonOK(response, json);
        } catch (Exception e) {
            LOG.error("Error in test servlet", e);
            ServletUtils.sendError(response, e.getMessage());
        }
    }
}
