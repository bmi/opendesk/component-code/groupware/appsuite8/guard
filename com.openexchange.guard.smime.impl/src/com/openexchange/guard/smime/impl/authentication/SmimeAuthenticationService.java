/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl.authentication;

import static com.openexchange.java.Autoboxing.B;
import static com.openexchange.java.Autoboxing.I;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonObject;
import com.openexchange.crypto.CryptoType;
import com.openexchange.crypto.CryptoType.PROTOCOL;
import com.openexchange.exception.OXException;
import com.openexchange.guard.antiabuse.GuardAntiAbuseService;
import com.openexchange.guard.auth.AuthUtils;
import com.openexchange.guard.auth.AuthenticationService;
import com.openexchange.guard.auth.CommonAuth;
import com.openexchange.guard.auth.GuardAuthenticationParameters;
import com.openexchange.guard.certificatemanagement.commons.SmimePrivateKeys;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.common.servlets.utils.AntiAbuseUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.crypto.AuthCryptoType;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.session.GuardSessionService;
import com.openexchange.guard.smime.SmimeCryptoKeyUtil;
import com.openexchange.guard.smime.SmimeKeyService;
import com.openexchange.java.Strings;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.arrays.Collections;

/**
 * {@link SmimeAuthenticationService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v7.10.4
 */
public class SmimeAuthenticationService implements AuthenticationService {

    private final static Logger LOG = LoggerFactory.getLogger(SmimeAuthenticationService.class);
    private final static String AUTH_RET_LOCKOUT = "Lockout";
    private final static String ENCR_PASSWORD_AUTH_FIELD_NAME = "encr_password";

    private final ServiceLookup services;

    /**
     * Initializes a new {@link SmimeAuthenticationService}.
     *
     * @param services The service lookup
     */
    public SmimeAuthenticationService(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Obtain a session token for this session
     *
     * @param GuardUserSession
     * @return String of the token for encryption
     * @throws OXException
     */
    private String getToken(GuardUserSession userSession) throws OXException {
        final GuardSessionService sessionService = services.getService(GuardSessionService.class);
        String token = sessionService.getToken(userSession.getGuardSession());
        if (token == null) {
            token = sessionService.newToken(userSession.getGuardSession(), userSession.getGuardUserId(), userSession.getGuardContextId());
        }
        if (token == null) {
            LOG.error("Auth unable to get token from database for cid = " + userSession.getGuardContextId() + " userid = " + userSession.getGuardUserId());
            throw GuardAuthExceptionCodes.TOKEN_ERROR.create();
        }
        return token;
    }

    private JsonObject getSettings(int id, int cid) throws OXException {
        final JsonObject json = new JsonObject();
        final GuardConfigurationService configService = services.getServiceSafe(GuardConfigurationService.class);
        json.addProperty("min_password_length", I(configService.getIntProperty(GuardProperty.minPasswordLength, id, cid)));
        json.addProperty("password_length", I(configService.getIntProperty(GuardProperty.newPassLength, id, cid)));
        return json;
    }

    @Override
    public PROTOCOL getCryptoType() {
        return CryptoType.PROTOCOL.SMIME;
    }

    @Override
    public AuthenticationResult authenticate(GuardUserSession userSession, GuardAuthenticationParameters parameters) throws OXException {
        final String encrpass = parameters.getStringParameter("encr_password");
        final JsonObject returnJson = CommonAuth.initJson(userSession.getUserId(), userSession.getContextId());
        final GuardAntiAbuseService antiabuse = services.getServiceSafe(GuardAntiAbuseService.class);

        if (antiabuse.blockLogin(AntiAbuseUtils.getAllowParameters(parameters.getClientIP(), //@formatter:off
                                                                   parameters.getUsetAgent(),
                                                                   parameters.getScheme(),
                                                                   userSession.getUserId(),
                                                                   userSession.getContextId(),
                                                                   encrpass))){ //@formatter:on
            returnJson.addProperty("auth", AUTH_RET_LOCKOUT);
            LOG.info("Lockout auth due to bad attempts");
            return new AuthenticationResult(returnJson);
        }
        returnJson.add("settings", getSettings(userSession.getUserId(), userSession.getContextId()));
        final SmimeKeyService keyService = services.getServiceSafe(SmimeKeyService.class);
        final List<SmimePrivateKeys> keys = keyService.getPrivateKeys(userSession.getUserId(), userSession.getContextId());
        boolean goodAuth = false;
        boolean recoveryAvail = false;
        String returnData = "Bad password";
        if (Collections.isNullOrEmpty(keys)) {
            returnData = "No Key";
        } else {
            for (final SmimePrivateKeys key : keys) {
                try {
                    recoveryAvail = recoveryAvail || key.hasRecovery();
                    if (Strings.isNotEmpty(encrpass) && SmimeCryptoKeyUtil.canDecryptPrivateKey(key, encrpass)) {
                        final GuardCipherService cipherService = services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.AES_GCM);
                        returnData = AuthCryptoType.SMIME.getValue();
                        returnData += cipherService.encrypt(AuthUtils.createAuthJson(userSession.getUserId(), // @formatter:off
                                                            userSession.getContextId(),
                                                            userSession.getUserId(), 
                                                            userSession.getContextId(),
                                                            "", 
                                                            ENCR_PASSWORD_AUTH_FIELD_NAME,
                                                            parameters.getStringParameter("encr_password", ""),
                                                            parameters.getStringParameter("language", "")).toString(),
                                                        getToken(userSession));// @formatter:on
                        //@formatter:off
                        antiabuse.report(AntiAbuseUtils.getReportParameter(true, 
                                                                           parameters.getClientIP(),
                                                                           parameters.getUsetAgent(),
                                                                           parameters.getScheme(),
                                                                           userSession.getUserId(),
                                                                           userSession.getContextId(),
                                                                           encrpass));
                        //@formatter:on

                        // Good auth
                        LOG.info("Good smime login");
                        goodAuth = true;
                        break;
                    }
                } catch (final OXException e) {
                    LOG.error("Unable to handle login request: {}", e.getMessage(), e);
                    throw e;
                }
            }
        }
        if (!goodAuth && Strings.isNotEmpty(encrpass)) {
            //@formatter:off
            antiabuse.report(AntiAbuseUtils.getReportParameter(false, 
                                                               parameters.getClientIP(),
                                                               parameters.getUsetAgent(),
                                                               parameters.getScheme(),
                                                               userSession.getUserId(),
                                                               userSession.getContextId(),
                                                               encrpass));
            //@formatter:on
        }
        returnJson.addProperty("auth", returnData);
        returnJson.addProperty("recoveryAvail", B(recoveryAvail));
        return new AuthenticationResult(returnJson);
    }
}
