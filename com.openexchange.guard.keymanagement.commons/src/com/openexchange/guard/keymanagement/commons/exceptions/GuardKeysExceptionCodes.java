package com.openexchange.guard.keymanagement.commons.exceptions;

import com.openexchange.i18n.LocalizableStrings;

public final class GuardKeysExceptionCodes implements LocalizableStrings {

    public static final String IO_ERROR = "An I/O error occurred: %1$s";

    public static final String PGP_ERROR = "A PGP error occurred: %1$s";

    public static final String DB_ERROR = "Unexpected database error: \"%1$s\"";

    public static final String SQL_ERROR = "An SQL error occurred: %1$s";

    // Technical error.  Key specification is invalid
    public static final String INVALID_KEY_SPECIFICATION = "The provided key specification is invalid.";

    // Requested crypto algorithm is not available.
    public static final String NO_SUCH_ALGORITHM = "The requested algorithm is not available.";

    // Failed to reset a users password.  The password recovery is disabled
    public static final String NO_RECOVERY_ERROR = "Failed to reset the password because recovery is disabled.";

    // No valid key found for the specified email address
    public static final String NO_KEY_ERROR = "No valid key found for email %1$s.";

    // Current password is required to change the password
    public static final String PREVIOUSLY_CHANGED = "Current password required for password change.";

    // Unable to revoke the keys due to the following reason.
    public static final String REVOKE_FAILED = "Unable to revoke keys. %1$s";


}
