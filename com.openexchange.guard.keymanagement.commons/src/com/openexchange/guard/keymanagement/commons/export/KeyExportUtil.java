/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons.export;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Objects;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.exceptions.KeysExceptionCodes;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;
import com.openexchange.tools.io.IOUtils;

/**
 * {@link KeyExportUtil} provides several key exporting helper methods.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class KeyExportUtil {

    /**
     * Exports a {@link PGPPublicKeyRing} object into ASCII-Armored string representation
     *
     * @param pgpKeyRing The {@link PGPPublicKeyRing} object to export
     * @return The ASCII-Armored string representation of the given {@link PGPPublicKeyRing} object
     * @throws OXException
     */
    public static String export(PGPPublicKeyRing pgpKeyRing) throws OXException {

        pgpKeyRing = Objects.requireNonNull(pgpKeyRing, "pgpKeyRing must not be null");
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ArmoredOutputStream arm = new ArmoredOutputStream(out);
            ByteArrayOutputStream bout = new ByteArrayOutputStream();

            Iterator<PGPPublicKey> keys = pgpKeyRing.getPublicKeys();
            while (keys.hasNext()) {
                PGPPublicKey k = keys.next();
                k.encode(bout);
            }
            arm.write(bout.toByteArray());

            arm.close();
            bout.close();
            out.close();

            return (new String(out.toByteArray(), StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw KeysExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }

    public static String exportPGPPrivateKey(PGPSecretKeyRing keys, String password, String salt) throws OXException {
        keys = Objects.requireNonNull(keys, "keys must not be null");
        password = Objects.requireNonNull(password, "password must not be null");
        salt = Objects.requireNonNull(salt, "salt must not be null");
        try {
            String oldpass = CipherUtil.getSHA(password, salt);
            PGPSecretKeyRing newkeyring = PGPKeysUtil.duplicateSecretKeyRing(keys, oldpass, password, keys.getSecretKey().getKeyEncryptionAlgorithm());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ArmoredOutputStream arm = new ArmoredOutputStream(out);
            arm.write(newkeyring.getEncoded());
            arm.close();
            out.close();
            return (new String(out.toByteArray()));
        } catch (PGPException e) {
            if(e.getMessage().contains("checksum mismatch")) {
                throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
            }
            throw KeysExceptionCodes.PGP_ERROR.create(e, e.getMessage());
        } catch (IOException e) {
            throw KeysExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Export a given {@link GuardKeys} object as ASCII-Armored string representation
     *
     * @param guardKeys The {@link GuardKeys} object to export
     * @param password The password required for exporting the key
     * @return The ASCII-Armored string representation of the given {@link GuardKeys} object
     * @throws OXException
     */
    public static String exportPGPPrivateKey(GuardKeys guardKeys, String password) throws OXException {
        return exportPGPPrivateKey(guardKeys.getPGPSecretKeyRing(), password, guardKeys.getSalt());
    }

    public static String exportPGPPublicAndPrivateKey(GuardKeys guardKeys, String password) throws OXException{
        return export(guardKeys.getPGPPublicKeyRing()) +
               "\r\n" +
               exportPGPPrivateKey(guardKeys, password);
    }

    public static String armorPGPObject(byte[] pgpobject) throws OXException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ArmoredOutputStream arm = new ArmoredOutputStream(out);
        try {
            arm.write(pgpobject);
        } catch (IOException e) {
            throw KeysExceptionCodes.PGP_ERROR.create(e, e.getMessage());
        } finally {
            IOUtils.closeStuff(arm);
            IOUtils.closeStuff(out);
        }
        return (new String(out.toByteArray(), StandardCharsets.UTF_8));
    }
}
