/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons;

import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPKeyRingGenerator;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.bouncycastle.openpgp.operator.bc.BcKeyFingerprintCalculator;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.common.util.LocaleUtil;
import com.openexchange.guard.keymanagement.commons.exceptions.KeysExceptionCodes;
import com.openexchange.pgp.keys.parsing.PGPPublicKeyRingFactory;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;

/**
 * Manage PGP and RSA keys. Call create and encrypt of keys
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 */
public class GuardKeys {

    private PGPSecretKeyRing keyring;
    private PGPPublicKeyRing publicKeyRing;
    private PublicKey publicKey;
    private String privkey;
    private String salt;
    private boolean passwordNeeded;
    private int userid;
    private int contextid;
    private java.util.Date lastup;
    private String misc;
    private JsonObject settings;
    private String email;
    private String question;
    private String answer;
    private String recovery;
    private long keyid;
    private boolean current;
    private boolean inline;
    private int version;
    private String language;
    private int masterKeyIndex;

    /**
     * Initialises a new {@link GuardKeys}.
     */
    public GuardKeys() {
        salt = CipherUtil.generateSalt();
    }

    /**
     *
     * Initializes a new {@link GuardKeys}.
     *
     * @param userId The id of the owner
     * @param contextId The context id of the owner
     * @param publickey
     * @param eMail
     */
    public GuardKeys(int userId, int contextId, PublicKey publickey, String eMail) {
        userid = userId;
        contextid = contextId;
        publicKey = publickey;
        email = eMail;
    }

    /**
     * Populate the {@link GuardKeys} based on the {@link DeletedKey}
     *
     * @param userId
     * @param contextId
     * @param keyRing
     * @param salt
     */
    public GuardKeys(int userId, int contextId, PGPSecretKeyRing keyRing, String lSalt) {
        keyring = keyRing;
        userid = userId;
        contextid = contextId;
        salt = lSalt;
    }

    /**
     * @param gen PGPKeyRingGenerator
     * @param pub PublicKey
     * @param priv PrivateKey
     * @param Password Password for the private keys
     * @param Salt Salt used for the passwords.
     */
    public GuardKeys(PGPKeyRingGenerator gen, String encryptedPrivateKey, PublicKey pub, String password, String salted) {
        keyring = gen.generateSecretKeyRing();
        setPGPPublicKeyRing(gen.generatePublicKeyRing());
        publicKey = pub;
        salt = salted;
        privkey = encryptedPrivateKey;
    }

    private PGPSecretKeyRing getSecretKeyRingInternal() {
       if(keyring != null && publicKeyRing != null) {
           //Use the public key material because it can contain additional user IDs, signatures, images, etc.
           keyring = PGPSecretKeyRing.replacePublicKeys(keyring, publicKeyRing);
       }
       return keyring;
    }

    /**
     * Sets the PGPSecretKeyRing
     *
     * @param ring the PGPSecretKeyRing
     */
    public void setPGPSecretKeyRing(PGPSecretKeyRing ring) {
        keyring = ring;
    }

    /**
     * Asserts whether the private key is <code>null</code>
     *
     * @return true if the private key is <code>null</code>; false otherwise
     */
    public boolean privKeyNull() {
        return privkey == null;
    }

    public boolean hasPrivateKey() {
        final PGPSecretKeyRing keyring = getSecretKeyRingInternal();
        return keyring != null && keyring.getSecretKey() != null;
    }

    /**
     * Set the already encoded private RSA
     *
     * @param priv
     */
    public void setEncodedPrivate(String priv) {
        privkey = priv;
    }

    /**
     * Set the RSA public key from stored string
     *
     * @param data
     */
    public void setPublicKeyFrom64String(String data) throws OXException {
        if (data == null) {
            publicKey = null;
            return;
        }
        try {
            KeyFactory kf = KeyFactory.getInstance("RSA");
            // for private keys use PKCS8EncodedKeySpec; for public keys use X509EncodedKeySpec
            X509EncodedKeySpec ks = new X509EncodedKeySpec(Base64.decodeBase64(data));
            publicKey = kf.generatePublic(ks);
        } catch (InvalidKeySpecException e) {
            throw KeysExceptionCodes.INVALID_KEY_SPECIFICATION.create(e);
        } catch (NoSuchAlgorithmException e) {
            throw KeysExceptionCodes.NO_SUCH_ALGORITHM.create(e);
        }
    }

    /**
     * Return public RSA key
     *
     * @return
     */
    public PublicKey getPublicKey() {
        return publicKey;
    }

    /**
     * Get Base64 representation of Public key
     *
     * @return
     */
    public String getEncodedPublic() {
        if (publicKey == null) {
            return (null);
        }
        return Base64.encodeBase64String(publicKey.getEncoded());
    }

    /**
     * Get the Base64 Encoded and encrypted Private key
     *
     * @return
     */
    public String getEncodedPrivate() {
        return privkey;
    }

    /**
     * Set RSA Public Key
     *
     * @param pub
     */
    public void setPublic(PublicKey pub) {
        publicKey = pub;
    }

    /**
     * Return the Public Master Key
     *
     * @return
     */
    public PGPPublicKey getPGPPublicKey() {
        Iterator<PGPPublicKey> keys = getPGPPublicKeyRing().getPublicKeys();
        PGPPublicKey key = null;
        while (keys.hasNext()) {
            key = keys.next();
            if (PGPKeysUtil.isEncryptionKey(key) && !(key.isMasterKey())) {
                return key;
            }
        }
        return getPGPPublicKeyRing().getPublicKey();

    }

    /**
     * Get Base64 Encoded String
     *
     * @return
     * @throws IOException
     */
    public String getEncodedPGPPublic() throws OXException {
        try {
            return Base64.encodeBase64String(getPGPPublicKey().getEncoded());
        } catch (IOException e) {
            throw KeysExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Return the secret master key
     *
     * @return The secret master key
     */
    public PGPSecretKey getPGPSecretKey() {
        final PGPSecretKeyRing keyring = getSecretKeyRingInternal();
        if (keyring == null) {
            return null;
        }
        return keyring.getSecretKey();
    }

    public PGPSecretKey getPGPSigningKey() {
        final PGPSecretKeyRing keyring = getSecretKeyRingInternal();
        if (keyring == null) {
            return null;
        }
        Iterator<PGPSecretKey> it = keyring.getSecretKeys();
        PGPSecretKey toreturn = null;
        while (it.hasNext()) {
            PGPSecretKey key = it.next();
            if (key.isMasterKey()) {
                toreturn = key;  // return master key only if no usable sub-keys found
            } else if (key.isSigningKey() && !key.getPublicKey().hasRevocation()) {
                return (key);
            }
        }
        return toreturn;
    }

    public PGPSecretKeyRing getPGPSecretKeyRing() {
        return getSecretKeyRingInternal();
    }

    /**
     * Get Base64 Encoded Secret Keys
     *
     * @return
     * @throws OXException
     */
    public String getEncodedPGPSecret() throws OXException {
        final PGPSecretKeyRing keyring = getSecretKeyRingInternal();
        if (keyring == null) {
            return null;
        }
        try {
            return Base64.encodeBase64String(keyring.getEncoded());
        } catch (IOException e) {
            throw KeysExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Set PGPSecret info from base64 string
     *
     * @param keystring
     * @return
     * @throws OXException
     */
    public void setPGPSecretFromString(String keystring) throws OXException {
        if (keystring == null) {
            keyring = null;
            return;
        }
        try {
            keyring = new PGPSecretKeyRing(Base64.decodeBase64(keystring), new BcKeyFingerprintCalculator());
        } catch (IOException e) {
            throw KeysExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } catch (PGPException e) {
            throw KeysExceptionCodes.PGP_ERROR.create(e, e.getMessage());
        }

    }

    /**
     * Get public key from base64 String
     *
     * @param keystring Base64String
     * @return
     * @throws OXException
     */
    public PGPPublicKey getPGPPublicKeyFromString(String keystring) throws OXException {
        try {
            PGPPublicKeyRing pkr = new PGPPublicKeyRing(Base64.decodeBase64(keystring), new BcKeyFingerprintCalculator());
            return pkr.getPublicKey();
        } catch (IOException e) {
            throw KeysExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }

    public void setSalt(String salted) {
        salt = salted;
    }

    public String getSalt() {
        return salt;

    }

    public void setPGPKeyRingFromAsc(String ascdata) throws OXException {
        try {
            setPGPPublicKeyRing(PGPPublicKeyRingFactory.create(ascdata));
        } catch (IOException e) {
            throw KeysExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Returns the length of the RSA public key
     *
     * @return the length of the RSA public key
     */
    public int getKeyLen() {
        RSAPublicKey rsapub = (RSAPublicKey) publicKey;
        return (rsapub.getModulus().bitLength());
    }

    /**
     * Returns the PGP public key ring
     *
     * @return the PGP public key ring
     */
    public PGPPublicKeyRing getPGPPublicKeyRing() {
        return publicKeyRing;
    }

    public PGPPublicKeyRing setPGPPublicKeyRing(PGPPublicKeyRing pubring) {
        this.publicKeyRing = pubring;
        return pubring;
    }

    public boolean isPasswordNeeded() {
        return passwordNeeded;
    }

    public void setPasswordNeeded(boolean passwordNeeded) {
        this.passwordNeeded = passwordNeeded;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getContextid() {
        return contextid;
    }

    public void setContextid(int contextid) {
        this.contextid = contextid;
    }

    public Date getLastup() {
        return lastup;
    }

    public void setLastup(Date lastup) {
        this.lastup = lastup;
    }

    public String getMisc() {
        return misc;
    }

    public void setMisc(String misc) {
        this.misc = misc;
    }

    public JsonObject getSettings() {
        return settings;
    }

    public void setSettings(JsonObject settings) {
        this.settings = settings;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getRecovery() {
        return recovery;
    }

    public void setRecovery(String recovery) {
        this.recovery = recovery;
    }

    public long getKeyid() {
        return keyid;
    }

    public void setKeyid(long keyid) {
        this.keyid = keyid;
    }

    public boolean isCurrent() {
        return current;
    }

    public void setCurrent(boolean current) {
        this.current = current;
    }

    public boolean isInline() {
        return inline;
    }

    public void setInline(boolean inline) {
        this.inline = inline;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getLanguage() {
        return language;
    }

    public Locale getLocale() {
        return LocaleUtil.getLocalFor(language);
    }

    public void setLocale(Locale locale) {
        setLanguage(locale != null ? locale.toString() : "");
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean isRecoveryAvail() {
        return this.getRecovery() == null ? false : !this.getRecovery().equals("");
    }

    public boolean getEncryptIncoming () {
        if (settings != null && settings.has("encryptIncoming")) {
            return settings.get("encryptIncoming").getAsBoolean();
        }
        return false;
    }

    public void setMasterKeyIndex(int index) {
        this.masterKeyIndex = index;
    }

    public int getMasterKeyIndex() {
        return this.masterKeyIndex;
    }
}
