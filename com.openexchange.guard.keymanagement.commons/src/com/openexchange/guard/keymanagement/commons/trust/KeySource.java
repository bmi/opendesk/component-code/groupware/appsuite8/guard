/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons.trust;

import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.keymanagement.commons.osgi.Services;

/**
 * {@link KeySource} represents the source of a key.
 * <p>
 * Each key can have it's own source. For example "Guard", "HKP", "Uploaded key"..etc.
 * </p>
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class KeySource {

    private final String name;
    private final GuardProperty sourceType;
    private final Integer trustValue;

    /**
     * Initializes a new {@link KeySource}.
     *
     * @param name The name of the {@link KeySource}
     * @param trustValue A fixed trust value
     */
    public KeySource(String name, int trustValue) {
        this.name = name;
        this.trustValue = Integer.valueOf(trustValue);
        this.sourceType = null;
    }

    /**
     * Initializes a new {@link KeySource}.
     *
     * @param name The name of the {@link KeySource}
     * @param sourceType GuardProperty for the sourceType
     */
    public KeySource(String name, GuardProperty sourceType) {
        this.name = name;
        this.sourceType = sourceType;
        this.trustValue = null;
    }

    /**
     * Gets the name
     *
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the trustLevel
     *
     * @return The trustLevel
     */
    public int getTrustLevel(int userId, int cid) {
        if (trustValue != null) {
            return trustValue.intValue();
        }
        if (sourceType == null) {
            return 0;
        }
        return Services.getService(GuardConfigurationService.class).getIntProperty(sourceType, userId, cid);
    }

    /**
     * Whether the {@link KeySource} is a trusted source or not
     *
     * @return True, if the source is a trusted source, false otherwise
     * @throws OXException
     */
    public boolean isTrusted(int userId, int cid) throws OXException {
        if (sourceType == null) {
            return false;
        }
        KeySourceTrustValidatorFactory factory = Services.getService(KeySourceTrustValidatorFactory.class);
        KeySourceTrustValidator keySourceTrustValidator = factory.create(userId, cid);
        return keySourceTrustValidator.isTrusted(this, userId, cid);
    }

}
