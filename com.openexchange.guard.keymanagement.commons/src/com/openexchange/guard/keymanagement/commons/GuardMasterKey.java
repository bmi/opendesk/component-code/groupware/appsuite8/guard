/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons;

import java.security.Key;
import com.openexchange.guard.common.java.Strings;

/**
 * {@link GuardMasterKey}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public final class GuardMasterKey {

    private final String mc;
    private final String rc;
    private final String publicKey;
    private final Key privateKey;

    /**
     * Initialises a new {@link GuardMasterKey}.
     */
    public GuardMasterKey(String mc, String rc, String publicKey, Key privateKey) {
        super();
        if (Strings.isEmpty(mc)) {
            throw new IllegalArgumentException("The 'MC' value of the OX Guard master key can be neither 'null' nor empty");
        }
        if (Strings.isEmpty(rc)) {
            throw new IllegalArgumentException("The 'RC' value of the OX Guard master key can be neither 'null' nor empty");
        }

        if (Strings.isEmpty(publicKey)) {
            throw new IllegalArgumentException("The 'publicKey' value of the OX Guard master key can be neither 'null' nor empty");
        }
        if (privateKey == null) {
            throw new IllegalArgumentException("The 'privateKey' of the OX Guard master key can be neither 'null' nor empty");
        }

        this.mc = mc;
        this.rc = rc;
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    /**
     * Gets the MC
     *
     * @return The mc
     */
    public String getMC() {
        return mc;
    }

    /**
     * Gets the RC
     *
     * @return The rc
     */
    public String getRC() {
        return rc;
    }

    /**
     * Gets the publicKey
     *
     * @return The publicKey
     */
    public String getPublicKey() {
        return publicKey;
    }

    /**
     * Gets the privateKey
     *
     * @return The privateKey
     */
    public Key getPrivateKey() {
        return privateKey;
    }
}
