/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl.lookup;

import java.time.Duration;
import java.time.Instant;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.services.lookup.RecipKeyLookupStrategy;

/**
 * {@link CompositeRecipKeyLookupStrategy} implements a composite pattern for a {@link RecipKeyLookupStrategy}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class CompositeRecipKeyLookupStrategy implements RecipKeyLookupStrategy {

    private static final Logger logger = LoggerFactory.getLogger(CompositeRecipKeyLookupStrategy.class);
    private final RecipKeyLookupStrategy[] strategies;

    /**
     * Initializes a new {@link CompositeRecipKeyLookupStrategy}.
     *
     * @param strategies
     */
    public CompositeRecipKeyLookupStrategy(RecipKeyLookupStrategy... strategies) {
        this.strategies = Objects.requireNonNull(strategies, "strategies must not be null");
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keymanagement.services.lookup.RecipKeyLookupStrategy#lookup(java.lang.String)
     */
    @Override
    public RecipKey lookup(String email, int timeout) throws OXException {
        Instant start = Instant.now();
        for (RecipKeyLookupStrategy strategy : strategies) {
            Instant current = Instant.now();
            Duration elapsed = Duration.between(start, current);  // Keep track of elapsed time and pass
            RecipKey recipKey = strategy.lookup(email, timeout - Math.toIntExact(elapsed.toMillis()));
            if (recipKey != null) {
                logger.debug("Found recipient key for email {} using {}",email, strategy.getClass().getCanonicalName());
                return recipKey;
            }
            logger.debug("Could not find recipient key for email {} using {}",email, strategy.getClass().getCanonicalName());
        }
        return null;
    }

}
