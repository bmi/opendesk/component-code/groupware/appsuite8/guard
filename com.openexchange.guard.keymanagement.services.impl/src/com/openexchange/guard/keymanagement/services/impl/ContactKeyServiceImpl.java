/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;
import org.bouncycastle.openpgp.PGPPublicKey;
import com.openexchange.contact.ContactID;
import com.openexchange.contact.ContactService;
import com.openexchange.contact.provider.composition.IDBasedContactsAccess;
import com.openexchange.contact.provider.composition.IDBasedContactsAccessFactory;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.container.Contact;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.OGPGPKeyRing;
import com.openexchange.guard.keymanagement.commons.export.KeyExportUtil;
import com.openexchange.guard.keymanagement.services.ContactKeyService;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.PublicExternalKeyService;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.pgp.keys.parsing.PGPPublicKeyRingFactory;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Origin;
import com.openexchange.session.Session;

/**
 * {@link ContactKeyServiceImpl}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class ContactKeyServiceImpl implements ContactKeyService {

    private final ServiceLookup services;

    /**
     * Initializes a new {@link ContactKeyServiceImpl}.
     *
     * @param keyService The {@link GuardKeyService} to use
     * @param externalKeyService The {@link PublicExternalKeyService} to use
     * @para contactsAccessFactory The {@link IDBasedContactsAccessFactory} to use
     * @param contactService The {@link ContactService} to use
     */
    public ContactKeyServiceImpl(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Internal factory method to create a session for the given user
     *
     * @param userId The user id
     * @param contextId the context id
     * @return A "fake" session based on the given parameters.
     */
    private Session createaFakeSession(int userId, int contextId) {

        return new Session() {

            private final ConcurrentMap<String, Object> parameters = new ConcurrentHashMap<String, Object>(8, 0.9f, 1);

            @Override
            public void setParameter(String name, Object value) {
                if (null == value) {
                    parameters.remove(name);
                } else {
                    parameters.put(name, value);
                }
            }

            @Override
            public void setLocalIp(String ip) {
                // NO-OP
            }

            @Override
            public void setHash(String hash) {
                // NO-OP
            }

            @Override
            public void setClient(String client) {
                // NO-OP
            }

            @Override
            public boolean isTransient() {
                return false;
            }

            @Override
            public String getUserlogin() {
                return null;
            }

            @Override
            public int getUserId() {
                return userId;
            }

            @Override
            public String getSessionID() {
                return null;
            }

            @Override
            public String getSecret() {
                return null;
            }

            @Override
            public String getRandomToken() {
                return null;
            }

            @Override
            public String getPassword() {
                return null;
            }

            @Override
            public Set<String> getParameterNames() {
                return parameters.keySet();
            }

            @Override
            public Object getParameter(String name) {
                return parameters.get(name);
            }

            @Override
            public String getLoginName() {
                return null;
            }

            @Override
            public String getLogin() {
                return null;
            }

            @Override
            public String getLocalIp() {
                return null;
            }

            @Override
            public String getHash() {
                return null;
            }

            @Override
            public int getContextId() {
                return contextId;
            }

            @Override
            public String getClient() {
                return null;
            }

            @Override
            public String getAuthId() {
                return null;
            }

            @Override
            public boolean containsParameter(String name) {
                return parameters.containsKey(name);
            }

            @Override
            public Origin getOrigin() {
                return null;
            }

            @Override
            public boolean isStaySignedIn() {
                return false;
            }
        };
    }

    private static String getKeyIdsString(GuardKeys key) {
        Iterator<PGPPublicKey> pubKeys = key.getPGPPublicKeyRing().getPublicKeys();
        StringBuilder sb = new StringBuilder();
        while (pubKeys.hasNext()) {
            PGPPublicKey pkey = pubKeys.next();
            String id = PGPKeysUtil.getFingerPrint(pkey.getFingerprint());
            sb.append(id.substring(id.length() - 8));
            if (pubKeys.hasNext()) {
                sb.append(" ");
            }
        }
        return sb.toString();
    }

    /**
     * Validate if each of the three contact emails are valid.
     * If so, adds to list and returns (without duplicates)
     * @param contact
     * @return
     */
    private List<String> getContactEmails(Contact contact) throws OXException {
        ArrayList<String> emails = new ArrayList<String>();
        GuardRatifierService ratifierService = services.getServiceSafe(GuardRatifierService.class);
        if (ratifierService.isValid(contact.getEmail1())) {
            emails.add(contact.getEmail1());
        }
        if (ratifierService.isValid(contact.getEmail2())) {
            emails.add(contact.getEmail2());
        }
        if (ratifierService.isValid(contact.getEmail3())) {
            emails.add(contact.getEmail3());
        }
        return emails.stream().distinct().collect(Collectors.toList());
    }

    private static OGPGPKeyRing toOGPGPKeyRing(int userId, int contextId, GuardKeys key) throws IOException, OXException {
        final boolean isGuardKey = true;
        final boolean isOwned = key.getUserid() == userId && key.getContextid() == contextId;
        return new OGPGPKeyRing(
            getKeyIdsString(key),
            PGPPublicKeyRingFactory.create(KeyExportUtil.export(key.getPGPPublicKeyRing())),
            isGuardKey).isOwned(isOwned);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keymanagement.services.ContactKeyService#getKeysForContact(int, int, java.lang.String, java.lang.String)
     */
    @Override
    public List<OGPGPKeyRing> getKeysForContact(int userId, int contextId, String contact_id, String folder_id) throws OXException {
        IDBasedContactsAccessFactory contactsAccessFactory = services.getServiceSafe(IDBasedContactsAccessFactory.class);
        IDBasedContactsAccess idBasedContactsAccess = contactsAccessFactory.createAccess(createaFakeSession(userId, contextId));
        Contact contact = idBasedContactsAccess.getContact(new ContactID(folder_id, contact_id));
        List<String> emails = getContactEmails(contact);
        PublicExternalKeyService externalKeyService = services.getServiceSafe(PublicExternalKeyService.class);
        List<OGPGPKeyRing> ret = emails.isEmpty() ? Collections.emptyList() : externalKeyService.get(userId, contextId, emails);
        try {
            GuardKeyService keyService = services.getServiceSafe(GuardKeyService.class);
            for (String email : emails) {
                GuardKeys key = keyService.getKeys(email);
                if (key != null) {
                    ret.add(toOGPGPKeyRing(userId, contextId, key));
                }
            }
        } catch (IOException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }

        return ret;
    }
}
