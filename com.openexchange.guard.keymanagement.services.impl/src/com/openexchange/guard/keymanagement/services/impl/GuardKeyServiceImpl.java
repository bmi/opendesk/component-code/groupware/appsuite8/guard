/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl;

import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.common.java.Strings;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.database.GuardShardingService;
import com.openexchange.guard.email.storage.ogEmail.Email;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.PGPKeys;
import com.openexchange.guard.keymanagement.commons.exceptions.KeysExceptionCodes;
import com.openexchange.guard.keymanagement.commons.util.PGPUtil;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.KeyRecoveryService;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.keymanagement.services.PasswordChangeService;
import com.openexchange.guard.keymanagement.services.RevocationReason;
import com.openexchange.guard.keymanagement.services.impl.exceptions.GuardKeyCreationExceptionCodes;
import com.openexchange.guard.keymanagement.services.impl.osgi.Services;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.guard.keymanagement.storage.PGPKeysStorage;
import com.openexchange.guard.user.GuardCapabilities;
import com.openexchange.guard.user.OXUserService;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;
import com.openexchange.tools.encoding.Base64;

/**
 * {@link GuardKeyServiceImpl}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
public class GuardKeyServiceImpl implements GuardKeyService {

    private static final Logger LOG = LoggerFactory.getLogger(GuardKeyServiceImpl.class);

    /**
     * Resets the "current" key to the key which has the highest version
     *
     * @param userid the user's id
     * @param cid the context id
     * @throws Exception
     */
    private void resetCurrentKey(int userid, int cid) throws OXException {
        KeyTableStorage ogKeyTableStorage = Services.getService(KeyTableStorage.class);

        //Getting the keys with the highest version from the user, and mark it as the "current" key
        GuardKeys highestKey = ogKeyTableStorage.getHighestVersionKeyForUser(userid, cid);
        if (highestKey != null) {
            ogKeyTableStorage.setCurrentKey(highestKey);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.GuardKeyService#getKey(long)
     */
    @Override
    public GuardKeys getKeys(long keyId) throws OXException {
        //This mapping will provide the email address for the given key id
        PGPKeys keyMapping = Services.getService(PGPKeysStorage.class).getById(keyId);
        if (keyMapping != null) {
            //This mapping will provide the user- and context-id
            Email emailMapping = Services.getService(EmailStorage.class).getByEmail(keyMapping.getEmail());
            if(emailMapping != null) {
                return getKeys(emailMapping.getUserId(), emailMapping.getContextId(), keyMapping.getKeyId());
            }
        }
        return null;
    }
    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keys.GuardKeyService#getKeys(int, int)
     */
    @Override
    public GuardKeys getKeys(int id, int cid) throws OXException {
        int shard = 0;
        if (cid < 0) {
            GuardShardingService sharding = Services.getService(GuardShardingService.class);
            if(sharding.hasShard(id, cid)) {
                shard = sharding.getShard(id, cid);
            }
            else {
                return null;
            }
        }
        KeyTableStorage ogKeyTableStorage = Services.getService(KeyTableStorage.class);
        return ogKeyTableStorage.getCurrentKeyForUser(id, cid, shard);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keys.GuardKeyService#getKeys(int, int, long, boolean)
     */
    @Override
    public GuardKeys getKeys(int id, int cid, long keyId) throws OXException {
        int shard = 0;
        if (cid < 0) {
            GuardShardingService sharding = Services.getService(GuardShardingService.class);
            if(sharding.hasShard(id, cid)) {
                shard = sharding.getShard(id, cid);
            }
            else {
                return null;
            }
        }
        KeyTableStorage ogKeyTableStorage = Services.getService(KeyTableStorage.class);
        return ogKeyTableStorage.getKeyForUserById(id, cid, shard, Long.toString(keyId));
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keys.GuardKeyService#getKeysFromEmail(java.lang.String)
     */
    @Override
    public GuardKeys getKeys(String email) throws OXException {
        return getKeys(email, 0L);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keys.GuardKeyService#getKeysFromEmail(java.lang.String, long)
     */
    @Override
    public GuardKeys getKeys(String email, long keyId) throws OXException {

        EmailStorage ogEmailStorage = Services.getService(EmailStorage.class);
        Email ogEmail = ogEmailStorage.getByEmail(email);
        if (ogEmail != null) {
            return keyId != 0L ?
                getKeys(ogEmail.getUserId(), ogEmail.getContextId(), keyId) :
                getKeys(ogEmail.getUserId(), ogEmail.getContextId());
        }
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.GuardKeyService#getKeysBySubKey(int, int, long)
     */
    @Override
    public GuardKeys getKeysBySubKeyId(int id, int cid, long keyId) throws OXException {
        PGPKeys keyMapping = Services.getService(PGPKeysStorage.class).getById(keyId);  // May be sub-key, key master for lookup
        if(keyMapping != null) {
             GuardKeys keys = getKeys(keyMapping.getKeyId());
             if(keys != null && keys.getUserid() == id && keys.getContextid() == cid) {
                 return keys;
             }
        }
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.GuardKeyService#getKeysBySubKeyId(java.lang.String, long)
     */
    @Override
    public GuardKeys getKeysBySubKeyId(String email, long keyId) throws OXException {
        EmailStorage ogEmailStorage = Services.getService(EmailStorage.class);
        Email emailMapping = ogEmailStorage.getByEmail(email);
        if (emailMapping != null) {
            return getKeysBySubKeyId(emailMapping.getUserId(), emailMapping.getContextId(), keyId);
        }
        return null;
    }


    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.GuardKeyService#getAllKeys(int, int)
     */
    @Override
    public Collection<GuardKeys> getAllKeys(int id, int cid) throws OXException {
        KeyTableStorage ogKeyTableStorage = Services.getService(KeyTableStorage.class);
        return ogKeyTableStorage.getKeysForUser(id, cid);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keys.GuardKeyService#storeKeys(com.openexchange.guard.keys.dao.GuardKeys, boolean)
     */
    @Override
    public GuardKeys storeKeys(GuardKeys key, boolean userCreated) throws OXException {
        GuardKeys ret = null;

        //Getting the shard to use
        int shard = 0;
        GuardShardingService shardingService = Services.getService(GuardShardingService.class);
        if (key.getContextid() < 0) {// If guest, get shard number, then check tables exist
            shard = shardingService.getNextShard();
            if (shard == 0) {
                LOG.error("Unable to get non-ox shard id");
                return null;
            }
        }

        //Adding the key to the storage
        KeyTableStorage keyTableStorage = Services.getService(KeyTableStorage.class);
        keyTableStorage.insert(key, userCreated, shard);
        //retrieve the created key in order to get the userid which was created while inserting
        ret = keyTableStorage.getKeyForEmailAndContext(key.getEmail(), key.getUserid(), key.getContextid(), shard);
        if (ret == null) {
            throw GuardKeyCreationExceptionCodes.KEY_CREATION_ERROR.create("Problem retrieving key from table storage after creation");
        }
        //Store key mappings
        if (ret.getUserid() > 0) {
            //Adding the new key to the email mapping storage
            EmailStorage ogEmailStorage = Services.getService(EmailStorage.class);
            ogEmailStorage.insertOrUpdate(ret.getEmail(), ret.getContextid(), ret.getUserid(), shard);

            //Adding the new key to the PGP key mapping storage
            PGPKeysStorage keysStorage = Services.getService(PGPKeysStorage.class);
            keysStorage.addPublicKeyIndex(ret.getContextid(), 0L, ret.getEmail(), ret.getPGPPublicKeyRing());
        }

        return ret;
    }


    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keys.GuardKeyService#updateKeys(int, int, com.openexchange.guard.keys.dao.GuardKeys, java.lang.String, boolean)
     */
    @Override
    public void updateKeys(int id, int cid, GuardKeys key, String recovery, boolean reset) throws OXException {
        KeyTableStorage ogKeyTableStorage = Services.getService(KeyTableStorage.class);
        ogKeyTableStorage.updatePassword(key, recovery, !reset /* setLastMod */);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.GuardKeyService#updateKeyMaterial(com.openexchange.guard.keymanagement.commons.GuardKeys)
     */
    @Override
    public void updateKeyMaterial(GuardKeys key) throws OXException {
        KeyTableStorage ogKeyTableStorage = Services.getService(KeyTableStorage.class);
        ogKeyTableStorage.updatePublicAndPrivateKey(key);
        PGPKeysStorage keysStorage = Services.getService(PGPKeysStorage.class);
        keysStorage.addPublicKeyIndex(key.getContextid(), 0L, key.getEmail(), key.getPGPPublicKeyRing());
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keys.GuardKeyService#addNewKey(com.openexchange.guard.keys.dao.GuardKeys)
     */
    @Override
    public void addNewKey(GuardKeys key) throws OXException {
        KeyTableStorage keyTableStorage = Services.getService(KeyTableStorage.class);

        //removing the "current" flag from existing keys,
        //because the new created key will be the "current" key by default
        keyTableStorage.unsetCurrentFlag(key.getUserid(), key.getContextid());
        //Marking the new key as the "current" key
        key.setCurrent(true);

        //Getting the key with the highest version
        GuardKeys highestKeys = keyTableStorage.getHighestVersionKeyForUser(key.getUserid(), key.getContextid());

        //Incrementing key version, if highest was found, and adding the new key to the storage
        key.setVersion(highestKeys != null ? highestKeys.getVersion() + 1 : 0);
        keyTableStorage.insert(key, true /* set lastMod to NOW() */);

        //Getting the shard to use
        int shard = 0;
        if (key.getContextid() < 0) {// If guest, get shard number, then check tables exist
            GuardShardingService shardingService = Services.getService(GuardShardingService.class);
            shard = shardingService.getNextShard();
        }
        EmailStorage ogEmailStorage = Services.getService(EmailStorage.class);
        ogEmailStorage.insertOrUpdate(key.getEmail(), key.getContextid(), key.getUserid(), shard);

        //Adding the new key to the public key index (lookup table)
        PGPKeysStorage keysStorage = Services.getService(PGPKeysStorage.class);
        keysStorage.addPublicKeyIndex(key.getContextid(), 0L, key.getEmail(), key.getPGPPublicKeyRing());
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keys.GuardKeyService#updatePin(int, int, java.lang.String)
     */
    @Override
    public void updatePin(int id, int cid, String pin) throws OXException {
        KeyTableStorage ogKeyTableStorage = Services.getService(KeyTableStorage.class);
        ogKeyTableStorage.updatePin(id, cid, pin);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keys.GuardKeyService#removePin(java.lang.String)
     */
    @Override
    public void removePin(String email) throws OXException {
        GuardKeys key = getKeys(email);

        if (key == null) {
            throw GuardCoreExceptionCodes.KEY_NOT_FOUND.create();
        }

        if (key.getContextid() > 0) {
            throw GuardCoreExceptionCodes.NOT_A_GUEST_ACCOUNT.create();
        }
        storeQuestion(key.getUserid(), key.getContextid(), null, null);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keys.GuardKeyService#recoverPasswordHash(int, int)
     */
    @Override
    public String recoverPasswordHash(int id, int cid) throws OXException {
        String hash = "";
        try {
            KeyTableStorage ogKeyTableStorage = Services.getService(KeyTableStorage.class);
            GuardKeys currentKey = ogKeyTableStorage.getCurrentKeyForUser(id, cid);
            if (currentKey != null && !Strings.isEmpty(currentKey.getRecovery())) {
                KeyRecoveryService keyRecoveryService = Services.getService(KeyRecoveryService.class);
                hash = keyRecoveryService.getRecoveryHash(
                    currentKey.getRecovery(),
                    currentKey.getMasterKeyIndex(),
                    currentKey.getSalt(),
                    currentKey.getPublicKey() == null ? "" : Integer.toString(currentKey.getEncodedPublic().hashCode()),
                    currentKey.getUserid(),
                    currentKey.getContextid());
            }
        } catch (Exception ex) {
            LOG.error("Error recovering the password hash for " + cid + "/" + id, ex);
        }

        return hash;
    }

    @Override
    public boolean isRecoveryEnabled(int userId, int contextId) throws OXException {
        GuardKeys currentKey = getKeys(userId, contextId);
        if (currentKey != null && !Strings.isEmpty(currentKey.getRecovery())) {
            return true;
        }
        return false;
    }

    @Override
    public String generatePassword(int userId, int cid) throws OXException {
        GuardCipherService cipherService = Services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.AES_CBC);
        return cipherService.generateRandomPassword(userId, cid);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keys.GuardKeyService#resetPassword(java.lang.String, java.lang.String, boolean)
     */
    @Override
    public void resetPassword(String email, String newpass) throws OXException {
        GuardKeys oldkeys = getKeys(email);
        if (oldkeys != null) {
            String hash = recoverPasswordHash(oldkeys.getUserid(), oldkeys.getContextid());
            if (hash.equals("")) {
                throw KeysExceptionCodes.NO_RECOVERY_ERROR.create();
            }
            PasswordChangeService passService = Services.getService(PasswordChangeService.class);
            GuardKeys newkeys = passService.changePasswordWithRecovery(hash, newpass, oldkeys);
            KeyRecoveryService recoveryService = Services.getService(KeyRecoveryService.class);
            String newRecovery = recoveryService.createRecovery(newkeys, newpass);
            updateKeys(oldkeys.getUserid(), oldkeys.getContextid(), newkeys, newRecovery, true);
        } else {
            //No key found for this email so we return null
            LOG.error("Error resetting password for " + email + ". No keys found for email");
            throw KeysExceptionCodes.NO_KEY_ERROR.create(email);
        }
    }


    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keys.GuardKeyService#storeQuestion(int, int, java.lang.String, java.lang.String)
     */
    @Override
    public boolean storeQuestion(int id, int cid, String question, String answer) throws OXException {
        if (answer == null || question.equals("")) {
            return true;
        }
        String encranswer = null;
        KeyTableStorage ogKeyTableStorage = Services.getService(KeyTableStorage.class);
        MasterKeyService masterKeyService = Services.getService(MasterKeyService.class);
        List<GuardKeys> keys = ogKeyTableStorage.getKeysForUser(id, cid);
        for (GuardKeys key: keys) {
            encranswer = masterKeyService.getRcEncryted(answer, question, key.getMasterKeyIndex());
            ogKeyTableStorage.updateQuestionForKey(id, cid, question, encranswer, key.getKeyid());
        }
        return true;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.GuardKeyService#updateSettingsForUser(com.openexchange.guard.keymanagement.commons.GuardKeys)
     */
    @Override
    public void updateSettingsForUser(GuardKeys key) throws OXException {
        KeyTableStorage ogKeyTableStorage = Services.getService(KeyTableStorage.class);
        ogKeyTableStorage.updateSettingsForUser(key.getUserid(), key.getContextid(), key.getSettings().toString());
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.GuardKeyService#updateAnswerQuestionForUser(com.openexchange.guard.keymanagement.commons.GuardKeys)
     */
    @Override
    public void updateAnswerQuestionForUser(GuardKeys key) throws OXException {
        KeyTableStorage ogKeyTableStorage = Services.getService(KeyTableStorage.class);
        // Answer is already encrypted with the question, no need to re-encrypt
        ogKeyTableStorage.updateQuestionForKey(key.getUserid(), key.getContextid(), key.getQuestion(), key.getAnswer(), key.getKeyid());
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keys.GuardKeyService#encryptPrivateKey(java.security.PrivateKey, java.lang.String, java.lang.String)
     */
    @Override
    public String encryptPrivateKey(PrivateKey privateKey, String password, String salt) throws OXException {
        GuardCipherService cipherService = Services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.AES_CBC);
        return cipherService.encrypt(Base64.encode(privateKey.getEncoded()), CipherUtil.getSHA(password, salt), salt);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keys.GuardKeyService#decryptPrivateKey(java.lang.String, java.lang.String, java.lang.String, boolean)
     */
    @Override
    public PrivateKey decryptPrivateKey(String privateKey, String password, String salt) throws OXException {
        return decryptPrivateKeyWithHash(privateKey, CipherUtil.getSHA(password, salt), salt);
    }

    @Override
    public PrivateKey decryptPrivateKeyWithHash(String privateKey, String password, String salt) throws OXException {
        GuardCipherService cipherService = Services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.AES_CBC);
        byte[] decoded;
        try {
            decoded = cipherService.decrypt(privateKey, password, salt).getBytes(StandardCharsets.UTF_8);
            if (decoded.length == 0) {
                throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
            }
        } catch (Exception e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
        // for private keys use PKCS8EncodedKeySpec; for public keys use X509EncodedKeySpec
        PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(Base64.decode(new String(decoded, StandardCharsets.UTF_8)));

        try {
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PrivateKey priv = kf.generatePrivate(ks);
            return priv;
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            LOG.error("Unable decode private", e);
            return null;
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.GuardKeyService#delete(com.openexchange.guard.keymanagement.commons.GuardKeys, java.lang.String, int, int)
     */
    @Override
    public void delete(GuardKeys key, String password ,int userId, int contextId) throws OXException {

        OXUserService userService = Services.getService(OXUserService.class);
        PGPKeysStorage pgpKeysStorage = Services.getService(PGPKeysStorage.class);
        KeyTableStorage ogKeyTableStorage = Services.getService(KeyTableStorage.class);
        EmailStorage ogEmailStorage = Services.getService(EmailStorage.class);

        GuardCapabilities settings = userService.getGuardCapabilieties(contextId, userId);
        if (settings.isNoDeletePrivate()) {
            LOG.info("Key deletion is disabled by configuration for user");
            throw GuardAuthExceptionCodes.FORBIDDEN.create();
        }
        else {

            //Verify password if the ring contains a private key
            if(key.hasPrivateKey()) {
                if(!PGPUtil.verifyPassword(key.getPGPSecretKeyRing(), password, key.getSalt())) {
                    throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
                }
            }

            //delete the key from storage
            ogKeyTableStorage.delete(key);
            //delete from Key-ID mapping table
            pgpKeysStorage.deleteByKeyId(key.getKeyid());
            //if key was the current key, then upgrade highest version key to current
            if (key.isCurrent()) {
                resetCurrentKey(key.getUserid(), key.getContextid());
            }
            //Check if the user has other keys
            Collection<GuardKeys> allKeys = getAllKeys(key.getUserid(), key.getContextid());
            if(allKeys == null || allKeys.size() == 0) {
                //no keys left; we can delete the from the email mapping table
                ogEmailStorage.deleteAllForUser(key.getContextid(), key.getUserid());
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keymanagement.services.GuardKeyService#setCurrentKey(int, int, com.openexchange.guard.keymanagement.commons.GuardKeys)
     */
    @Override
    public void setCurrentKey(GuardKeys key) throws OXException {
        key = Objects.requireNonNull(key, "key must not be null");
        KeyTableStorage ogKeyTableStorage = Services.getService(KeyTableStorage.class);
        //Removing the current flag from all keys
        ogKeyTableStorage.unsetCurrentFlag(key.getUserid(), key.getContextid());
        //Setting the current flag for the given key
        ogKeyTableStorage.setCurrentKey(key);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.GuardKeyService#addUserId(com.openexchange.guard.keymanagement.commons.GuardKeys, java.lang.String, java.lang.String)
     */
    @Override
    public void addUserId(GuardKeys key, String newUserId, String password) throws OXException {
        key = Objects.requireNonNull(key, "key must not be null");
        PGPSecretKey masterSecretKey = key.getPGPSecretKey();
        if(masterSecretKey == null || masterSecretKey.isPrivateKeyEmpty()) {
           throw GuardCoreExceptionCodes.KEY_NOT_FOUND_PRIVATE_MASTER.create();
        }

        //Getting the private key used for singing
        PGPPrivateKey signingPrivateKey = null;
        try {
            signingPrivateKey = PGPUtil.decodePrivate(masterSecretKey, password, key.getSalt());
        } catch (PGPException e) {
            throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
        }

        try {
            //Adding the user id to the public key ring material
            PGPPublicKeyRing modifiedPublicKeyRing = PGPKeysUtil.addUID(key.getPGPPublicKeyRing(), signingPrivateKey, newUserId);
            key.setPGPPublicKeyRing(modifiedPublicKeyRing);
            updateKeyMaterial(key);
        } catch (PGPException e) {
            throw KeysExceptionCodes.PGP_ERROR.create(e, e.getMessage());
        }

    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.GuardKeyService#revokeKey(com.openexchange.guard.keymanagement.commons.GuardKeys, com.openexchange.guard.keymanagement.services.RevocationReason, java.lang.String)
     */
    @Override
    public void revokeKey(GuardKeys key, long keyId, RevocationReason revocationReason, String password) throws OXException {
        key = Objects.requireNonNull(key, "key must not be null");

        //Getting the private key used for singing
        PGPPrivateKey privateKey = null;
        try {
            PGPSecretKey secretKey = key.getPGPSecretKey();
            if (secretKey == null) {
                throw KeysExceptionCodes.NO_KEY_ERROR.create();
            }
            privateKey = PGPUtil.decodePrivate(key.getPGPSecretKey(), password, key.getSalt());
        } catch (PGPException e) {
            throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
        }

        PGPPublicKeyRing modifiedKeyRing = null;
        try {
            modifiedKeyRing = PGPKeysUtil.revokeKey(privateKey, key.getPGPPublicKeyRing(), keyId, revocationReason.toString());
        } catch (PGPException e) {
            KeysExceptionCodes.PGP_ERROR.create(e, e.getMessage());
        }

        //Set and update
        key.setPGPPublicKeyRing(modifiedKeyRing);
        updateKeyMaterial(key);
    }
}
