/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl.lookup;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.OGPGPKey;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.commons.trust.GuardKeySources;
import com.openexchange.guard.keymanagement.services.impl.osgi.Services;
import com.openexchange.guard.keymanagement.services.lookup.RecipKeyLookupStrategy;
import com.openexchange.guard.keymanagement.storage.OGPGPKeysStorage;
import com.openexchange.pgp.keys.parsing.PGPPublicKeyRingFactory;

/**
 * {@link UserUploadedRecipKeyLookupStrategy} searches for a {@link RecipKey} in the user's list of custom uploaded public keys.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class UserUploadedRecipKeyLookupStrategy implements RecipKeyLookupStrategy {

    private final int userId;
    private final int contextId;

    /**
     * Initializes a new {@link UserUploadedRecipKeyLookupStrategy}.
     *
     * @param userId The ID of the user who wants to lookup a certain key
     * @param contextId The context ID of the user who wants to lookup a certain key
     */
    public UserUploadedRecipKeyLookupStrategy(int userId, int contextId) {
        this.userId = userId;
        this.contextId = contextId;
    }

    /**
     * Internal method for converting a {@link OGPGPKey} object to a {@link GuardKeys} object
     *
     * @param ogPGPKey The {@link OGPGPKey} object.
     * @return The constructed {@link GuardKeys} object.
     * @throws OXException
     */
    private GuardKeys toGuardKeys(OGPGPKey ogPGPKey) throws OXException {
        GuardKeys guardKey = new GuardKeys();
        try {
            guardKey.setPGPPublicKeyRing(PGPPublicKeyRingFactory.create(ogPGPKey.getPublicPGPAscData()));
        } catch (IOException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
        guardKey.setInline(ogPGPKey.isInline());
        guardKey.setContextid(ogPGPKey.getContextId());
        guardKey.setUserid(ogPGPKey.getUserId());
        return guardKey;
    }

    /**
     * Internal method for converting a {@link GuardKeys} object to a {@link RecipKey} object
     *
     * @param key The {@link GuardKeys} object.
     * @param shared Whether or not the key is shared among users in the same context
     * @return The constructed {@link RecipKey} object.
     * @throws OXException
     */
    private RecipKey toRecipKey(GuardKeys key, boolean shared) {
        if (shared) {
            return new RecipKey(key, GuardKeySources.GUARD_USER_SHARED);
        } else {
            return new RecipKey(key, GuardKeySources.GUARD_USER_UPLOADED);
        }
    }

    /**
     * Internal method for converting a {@link OGPGPKey} object to a {@link RecipKey} object
     *
     * @param ogPGPKey The {@link OGPGPKey} object.
     * @param shared Whether or not the key is shared among users in the same context
     * @return The constructed {@link RecipKey} object.
     * @throws OXException
     */
    private RecipKey toRecipKey(OGPGPKey ogPGPKey, boolean shared) throws OXException {
        return toRecipKey(toGuardKeys(ogPGPKey), shared);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keymanagement.services.impl.RecipKeyLookupStrategy#lookup(java.lang.String)
     */
    @Override
    public RecipKey lookup(String email, int timeout) throws OXException {
        OGPGPKeysStorage ogpgpKeysStorage = Services.getService(OGPGPKeysStorage.class);
        List<OGPGPKey> ogPGPKeys = ogpgpKeysStorage.getForUserByEmail(userId, contextId, Arrays.asList(new String[] { email }));
        for (OGPGPKey ogPGPKey : ogPGPKeys) {
            //Check if the key was uploaded by the user or shared by another user
            RecipKey recipKey = toRecipKey(ogPGPKey, ogPGPKey.getUserId() != userId);
            if (recipKey.getEncryptionKey() != null) {
                return recipKey;
            }
        }
        return null;
    }
}
