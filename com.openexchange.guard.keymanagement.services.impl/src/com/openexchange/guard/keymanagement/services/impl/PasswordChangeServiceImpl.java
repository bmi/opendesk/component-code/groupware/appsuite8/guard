package com.openexchange.guard.keymanagement.services.impl;
import java.security.PrivateKey;
import java.util.Date;
import org.bouncycastle.openpgp.PGPEncryptedData;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.openexchange.exception.OXException;
import com.openexchange.guard.antiabuse.GuardAntiAbuseService;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.common.auth.PasswordChangedResult;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.exceptions.KeysExceptionCodes;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.KeyRecoveryService;
import com.openexchange.guard.keymanagement.services.PasswordChangeService;
import com.openexchange.guard.keymanagement.services.impl.exceptions.GuardChangePasswordExceptionCodes;
import com.openexchange.guard.keymanagement.services.impl.exceptions.GuardCryptoExceptionCodes;
import com.openexchange.guard.keymanagement.services.impl.osgi.Services;
import com.openexchange.guard.secondfactor.SecondFactorService;
import com.openexchange.guard.session.GuardSessionService;
import com.openexchange.guard.user.OXUserService;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;

public class PasswordChangeServiceImpl implements PasswordChangeService {

    private static final Logger LOG = LoggerFactory.getLogger(PasswordChangeService.class);

    @Override
    public void checkPasswordRequirements(String newpass, int userId, int cid) throws OXException {
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        int minPasswordLength = configService.getIntProperty(GuardProperty.minPasswordLength, userId, cid);
        if (newpass.length() < minPasswordLength) {
            throw GuardChangePasswordExceptionCodes.BAD_LENGTH.create(minPasswordLength);
        }
    }

    @Override
    public PasswordChangedResult changePassword(int userid, int cid, String userEmail, String oldpass, String newpass, String question, String answer, String pin, String sessionId) throws OXException {
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        GuardAntiAbuseService antiabuse = Services.getService(GuardAntiAbuseService.class);
        if (antiabuse.isBad(sessionId, configService.getIntProperty(GuardProperty.badPasswordCount))) {
            LOG.debug("Lockout for bad attempts");
            throw GuardAuthExceptionCodes.LOCKOUT.create();
        }

        checkPasswordRequirements(newpass, userid, cid);

        // OK, logged in or guest, so we can change
        LOG.debug("change password request");

        GuardKeyService keyService = Services.getService(GuardKeyService.class);
        GuardKeys keys = keyService.getKeys(userid, cid);
        if (keys == null) {
            // Keys not found.  Possible Guest user
            OXUserService userService = Services.getService(OXUserService.class);
            if (userService.isGuest(userid, cid)) {  // Make sure guest.  If so, try to load keys based on email
                keys = keyService.getKeys(userEmail);
            }
            if (keys == null) {
                throw GuardCoreExceptionCodes.KEY_NOT_FOUND.create();
            }
        }
        boolean hasPin = false;
        SecondFactorService secondFactor = Services.getService(SecondFactorService.class);
        if (secondFactor.hasSecondFactor(userid, cid, keys.getUserid(), keys.getContextid())) {
            hasPin = true;
            secondFactor.verifySecondFactor(userid, cid, keys.getUserid(), keys.getContextid(), pin);
        }
        String oldQuestion = keys.getQuestion();
        if (oldpass == null || oldpass.equals("")) {
            keys = changeTemporaryPassword(keys.getUserid(), keys.getContextid(), newpass);
        } else {
            keys = changePassword(oldpass, newpass, keys);
        }
        if (keys == null) {
            LOG.info("Bad password for change");
            antiabuse.addBad(sessionId);
            throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
        }
        if (hasPin) {
            secondFactor.removeSecondFactorIfSingleUse(keys.getUserid(), keys.getContextid());
        }
        String recovery = "";
        boolean recoveryAvail = false;
        KeyRecoveryService recoveryService = Services.getService(KeyRecoveryService.class);
        if (recoveryService.checkCreateRecovery(cid, userid)) {
            recoveryAvail = true;
            recovery = recoveryService.createRecovery(keys, newpass);
        }
        Date now = new Date();
        keys.setLastup(new java.sql.Date(now.getTime()));
        keyService.updateKeys(keys.getUserid(), keys.getContextid(), keys, recovery, false);

        if (cid < 0) {  // Guests have question/answer to store
            if (question.equals("") && !answer.equals("")) {
                question = "default";
            }
            if (answer != null && !answer.equals("")) {
                keyService.storeQuestion(keys.getUserid(), keys.getContextid(), question, answer);
            }
        } else {
            if (oldQuestion == null || ((answer != null) && (answer.length() > 1))) {
                keyService.storeQuestion(keys.getUserid(), keys.getContextid(), question, answer);
            }
        }

        // String sessionID = json.get("sessionID").getAsString();
        GuardSessionService sessionService = Services.getService(GuardSessionService.class);
        String token = sessionService.getToken(sessionId);
        if (token == null) {
            token = sessionService.newToken(sessionId, userid, cid);
        }
        GuardCipherService cipherService = Services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.AES_GCM);

        // The enrypted auth data
        JsonObject json = new JsonObject();
        json.addProperty("user_id", keys.getUserid());
        json.addProperty("cid", keys.getContextid());
        json.addProperty("email", keys.getEmail());
        json.add("encr_password", new JsonPrimitive(newpass));
        String encryptedAuth = cipherService.encrypt(json.toString(), token);
        LOG.info("OK Password Change");
        return new PasswordChangedResult(encryptedAuth).withRecovery(recoveryAvail);
    }

    @Override
    public GuardKeys changeTemporaryPassword (int userid, int cid, String newpass) throws OXException {
        GuardKeyService keyService = Services.getService(GuardKeyService.class);
        GuardKeys oldkeys = keyService.getKeys(userid, cid);
        if (oldkeys == null) {
            LOG.error("Unable to retrieve keys for " + userid + " cid " + cid);
            throw KeysExceptionCodes.NO_KEY_ERROR.create();
        }
        if (oldkeys.getLastup() != null || (oldkeys.getQuestion() != null && !oldkeys.getQuestion().equals("PIN"))) {  // Only allowed for first use
            LOG.error("Unable to change temporary password for %d cid %d due to already updated", userid, cid);
            throw KeysExceptionCodes.PREVIOUSLY_CHANGED.create();
        }
        String hash = keyService.recoverPasswordHash(oldkeys.getUserid(), oldkeys.getContextid());
        if (hash.equals("")) {
            throw KeysExceptionCodes.NO_RECOVERY_ERROR.create();
        }
        GuardKeys newkeys = changePasswordWithRecovery(hash, newpass, oldkeys);
        return (newkeys);

    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keys.GuardKeyService#changePassword(java.lang.String, java.lang.String, com.openexchange.guard.keys.dao.GuardKeys)
     */
    @Override
    public GuardKeys changePassword(String oldpass, String newpass, GuardKeys oldKey) throws OXException {
        GuardKeyService keyService = Services.getService(GuardKeyService.class);
        if (!oldKey.privKeyNull()) {
            PrivateKey priv = keyService.decryptPrivateKey(oldKey.getEncodedPrivate(), oldpass, oldKey.getSalt());
            if (priv == null) {
                return null;
            }
            String encodedPrivateKey = keyService.encryptPrivateKey(priv, newpass, oldKey.getSalt());
            oldKey.setEncodedPrivate(encodedPrivateKey);
        }
        oldpass = CipherUtil.getSHA(oldpass, oldKey.getSalt());
        newpass = CipherUtil.getSHA(newpass, oldKey.getSalt());
        PGPSecretKeyRing keyring = oldKey.getPGPSecretKeyRing();
        try {
            PGPSecretKeyRing newkeyring = PGPKeysUtil.duplicateSecretKeyRing(keyring, oldpass, newpass, getPGPEncryptedData());
            if (newkeyring == null) {
                return null;
            }
            oldKey.setPGPSecretKeyRing(newkeyring);
            return oldKey;
        } catch (PGPException e) {
            LOG.error("Error changing password: {}", e.getMessage(), e);
            return null;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keys.GuardKeyService#changePasswordWithRecovery(java.lang.String, java.lang.String, com.openexchange.guard.keys.dao.GuardKeys)
     */
    @Override
    //FIXME: double code with changePassword, remove that
    public GuardKeys changePasswordWithRecovery(String oldPassHash, String newPass, GuardKeys oldKey) throws OXException {
        GuardKeyService keyService = Services.getService(GuardKeyService.class);
        if (!oldKey.privKeyNull()) {
            PrivateKey priv = keyService.decryptPrivateKeyWithHash(oldKey.getEncodedPrivate(), oldPassHash, oldKey.getSalt());
            if (priv == null) {
                LOG.error("Failed to change password");
                throw KeysExceptionCodes.NO_KEY_ERROR.create(oldKey.getEmail());
            }
            String encodedPrivateKey = keyService.encryptPrivateKey(priv, newPass, oldKey.getSalt());
            oldKey.setEncodedPrivate(encodedPrivateKey);
        }
        newPass = CipherUtil.getSHA(newPass, oldKey.getSalt());
        PGPSecretKeyRing keyring = oldKey.getPGPSecretKeyRing();
        try {
            PGPSecretKeyRing newkeyring = PGPKeysUtil.duplicateSecretKeyRing(keyring, oldPassHash, newPass, getPGPEncryptedData());
            if (newkeyring == null) {
                LOG.error("Failed to change password");
                throw KeysExceptionCodes.NO_KEY_ERROR.create(oldKey.getEmail());
            }
            oldKey.setPGPSecretKeyRing(newkeyring);
            return oldKey;
        } catch (PGPException e) {
            LOG.error("Error changing password: {}", e.getMessage(), e);
            throw GuardCryptoExceptionCodes.PGP_ENCODING_DECODING_ERROR.create(e.getMessage());
        }
    }

    /**
     * Get the key length
     *
     * @return The key length
     * @throws OXException
     */
    private int getPGPEncryptedData() throws OXException {
        GuardConfigurationService guardConfigService = Services.getService(GuardConfigurationService.class);
        int keyLength = guardConfigService.getIntProperty(GuardProperty.aesKeyLength);
        if (keyLength == 256) {
            return PGPEncryptedData.AES_256;
        }
        return PGPEncryptedData.AES_128;
    }
}
