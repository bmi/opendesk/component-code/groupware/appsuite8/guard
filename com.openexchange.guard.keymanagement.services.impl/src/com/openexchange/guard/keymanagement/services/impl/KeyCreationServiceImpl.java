/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Locale;
import org.bouncycastle.bcpg.CompressionAlgorithmTags;
import org.bouncycastle.bcpg.HashAlgorithmTags;
import org.bouncycastle.bcpg.SymmetricKeyAlgorithmTags;
import org.bouncycastle.bcpg.sig.Features;
import org.bouncycastle.bcpg.sig.KeyFlags;
import org.bouncycastle.openpgp.PGPEncryptedData;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPKeyPair;
import org.bouncycastle.openpgp.PGPKeyRingGenerator;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureSubpacketGenerator;
import org.bouncycastle.openpgp.operator.PBESecretKeyEncryptor;
import org.bouncycastle.openpgp.operator.PGPDigestCalculator;
import org.bouncycastle.openpgp.operator.bc.BcPBESecretKeyEncryptorBuilder;
import org.bouncycastle.openpgp.operator.bc.BcPGPContentSignerBuilder;
import org.bouncycastle.openpgp.operator.bc.BcPGPDigestCalculatorProvider;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPKeyConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.KeyCreationService;
import com.openexchange.guard.keymanagement.services.KeyPairSource;
import com.openexchange.guard.keymanagement.services.KeyRecoveryService;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.keymanagement.services.impl.exceptions.GuardKeyCreationExceptionCodes;
import com.openexchange.guard.update.internal.GuardKeysUpdater;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.encoding.Base64;

/**
 * {@link KeyCreationServiceImpl} default implementation for {@link KeyCreationService}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class KeyCreationServiceImpl implements KeyCreationService {

    private static final Logger LOG = LoggerFactory.getLogger(KeyCreationServiceImpl.class);
    private static final int[] PREFERRED_SYMETRIC_ALGORITHMS = new int[] { SymmetricKeyAlgorithmTags.AES_256, SymmetricKeyAlgorithmTags.AES_192, SymmetricKeyAlgorithmTags.AES_128 };
    private static int[] PREFERRED_HASH_ALGORITHMS = new int[] { HashAlgorithmTags.SHA512, HashAlgorithmTags.SHA384, HashAlgorithmTags.SHA256, HashAlgorithmTags.SHA224, HashAlgorithmTags.SHA1 };
    private static int[] PREFERRED_COPMRESSION = new int[] { CompressionAlgorithmTags.ZLIB, CompressionAlgorithmTags.BZIP2, CompressionAlgorithmTags.ZIP, CompressionAlgorithmTags.UNCOMPRESSED };
    private KeyPairSource defaultKeyPairSource;
    private final ServiceLookup services;
    private GuardCipherService cipherService;

    /**
     * Initializes a new {@link KeyCreationServiceImpl}.
     *
     * @param cipherService The {@link GuardCipherService}
     * @param keyService The {@link GuardKeyService}
     * @param keyRecoveryService The {@link KeyRecoveryService}
     */
    public KeyCreationServiceImpl(ServiceLookup services, GuardCipherService cipherService) {
        this.services = services;
        this.cipherService = cipherService;
    }

    private int getSymetricKeyAlgorithm(int userId, int cid) throws OXException {
        GuardConfigurationService guardConfigurationService = services.getServiceSafe(GuardConfigurationService.class);
        int rsaKeyLength = guardConfigurationService.getIntProperty(GuardProperty.aesKeyLength, userId, cid);
        switch (rsaKeyLength) {
            case 128:
                return PGPEncryptedData.AES_128;
            case 192:
                return PGPEncryptedData.AES_192;
            case 256:
                return PGPEncryptedData.AES_256;
            default:
                throw new IllegalArgumentException("Invalid configured AES keysize: " + rsaKeyLength);
        }
    }

    private int getKeyExpirationDays(int userId, int cid) throws OXException {
        GuardConfigurationService guardConfigurationService = services.getServiceSafe(GuardConfigurationService.class);
        return guardConfigurationService.getIntProperty(GuardProperty.keyValidDays, userId, cid);
    }

    private KeyRecoveryService getKeyRecoveryService() throws OXException {
        return services.getServiceSafe(KeyRecoveryService.class);
    }

    private MasterKeyService getMasterKeyService() throws OXException {
        return services.getServiceSafe(MasterKeyService.class);
    }

    /**
     * Internal method for creating a recovery token for the new key
     *
     * @param keys The key to create a recovery token for
     * @param password The current password
     * @return The recovery token created for the given key
     * @throws OXException
     */
    private String createKeyRecovery(GuardKeys keys, String password) throws OXException {
        return getKeyRecoveryService().createRecovery(keys, password);
    }

    private String encryptPrivateKey(PrivateKey privateKey, String password, String salt) throws OXException {
        return cipherService.encrypt(Base64.encode(privateKey.getEncoded()), CipherUtil.getSHA(password, salt), salt);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.KeyCreationService#getKeyPairSource()
     */
    @Override
    public KeyPairSource getKeyPairSource() {
        return this.defaultKeyPairSource;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.KeyCreationService#setKeyPairSource(com.openexchange.guard.keymanagement.services.KeyPairSource)
     */
    @Override
    public void setKeyPairSource(KeyPairSource keyPairSource) {
        this.defaultKeyPairSource = keyPairSource;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keymanagement.services.KeyCreationService#create(java.lang.String, java.lang.String, char[])
     */
    @Override
    public GuardKeys create(int userid, int contextid, String name, String email, String password, Locale locale, boolean markAsCurrent, boolean createRecovery, int oxUserId, int oxCid) throws OXException {
        return create(this.defaultKeyPairSource, userid, contextid, name, email, password, locale, markAsCurrent, createRecovery, oxUserId, oxCid);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keymanagement.services.KeyCreationService#create(java.lang.String, java.lang.String, char[])
     */
    @Override
    public GuardKeys create(int userid, int contextid, String name, String email, String password, Locale locale, boolean markAsCurrent, boolean createRecovery) throws OXException {
        // Create keys for non-guests users.
        return create(this.defaultKeyPairSource, userid, contextid, name, email, password, locale, markAsCurrent, createRecovery, 0, 0);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keymanagement.services.KeyCreationService#create(com.openexchange.guard.keymanagement.services.KeyPairCreationStrategy, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public GuardKeys create(KeyPairSource keyPairSource, int userid, int contextid, String name, String email, String password, Locale locale, boolean markAsCurrent, boolean createRecovery, int oxUserId, int oxCid) throws OXException {

        try {
            long start = System.currentTimeMillis();
            // Avoid key name being null
            if (name == null || name.isEmpty()) {
                name = email;
            }

            String salt = CipherUtil.generateSalt();
            char[] pass = CipherUtil.getSHA(password, salt).toCharArray();
            if (password.equals("")) { // If password is blank (unknown), then just create with salt as password
                pass = CipherUtil.getSHA(salt, salt).toCharArray();
                password = salt;
            }

            //Creating raw key pairs for master and for the sub key
            PGPKeyPair masterKey = keyPairSource.get();
            PGPKeyPair subKey = keyPairSource.get();

            if (masterKey == null || subKey == null) {
                throw GuardKeyCreationExceptionCodes.KEY_CREATION_ERROR.create("Unable to retrieve/generate new keys");
            }

            PGPDigestCalculator sha1Calc = new BcPGPDigestCalculatorProvider().get(HashAlgorithmTags.SHA1);
            PGPDigestCalculator sha256Calc = new BcPGPDigestCalculatorProvider().get(HashAlgorithmTags.SHA256);

            // Encrypting the private key
            PBESecretKeyEncryptor keyEncr = new BcPBESecretKeyEncryptorBuilder(getSymetricKeyAlgorithm(userid, contextid), sha256Calc).build(pass);

            //Add self signed signature to the master
            PGPSignatureSubpacketGenerator signhashgenGenerator = new PGPSignatureSubpacketGenerator();
            signhashgenGenerator.setKeyFlags(false, KeyFlags.SIGN_DATA | KeyFlags.CERTIFY_OTHER);
            signhashgenGenerator.setPreferredSymmetricAlgorithms(false, PREFERRED_SYMETRIC_ALGORITHMS);
            signhashgenGenerator.setPreferredHashAlgorithms(false, PREFERRED_HASH_ALGORITHMS);
            signhashgenGenerator.setPreferredCompressionAlgorithms(false, PREFERRED_COPMRESSION);
            signhashgenGenerator.setFeature(true, Features.FEATURE_MODIFICATION_DETECTION);

            // Now for subKey (encrypting)
            PGPSignatureSubpacketGenerator enchashgen = new PGPSignatureSubpacketGenerator();
            enchashgen.setKeyFlags(false, KeyFlags.ENCRYPT_COMMS | KeyFlags.ENCRYPT_STORAGE);
            enchashgen.setPreferredSymmetricAlgorithms(false, PREFERRED_SYMETRIC_ALGORITHMS);
            enchashgen.setPreferredHashAlgorithms(false, PREFERRED_HASH_ALGORITHMS);
            enchashgen.setPreferredCompressionAlgorithms(false, PREFERRED_COPMRESSION);
            enchashgen.setFeature(true, Features.FEATURE_MODIFICATION_DETECTION);

            //Set expiration days
            final int keyExpirationDays = getKeyExpirationDays(userid, contextid);
            if (keyExpirationDays != 0) {
                int seconds = keyExpirationDays * 24 * 60 * 60;
                signhashgenGenerator.setKeyExpirationTime(false, seconds);
                enchashgen.setKeyExpirationTime(false, seconds);
            }

            //Creating the key generator
            PGPKeyRingGenerator keyRingGen =
                new PGPKeyRingGenerator(PGPSignature.POSITIVE_CERTIFICATION, masterKey, name + " <" + email + ">", sha1Calc, signhashgenGenerator.generate(), null, new BcPGPContentSignerBuilder(masterKey.getPublicKey().getAlgorithm(), HashAlgorithmTags.SHA256), keyEncr);
            keyRingGen.addSubKey(subKey, enchashgen.generate(), null);

            // Get the private and public encr RSA keys
            JcaPGPKeyConverter conv = new JcaPGPKeyConverter();
            PrivateKey privkey = conv.getPrivateKey(subKey.getPrivateKey());
            PublicKey pubkey = conv.getPublicKey(subKey.getPublicKey());
            String encryptedPrivateKey = encryptPrivateKey(privkey, password, salt);
            // Create key object
            GuardKeys keys = new GuardKeys(keyRingGen, encryptedPrivateKey, pubkey, password, salt);
            keys.setEmail(email);
            keys.setUserid(userid);
            keys.setContextid(contextid);
            keys.setLocale(locale);
            keys.setCurrent(markAsCurrent);
            keys.setMasterKeyIndex(contextid >= 0 ? getMasterKeyService().getIndexForUser(userid, contextid) : getMasterKeyService().getIndexForUser(oxUserId, oxCid));
            keys.setRecovery(createRecovery ? createKeyRecovery(keys, password) : "");
            JsonObject version = new JsonObject();
            version.addProperty("version", GuardKeysUpdater.getCurrentVersion());
            keys.setMisc(version.toString());
            keys.setKeyid(keys.getPGPPublicKeyRing().getPublicKey().getKeyID());
            LOG.debug("Created PGP/RSA Keys at " + start);
            return keys;
        } catch (PGPException e) {
            throw GuardKeyCreationExceptionCodes.KEY_CREATION_ERROR.create(e.getMessage());
        }
    }
}
