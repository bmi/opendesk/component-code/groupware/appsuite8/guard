/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.bouncycastle.openpgp.PGPEncryptedData;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.bouncycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.PGPKeys;
import com.openexchange.guard.keymanagement.commons.util.PGPMergeUtil;
import com.openexchange.guard.keymanagement.commons.util.PGPUtil;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.KeyImportService;
import com.openexchange.guard.keymanagement.services.KeyRecoveryService;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.keymanagement.services.PasswordChangeService;
import com.openexchange.guard.keymanagement.services.impl.exceptions.GuardKeyImportExceptionCodes;
import com.openexchange.guard.keymanagement.services.impl.osgi.Services;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.guard.keymanagement.storage.PGPKeysStorage;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;
import com.openexchange.server.ServiceLookup;

/**
 * {@link KeyImportServiceImpl}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class KeyImportServiceImpl implements KeyImportService {

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(KeyImportServiceImpl.class);
    private ServiceLookup services;
    
    /**
     * Initializes a new {@link KeyImportServiceImpl}.
     *
     * @param ServiceLookup
     */
    public KeyImportServiceImpl(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Gets the guard default symmetric encryption algorithm used for PGP keys
     *
     * @return The ID of the symmetric encryption algorithm
     * @throws OXException
     */
    private int getEncryptionAlgorithm() throws OXException {
        GuardConfigurationService guardConfigService = Services.getService(GuardConfigurationService.class);
        int keyLength = guardConfigService.getIntProperty(GuardProperty.aesKeyLength);
        if (keyLength == 256) {
            return (PGPEncryptedData.AES_256);
        }
        return (PGPEncryptedData.AES_128);
    }

    private PGPKeysStorage getPGPKeyStorage() throws OXException {
        return services.getServiceSafe(PGPKeysStorage.class);
    }

    private GuardKeyService getKeyService() throws OXException {
        return services.getServiceSafe(GuardKeyService.class);
    }

    private KeyTableStorage getKeyTableStorage() throws OXException {
        return services.getServiceSafe(KeyTableStorage.class);
    }

    /**
     * Extracts the {@link PGPPublicKeyRing} from a given {@link PGPSecretKeyRing}
     *
     * @param secretKeyRing The secret key ring to extract the public key ring from.
     * @return The public key ring extracted from the given secret key ring.
     * @throws OXException
     */
    private PGPPublicKeyRing createPublicKeyRingFromPrivateKeyRing(PGPSecretKeyRing secretKeyRing) throws OXException {
        PGPPublicKeyRing publicKeyRing = null;

        for (Iterator<PGPPublicKey> it = secretKeyRing.getPublicKeys(); it.hasNext();) {
            PGPPublicKey key = it.next();
            if (publicKeyRing == null) {
                try {
                    publicKeyRing = new PGPPublicKeyRing(key.getEncoded(), new JcaKeyFingerprintCalculator());
                } catch (IOException e) {
                    throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
                }
            } else {
                publicKeyRing = PGPPublicKeyRing.insertPublicKey(publicKeyRing, key);
            }
        }
        if (publicKeyRing != null) {
            for (Iterator<PGPPublicKey> it2 = secretKeyRing.getExtraPublicKeys(); it2.hasNext();) {
                PGPPublicKey key = it2.next();
                PGPPublicKeyRing.insertPublicKey(publicKeyRing, key);
            }
        }
        return publicKeyRing;
    }

    /**
     * Checks if a key contains a given user identity
     *
     * @param keys The key
     * @param identity The user identity
     * @return True, if the key contains the identity, false otherwise.
     */
    private boolean hasIdentity(GuardKeys keys, String identity) {
        if (keys.getPGPPublicKeyRing() != null) {
            Iterator<PGPPublicKey> iter = keys.getPGPPublicKeyRing().getPublicKeys();
            if (iter != null) {
                while (iter.hasNext()) {
                    PGPPublicKey publicKey = iter.next();
                    Iterator userIdIter = publicKey.getUserIDs();
                    while (userIdIter.hasNext()) {
                        String userId = (String) userIdIter.next();
                        if (userId.toLowerCase().contains(keys.getEmail().toLowerCase())) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Helper method to get a {@link PGPKeys} mapping object for a given {@link GuardKeys} object.
     *
     * @param keys The key
     * @return The {@link PGPKeys} mapping for the given key, or null if no such mapping was found
     * @throws OXException
     */
    private PGPKeys getExistingKeyMapping(GuardKeys keys) throws OXException {

        //getting all public key IDs
        List<Long> publicKeyIds = new ArrayList<Long>();
        Iterator<PGPPublicKey> publicKeyIter = keys.getPGPPublicKeyRing().getPublicKeys();
        while (publicKeyIter.hasNext()) {
            publicKeyIds.add(publicKeyIter.next().getKeyID());
        }

        //Search if we already have one of the key ids for the user
        PGPKeysStorage pgpKeysStorage = getPGPKeyStorage();
        List<PGPKeys> keyMappings = pgpKeysStorage.getByIds(publicKeyIds);
        return keyMappings.size() > 0 ? keyMappings.get(0) : null;
    }

    /**
     * Internal method to import a PGPSecretKeyRing
     *
     * @param userId The ID of the user
     * @param contextId The ID of the user's context
     * @param email The user's email
     * @param locale The user's locale
     * @param password The password associated with the given secret key ring.
     * @param newPassword The password to set during import
     * @param secretKeyRing The key ring to import.
     * @return The imported object
     * @throws OXException
     */
    private GuardKeys importPrivateKeyRing(int userId, int contextId, String email, Locale locale, String password, String newPassword, PGPSecretKeyRing secretKeyRing) throws OXException {

        GuardKeyService guardKeyService = getKeyService();
        KeyRecoveryService keyRecoveryService = services.getServiceSafe(KeyRecoveryService.class);
        GuardKeys newKeys = new GuardKeys();
        newKeys.setEmail(email);
        newKeys.setUserid(userId);
        newKeys.setContextid(contextId);
        newKeys.setLocale(locale);
        PasswordChangeService passwordChangeService = Services.getService(PasswordChangeService.class);
        passwordChangeService.checkPasswordRequirements(newPassword, userId, contextId);
        final String realNewPassword = CipherUtil.getSHA(newPassword, newKeys.getSalt());

        //duplicate the key with the salted password
        try {
            secretKeyRing = PGPKeysUtil.duplicateSecretKeyRing(secretKeyRing, password, realNewPassword, getEncryptionAlgorithm());
        } catch (PGPException e) {
            logger.error("Error while duplicating SecretKeyRing: " + e.getMessage());
            if (e.getMessage().contains("checksum")) {
                throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
            }
            else {
                throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
            }
        }

        //apply secret key ring
        newKeys.setPGPSecretKeyRing(secretKeyRing);

        //apply public key ring
        newKeys.setPGPPublicKeyRing(createPublicKeyRingFromPrivateKeyRing(secretKeyRing));
        newKeys.setKeyid(newKeys.getPGPPublicKeyRing().getPublicKey().getKeyID());
        //Verify that the password works
        if(!PGPUtil.verifyPassword(newKeys.getPGPSecretKey(), newPassword, newKeys.getSalt())) {
            throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
        }

        //Check if the uploaded key contains the user's email as user id
        if(!hasIdentity(newKeys, email)) {
            throw GuardKeyImportExceptionCodes.NO_SUCH_USERID_IN_KEY_ERROR.create();
        }

        //Create a password recovery for the new key if desired
        if(keyRecoveryService.checkCreateRecovery(contextId, userId)) {
            String passwordRecovery = keyRecoveryService.createRecovery(newKeys, newPassword);
            newKeys.setRecovery(passwordRecovery);
        }

        //Check if we already have the key
        PGPKeys existingKeyMapping = getExistingKeyMapping(newKeys);
        final boolean isDuplicated = existingKeyMapping != null;
        if(!isDuplicated) { //Add new key
            GuardKeys currentKey = guardKeyService.getKeys(userId, contextId);
            guardKeyService.addNewKey(newKeys);
            if (currentKey != null) {
                guardKeyService.updateSettingsForUser(currentKey);
                newKeys.setQuestion(currentKey.getQuestion());
                MasterKeyService mKeyService = Services.getService(MasterKeyService.class);
                if (mKeyService != null) {
                    String decrypted = mKeyService.getRcDecrypted(currentKey.getAnswer(), currentKey.getQuestion(), currentKey.getMasterKeyIndex());
                    newKeys.setAnswer(mKeyService.getRcEncryted(decrypted, newKeys.getQuestion(), newKeys.getMasterKeyIndex()));
                }
                guardKeyService.updateAnswerQuestionForUser(newKeys);
            }
        }
        else { //Merge existing keys
            newKeys.setKeyid(existingKeyMapping.getKeyId());
            //Getting the existing key
            GuardKeys existingKeys = guardKeyService.getKeys(existingKeyMapping.getKeyId());
            if (existingKeys == null) {
                throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create("Problem retrieving existing key");
            }

            //Verify it has the same password
            if(existingKeys.hasPrivateKey() && !PGPUtil.verifyPassword(existingKeys.getPGPSecretKeyRing(), newPassword, existingKeys.getSalt())) {
                throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
            }

            //Merge the key rings
            PGPPublicKeyRing mergedKeyRings = PGPMergeUtil.mergeKeyRings(newKeys.getPGPPublicKeyRing(), existingKeys.getPGPPublicKeyRing());
            if(mergedKeyRings != null) {
                //..apply merged key ring
                newKeys.setPGPPublicKeyRing(mergedKeyRings);

                //Create recovery for merged key if desired
                if(keyRecoveryService.checkCreateRecovery(contextId, userId)) {
                    //The getPublicKey (RSAPublicKey) property is also taken into account when creating recovery
                    newKeys.setPublic(existingKeys.getPublicKey());
                    //create and set a new recovery token
                    String passwordRecovery = keyRecoveryService.createRecovery(newKeys, newPassword);
                    newKeys.setRecovery(passwordRecovery);
                }

                //Store keys
                //Add new potential sub-keys to Mapping table;
                getPGPKeyStorage().addPublicKeyIndex(contextId, newKeys.getKeyid(), email, newKeys.getPGPPublicKeyRing());
                //Store the merged key
                getKeyTableStorage().updateDuplicate(newKeys);
            } else {
                logger.error("Problem handling duplicate public key");
                throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create("Error while merging keys");
            }
        }
        return newKeys;
    }

    /**
     * Internal method to import a PGPPublicKeyRing
     *
     * @param userId The ID of the user
     * @param contextId The ID of the user's context
     * @param email The email of the user
     * @param locale The user's locale
     * @param publicKeyRing The PublicKeyRing to import
     * @return The imported object
     * @throws OXException
     */
    private GuardKeys importPublicKeyRing(int userId, int contextId, String email, Locale locale, PGPPublicKeyRing publicKeyRing) throws OXException {
        GuardKeyService guardKeyService = getKeyService();
        GuardKeys newKeys = new GuardKeys();
        newKeys.setEmail(email);
        newKeys.setUserid(userId);
        newKeys.setContextid(contextId);
        newKeys.setLocale(locale);
        newKeys.setPGPPublicKeyRing(publicKeyRing);

        //Check if the uploaded key contains the user's email as user id
        if(!hasIdentity(newKeys, email)) {
            throw GuardKeyImportExceptionCodes.NO_SUCH_USERID_IN_KEY_ERROR.create();
        }

        //Check if we already have the key
        PGPKeys existingKeyMapping = getExistingKeyMapping(newKeys);
        final boolean isDuplicated = existingKeyMapping != null;
        if(!isDuplicated) { //Add new Key
            guardKeyService.addNewKey(newKeys);
        }
        else { //Merge keys
            newKeys.setKeyid(existingKeyMapping.getKeyId());
            //Getting the existing key
            GuardKeys existingKeys = guardKeyService.getKeys(existingKeyMapping.getKeyId());
            if (existingKeys == null) {
                throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create("Error while merging keys, problem retrieving existing key");
            }

            //Merge the key rings
            PGPPublicKeyRing mergedKeyRings = PGPMergeUtil.mergeKeyRings(newKeys.getPGPPublicKeyRing(), existingKeys.getPGPPublicKeyRing());
            if(mergedKeyRings != null) {
                //..apply merged key ring
                newKeys.setPGPPublicKeyRing(mergedKeyRings);

                //Store public key only
                //Add new potential sub-keys to Mapping table
                getPGPKeyStorage().addPublicKeyIndex(contextId, newKeys.getKeyid(), email, newKeys.getPGPPublicKeyRing());
                //Store public key ring
                getKeyTableStorage().updatePublicKey(newKeys);
            }
            else {
                logger.error("Problem handling duplicate public key");
                throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create("Error while merging keys");
            }
        }
        return newKeys;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.KeyImportService#importPublicKeyRing(int, int, org.bouncycastle.openpgp.PGPPublicKeyRing)
     */
    @Override
    public Collection<GuardKeys> importPublicKeyRing(int userId, int contextId, String email, Locale locale, PGPPublicKeyRing...publicKeyRings) throws OXException {
        Collection<GuardKeys> importedKeyRings = new ArrayList<GuardKeys>(publicKeyRings.length);
        for(PGPPublicKeyRing ring : publicKeyRings) {
            importedKeyRings.add(importPublicKeyRing(userId, contextId, email, locale, ring));
        }
        return importedKeyRings;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.KeyImportService#importPrivateKeyRing(int, int, org.bouncycastle.openpgp.PGPSecretKeyRing)
     */
    @Override
    public Collection<GuardKeys> importPrivateKeyRing(int userId, int contextId, String email, Locale locale, String password, String newPassword, PGPSecretKeyRing...privateKeyRings) throws OXException {
        Collection<GuardKeys> importedKeyRings = new ArrayList<GuardKeys>(privateKeyRings.length);
        for(PGPSecretKeyRing ring : privateKeyRings) {
            importedKeyRings.add(importPrivateKeyRing(userId, contextId, email, locale, password, newPassword, ring));
        }
        return importedKeyRings;
    }
}
