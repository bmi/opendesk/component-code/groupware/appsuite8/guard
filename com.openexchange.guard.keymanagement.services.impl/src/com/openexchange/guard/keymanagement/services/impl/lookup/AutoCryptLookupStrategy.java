/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl.lookup;

import com.openexchange.exception.OXException;
import com.openexchange.guard.autocrypt.AutoCryptService;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.services.impl.osgi.Services;
import com.openexchange.guard.keymanagement.services.lookup.RecipKeyLookupStrategy;

/**
 * {@link AutoCryptLookupStrategy}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.2
 */
public class AutoCryptLookupStrategy implements RecipKeyLookupStrategy {

    private final int userId;
    private final int contextId;

    public AutoCryptLookupStrategy (int userId, int cid) {
        this.userId = userId;
        this.contextId = cid;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.lookup.RecipKeyLookupStrategy#lookup(java.lang.String, int)
     */
    @Override
    public RecipKey lookup(String email, int timeout) throws OXException {
        AutoCryptService autoCryptService = Services.getServiceLookup().getOptionalService(AutoCryptService.class);
        if (autoCryptService == null) {
            return null;
        }
        RecipKey recipKey = autoCryptService.getRecipKey(userId, contextId, email);
        if (recipKey != null && recipKey.getEncryptionKey() != null) {
            return recipKey;
        }
        return null;
    }

}
