/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.storage.impl.keytable;

class KeyTableSql {

    static final String OG_KEY_TABLE = "og_KeyTable";
    static final String SELECT_GUARD_DAEMON_KEY_STMT =
        "SELECT PGPSecret, PGPPublic, RSAPrivate, RSAPublic, Salt, misc, settings, email, lastMod, question, answer, cid, id, keyid, version, current, lang, Recovery, mKeyIndex FROM og_KeyTable WHERE id = -1 AND cid = 0";
    static final String SELECT_FOR_USER_STMT =
        "SELECT PGPSecret, PGPPublic, RSAPrivate, RSAPublic, Salt, misc, settings, email, lastMod, question, answer, cid, id, keyid, version, current, lang, Recovery, mKeyIndex FROM og_KeyTable WHERE id = ? AND cid = ? ORDER BY version DESC";
    static final String SELECT_FOR_USER_BY_KEYID_STMT =
        "SELECT PGPSecret, PGPPublic, RSAPrivate, RSAPublic, Salt, misc, settings, email, lastMod, question, answer, cid, id, keyid, version, current, lang, Recovery, mKeyIndex FROM og_KeyTable WHERE id = ? AND cid = ? AND keyid = ?";
    static final String SELECT_FOR_CONTEXT_STMT =
        "SELECT PGPSecret, PGPPublic, RSAPrivate, RSAPublic, Salt, misc, settings, email, lastMod, question, answer, cid, id, keyid, version, current, lang, Recovery, mKeyIndex FROM og_KeyTable WHERE cid = ?";
    static final String SELECT_FOR_EMAIL_IN_CONTEXT_STMT =
        "SELECT PGPSecret, PGPPublic, RSAPrivate, RSAPublic, Salt, misc, settings, email, lastMod, question, answer, cid, id, keyid, version, current, lang, Recovery, mKeyIndex FROM og_KeyTable WHERE email = ?";
    static final String SELECT_CURRENT_FOR_USER_STMT =
        "SELECT PGPSecret, PGPPublic, RSAPrivate, RSAPublic, Salt, misc, settings, email, lastMod, question, answer, cid, id, keyid, version, current, lang, Recovery, mKeyIndex FROM og_KeyTable WHERE id = ? AND cid = ? AND current=1";
    static final String SELECT_CURRENT_FOR_USER_EMAIL_STMT =
        "SELECT PGPSecret, PGPPublic, RSAPrivate, RSAPublic, Salt, misc, settings, email, lastMod, question, answer, cid, id, keyid, version, current, lang, Recovery, mKeyIndex FROM og_KeyTable WHERE email = ? AND current = 1";;
    static final String SELECT_HIGHEST_VERSION_KEY_STMT =
        "SELECT PGPSecret, PGPPublic, RSAPrivate, RSAPublic, Salt, misc, settings, email, lastMod, question, answer, cid, id, keyid, version, current, lang, Recovery, mKeyIndex FROM og_KeyTable WHERE cid = ? AND id = ? AND version=(SELECT MAX(version) from og_KeyTable where cid = ? and id = ?)";
    static final String INSERT_STMT =
        "INSERT INTO og_KeyTable (id, cid, PGPSecret, PGPPublic, RSAPrivate, RSAPublic, Recovery, Salt, email, lastMod, lang, keyid, version, question, answer, settings, current, misc, mKeyIndex) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    static final String INSERT_AND_SET_LASTMOD_STMT =
        "INSERT INTO og_KeyTable (id, cid, PGPSecret, PGPPublic, RSAPrivate, RSAPublic, Recovery, Salt, email, lastMod, lang, keyid, version, question, answer, settings, current, misc, mKeyIndex) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    static final String DELETE_KEY_STMT = "DELETE FROM og_KeyTable WHERE cid = ? AND id = ? AND keyid = ?";
    static final String DELETE_FOR_USER_STMT = "DELETE FROM og_KeyTable WHERE id = ? AND cid = ?";
    static final String DELETE_FOR_CONTEXT_STMT = "DELETE FROM og_KeyTable WHERE cid = ?";
    static final String DELETE_RECOVERY_FROM_KEY_STMT = "UPDATE og_KeyTable SET Recovery = '' WHERE id = ? and cid = ?";
    static final String UPDATE_PGP_PUBLIC_STMT = "UPDATE og_KeyTable SET PGPPublic = ? WHERE keyid = ?";
    static final String UPDATE_PGP_PUBLIC_AND_PRIVATE_STMT = "UPDATE og_KeyTable Set PGPSecret = ?, PGPPublic = ? WHERE id = ? AND cid = ? AND keyid = ?";
    static final String UPDATE_QUESTION_STMT = "UPDATE og_KeyTable SET question = ?, answer = ?" + " WHERE id = ? AND cid = ? AND keyid = ?";
    static final String UPDATE_PASSWORDS_AND_LASTMOD_STMT = "UPDATE og_KeyTable Set PGPSecret = ?, RSAPrivate = ?, Recovery = ?, lastMod = NOW() WHERE id = ? AND cid = ? AND keyid = ?;";
    static final String UPDATE_PASSWORDS_STMT = "UPDATE og_KeyTable Set PGPSecret = ?, RSAPrivate = ?, Recovery = ?, lastMod = NULL, question = IFNULL(question, '') WHERE id = ? AND cid = ? AND keyid = ?;";
    static final String UPDATE_DUPLICATED_STMT = "UPDATE og_KeyTable SET RSAPrivate = ?, PGPSecret = ?, PGPPublic = ?, Recovery = ?, Salt = ? WHERE id = ? AND cid = ? and keyid = ?";
    static final String UPDATE_PIN_STMT = "UPDATE og_KeyTable Set question = 'PIN', answer = ? WHERE id = ? AND cid = ?;";
    static final String UPDATE_VERSION_STMT = "UPDATE og_KeyTable SET version = ? WHERE cid = ? AND id = ? AND version = ?";
    static final String UPDATE_MISC_STMT = "UPDATE og_KeyTable SET misc = ? WHERE cid = ? AND id = ? AND keyid = ?";
    static final String UPDATE_SETTINGS_FOR_USER_STMT = "UPDATE og_KeyTable SET og_KeyTable.settings = ? WHERE og_KeyTable.id = ? and cid = ?";
    static final String SET_CURRENT_STMT = "UPDATE og_KeyTable SET current = 1 WHERE id = ? and cid = ? and keyid = ?";
    static final String UNSET_CURRENT_FOR_USER_STMT = "UPDATE og_KeyTable SET current = 0 WHERE id = ? and cid = ?";
    static final String EXISTS_STMT = "SELECT id FROM og_KeyTable LIMIT 1";
    static final String ALTER_AUTOINCREMENT_STMT = "ALTER TABLE `og_KeyTable` AUTO_INCREMENT=?";
    static final String GET_MIN_FROM_SYSTEM_TABLE = "SELECT MIN(id) FROM og_KeyTable";
    static final String UPDATE_EMAIL_ADDRESS = "UPDATE og_KeyTable SET email = ? WHERE cid = ? AND email = ?";
    static final String UPDATE_LOCALE = "UPDATE og_KeyTable SET lang = ? WHERE cid = ? AND id = ?";
}
