/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.storage.impl.keytable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.JsonUtil;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.GuardShardingService;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.guard.database.utils.GuardConnectionWrapper;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.export.KeyExportUtil;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.guard.keymanagement.storage.exceptions.GuardKeyTableStorageExceptionCodes;
import com.openexchange.guard.keymanagement.storage.impl.osgi.Services;

public class KeyTableStorageImpl implements KeyTableStorage {

    /**
     * Factory method to create a GuardKey from a database result set
     *
     * @param resultSet the result set to create the key from
     * @return A GuardKeys object created from the given result set
     * @throws OXException due an error
     */
    private GuardKeys createKey(ResultSet resultSet) throws OXException {
        String rsaPublic = null;
        try {
            GuardKeys key = new GuardKeys();
            key.setPGPSecretFromString(resultSet.getString("PGPSecret"));
            key.setPGPKeyRingFromAsc(resultSet.getString("PGPPublic"));
            key.setEncodedPrivate(resultSet.getString("RSAPrivate"));
            rsaPublic = resultSet.getString("RSAPublic");
            key.setPublicKeyFrom64String(rsaPublic);
            key.setSalt(resultSet.getString("Salt"));
            key.setMisc(resultSet.getString("misc"));
            key.setSettings(getJson(resultSet, "settings"));
            key.setEmail(resultSet.getString("email"));
            key.setLastup(resultSet.getTimestamp("lastMod"));
            key.setQuestion(resultSet.getString("question"));
            key.setAnswer(resultSet.getString("answer"));
            key.setContextid(resultSet.getInt("cid"));
            key.setUserid(resultSet.getInt("id"));
            key.setKeyid(resultSet.getLong("keyid"));
            key.setVersion(resultSet.getInt("version"));
            key.setCurrent(resultSet.getBoolean("current"));
            key.setLanguage(resultSet.getString("lang"));
            key.setRecovery(resultSet.getString("Recovery"));
            key.setMasterKeyIndex(resultSet.getInt("mKeyIndex"));
            if (key.getRecovery() != null) {
                if (key.getRecovery().equals("-1") || (key.getLastup() == null)) {
                    key.setPasswordNeeded(true);
                }
            }
            return key;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public List<GuardKeys> getKeysForUser(int userId, int contextId) throws OXException {
        GuardShardingService shardingService = Services.getService(GuardShardingService.class);
        int shard = 0;
        if (contextId < 0) {// If guest, get shard number, then check tables exist
            if(shardingService.hasShard(userId, contextId)) {
                shard = shardingService.getShard(userId, contextId);
            }
            else {
                return Collections.emptyList();
            }
        }
        return getKeysForUser(userId,contextId,shard);
    }

    @Override
    public List<GuardKeys> getKeysForUser(int userId, int contextId, int shard) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getReadOnly(userId, contextId, shard);
        Connection connection = connectionWrapper.getConnection();
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(KeyTableSql.SELECT_FOR_USER_STMT);
            stmt.setInt(1, userId);
            stmt.setInt(2, contextId);

            ArrayList<GuardKeys> ret = new ArrayList<GuardKeys>();
            resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                ret.add(createKey(resultSet));
            }
            return ret;

        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
    }

    @Override
    public GuardKeys getKeyForUserById(int userId, int contextId, String keyId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getReadOnly(userId, contextId, 0);

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.SELECT_FOR_USER_BY_KEYID_STMT);
            stmt.setInt(1, userId);
            stmt.setInt(2, contextId);
            stmt.setString(3, keyId);

            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return createKey(resultSet);
            }
            return null;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
    }

    @Override
    public GuardKeys getKeyForUserById(int userId, int contextId, int shardId, String keyId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getReadOnly(userId, contextId, 0);

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.SELECT_FOR_USER_BY_KEYID_STMT);
            stmt.setInt(1, userId);
            stmt.setInt(2, contextId);
            stmt.setString(3, keyId);

            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return createKey(resultSet);
            }
            return null;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
    }

    @Override
    public GuardKeys getHighestVersionKeyForUser(int userId, int contextId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getReadOnly(userId, contextId, 0);

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.SELECT_HIGHEST_VERSION_KEY_STMT);
            stmt.setInt(1, contextId);
            stmt.setInt(2, userId);
            stmt.setInt(3, contextId);
            stmt.setInt(4, userId);

            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return createKey(resultSet);
            }
            return null;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getCause());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
    }

    @Override
    public List<GuardKeys> getKeysForContext(int contextId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        Connection connection = guardDatabaseService.getReadOnly(contextId);

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(KeyTableSql.SELECT_FOR_CONTEXT_STMT);
            stmt.setInt(1, contextId);

            resultSet = stmt.executeQuery();
            ArrayList<GuardKeys> ret = new ArrayList<GuardKeys>();
            while (resultSet.next()) {
                ret.add(createKey(resultSet));
            }
            return ret;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnly(contextId, connection);
        }
    }

    @Override
    public GuardKeys getKeyForEmailAndContext(String email, int contextid) throws OXException {
        int shard = 0;
        GuardShardingService shardingService = Services.getService(GuardShardingService.class);
        if (contextid < 0) {// If guest, get shard number, then check tables exist
            shard = shardingService.getShard(email);
            if (shard == 0) {
                return null;
            }
        }
        return getKeyForEmailAndContext(email, contextid, shard);
    }

    @Override
    public GuardKeys getKeyForEmailAndContext(String email, int userId, int contextId, int shard) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getReadOnly(userId, contextId, shard);
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.SELECT_FOR_EMAIL_IN_CONTEXT_STMT);
            stmt.setString(1, email);

            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return createKey(resultSet);
            }
            return null;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
    }


    @Override
    public GuardKeys getKeyForEmailAndContext(String email, int contextId, int shard) throws OXException {
        return getKeyForEmailAndContext(email,0,contextId,shard);
    }

    @Override
    public GuardKeys getCurrentKeyForUser(int userId, int contextId, String email) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getReadOnly(userId, contextId, 0);
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.SELECT_CURRENT_FOR_USER_EMAIL_STMT);
            stmt.setString(1, email);

            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return createKey(resultSet);
            }
            return null;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
    }

    @Override
    public GuardKeys getCurrentKeyForUser(int userId, int contextId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getReadOnly(userId, contextId, 0);
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.SELECT_CURRENT_FOR_USER_STMT);
            stmt.setInt(1, userId);
            stmt.setInt(2, contextId);

            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return createKey(resultSet);
            }
            return null;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
    }

    @Override
    public GuardKeys getCurrentKeyForUser(int userId, int contextId, int shardId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getReadOnly(userId, contextId, shardId);

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.SELECT_CURRENT_FOR_USER_STMT);
            stmt.setInt(1, userId);
            stmt.setInt(2, contextId);

            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return createKey(resultSet);
            }
            return null;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
    }

    @Override
    public void insert(GuardKeys key, boolean setLastMod) throws OXException {
        insert(key, setLastMod, 0);
    }

    @Override
    public void insert(GuardKeys key, boolean setLastMod, int shard) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = null;
        try {
            if (key.getContextid() >= 0) {
                if (key.getUserid() < 0) {// minus 1 is the recovery password, which is in OG
                    connection = guardDatabaseService.getWritableForGuard();
                } else {
                    connection = guardDatabaseService.getWritable(key.getContextid());
                }
            } else {
                GuardShardingService shardingService = Services.getService(GuardShardingService.class);
                if (shard == 0) {
                    shard = shardingService.getNextShard();
                }
                if (shard > 0) {
                    connection = guardDatabaseService.getWritableForShard(shard);
                } else {
                    throw GuardKeyTableStorageExceptionCodes.UNABLE_TO_GET_NEW_GUEST_SHARD.create(shard);
                }
            }
            insert(connection, key, setLastMod);
        } finally {
            if (key.getContextid() >= 0) {
                if (key.getUserid() < 0) {// minus 1 is the recovery password, which is in OG
                    guardDatabaseService.backWritableForGuard(connection);
                } else {
                    guardDatabaseService.backWritable(key.getContextid(), connection);
                }
            } else {
                guardDatabaseService.backWritableForShard(shard, connection);
            }
        }
    }

    @Override
    public int getMinFromSystemTable () throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(KeyTableSql.GET_MIN_FROM_SYSTEM_TABLE);
            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return (resultSet.getInt(1));
            } else {
                throw GuardCoreExceptionCodes.SQL_ERROR.create("Unable to get key index.");
            }
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }



    }

    @Override
    public void insert(Connection connection, GuardKeys key, boolean setLastMod) throws OXException {
        PreparedStatement stmt = null;
        try {
            String sql = setLastMod ? KeyTableSql.INSERT_AND_SET_LASTMOD_STMT : KeyTableSql.INSERT_STMT;
            String question = key.getQuestion();
            if (key.getQuestion() == null && setLastMod) {  // If user created key, question should not be null, bug 55911
                question = "";
            }
            stmt = connection.prepareStatement(sql);
            if (key.getUserid() == 0) {
                stmt.setNull(1, java.sql.Types.INTEGER);
            } else {
                stmt.setInt(1, key.getUserid());
            }
            stmt.setInt(2, key.getContextid());
            stmt.setString(3, key.getEncodedPGPSecret());
            stmt.setString(4, KeyExportUtil.export(key.getPGPPublicKeyRing()));
            stmt.setString(5, key.getEncodedPrivate());
            stmt.setString(6, key.getEncodedPublic());
            stmt.setString(7, key.getRecovery());
            stmt.setString(8, key.getSalt());
            stmt.setString(9, key.getEmail());
            stmt.setString(10, key.getLanguage());
            stmt.setLong(11, key.getPGPPublicKeyRing().getPublicKey().getKeyID());
            stmt.setInt(12, key.getVersion());
            stmt.setString(13, question);
            stmt.setString(14, key.getAnswer());
            if (key.getSettings() == null) {
                stmt.setNull(15, java.sql.Types.VARCHAR);
            } else {
                stmt.setString(15, key.getSettings().toString());
            }
            stmt.setBoolean(16, key.isCurrent());
            stmt.setString(17, key.getMisc());
            stmt.setInt(18, key.getMasterKeyIndex());

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
        }
    }

    @Override
    public void updatePublicKey(GuardKeys key) throws OXException {
        if (key.getPGPPublicKeyRing() == null) {
            throw GuardCoreExceptionCodes.PARAMETER_MISSING.create("pubring in parameter key");
        }

        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(0, key.getContextid(), 0);

        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.UPDATE_PGP_PUBLIC_STMT);
            stmt.setString(1, KeyExportUtil.export(key.getPGPPublicKeyRing()));
            stmt.setLong(2, key.getKeyid());

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }

    @Override
    public void updateEmailAddress(GuardKeys key, String newEmail) throws OXException {

        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(0, key.getContextid(), 0);

        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.UPDATE_EMAIL_ADDRESS);
            stmt.setString(1, newEmail);
            stmt.setInt(2, key.getContextid());
            stmt.setString(3, key.getEmail());

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }
    @Override
    public void updatePublicAndPrivateKey(GuardKeys key) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(key.getUserid(), key.getContextid(), 0);

        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.UPDATE_PGP_PUBLIC_AND_PRIVATE_STMT);
            stmt.setString(1, key.getEncodedPGPSecret());
            stmt.setString(2, KeyExportUtil.export(key.getPGPPublicKeyRing()));
            stmt.setInt(3, key.getUserid());
            stmt.setInt(4, key.getContextid());
            stmt.setLong(5, key.getKeyid());

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }

    @Override
    public void updatePassword(GuardKeys key, String recovery, boolean setLastMod) throws OXException {
        if (key.getKeyid() == 0) {
            throw GuardCoreExceptionCodes.PARAMETER_MISSING.create("keyid");
        }
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(key.getUserid(), key.getContextid(), 0);

        PreparedStatement stmt = null;
        try {
            String sql = setLastMod ? KeyTableSql.UPDATE_PASSWORDS_AND_LASTMOD_STMT : KeyTableSql.UPDATE_PASSWORDS_STMT;
            stmt = connectionWrapper.getConnection().prepareStatement(sql);
            stmt.setString(1, key.getEncodedPGPSecret());
            stmt.setString(2, key.getEncodedPrivate());
            stmt.setString(3, recovery);
            stmt.setInt(4, key.getUserid());
            stmt.setInt(5, key.getContextid());
            stmt.setLong(6, key.getKeyid());

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }



    @Override
    public void updatePin(int userId, int contextId, String newPin) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(userId, contextId, 0);

        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.UPDATE_PIN_STMT);
            stmt.setString(1, newPin);
            stmt.setInt(2, userId);
            stmt.setInt(3, contextId);

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }

    @Override
    public void updateDuplicate(GuardKeys key) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(key.getUserid(), key.getContextid(), 0);

        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.UPDATE_DUPLICATED_STMT);

            stmt.setString(1, key.getEncodedPrivate());
            stmt.setString(2, key.getEncodedPGPSecret());
            stmt.setString(3, KeyExportUtil.export(key.getPGPPublicKeyRing()));
            stmt.setString(4, key.getRecovery());
            stmt.setString(5, key.getSalt());
            stmt.setInt(6, key.getUserid());
            stmt.setInt(7, key.getContextid());
            stmt.setLong(8, key.getKeyid());

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }

    @Override
    public void updateQuestionForKey(int userId, int contextId, String question, String answer, long keyId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(userId, contextId, 0);
        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.UPDATE_QUESTION_STMT);
            stmt.setString(1, question);
            stmt.setString(2, answer);
            stmt.setInt(3, userId);
            stmt.setInt(4, contextId);
            stmt.setLong(5, keyId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }

    @Override
    public void updateKeyVersion(GuardKeys key, int newVersion) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(key.getUserid(), key.getContextid(), 0);

        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.UPDATE_VERSION_STMT);
            stmt.setInt(1, newVersion);
            stmt.setInt(2, key.getContextid());
            stmt.setInt(3, key.getUserid());
            stmt.setInt(4, key.getVersion());

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }

    @Override
    public void updateMisc(GuardKeys key, String misc) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(key.getUserid(), key.getContextid(), 0);

        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.UPDATE_MISC_STMT);
            stmt.setString(1, misc);
            stmt.setInt(2, key.getContextid());
            stmt.setInt(3, key.getUserid());
            stmt.setLong(4, key.getKeyid());

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }

    @Override
    public void updateLocale(GuardKeys key) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(key.getUserid(), key.getContextid(), 0);

        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.UPDATE_LOCALE);
            stmt.setString(1, key.getLanguage());
            stmt.setInt(2, key.getContextid());
            stmt.setInt(3, key.getUserid());
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }

    @Override
    public void updateSettingsForUser(int userId, int contextId, String settings) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(userId, contextId, 0);

        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.UPDATE_SETTINGS_FOR_USER_STMT);
            stmt.setString(1, settings);
            stmt.setInt(2, userId);
            stmt.setInt(3, contextId);

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }


    @Override
    public void setCurrentKey(GuardKeys key) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(key.getUserid(), key.getContextid(), 0);

        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.SET_CURRENT_STMT);
            stmt.setInt(1, key.getUserid());
            stmt.setInt(2, key.getContextid());
            stmt.setLong(3, key.getKeyid());

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }

    @Override
    public void unsetCurrentFlag(int userId, int contextId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(userId, contextId, 0);

        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.UNSET_CURRENT_FOR_USER_STMT);
            stmt.setInt(1, userId);
            stmt.setInt(2, contextId);

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }

    @Override
    public void delete(GuardKeys key) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(key.getUserid(), key.getContextid(), 0);

        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.DELETE_KEY_STMT);
            stmt.setInt(1, key.getContextid());
            stmt.setInt(2, key.getUserid());
            stmt.setLong(3, key.getKeyid());

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }

    @Override
    public void deleteAllForUser(int userId, int contextId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(userId, contextId, 0);
        try {
            deleteAllForUser(connectionWrapper.getConnection(), userId, contextId);
        } finally {
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }

    @Override
    public void deleteAllForUser(Connection connection, int userId, int contextId) throws OXException {
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(KeyTableSql.DELETE_FOR_USER_STMT);
            stmt.setInt(1, userId);
            stmt.setInt(2, contextId);

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
        }
    }

    @Override
    public void deleteAllForContext(int contextId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        Connection connection = guardDatabaseService.getWritable(contextId);
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(KeyTableSql.DELETE_FOR_CONTEXT_STMT);
            stmt.setInt(1, contextId);

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(contextId, connection);
        }
    }

    @Override
    public void deleteRecovery(GuardKeys key) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(key.getUserid(), key.getContextid(), 0);

        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(KeyTableSql.DELETE_RECOVERY_FROM_KEY_STMT);
            stmt.setInt(1, key.getUserid());
            stmt.setInt(2, key.getContextid());

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }

    @Override
    public void updateAutoIncrementInShard(int shardId, int value) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForShard(shardId);
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(KeyTableSql.ALTER_AUTOINCREMENT_STMT);
            stmt.setInt(1, value);

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForShard(shardId, connection);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keys.GuardKeyService#getOldGuardKeys(int, int)
     */
    @Override
    public GuardKeys getOldGuardKeys(int userId, int contextId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getReadOnly(userId, contextId, 0);

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        GuardKeys key = null;
        try {
            String command = "SELECT PGPSecret, PGPPublic, RSAPrivate, RSAPublic, Salt, misc, settings, email, lastMod, question, answer, cid, id, keyid, version, current, lang, Recovery FROM og_KeyTable WHERE id = ? AND cid = ? AND version < 1";//Version may be 0 or -1 for Guard key (-1 during upgrade)
            stmt = connectionWrapper.getConnection().prepareStatement(command);
            stmt.setInt(1, userId);
            stmt.setInt(2, contextId);

            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                key = getKeyFromRS(resultSet);
            }
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
        return key;
    }


    @Override
    public GuardKeys getKeyForGuest(String email, int shardId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        Connection connection = guardDatabaseService.getReadOnlyForShard(shardId);

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(KeyTableSql.SELECT_FOR_EMAIL_IN_CONTEXT_STMT);
            stmt.setString(1, email);

            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return createKey(resultSet);
            }
            return null;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForShard(shardId, connection);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keys.storage.KeyTableStorage#exists()
     */
    @Override
    public boolean exists(int userId, int contextId, int shardId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        GuardConnectionWrapper readOnly = guardDatabaseService.getReadOnly(userId, contextId, shardId);
        try {
            return DBUtils.tableExists(readOnly.getConnection(), KeyTableSql.OG_KEY_TABLE, false);
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        }
        finally {
            guardDatabaseService.backReadOnly(readOnly);
        }
    }

    /////////////////////////////////// HELPERS //////////////////////////////////

    /**
     * Create GuardKey from database result set
     *
     * @param rs
     * @param id ID of user, -1 if unknown
     * @param cid
     * @return
     * @throws OXException
     */
    private GuardKeys getKeyFromRS(ResultSet rs) throws OXException {
        try {
            GuardKeys key = new GuardKeys();
            key.setPGPSecretFromString(rs.getString("PGPSecret"));
            key.setPGPKeyRingFromAsc(rs.getString("PGPPublic"));
            key.setEncodedPrivate(rs.getString("RSAPrivate"));
            key.setPublicKeyFrom64String(rs.getString("RSAPublic"));
            key.setSalt(rs.getString("Salt"));
            key.setMisc(rs.getString("misc"));
            key.setSettings(getJson(rs, "settings"));
            key.setEmail(rs.getString("email"));
            key.setLastup(rs.getDate("lastMod"));
            key.setQuestion(rs.getString("question"));
            key.setAnswer(rs.getString("answer"));
            key.setContextid(rs.getInt("cid"));
            key.setUserid(rs.getInt("id"));
            key.setKeyid(rs.getLong("keyid"));
            key.setVersion(rs.getInt("version"));
            key.setCurrent(rs.getBoolean("current"));
            key.setLanguage(rs.getString("lang"));
            key.setRecovery(rs.getString("Recovery"));
            if (key.getRecovery() != null) {
                if (key.getRecovery().equals("-1") || (key.getLastup() == null)) {
                    key.setPasswordNeeded(true);
                }
            }
            key.setMasterKeyIndex(rs.getInt("mKeyIndex"));
            return key;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        }
    }

    /**
     * Get the JsonObject from the specified result set (Shouldn't be on this layer)
     *
     * @param resultSet The result set to fetch the json from
     * @param name The name of the column
     * @return The json object
     * @throws SQLException
     */
    private JsonObject getJson(ResultSet resultSet, String name) throws SQLException {
        String string = resultSet.getString(name);
        if (string == null) {
            return new JsonObject();
        }
        JsonObject json = JsonUtil.parseAsJsonObject(string);
        if (json.isJsonNull()) {
            return new JsonObject();
        }
        return json;
    }

}
