/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.storage.impl.remotekeys;

public class RemoteKeyCacheSql {
    public static final String SELECT_BY_EMAIL_STMT = "SELECT ring, keysource from remote_key_cache rkc INNER JOIN remote_keys rk ON rkc.id = rk.ref WHERE rk.email = ?";
    public static final String SELECT_BY_ID_STMT = "SELECT ring, keysource from remote_key_cache rkc INNER JOIN remote_keys rk ON rkc.id = rk.ref WHERE rk.id = ?";
    public static final String INSERT_RING_DATA_STMT = "INSERT INTO remote_key_cache (id, ring, created) VALUES (?, ?, NOW()) ON DUPLICATE KEY UPDATE ring = ?";
    public static final String INSERT_RING_ID_STMT = "INSERT INTO remote_keys (id, email, ref, keysource) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE ref = ?";
    public static final String DELETE_REMOTE_KEYS_STMT = "DELETE FROM remote_keys WHERE ref IN (SELECT id FROM remote_key_cache WHERE created < DATE_SUB(NOW(), INTERVAL ? DAY));";
    public static final String DELETE_REMOTE_KEY_CACHE_STMT = "DELETE FROM remote_key_cache WHERE created < DATE_SUB(NOW(), INTERVAL ? DAY);";
    public static final String WIPE_KEY_CACHE = "TRUNCATE TABLE remote_key_cache";
    public static final String WIPE_REMTOE_KEYS = "TRUNCATE TABLE remote_keys";
}
