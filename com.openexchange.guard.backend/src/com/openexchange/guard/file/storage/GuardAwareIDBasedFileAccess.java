
package com.openexchange.guard.file.storage;

import java.io.InputStream;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.Document;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.FileStorageCapability;
import com.openexchange.file.storage.File.Field;
import com.openexchange.file.storage.composition.AbstractDelegatingIDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.crypto.CryptographyMode;
import com.openexchange.guard.api.GuardApi;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.session.Session;

/**
 * {@link GuardAwareIDBasedFileAccess} decorates {@link IDBasedFileAccess} objects in order to support OX Guard encryption and decryption
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.2
 */
public class GuardAwareIDBasedFileAccess extends AbstractDelegatingIDBasedFileAccess {

    private IDBasedFileAccess writingFileAccess;
    private IDBasedFileAccess readingFileAccess;
    private boolean restricted;
    private final List<FileStorageCapability> notSupportedCapabilities = Arrays.asList(new FileStorageCapability[] { FileStorageCapability.RANDOM_FILE_ACCESS, FileStorageCapability.THUMBNAIL_IMAGES });
    private final EncryptedFileRecognizer encryptedFileRecognizer;
    private Session session;

    /**
     * Initializes a new {@link GuardAwareIDBasedFileAccess}.
     * <p>
     * This will try to obtain the {@link GuardAuthenticationToken} from the given session's parameter.
     * </p>
     *
     * @param fileAccess The underlying {@link IDBasedFileAccess}
     * @param guardApi The Guard API endpoint to use
     * @param session The session
     * @param encryptedFileRecognizer A strategy for recognizing Guard encrypted files.
     */
    public GuardAwareIDBasedFileAccess(IDBasedFileAccess fileAccess, EnumSet<CryptographyMode> cryptMode, GuardApi guardApi, Session session, EncryptedFileRecognizer encryptedFileRecognizer, boolean restricted) {
        this(fileAccess, cryptMode, guardApi, session, encryptedFileRecognizer, null, restricted);
    }

    /**
     * Initializes a new {@link GuardAwareIDBasedFileAccess}.
     *
     * @param fileAccess The underlying {@link IDBasedFileAccess}
     * @param guardApi The Guard API endpoint to use
     * @param session The session
     * @param encryptedFileRecognizer A strategy for recognizing Guard encrypted files.
     * @param authenticationToken The {@link GuardAuthenticationToken} used for authentication.
     */
    public GuardAwareIDBasedFileAccess(IDBasedFileAccess fileAccess, EnumSet<CryptographyMode> cryptMode, GuardApi guardApi, Session session, EncryptedFileRecognizer encryptedFileRecognizer, GuardAuthenticationToken authenticationToken, boolean restricted) {
        super(fileAccess);

        this.restricted = restricted;

        this.session = session;

        this.encryptedFileRecognizer = encryptedFileRecognizer;

        if (cryptMode.contains(CryptographyMode.ENCRYPT)) {
            if (cryptMode.contains(CryptographyMode.SIGN)) {
                this.writingFileAccess = new EncryptingGuardAwareIDBasedFileAccess(fileAccess, guardApi, session, encryptedFileRecognizer, true /* also sign data */, authenticationToken);
            } else {
                this.writingFileAccess = new EncryptingGuardAwareIDBasedFileAccess(fileAccess, guardApi, session, encryptedFileRecognizer);
            }
        } else {
            this.writingFileAccess = fileAccess;
        }

        if (cryptMode.contains(CryptographyMode.DECRYPT)) {
            this.readingFileAccess = new DecryptingGuardAwareIDBasedFileAccess(fileAccess, guardApi, session, encryptedFileRecognizer, authenticationToken);
        } else if (cryptMode.contains(CryptographyMode.VERIFY)) {
            //TODO: Verification of plain text data
            this.readingFileAccess = fileAccess;
        } else {
            this.readingFileAccess = fileAccess;
        }
    }

    /**
     * If restricted file access, make sure that the file is not owned by this user.  I.E. make sure it is a guest user
     * accessing a share.
     * @param document
     * @throws OXException  if not share
     */
    private void checkRestricted (File document) throws OXException {
        if (!restricted) return;
        int createdBy = document.getCreatedBy();
        if (createdBy == 0) {
            if (document.getId() == null) {
                // New file
                throw GuardExceptionCodes.ACTION_NOT_ALLOWED.create("Missing guard-drive capability");
            }
            File checkSource = fileAccess.getFileMetadata(document.getId(), document.getVersion());
            createdBy = checkSource.getCreatedBy();
        }
        if (createdBy == session.getUserId()) {
            throw GuardExceptionCodes.ACTION_NOT_ALLOWED.create("Missing guard-drive capability");
        }
        return;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.file.storage.composition.AbstractDelegatingIDBasedFileAccess#supports(java.lang.String, java.lang.String, com.openexchange.file.storage.FileStorageCapability[])
     */
    @Override
    public boolean supports(String serviceID, String accountID, FileStorageCapability... capabilities) throws OXException {
        //Some capabilities are not supported
        for (FileStorageCapability fileStorageCapability : capabilities) {
            if (notSupportedCapabilities.contains(fileStorageCapability)) {
                return false;
            }
        }
        return super.supports(serviceID, accountID, capabilities);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.file.storage.composition.AbstractDelegatingIDBasedFileAccess#copy(java.lang.String, java.lang.String, java.lang.String, com.openexchange.file.storage.File, java.io.InputStream, java.util.List)
     */
    @Override
    public String copy(String sourceId, String version, String destFolderId, File update, InputStream newData, List<Field> modifiedFields) throws OXException {
        File source = fileAccess.getFileMetadata(sourceId, version);
        checkRestricted (source);
        if (encryptedFileRecognizer.isEncrypted(source)) {
            return readingFileAccess.copy(sourceId, version, destFolderId, update, newData, modifiedFields);
        }
        return writingFileAccess.copy(sourceId, version, destFolderId, update, newData, modifiedFields);
    }

    @Override
    public InputStream getDocument(String id, String version) throws OXException {
        return readingFileAccess.getDocument(id, version);
    }

    @Override
    public InputStream getDocument(String id, String version, long offset, long length) throws OXException {
        return readingFileAccess.getDocument(id, version, offset, length);
    }

    @Override
    public Document getDocumentAndMetadata(String id, String version) throws OXException {
        return readingFileAccess.getDocumentAndMetadata(id, version);
    }

    @Override
    public Document getDocumentAndMetadata(String id, String version, String clientEtag) throws OXException {
        return readingFileAccess.getDocumentAndMetadata(id, version, clientEtag);
    }

    @Override
    public InputStream optThumbnailStream(String id, String version) throws OXException {
        return readingFileAccess.optThumbnailStream(id, version);
    };

    /* (non-Javadoc)
     * @see com.openexchange.file.storage.composition.AbstractDelegatingIDBasedFileAccess#getFileMetadata(java.lang.String, java.lang.String)
     */
    @Override
    public File getFileMetadata(String id, String version) throws OXException {
        return readingFileAccess.getFileMetadata(id, version);
    }

    @Override
    public String saveDocument(File document, InputStream data, long sequenceNumber) throws OXException {
        checkRestricted (document);
        return writingFileAccess.saveDocument(document, data, sequenceNumber);
    }

    @Override
    public String saveDocument(File document, InputStream data, long sequenceNumber, List<File.Field> modifiedColumns) throws OXException {
        checkRestricted (document);
        return writingFileAccess.saveDocument(document, data, sequenceNumber, modifiedColumns);
    }

    @Override
    public String saveDocument(File document, InputStream data, long sequenceNumber, List<File.Field> modifiedColumns, boolean ignoreVersion) throws OXException {
        checkRestricted (document);
        return writingFileAccess.saveDocument(document, data, sequenceNumber, modifiedColumns, ignoreVersion);
    }

    @Override
    public String saveDocument(File document, InputStream data, long sequenceNumber, List<File.Field> modifiedColumns, boolean ignoreVersion, boolean ignoreWarnings, boolean tryAddVersion) throws OXException {
        checkRestricted (document);
        return writingFileAccess.saveDocument(document, data, sequenceNumber, modifiedColumns, ignoreVersion, ignoreWarnings, tryAddVersion);
    }

    @Override
    public String saveDocument(File document, InputStream data, long sequenceNumber, List<File.Field> modifiedColumns, long offset) throws OXException {
        checkRestricted (document);
        return writingFileAccess.saveDocument(document, data, sequenceNumber, modifiedColumns, offset);
    }
}
