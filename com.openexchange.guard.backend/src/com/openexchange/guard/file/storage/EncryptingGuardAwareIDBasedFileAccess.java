/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.file.storage;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.DefaultFile;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.File.Field;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.composition.FileID;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.groupware.results.TimedResult;
import com.openexchange.guard.api.GuardApi;
import com.openexchange.guard.internal.authentication.AuthenticationTokenHandler;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.session.Session;
import com.openexchange.tools.iterator.SearchIterator;

/**
 * {@link EncryptionOnlyGuardAwareIDBasedFileAccess}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.3
 */
public class EncryptingGuardAwareIDBasedFileAccess extends AbstractGuardIDBasedFileAccess {

    private final GuardAuthenticationToken signingAuthentication;
    private final boolean sign;


    /**
     * Initializes a new {@link EncryptingGuardAwareIDBasedFileAccess}.
     *
     * @param fileAccess The underlying {@link IDBasedFileAccess} object to use.
     * @param guardApi The {@link GuardApi} instance used for accessing OX Guard.
     * @param session The user's session.
     * @param encryptedFileRecognizer A strategy for detecting encrypted files.
     * @param sign Whether the encrypted data should also be signed or not.
     * @param authenticationToken The {@link GuardAuthenticationToken} used for authentication while signing.
     *            If this parameter is set to <code>null</code> and the <code>sign</code> parameter is set to <code>true</code>, the {@link GuardAuthenticationToken} is obtained from the given session's parameter if present.
     *            If the <code>sign</code> parameter is set to <code>false</code>, the <code>authenticationToken</code> is not used and can be set to <code>null</code>.
     */
    public EncryptingGuardAwareIDBasedFileAccess(IDBasedFileAccess fileAccess, GuardApi guardApi, Session session, EncryptedFileRecognizer encryptedFileRecognizer, boolean sign, GuardAuthenticationToken signingAuthentication) {
        super(fileAccess, guardApi, session, encryptedFileRecognizer);
        this.sign = sign;
        this.signingAuthentication = signingAuthentication;
    }

    /**
     * Initializes a new {@link EncryptingGuardAwareIDBasedFileAccess} which does encryption only an no signing.
     * <p>
     * This is equivalent to use {@link #EncryptingGuardAwareIDBasedFileAccess(IDBasedFileAccess, GuardApi, Session, EncryptedFileRecognizer, boolean, GuardAuthenticationToken)}
     * with <code>sign</code> set to <code>false</code> and <code>signingAuthentication</code> to <code>null</code>.
     * </p>
     *
     * @param fileAccess The underlying {@link IDBasedFileAccess} object to use.
     * @param guardApi@param guardApi The {@link GuardApi} instance used for accessing OX Guard.
     * @param session The user's session.
     * @param encryptedFileRecognizer A strategy for detecting encrypted files.
     */
    public EncryptingGuardAwareIDBasedFileAccess(IDBasedFileAccess fileAccess, GuardApi guardApi, Session session, EncryptedFileRecognizer encryptedFileRecognizer) {
        this(fileAccess, guardApi, session, encryptedFileRecognizer, false, null);
    }


    /**
     * Creates an InputStream whose data will be encrypted using OX Guard while reading from it
     *
     * @param inputStream The InputStream
     * @return The encrypted InputStream
     * @throws OXException
     */
    private InputStream createEncryptedStream(File file, InputStream inputStream) throws OXException {
        return createEncryptedStream(file, inputStream, true);
    }

    /**
     * Creates an InputStream whose data will be encrypted using OX Guard while reading from it
     *
     * @param File The original file
     * @param inputStream The InputStream
     * @param includeSharing If sharing details should be preserved
     * @return The encrypted InputStream
     * @throws OXException
     */
    private InputStream createEncryptedStream(File file, InputStream inputStream, boolean includeSharing) throws OXException {
        File document = (file.getId() == null) || !includeSharing ?
            null: fileAccess.getFileMetadata(file.getId(), file.getVersion());  // Pull for any existing sharing data
        final String userIdentifier = session.getContextId() + "/" + session.getUserId();
        if (sign) {
            GuardAuthenticationToken authTokenForSigning = this.signingAuthentication;
            if (authTokenForSigning == null) {
                authTokenForSigning = new AuthenticationTokenHandler().requireForSession(session, CryptoType.PROTOCOL.PGP.getValue());
            }
            return new GuardEncryptingInputStream(document, guardApi, inputStream, Arrays.asList(new String[] { userIdentifier }), false, authTokenForSigning, session);
        }
        return new GuardEncryptingInputStream(document, guardApi, inputStream, Arrays.asList(new String[] { userIdentifier }), false, session);
    }

    /**
     * Marks a file as encrypted
     *
     * @param file The file
     * @return The fields which has been modified in order to mark the file as encrypted.
     */
    private Collection<Field> markEncrypted(File file) {
        return encryptedFileRecognizer.markEncrypted(file);
    }


    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.file.storage.composition.AbstractDelegatingIDBasedFileAccess#copy(java.lang.String, java.lang.String, java.lang.String, com.openexchange.file.storage.File, java.io.InputStream, java.util.List)
     */
    @Override
    public String copy(String sourceId, String versionId, String destFolderId, File update, InputStream newData, List<Field> modifiedFields) throws OXException {

        if (newData == null) {
            //If the caller did not provide data we fetch them from the original file
            newData = fileAccess.getDocument(sourceId, versionId);
        }

        File sourceFile = fileAccess.getFileMetadata(sourceId, versionId);
        if (isEncryptedGuardFile(sourceFile)) {  // If already encrypted, just copy
            return fileAccess.copy(sourceId, versionId, destFolderId, update, newData, modifiedFields);
        }
        if (update == null) {
            update = fileAccess.getFileMetadata(sourceId, versionId);
        } else {
            if (update.getFileName() == null || update.getFileName().isEmpty()) {
                update.setFileName(sourceFile.getFileName());
            }
            update.setFileMIMEType(sourceFile.getFileMIMEType());
        }

        TimedResult<File> tversions = fileAccess.getVersions(sourceId);
        SearchIterator<File> files = tversions.results();
        ArrayList<File> versions = new ArrayList<File>();  // Collect list of versions
        while (files.hasNext()) {
            File version = files.next();
            versions.add(version);
        }
        files.close();
        modifiedFields.addAll(markEncrypted(update));
        // If no versions, just send to copy.  Version 0 is empty, size will be 2 for only 1 version
        if (versions.size() <= 2) {
            try (InputStream encrData = createEncryptedStream(update, newData, false)) {
                String newId = fileAccess.copy(sourceId, versionId, destFolderId, update, encrData, modifiedFields);
                return newId;
            } catch (IOException e) {
                throw OXException.general("Error encrypting file", e);
            }
        }
        // Versioned
        boolean first = true;
        String copy = null;
        FileID copyId = null;
        String currentVersionId = "";
        String lastVersion = null;
        // Loop versions
        for (int i = 1; i < versions.size(); i++) {
            File thisVersion = versions.get(i);
            if (thisVersion.getFileSize() != 0) {
                try (InputStream data = fileAccess.getDocument(sourceId, thisVersion.getVersion())) {
                    File currentFile = new DefaultFile(fileAccess.getFileMetadata(sourceId, thisVersion.getVersion()));
                    if (currentFile.isCurrentVersion()) {
                        currentVersionId = currentFile.getVersion();
                        if (update != null) {  // If current version, apply any updated modified fields from update file
                            if (modifiedFields.size() > 0) {
                                currentFile.copyFrom(update, modifiedFields.toArray(new Field[modifiedFields.size()]));
                            } else {
                                currentFile.copyFrom(update);
                            }
                        }
                    }
                    markEncrypted(currentFile);
                    // First file, make a copy and move to the destination folder.
                    try (InputStream encrData = createEncryptedStream(thisVersion, data, true)) {
                        if (first) {
                            copy = fileAccess.copy(sourceId,
                                thisVersion.getVersion(),
                                destFolderId,
                                update,
                                encrData,
                                modifiedFields);
                            copyId = new FileID(copy);
                            first = false;
                        } else {  // Subsequent versions
                            currentFile.setId(copyId.getFileId());   // Our destination is the copied file
                            currentFile.setFolderId(copyId.getFolderId());
                            lastVersion = currentFile.getVersion();
                            currentFile.setVersion("0");
                            fileAccess.saveDocument(currentFile,
                                encrData,
                                FileStorageFileAccess.DISTANT_FUTURE,
                                modifiedFields);
                        }
                    }
                } catch (IOException e) {
                    throw OXException.general("Error encrypting file", e);
                }
            }

        }
        // Having just saved all versions, latest will be marked current.  Restore user configured current version
        if (lastVersion != null && !lastVersion.equals(currentVersionId)) {
            makeCurrentFile(copy, currentVersionId);
        }
        return copy;
    }

    @Override
    public String saveDocument(File document, InputStream data, long sequenceNumber) throws OXException {
        data = createEncryptedStream(document, data);
        markEncrypted(document);
        return fileAccess.saveDocument(document, data, sequenceNumber);
    }

    @Override
    public String saveDocument(File document, InputStream data, long sequenceNumber, List<File.Field> modifiedColumns) throws OXException {
        data = createEncryptedStream(document, data);
        markEncrypted(document);
        return fileAccess.saveDocument(document, data, sequenceNumber, modifiedColumns);
    }

    @Override
    public String saveDocument(File document, InputStream data, long sequenceNumber, List<File.Field> modifiedColumns, boolean ignoreVersion) throws OXException {
        data = createEncryptedStream(document, data);
        markEncrypted(document);
        return fileAccess.saveDocument(document, data, sequenceNumber, modifiedColumns, ignoreVersion);
    }

    @Override
    public String saveDocument(File document, InputStream data, long sequenceNumber, List<File.Field> modifiedColumns, boolean ignoreVersion, boolean ignoreWarnings, boolean tryAddVersion) throws OXException {
        data = createEncryptedStream(document, data);
        markEncrypted(document);
        return fileAccess.saveDocument(document, data, sequenceNumber, modifiedColumns, ignoreVersion, ignoreWarnings, tryAddVersion);
    }

    @Override
    public String saveDocument(File document, InputStream data, long sequenceNumber, List<File.Field> modifiedColumns, long offset) throws OXException {
        data = createEncryptedStream(document, data);
        markEncrypted(document);
        return fileAccess.saveDocument(document, data, sequenceNumber, modifiedColumns, offset);
    }
}
