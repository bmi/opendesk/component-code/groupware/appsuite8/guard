/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.file.storage;

import java.util.Collection;
import com.openexchange.file.storage.Document;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.File.Field;

/**
 * {@link EncryptedFileRecognizer} defines how to recognize encrypted files.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.2
 */
public interface EncryptedFileRecognizer {

    /**
     * Checks if the given file is an encrypted document
     *
     * @param file The file
     * @return True if the file is an encrypted file, false otherwise
     */
    boolean isEncrypted(File file);

    /**
     * Marks a specific file as encrypted
     *
     * @param file The file to mark as encrypted
     * @return A collection of {@link com.openexchange.file.storage.File.Field} which are relevant for marking a file as encrypted.
     */
    Collection<Field> markEncrypted(File file);

    /**
     * Marks a specific document as encrypted
     *
     * @param document The document to mark as encrypted
     * @return A collection of {@link com.openexchange.file.storage.File.Field} which are relevant for marking a file as encrypted.
     */
    Collection<Field> markEncrypted(Document document);

    /**
     * Marks a specific file as decrypted
     *
     * @param file The file to mark as decrypted
     * @return A collection of {@link com.openexchange.file.storage.File.Field} which are relevant for marking a file as decrypted.
     */
    Collection<Field> markDecrypted(File file);

    /**
     * Marks a specific document as decrypted
     *
     * @param document The document to mark as decrypted
     * @return A collection of {@link com.openexchange.file.storage.File.Field} which are relevant for marking a document as decrypted.
     */
    Collection<Field> markDecrypted(Document document);
}
