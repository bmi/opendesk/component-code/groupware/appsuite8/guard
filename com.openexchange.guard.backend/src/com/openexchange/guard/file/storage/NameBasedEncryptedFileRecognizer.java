/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.file.storage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import org.apache.commons.io.FilenameUtils;
import com.openexchange.file.storage.Document;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.File.Field;

/**
 * {@link NameBasedEncryptedFileRecognizer} recognizes a file as encrypted based on the file extension
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.2
 */
public class NameBasedEncryptedFileRecognizer implements EncryptedFileRecognizer {

    private static final String[] EXTENSIONS = new String[] { "pgp" };
    private static final String MARK_EXTENSION = ".pgp";
    private final boolean caseSensitive;

    /**
     * Initializes a new {@link NameBasedEncryptedFileRecognizer}.
     */
    public NameBasedEncryptedFileRecognizer() {
        this(false);
    }

    /**
     * Initializes a new {@link NameBasedEncryptedFileRecognizer}.
     *
     * @param caseSensitive True, if name based recognition should be case sensitive, false otherwise.
     */
    public NameBasedEncryptedFileRecognizer(boolean caseSensitive) {
        this.caseSensitive = caseSensitive;
    }

    /**
     * Internal method for comparing
     *
     * @param one
     * @param two
     * @return True, if the two strings are equal, false otherwise.
     */
    private boolean areEquals(String one, String two) {
        if (one == null && two == null) {
            return true;
        }
        if (one != null && two != null) {
            if (this.caseSensitive) {
                return one.equals(two);
            } else {
                return one.toLowerCase().equals(two.toLowerCase());
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.file.storage.GuardFileRecognizer#isGuardFile(com.openexchange.file.storage.Document)
     */
    @Override
    public boolean isEncrypted(File file) {
        String documentExtension = FilenameUtils.getExtension(file.getFileName());
        for (String extension : EXTENSIONS) {
            if (areEquals(extension, documentExtension)) {
                return true;
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.file.storage.EncryptedFileRecognizer#markEncrypted(com.openexchange.file.storage.File)
     */
    @Override
    public Collection<Field> markEncrypted(File file) {
        ArrayList<Field> ret = new ArrayList<Field>();
        if (file.getFileName() != null && !file.getFileName().endsWith(MARK_EXTENSION)) {
            file.setFileName(file.getFileName() + MARK_EXTENSION);
            ret.add(Field.FILENAME);
        }
        return ret;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.file.storage.EncryptedFileRecognizer#markEncrypted(com.openexchange.file.storage.Document)
     */
    @Override
    public Collection<Field> markEncrypted(Document document) {
        if(document.getName() != null && !document.getName().endsWith(MARK_EXTENSION)) {
            document.setName(document.getName() + MARK_EXTENSION);
        }
        return markEncrypted(document.getFile());
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.file.storage.EncryptedFileRecognizer#markDecrypted(com.openexchange.file.storage.File)
     */
    @Override
    public Collection<Field> markDecrypted(File file) {
        String oldFileName = file.getFileName();
        if (oldFileName != null && oldFileName.endsWith(MARK_EXTENSION)) {
            String newFileName = oldFileName.substring(0, oldFileName.length() - MARK_EXTENSION.length());
            file.setFileName(newFileName);
        }
        return Arrays.asList(new Field[] { Field.FILENAME });
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.file.storage.EncryptedFileRecognizer#markDecrypted(com.openexchange.file.storage.Document)
     */
    @Override
    public Collection<Field> markDecrypted(Document document) {
        String oldFileName = document.getName();
        if (oldFileName != null && oldFileName.endsWith(MARK_EXTENSION)) {
            String newFileName = oldFileName.substring(0, oldFileName.length() - MARK_EXTENSION.length());
            document.setName(newFileName);
        }
        return markDecrypted(document.getFile());
    }
}
