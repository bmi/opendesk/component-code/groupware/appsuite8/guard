/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.file.storage;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.DefaultFile;
import com.openexchange.file.storage.Document;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.File.Field;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.composition.FileID;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.groupware.results.TimedResult;
import com.openexchange.guard.api.GuardApi;
import com.openexchange.guard.file.storage.exceptions.GuardFileStorageExceptionCodes;
import com.openexchange.guard.internal.authentication.AuthenticationTokenHandler;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.mail.mime.MimeType2ExtMap;
import com.openexchange.session.Session;
import com.openexchange.tools.io.IOUtils;
import com.openexchange.tools.iterator.SearchIterator;

/**
 * {@link DecryptingGuardAwareIDBasedFileAccess}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.3
 */
public class DecryptingGuardAwareIDBasedFileAccess extends AbstractGuardIDBasedFileAccess {

    private final GuardAuthenticationToken authenticationToken;

    private class GuardDocument extends Document {

        private final Document document;

        /**
         * Initializes a new {@link GuardDocument}.
         *
         * @param document The original {@link Document} to create a GuardDocument for
         * @throws OXException
         */
        public GuardDocument(Document document) throws OXException {
            super(document);
            this.document = document;
            //We do not know the size of the decrypted file yet
            setSize(-1);
            //Updating file data as well
            setFile(updateFile(getFile()));
        }

        /*
         * (non-Javadoc)
         *
         * @see com.openexchange.file.storage.Document#getData()
         */
        @Override
        public InputStream getData() throws OXException {
            return createGuardDecryptringInputStream(authenticationToken, document.getData());
        }

        /**
         * Signals that the InputStream returned by {@link GuardDocument#getData()} is not repetitive.
         *
         * @return false
         * @see com.openexchange.file.storage.Document#isRepetitive()
         */
        @Override
        public boolean isRepetitive() {
            return false;
        }
    }

    /**
     * Initializes a new {@link DecryptingGuardAwareIDBasedFileAccess}.
     *
     * @param fileAccess The underlying {@link IDBasedFileAccess} object to use.
     * @param guardApi The {@link GuardApi} instance used for accessing OX Guard.
     * @param session The user's session.
     * @param encryptedFileRecognizer A strategy for detecting encrypted files.
     * @param authenticationToken The {@link GuardAuthenticationToken} used for authentication.
     */
    public DecryptingGuardAwareIDBasedFileAccess(IDBasedFileAccess fileAccess, GuardApi guardApi, Session session, EncryptedFileRecognizer encryptedFileRecognizer, GuardAuthenticationToken authenticationToken) {
        super(fileAccess, guardApi, session, encryptedFileRecognizer);
        this.authenticationToken = authenticationToken;
    }

    /**
     * Initializes a new {@link DecryptingGuardAwareIDBasedFileAccess}.
     * <p>
     * This will try to obtain the {@link GuardAuthenticationToken} from the given session's parameter.
     * </p>
     *
     * @param fileAccess The underlying {@link IDBasedFileAccess} object to use.
     * @param guardApi The {@link GuardApi} instance used for accessing OX Guard.
     * @param session The user's session.
     * @param encryptedFileRecognizer A strategy for detecting encrypted files.
     */
    public DecryptingGuardAwareIDBasedFileAccess(IDBasedFileAccess fileAccess, GuardApi guardApi, Session session, EncryptedFileRecognizer encryptedFileRecognizer) {
        this(fileAccess, guardApi, session, encryptedFileRecognizer, null);
    }

    /**
     * Creates a new InputStream which decrypts data using OX Guard
     *
     * @param authenticationToken An {@link GuardAuthenticationToken} to use, or null in order to obtain the token from the current session.
     * @param inputStream The stream which contains the data to be decrypted.
     * @return An decrypting InputStream
     * @throws OXException
     */
    private InputStream createGuardDecryptringInputStream(GuardAuthenticationToken authenticationToken, InputStream inputStream) throws OXException {
        if (authenticationToken == null) {
            authenticationToken = new AuthenticationTokenHandler().requireForSession(session, CryptoType.PROTOCOL.PGP.getValue());
        }
        return new GuardDecryptringInputStream(guardApi, authenticationToken, inputStream, session);
    }



    /**
     * Decrypts the given document
     *
     * @param document The document to decrypt
     * @return The decrypted document if the document is encrypted or the original document if the document is not encrypted
     * @throws OXException
     */
    private Document createDecryptedDocument(Document document) throws OXException {
        if (isEncryptedGuardFile(document.getFile())) {
            return markDecrypted(new GuardDocument(document));
        }
        return document;
    }

    /**
     * @param document
     * @return
     */
    private Document markDecrypted(GuardDocument document) {
        File file = document.getFile();
        encryptedFileRecognizer.markDecrypted(document);
        document.setFile(file);
        document.setMimeType(MimeType2ExtMap.getContentType(document.getName()));
        return document;
    }

    /**
     * Internal method to update/set Guard related meta data.
     *
     * @param file The meta data to update
     * @return The meta data
     */
    private File updateFile(File file) {
        //We do not know the size of the decrypted file yet
        file.setFileSize(-1);
        file.setFileMD5Sum("");
        file.setFileMIMEType(MimeType2ExtMap.getContentType(file.getFileName()));
        encryptedFileRecognizer.markDecrypted(file);
        return file;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.file.storage.composition.AbstractDelegatingIDBasedFileAccess#copy(java.lang.String, java.lang.String, java.lang.String, com.openexchange.file.storage.File, java.io.InputStream, java.util.List)
     */
    @Override
    public String copy(String sourceId, String versionId, String destFolderId, File update, InputStream newData, List<Field> modifiedFields) throws OXException {

        if (newData == null) {
            //If the caller did not provide data we fetch them from the original file
            newData = fileAccess.getDocument(sourceId, versionId);
        }
        if (update == null) {
            update = fileAccess.getFileMetadata(sourceId, versionId);
        } else {
            File sourceFile = fileAccess.getFileMetadata(sourceId, versionId);
            if (update.getFileName() == null || update.getFileName().isEmpty()) {
                update.setFileName(sourceFile.getFileName());
            }
            update.setFileMIMEType(sourceFile.getFileMIMEType());
            update.setMeta(sourceFile.getMeta());
        }

        TimedResult<File> tversions = fileAccess.getVersions(sourceId);
        SearchIterator<File> files = tversions.results();
        ArrayList<File> versions = new ArrayList<File>();  // Collect list of versions
        while (files.hasNext()) {
            File version = files.next();
            versions.add(version);
        }
        files.close();
        modifiedFields.addAll(encryptedFileRecognizer.markDecrypted(update));
        // If no versions, just send to copy.  Version 0 is empty, size will be 2 for only 1 version
        if (versions.size() <= 2) {
            InputStream decrData = createGuardDecryptringInputStream(authenticationToken, newData);
            String newId = fileAccess.copy(sourceId, versionId, destFolderId, update, decrData, modifiedFields);
            IOUtils.closeQuietly(decrData);
            return newId;
        }
        // Versioned
        boolean first = true;
        String copy = null;
        FileID copyId = null;
        String currentVersionId = "";
        String lastVersion = null;
        // Loop versions
        for (int i = 1; i < versions.size(); i++) {
            File thisVersion = versions.get(i);
            if (thisVersion.getFileSize() != 0) {
                try (InputStream data = fileAccess.getDocument(sourceId, thisVersion.getVersion())) {
                    File currentFile = new DefaultFile(fileAccess.getFileMetadata(sourceId, thisVersion.getVersion()));
                    if (currentFile.isCurrentVersion()) {
                        currentVersionId = currentFile.getVersion();
                    }
                    updateFile(currentFile);
                    // First file, make a copy and move to the destination folder.
                    InputStream decrData = createGuardDecryptringInputStream(authenticationToken, data);
                    if (first) {
                        copy = fileAccess.copy(sourceId,
                            thisVersion.getVersion(),
                            destFolderId,
                            currentFile,
                            decrData,
                            modifiedFields);
                        copyId = new FileID(copy);
                        first = false;
                    } else {  // Subsequent versions
                        currentFile.setId(copyId.getFileId());   // Our destination is the copied file
                        currentFile.setFolderId(copyId.getFolderId());
                        lastVersion = currentFile.getVersion();
                        currentFile.setVersion("0");
                        fileAccess.saveDocument(currentFile,
                            decrData,
                            FileStorageFileAccess.DISTANT_FUTURE,
                            modifiedFields);
                    }
                    IOUtils.closeQuietly(decrData);
                } catch (IOException e) {
                    throw OXException.general("Error decrypting file", e);
                }
            }
        }
        // Having just saved all versions, latest will be marked current.  Restore user configured current version
        if (lastVersion != null && !lastVersion.equals(currentVersionId)) {
            makeCurrentFile(copy, currentVersionId);
        }
        return copy;
    }

    @Override
    public InputStream getDocument(String id, String version) throws OXException {
        return createDecryptedDocument(fileAccess.getDocumentAndMetadata(id, version)).getData();
    }

    @Override
    public InputStream getDocument(String id, String version, long offset, long length) throws OXException {
        Document document = fileAccess.getDocumentAndMetadata(id, version);
        if (isEncryptedGuardFile(document.getFile())) {
            try {
                //we cannot support random file access so we provide a PartialInputStream which allows to skip
                return new PartialInputStream(createDecryptedDocument(document).getData(), offset, length);
            } catch (IOException e) {
                throw GuardFileStorageExceptionCodes.IO_ERROR.create(e, e.getMessage());
            }
        }
        return fileAccess.getDocument(id, version, offset, length);
    }

    @Override
    public Document getDocumentAndMetadata(String id, String version) throws OXException {
        return createDecryptedDocument(fileAccess.getDocumentAndMetadata(id, version));
    }

    @Override
    public Document getDocumentAndMetadata(String id, String version, String clientEtag) throws OXException {
        return createDecryptedDocument(fileAccess.getDocumentAndMetadata(id, version, clientEtag));
    }

    @Override
    public InputStream optThumbnailStream(String id, String version) throws OXException {
        Document document = fileAccess.getDocumentAndMetadata(id, version);
        if (document != null) {
            File file = document.getFile();
            if (file != null) {
                if (!isEncryptedGuardFile(file)) {
                    return fileAccess.optThumbnailStream(id, version);
                }
            }
        }

        /* Either document/file is null or the file is encrypted and we cannot provide a thumbnail */
        return null;
    };

    @Override
    public File getFileMetadata(String id, String version) throws OXException {
        return updateFile(fileAccess.getFileMetadata(id, version));
    }
}
