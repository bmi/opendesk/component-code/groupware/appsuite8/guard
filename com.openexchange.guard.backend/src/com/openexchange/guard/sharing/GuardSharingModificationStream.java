/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.sharing;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.DefaultFileStorageGuestObjectPermission;
import com.openexchange.file.storage.DefaultFileStorageObjectPermission;
import com.openexchange.file.storage.FileStorageObjectPermission;
import com.openexchange.groupware.ComparedPermissions;
import com.openexchange.guard.api.GuardApi;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.file.storage.AbstractGuardInputStream;
import com.openexchange.guard.internal.authentication.AuthenticationTokenUtils;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.session.Session;
import com.openexchange.share.recipient.GuestRecipient;

/**
 * {@link GuardSharingModificationStream}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class GuardSharingModificationStream extends AbstractGuardInputStream {

    private final GuardApi guardApi;
    private final GuardAuthenticationToken authentication;
    private final Session session;
    private InputStream originalFile;
    private List<FileStorageObjectPermission> newGuestPermissions;
    private Collection newPermissions;

    public GuardSharingModificationStream (GuardApi guardApi, InputStream originalFile, ComparedPermissions permissions, GuardAuthenticationToken authentication, Session session) {
        this.guardApi = guardApi;
        this.authentication = authentication;
        this.session = session;
        this.originalFile = originalFile;
        this.newGuestPermissions = null;
        this.newPermissions = permissions.getNewPermissions();
    }

    public GuardSharingModificationStream (GuardApi guardApi, InputStream originalFile, List<FileStorageObjectPermission> guestPermissions, GuardAuthenticationToken authentication, Session session) {
        this.guardApi = guardApi;
        this.authentication = authentication;
        this.session = session;
        this.originalFile = originalFile;
        this.newGuestPermissions = guestPermissions;
        this.newPermissions = null;
    }


    /**
     * Get list of email addresses of users being added that have not previously been created in OX
     * @param list
     * @return
     * @throws JSONException
     */
    private JSONArray getEmails (List<DefaultFileStorageGuestObjectPermission> list) throws JSONException {
        JSONArray items = new JSONArray();
        for (DefaultFileStorageGuestObjectPermission pem : list) {
            JSONObject user = new JSONObject();
            GuestRecipient recip = (GuestRecipient) pem.getRecipient();
            user.put("email", recip.getEmailAddress());
            items.put(user);
        }
        return items;
    }


    private JSONObject createJSON () throws JSONException {
        JSONObject data = AuthenticationTokenUtils.toJsonObject(authentication);
        data.put("updatedShares", newGuestPermissions == null ? SharingUtil.getIDs(newPermissions) : SharingUtil.getIDs(newGuestPermissions));
        data.put("contextId", session.getContextId());
        data.put("user_Id", session.getUserId());
        return data;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.file.storage.AbstractGuardInputStream#getGuardStream()
     */
    @Override
    protected InputStream getGuardStream() throws OXException {
        JSONObject json;
        try {
            json = createJSON();
        } catch (JSONException e) {
            // Improve throw
            throw OXException.general("Unable to create JSON");
        }
        ByteArrayInputStream jsonStream = new ByteArrayInputStream(json.toString().getBytes(StandardCharsets.UTF_8));
        LinkedHashMap<String, InputStream> multipartParameters = new LinkedHashMap<>();
        multipartParameters.put("json", jsonStream);
        multipartParameters.put("data", originalFile);

        return guardApi.processResources(GuardApis.mapFor("action", "changeshare", "respondWithJSON", "true" /* forcing guard to render errors in JSON */), multipartParameters, null, session);
    }

}
