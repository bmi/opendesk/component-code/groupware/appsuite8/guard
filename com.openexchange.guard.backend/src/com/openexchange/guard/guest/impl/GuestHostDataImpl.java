/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.impl;

import com.openexchange.groupware.notify.hostname.HostData;

/**
 * {@link GuestHostDataImpl}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class GuestHostDataImpl implements HostData {

    private final String prefix;

    public GuestHostDataImpl (String prefix) {
        this.prefix = prefix;
    }

    /* (non-Javadoc)
     * @see com.openexchange.groupware.notify.hostname.HostData#getHTTPSession()
     */
    @Override
    public String getHTTPSession() {
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.groupware.notify.hostname.HostData#getRoute()
     */
    @Override
    public String getRoute() {
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.groupware.notify.hostname.HostData#getHost()
     */
    @Override
    public String getHost() {
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.groupware.notify.hostname.HostData#getPort()
     */
    @Override
    public int getPort() {
        return 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.groupware.notify.hostname.HostData#isSecure()
     */
    @Override
    public boolean isSecure() {
        return true;
    }

    /* (non-Javadoc)
     * @see com.openexchange.groupware.notify.hostname.HostData#getDispatcherPrefix()
     */
    @Override
    public String getDispatcherPrefix() {
        return prefix;
    }

}
