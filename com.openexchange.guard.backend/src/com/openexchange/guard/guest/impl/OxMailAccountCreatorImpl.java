/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.impl;

import java.util.Set;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.alias.UserAliasStorage;
import com.openexchange.groupware.contexts.impl.ContextStorage;
import com.openexchange.groupware.userconfiguration.UserConfigurationCodes;
import com.openexchange.guard.guest.OxMailAccountCreatorService;
import com.openexchange.mail.usersetting.UserSettingMail;
import com.openexchange.mail.usersetting.UserSettingMailStorage;
import com.openexchange.mailaccount.Constants;
import com.openexchange.mailaccount.MailAccount;
import com.openexchange.mailaccount.MailAccountDescription;
import com.openexchange.mailaccount.MailAccountStorageService;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;
import com.openexchange.share.GuestInfo;

/**
 * {@link OxMailAccountCreatorImpl}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class OxMailAccountCreatorImpl implements OxMailAccountCreatorService {

    private ServiceLookup services;

    /**
     * Initializes a new {@link OxMailAccountCreatorImpl}.
     *
     * @param baseEndPoint The guard endpoint
     * @param mailAccountStorageService The {@link MailAccountStorageService}
     * @param aliasStorage The {@link UserAliasStorage}
     */
    public OxMailAccountCreatorImpl(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Internal method to create an alias entry for the new guest
     *
     * @param guest The new guest to create an alias for
     * @throws OXException
     */
    private void createAlias(GuestInfo guest) throws OXException {
        services.getServiceSafe(UserAliasStorage.class).createAlias(null, guest.getContextID(), guest.getGuestID(), guest.getEmailAddress());
    }

    /**
     * Internal method to check whether or not an alias exists for the given guest
     *
     * @param guest The guest
     * @return True if an alias for this user already exists
     * @throws OXException
     */
    private boolean aliasExists(GuestInfo guest) throws OXException {
        Set<String> aliases = services.getServiceSafe(UserAliasStorage.class).getAliases(guest.getContextID(), guest.getGuestID());
        return aliases != null && aliases.contains(guest.getEmailAddress());
    }

    /**
     * Internal method to check whether or not the given guest already owns any mail account;
     *
     * @param guest The guest
     * @return True if the given guest has at least one mail account
     * @throws OXException
     */
    private boolean hasMailAccounts(GuestInfo guest) throws OXException {
        MailAccount[] accounts = services.getServiceSafe(MailAccountStorageService.class).getUserMailAccounts(guest.getGuestID(), guest.getContextID());
        return accounts != null && accounts.length > 0;
    }

    /**
     * Internal method to check whether or not the guest already has a guard mail account
     *
     * @param guest The guest The guest
     * @return True, if the given guest already has a guard guest mail account, false otherwise
     * @throws OXException
     */
    private boolean guardGuestMailAccountExists(GuestInfo guest) throws OXException {
        MailAccountStorageService mailAccountStorageService = services.getServiceSafe(MailAccountStorageService.class);
        if (mailAccountStorageService.existsMailAccount(MailAccount.DEFAULT_ID, guest.getGuestID(), guest.getContextID())) {
            MailAccount[] accounts = mailAccountStorageService.getUserMailAccounts(guest.getGuestID(), guest.getContextID());
            if (accounts.length > 0) {
                for (MailAccount account: accounts) {
                    if (account.getName().equals(Constants.NAME_GUARD_GUEST)) {
                        return true;   // Guard Guest mail already exists
                    }
                }
            }
        }
        return false;
    }

    private void createGuardGuestMailAccount(GuestInfo guest, boolean defaultAccount) throws OXException {
        MailAccountDescription mad = new MailAccountDescription();
        mad.setMailProtocol(Constants.MAIL_PROTOCOL_GUARD_GUEST);
        mad.setMailServer("localhost");  // change
        mad.setPrimaryAddress(guest.getEmailAddress());
        mad.setLogin(guest.getEmailAddress());
        mad.setDrafts("Drafts");
        mad.setTrash("Trash");
        mad.setSent("Sent Items");
        mad.setName(Constants.NAME_GUARD_GUEST);
        mad.setSpamHandler("NoSpamHandler");
        mad.setDefaultFlag(defaultAccount);
        mad.setTransportServer("localhost");
        mad.setTransportProtocol("smtp");
        if(defaultAccount) {
            mad.setId(MailAccount.DEFAULT_ID);
        }
        services.getServiceSafe(MailAccountStorageService.class).insertMailAccount(mad, guest.getGuestID(), ContextStorage.getStorageContext(guest.getContextID()), null); // Change null to session if adding password
    }

    /**
     * Creates a user mail account entry for a Guard Guest Mail account
     * Adds as default if no accounts exist, otherwise adds as extra account
     * @param guest
     * @param session
     * @throws OXException
     */
    @Override
    public void createAccount(GuestInfo guest, Session session) throws OXException {
        final boolean defaultAccount = !hasMailAccounts(guest);
        if(!guardGuestMailAccountExists(guest)) {
            createGuardGuestMailAccount(guest, defaultAccount);
            if (defaultAccount) {
                addMailSettings(guest);  // Add mail settings if this is the default, new account
            }
        }

        //Create the main alias for the new guest
        if(!aliasExists(guest)) {
            createAlias(guest);
        }
    }

    @Override
    public void addMailSettings(GuestInfo guest) throws OXException {
        boolean found = false;
        try {
            UserSettingMail userSettings = UserSettingMailStorage.getInstance().loadUserSettingMail(guest.getGuestID(), ContextStorage.getStorageContext(guest.getContextID()), null);
            if (userSettings != null) {
                found = true;
            }
        } catch (OXException ex) {
            if (ex.getCode() != UserConfigurationCodes.MAIL_SETTING_NOT_FOUND.getNumber()) {
                throw ex;
            }
        }
        if (found) {
            return;
        }
        UserSettingMail usm = new UserSettingMail(guest.getGuestID(), guest.getContextID());
        usm.setDisplayHtmlInlineContent(true);
        usm.setAllowHTMLImages(true);
        usm.setAttachOriginalMessage(false);
        usm.setMsgFormat(3);
        usm.setReplyToAddr(guest.getEmailAddress());
        usm.setSendAddr(guest.getEmailAddress());
        usm.setStdDraftsName("Drafts");
        usm.setStdSentName("Sent Items");
        usm.setStdSpamName("Spam");
        usm.setStdTrashName("Trash");
        UserSettingMailStorage.getInstance().saveUserSettingMail(usm, guest.getGuestID(), ContextStorage.getStorageContext(guest.getContextID()));
    }
}
