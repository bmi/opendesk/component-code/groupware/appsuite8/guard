/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.impl;

import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.api.GuardApi;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.internal.AbstractGuardAccess;

/**
 * Service for checking status of Guest users
 * {@link GuardGuestUserService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class GuardGuestUserService extends AbstractGuardAccess {

    /**
     * Check if the Guest user exists as a guard user, and if so, still has any emails in File cache
     * Used for cleanup, etc
     * @param guestId   OX Guest Id
     * @param guestContext  OX Guest Context
     * @return  True if there are items for an existing guest user
     * @throws OXException
     */
    public boolean hasMailItems(int guestId, int guestContext) throws OXException {
        GuardApi api = requireGuardApi(this.GUARDADMIN_ENDPOINT);
        JSONObject result = api.doCallGet(GuardApis.mapFor("action", "check_guest", "context_id", Integer.toString(guestContext), "user_id", Integer.toString(guestId)), JSONObject.class, null);
        try {
            if (result != null && result.has("error")) {
                StringBuilder sb = new StringBuilder();
                sb.append("Error occured checking guard guest emails ");
                sb.append(result.getString("error"));
                if (result.has("error_desc")) {
                    sb.append(": ");
                    sb.append(result.getString("error_desc"));
                }
                throw OXException.general(sb.toString());
            }
            return (result != null && result.has("hasItems") && result.getBoolean("hasItems"));
        } catch (JSONException e) {
            throw OXException.general("Problem checking if guest emails exist", e);
        }
    }

}
