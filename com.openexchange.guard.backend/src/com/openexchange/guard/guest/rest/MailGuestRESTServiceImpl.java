/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.rest;

import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.guest.MailGuestService;
import com.openexchange.rest.services.annotation.Role;
import com.openexchange.rest.services.annotation.RoleAllowed;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;
import com.openexchange.sessiond.SessiondService;
import com.openexchange.tools.servlet.AjaxExceptionCodes;
import com.openexchange.user.UserService;

/**
 * {@link MailGuestRESTServiceImpl}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
@RoleAllowed(Role.BASIC_AUTHENTICATED)
public class MailGuestRESTServiceImpl implements MailGuestRESTService {

    private static final String PARAM_SESSION_ID        = "session";
    private static final String PARAM_TARGET_MAIL       = "email";
    private static final String PARAM_MAIL_ITEM_ID      = "mailId";
    private static final String RESULT_PARAM_SHARE_LINK = "shareLink";
    private static final String PARAM_USER_ID           = "userId";
    private static final String PARAM_CONTEXT_ID        = "cid";
    private static final String PARAM_LANGUAGE          = "language";

    private final ServiceLookup services;

    /**
     * Initializes a new {@link MailGuestRESTServiceImpl}.
     *
     * @param mailGuestService The {@link MailGuestRESTServiceImpl} to use
     */
    public MailGuestRESTServiceImpl(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Internal method to parse a value from the provided json body
     *
     * @param body The body to parse the values from
     * @param parameterName The name of the parameter to parse from the given body
     * @return The value parsed from the body
     * @throws JSONException
     * @throws OXException
     */
    private String requireJSONParameter(JSONObject body, String parameterName) throws JSONException, OXException {
        if (body.has(parameterName)) {
            return body.getString(parameterName);
        }
        throw AjaxExceptionCodes.MISSING_FIELD.create(parameterName);
    }

    /**
     * Internal method to parse a value from the provided json body
     *
     * @param body The body to parse the values from
     * @param parameterName The name of the parameter to parse from the given body
     * @return The value parsed from the body
     * @throws JSONException
     * @throws OXException
     */
    private int requireJSONInt(JSONObject body, String parameterName) throws JSONException, OXException {
        if (body.has(parameterName)) {
            return body.getInt(parameterName);
        }
        throw AjaxExceptionCodes.MISSING_FIELD.create(parameterName);
    }

    /**
     * Internal method to parse a value from the provided json body
     *
     * @param body The body to parse the values from
     * @param parameterName The name of the parameter to parse from the given body
     * @return The value parsed from the body
     * @throws JSONException
     * @throws OXException
     */
    private String getJSONParameter(JSONObject body, String parameterName) throws JSONException {
        if (body.has(parameterName)) {
            return body.getString(parameterName);
        }
        return null;
    }

    /**
     * Gets the {@link Session} or the given ID
     *
     * @param sessionId The ID of the session
     * @return The {@link Session} for the given ID, or null if no such session was found
     * @throws OXException
     */
    private Session getSession(String sessionId) throws OXException {
        return services.getServiceSafe(SessiondService.class).getSession(sessionId);
    }

    private Session createFakeSession(int userId, int cid) {
        return new FakeSession(userId, cid);
    }

    /**
     * Creates a JSON result ready to be returned to the caller
     *
     * @param shareLink The link created
     * @return The JSON object
     * @throws JSONException
     */
    private JSONObject createResult(String shareLink) throws JSONException {
        return new JSONObject().put(RESULT_PARAM_SHARE_LINK, shareLink);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.rest.MailGuestRESTService#create(org.json.JSONObject)
     */
    @Override
    public JSONObject create(JSONObject body) throws OXException {

        if (body == null || body.isEmpty()) {
            throw AjaxExceptionCodes.MISSING_REQUEST_BODY.create();
        }

        try {
            final String guestEmail = requireJSONParameter(body, PARAM_TARGET_MAIL);
            final String mailItemId = requireJSONParameter(body, PARAM_MAIL_ITEM_ID);
            final String sessionId = getJSONParameter(body, PARAM_SESSION_ID);
            final String language = getJSONParameter(body, PARAM_LANGUAGE);
            final Session session;
            if (sessionId != null) {
                session = getSession(sessionId);
            } else {
                final int userId = requireJSONInt(body, PARAM_USER_ID);
                final int cid = requireJSONInt(body, PARAM_CONTEXT_ID);
                session = createFakeSession(userId, cid);
            }
            if (session != null) {
                final String guestLink = services.getServiceSafe(MailGuestService.class).createGuestLink(session, guestEmail, mailItemId, language);
                return createResult(guestLink);
            }
            throw AjaxExceptionCodes.INVALID_PARAMETER_VALUE.create(PARAM_SESSION_ID, sessionId);
        } catch (JSONException e) {
            throw AjaxExceptionCodes.JSON_ERROR.create(e, e.getMessage());
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.rest.MailGuestRESTService#getEmails(org.json.JSONObject)
     */
    @Override
    public JSONObject getEmails(int userid, int context) throws OXException {
        return new MailGuestEmailAddressHandler(services.getServiceSafe(UserService.class)).getEmails(userid, context);
    }

    @Override
    public JSONObject getId(String email, int guestContext) throws OXException {
        return GuestToOxLookup.lookup(services.getServiceSafe(UserService.class), email, guestContext);
    }
}
