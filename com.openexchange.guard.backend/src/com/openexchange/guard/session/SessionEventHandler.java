/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.session;

import java.util.Map;
import com.openexchange.exception.OXException;
import com.openexchange.session.Session;
import com.openexchange.sessiond.event.SessiondEventListener;

/**
 * {@link SessionEventHandler}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class SessionEventHandler implements SessiondEventListener {

    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(SessionEventHandler.class);

    /* (non-Javadoc)
     * @see com.openexchange.sessiond.event.SessiondEventListener#handleSessionRemoval(com.openexchange.session.Session)
     */
    @Override
    public void handleSessionRemoval(Session session) {

        try {
            new SessionNotifier().deleteSession(session);
        } catch (OXException e) {
            logger.error("Failure to delete Guard session ", e);
        }

    }

    /* (non-Javadoc)
     * @see com.openexchange.sessiond.event.SessiondEventListener#handleContainerRemoval(java.util.Map)
     */
    @Override
    public void handleContainerRemoval(Map<String, Session> sessions) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see com.openexchange.sessiond.event.SessiondEventListener#handleError(com.openexchange.exception.OXException)
     */
    @Override
    public void handleError(OXException error) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see com.openexchange.sessiond.event.SessiondEventListener#handleSessionDataRemoval(java.util.Map)
     */
    @Override
    public void handleSessionDataRemoval(Map<String, Session> sessions) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see com.openexchange.sessiond.event.SessiondEventListener#handleSessionReactivation(com.openexchange.session.Session)
     */
    @Override
    public void handleSessionReactivation(Session session) {
        // TODO Auto-generated method stub

    }

}
