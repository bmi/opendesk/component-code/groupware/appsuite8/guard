/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.session;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import com.openexchange.exception.OXException;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.AbstractGuardAccess;
import com.openexchange.guard.internal.GuardApiImpl;
import com.openexchange.session.Session;

/**
 * {@link SessionNotifier}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class SessionNotifier extends AbstractGuardAccess {

    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(SessionNotifier.class);
    private static final String SESSION_ID = "sessionId";

    private String hash(String value) throws OXException {
        if (value == null) {
            return null;
        }
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-512");
        } catch (Exception e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, e.getMessage());
        }
        byte[] bytes = md.digest(value.getBytes(StandardCharsets.UTF_8));
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        String sessionId = sb.toString();
        sessionId = sessionId.substring(0, 40);
        return sessionId.toString();
    }

    /**
     * Sends sessionId to Guard backend to delete / remove session
     * 
     * @param sessionId
     * @throws OXException
     */
    public void deleteSession(Session session) throws OXException {
        String guardId = hash(session.getSecret());
        if (guardId == null)
            return;
        GuardApiImpl guardApi = requireGuardApi(LOGIN_ENDPOINT);
        try {
            guardApi.doCallPut(GuardApis.mapFor("action", "delete_session", SESSION_ID, guardId), null, Void.class, session);
        } catch (OXException ex) {
            logger.error("Problem deleting Guard Session ", ex);
        }

    }

}
