/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.dataObjects;

import static com.openexchange.java.Autoboxing.l;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import com.openexchange.crypto.CryptoType;
import com.openexchange.java.Strings;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.mail.dataobjects.SecurityResult;
import com.openexchange.mail.dataobjects.SignatureResult;
import com.openexchange.tools.encoding.Base64;

/**
 * {@link CryptoSecurityResult}
 * Contains results of Crypto action
 * Signature data, if decoded
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.8.0
 */
public class CryptoSecurityResult extends SecurityResult{

    private static final String VERIFIED_PROPERTY = "verified";
    private static final String DATE_PROPERTY = "signaturecreatedon";
    private static final String MISSING_PROPERTY = "missing";
    private static final String ERROR_PROPERTY = "error";
    private static final String TYPE_PROPERTY = "type";
    private static final String ISSUER_USERIDS_PROPERTY = "issueruserids";
    private static final String ISSUER_KEY_FINGERPRINT_PROPERTY = "issuerkeyfingerprint";
    private static final String ISSUER_KEY_ID_PROPERTY = "issuerkeyid";



    public CryptoSecurityResult(MailMessage msg, CryptoType.PROTOCOL type) {
        super();
        this.signatureResults = null;
        addHeaderSignatureResults(msg.getHeader("X-Guard-Signature-Result"));
        msg.removeHeader("X-Guard-Signature-Result");
        if (!msg.getHeaders().isEmpty() && msg.hasHeaders("X-PGPFormat")) {
            this.pgpInline = msg.getHeader("X-PGPFormat", null).trim().equals("inline");
        }
        this.decryptSuccess = false;
        this.error = null;
        this.type = type;
    }

    /**
     * Internal method parse the user IDs from the given header value
     *
     * @param headerValue The user IDs
     * @return A list containing the parsed user IDs
     */
    private List<String> parseUserIds(String headerValue){
        if(Strings.isNotEmpty(headerValue)) {
            String[] userIds = headerValue.split(",");
            return Arrays.asList(userIds);
        }
        return Collections.emptyList();
    }

    /**
     * Set that this message was decoded
     */
    public void setDecoded() {
        this.decryptSuccess = true;
    }

    /**
     * Failed message
     * @param error
     */
    public void setFail(String error) {
        this.decryptSuccess = false;
        this.error = error;
    }

    /**
     * Create from string of headers
     * @param headers
     */
    public void addHeaderSignatureResults(String [] headers) {
        if (headers == null) {
            return;
        }
        for (String header: headers) {
            addHeaderSignatureResult (header);
        }
    }

    public void addHeaderSignatureResult (String base64header) {
        String header = new String(Base64.decode(base64header), StandardCharsets.UTF_8);
        String[] lines = header.split(";");
        SignatureResult sig = new SignatureResult();
        for (String line : lines) {
            String [] result = line.split("=");
            if(result.length > 1) {
                switch (result[0].toLowerCase().trim()) {
                    case VERIFIED_PROPERTY:
                       sig.setVerified(result[1].toLowerCase().trim().equals("true"));
                       break;
                    case MISSING_PROPERTY:
                        sig.setMissing(result[1].toLowerCase().trim().equals("true"));
                        break;
                    case DATE_PROPERTY:
                        sig.setDate(Long.parseLong(result[1].trim()));
                        break;
                    case ERROR_PROPERTY:
                        sig.setError(result[1]);
                        break;
                    case ISSUER_USERIDS_PROPERTY:
                        sig.setIssuerUserIds(parseUserIds(result[1]));
                        break;
                    case ISSUER_KEY_FINGERPRINT_PROPERTY:
                        sig.setIssuerKeyFingerprint(result[1]);
                        break;
                    case ISSUER_KEY_ID_PROPERTY:
                        sig.setIssuerKeyId(l(Long.valueOf(result[1])));
                        break;
                    case TYPE_PROPERTY:
                        this.type = CryptoType.getTypeFromString(result[1]);
                }
            }
        }
        if (signatureResults == null) {
            signatureResults = new ArrayList<SignatureResult> ();
        }
        signatureResults.add(sig);
    }


}
