/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl.guests;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.AbstractGuardAccess;
import com.openexchange.guard.internal.GuardApiImpl;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.session.Session;

/**
 * {@link GuardGuestMailCopier} Copies a guest mail item in OX Guard to another folder
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardGuestMailCopier extends AbstractGuardAccess {

    private Object createCopy(String[] mailIds, String destinationFolderId) {
        return new JSONObject()
        .putSafe("mailIds", new JSONArray(Arrays.asList(mailIds)))
        .putSafe("newFolderId", destinationFolderId);
    }

    private String[] parseResponse(InputStream response) throws OXException {
        MailMessage[] messages = new MailMessageFactory().createFrom(toJSON(response));
        return Arrays.stream(messages).map( message -> message.getMailId()).toArray(String[]::new);
    }

    /**
     * Internal method to read a JSONObject from the given InputStream
     *
     * @param response The inputStream
     * @return The JSONObject read from the steam
     * @throws OXException
     */
    private JSONObject toJSON(InputStream response) throws OXException {
        try {
            return new JSONObject(new InputStreamReader(response, StandardCharsets.UTF_8));
        } catch (JSONException e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, e.getMessage());
        }
    }

    private String[] copyMessagesInternal(Session session, GuardAuthenticationToken authToken, String[] mailIds, String destinationFolderId) throws OXException {

        final GuardApiImpl guardGuestApi = requireGuardApi(GUEST_ENDPOINT);
        final JSONObject authJson = GuardGuestAuthHelper.toAuthJSON(authToken, session);
        final JSONObject json = new JSONObject()
            .putSafe("auth", authJson)
            .putSafe("copy", createCopy(mailIds, destinationFolderId));
        ByteArrayInputStream jsonStream = new ByteArrayInputStream(json.toString().getBytes(StandardCharsets.UTF_8));

        try (InputStream result = guardGuestApi.processResource(GuardApis.mapFor("action", "copymessages"),
            jsonStream,
            null,
            "json",
            session)) {
            return parseResponse(result);
        } catch (IOException e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Copies a bunch of messges to a folder
     *
     * @param session The session
     * @param authToken The Guard authentication token
     * @param mailIds The IDs of the emails to copy
     * @param destinationFolderId The ID of the destination folder
     * @return A set of new item ids coresponding to the given mailIds
     * @throws OXException
     */
    public String[] copyMessages(Session session, GuardAuthenticationToken authToken, String[] mailIds, String destinationFolderId) throws OXException {
        return copyMessagesInternal(session,authToken, mailIds, destinationFolderId);
    }
}
