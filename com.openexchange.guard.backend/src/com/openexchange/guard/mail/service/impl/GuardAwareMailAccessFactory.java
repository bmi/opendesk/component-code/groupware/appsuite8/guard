/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl;

import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.mail.api.IMailFolderStorage;
import com.openexchange.mail.api.IMailMessageStorage;
import com.openexchange.mail.api.MailAccess;
import com.openexchange.mail.api.crypto.CryptographicAwareMailAccessFactory;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;

/**
 * {@link GuardAwareMailAccessFactory}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.4
 */
public class GuardAwareMailAccessFactory implements CryptographicAwareMailAccessFactory {

    private static final GuardAuthenticationToken NULL_TOKEN = null;
    private final ServiceLookup services;

    /**
     * Initializes a new {@link GuardAwareMailAccessFactory}.
     * @param pgpMailRecognizer Strategy to use for recognizing PGP email
     */
    public GuardAwareMailAccessFactory(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Internal method to construct an {@link GuardAuthenticationToken} from a given string.
     *
     * @param authentication The string representation of the token.
     * @return The {@link GuardAuthenticationToken} created from the given string
     * @throws OXException
     */
    private GuardAuthenticationToken createToken(String authentication) throws OXException {
        try {
            if(authentication != null) {
                GuardAuthenticationToken token = GuardAuthenticationToken.fromString(authentication);
                if (token != null) {
                    return token;
                } else {
                    throw GuardExceptionCodes.MISSING_AUTH.create();
                }
            } else {
                //Authentication is not required for all crypto modes
                return null;
            }
        } catch (Exception e) {
            throw GuardExceptionCodes.MISSING_AUTH.create();
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.crypto.CryptographicAwareMailAccessFactory#createAccess(com.openexchange.mail.api.MailAccess, com.openexchange.session.Session, java.lang.String)
     */
    @Override
    public MailAccess<IMailFolderStorage, IMailMessageStorage> createAccess(MailAccess<IMailFolderStorage, IMailMessageStorage> delegate, Session session, String authentication, CryptoType.PROTOCOL type) throws OXException {
        return new GuardMailAccess(delegate, session, services, createToken(authentication), type);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.crypto.CryptographicAwareMailAccessFactory#createAccess(com.openexchange.mail.api.MailAccess, com.openexchange.session.Session)
     */
    @Override
    public MailAccess<IMailFolderStorage, IMailMessageStorage> createAccess(MailAccess<IMailFolderStorage, IMailMessageStorage> delegate, Session session, CryptoType.PROTOCOL type) throws OXException {
        return new GuardMailAccess(delegate, session, services, NULL_TOKEN, type);
    }

}
