/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl.guests;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.AbstractGuardAccess;
import com.openexchange.guard.internal.GuardApiImpl;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.mail.IndexRange;
import com.openexchange.mail.MailField;
import com.openexchange.mail.MailSortField;
import com.openexchange.mail.OrderDirection;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.session.Session;

/**
 * {@link GuardGuestMailFetcher} fetches a guest mail item from OX Guard
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardGuestMailFetcher extends AbstractGuardAccess {

    /**
     * A constant used to fetch all mails
     */
    public static final String[] ALL_IDS = new String[] {"ALL_MAIL_IDS"};

    /**
     * Internal method to check if the full body of guest mail should be fetched from OX Guard
     *
     * @param mailFields The mail fields
     * @return True if the full body should be fetched from OX Guard, False otherwise
     */
    private boolean fetchMessageBody(MailField[] mailFields) {
        return Arrays.stream(mailFields).anyMatch( mf ->
            mf == MailField.FULL ||
            mf == MailField.HEADERS ||
            mf == MailField.FULL);
    }

    /**
     * Internal method to create a query for guest emails
     *
     * @param folder The folder to fetch messages from, or null to fetch messages from all folders
     * @param mailIds The IDs of the messages to fetch
     * @param indexRange The index range to query
     * @param sortField defines how the returned messages are sorted
     * @param orderDirection The order of sorting
     * @param mailFields defines which data is returned for the messages
     * @param notMessageFlags defines which messages should NOT be returned
     * @param limit defines the maximum amount of returned items
     * @return
     */
    private JSONObject createQuery(String folder, String[] mailIds, IndexRange indexRange, MailSortField sortField, OrderDirection orderDirection, MailField[] mailFields, Integer notMailFlags, int limit) {
        JSONObject query = new JSONObject()
            .putSafe("mailIds", new JSONArray(Arrays.asList(mailIds)))
            .putSafe("fetchMessageBody", fetchMessageBody(mailFields));

        if(folder != null) {
            query.putSafe("folderId", folder);
        }

        if(sortField != null) {
            query.putSafe("orderBy", sortField.toString());
        }
        if(orderDirection != null) {
            query.putSafe("orderDirection", orderDirection.toString());
        }

        if(notMailFlags != null) {
            query.putSafe("notMailFlags", notMailFlags);
        }

        if(limit > 0) {
            query.putSafe("limit", limit);
        }

        if(indexRange != null) {
            query.putSafe("indexRangeStart", indexRange.getStart());
            query.putSafe("indexRangeEnd", indexRange.getEnd());
        }
        return query;
    }

    /**
     * Internal method to fetch guest messages from OX Guard
     *
     * @param session The session
     * @param authToken The auth token
     * @param folder The folder to fetch messages from, or null to fetch messages from all folders
     * @param mailIds The IDs of the messages to fetch
     * @param indexRange The range to query
     * @param sortField defines how the returned messages are sorted
     * @param order The order of sorting
     * @param mailFields defines which data is returned for the messages
     * @param notMessageFlags defines which messages should NOT be returned
     * @param markSeen Whether the messages should be marked as seen or not
     * @param limit defines the maximum amount of returned items
     * @return The result of the request as InputStream
     * @throws OXException
     */
    private InputStream fetchMessagesInternal(Session session,
                                              GuardAuthenticationToken authToken,
                                              String folder,
                                              String[] mailIds,
                                              IndexRange indexRange,
                                              MailSortField sortField,
                                              OrderDirection order,
                                              MailField[] mailFields,
                                              Integer notMessageFlags,
                                              boolean markSeen,
                                              int limit) throws OXException {

        final GuardApiImpl guardGuestApi = requireGuardApi(GUEST_ENDPOINT);
        JSONObject authJson = GuardGuestAuthHelper.toAuthJSON(authToken, session);
        final JSONObject json = new JSONObject()
            .putSafe("auth", authJson)
            .putSafe("query", createQuery(folder, mailIds, indexRange, sortField, order, mailFields, notMessageFlags, limit));
        final ByteArrayInputStream jsonStream = new ByteArrayInputStream(json.toString().getBytes(StandardCharsets.UTF_8));

        return guardGuestApi.processResource(GuardApis.mapFor("action", "getmessages", "respondWithJSON", "true"),
            jsonStream,
            null,
            "json",
            session);
    }

    /**
     * Internal method to read a JSONObject from the given InputStream
     *
     * @param response The inputStream
     * @return The JSONObject read from the steam
     * @throws OXException
     */
    private JSONObject toAuthJSON(InputStream response) throws OXException {
        try {
            return new JSONObject(new InputStreamReader(response, StandardCharsets.UTF_8));
        } catch (JSONException e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Fetches a single message with all filled MailFields
     *
     * @param session The session
     * @param authToken The auth token
     * @param folder, The name of the folder to fetch messages from, or null too fetch from all folders
     * @param mailId The ID of the mail
     * @param markSeen Whether the message should be marked as seen or not
     * @return
     * @throws OXException
     */
    public MailMessage fetchMessage(Session session, GuardAuthenticationToken authToken, String folder, String mailId, boolean markSeen) throws OXException {
        final IndexRange noIndexRange = null;
        MailMessage[] result = fetchMessages(session,
            authToken,
            folder,
            new String[] {mailId},
            noIndexRange,
            MailSortField.SENT_DATE, //Sorting does not really matter since we are fetching only a single mail
            OrderDirection.DESC,
            new MailField[] { MailField.FULL },
            markSeen);
        return result != null && (result.length > 0) ? result[0] : null;
    }

    /**
     * Fetches unread Guest messages from OX Guard
     *
     * @param session The session
     * @param authToken The auth token
     * @param folder, The name of the folder to fetch messages from, or null too fetch from all folders
     * @param mailIds The IDs of the messages to fetch
     * @param sortField defines how the returned messages are sorted
     * @param order The order of sorting
     * @param mailFields defines which data is returned for the messages
     * @param limit defines the maximum amount of returned items
     * @return
     * @throws OXException
     */
    public MailMessage[] fetchUnreadMessages(Session session,
                                             GuardAuthenticationToken authToken,
                                             String folder,
                                             String[] mailIds,
                                             IndexRange indexRange,
                                             MailSortField sortField,
                                             OrderDirection order,
                                             MailField[] mailFields,
                                             int limit) throws OXException {
        final boolean markSeen = false;
        try (InputStream resultStream = fetchMessagesInternal(session, authToken, folder, mailIds, indexRange, sortField, order, mailFields, MailMessage.FLAG_SEEN, markSeen, limit)) {
            return new MailMessageFactory().createFrom(toAuthJSON(resultStream));
        } catch (IOException e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Fetches Guest messages from OX Guard
     *
     * @param session The session
     * @param authToken The auth token
     * @param folder, The name of the folder to fetch messages from, or null too fetch from all folders
     * @param mailIds The IDs of the messages to fetch
     * @param indexRange The range of email items to fetch
     * @param sortField defines how the returned messages are sorted
     * @param order The order of sorting
     * @param mailFields defines which data is returned for the messages
     * @param markSeen Whether the messages should be marked as seen or not
     * @return The messages, or an empty array if no messages were found
     * @throws OXException
     */
    public MailMessage[] fetchMessages(Session session,
                                       GuardAuthenticationToken authToken,
                                       String folder,
                                       String[] mailIds,
                                       IndexRange indexRange,
                                       MailSortField sortField,
                                       OrderDirection order,
                                       MailField[] mailFields,
                                       boolean markSeen) throws OXException {
        final int limit = -1;
        final Integer noMessageFlags = null;
        try (InputStream resultStream = fetchMessagesInternal(session, authToken, folder, mailIds, indexRange, sortField, order, mailFields, noMessageFlags, markSeen,
            limit)) {
            return new MailMessageFactory().createFrom(toAuthJSON(resultStream));
        } catch (IOException e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, e.getMessage());
        }
    }
}
