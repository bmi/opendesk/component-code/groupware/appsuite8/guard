/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl.guests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.mail.IndexRange;
import com.openexchange.mail.MailField;
import com.openexchange.mail.MailSortField;
import com.openexchange.mail.OrderDirection;
import com.openexchange.mail.api.ISimplifiedThreadStructureEnhanced;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.mail.dataobjects.MailPart;
import com.openexchange.mail.dataobjects.compose.ComposedMailMessage;
import com.openexchange.mail.search.SearchTerm;
import com.openexchange.session.Session;

/**
 * {@link GuardGuestMessageStorage}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardGuestMessageStorage implements IGuardGuestMessageStorage, ISimplifiedThreadStructureEnhanced {

    private final Session session;
    private final String trashFolderName;
    private static final GuardAuthenticationToken EMPTY_AUTH_TOKEN = null;

    /**
     * Initializes a new {@link GuardGuestMessageStorage}.
     *
     * @param session A session object
     */
    public GuardGuestMessageStorage(Session session, String trashFolderName) {
        this.session = session;
        this.trashFolderName = trashFolderName;
    }

    private List<List<MailMessage>> getThreadSortedList(String folder, IndexRange indexRange, MailSortField sortField, OrderDirection order, MailField[] fields) throws OXException{
        //Just returning all messages, because thread sorting is not supported for OX Guard guest e-mails
        MailMessage[] allMessages = getAllMessages(folder, indexRange, sortField, order, fields);
        List<List<MailMessage>> ret = new ArrayList<List<MailMessage>>();
        for(MailMessage m : allMessages) {
            ret.add(Arrays.asList(m));
        }
        return ret;
    }


    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#appendMessages(java.lang.String, com.openexchange.mail.dataobjects.MailMessage[])
     */
    @Override
    public String[] appendMessages(String destFolder, MailMessage[] msgs) throws OXException {
        return new GuardGuestMailAppender().appendMessage(session, EMPTY_AUTH_TOKEN, destFolder, msgs);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#copyMessages(java.lang.String, java.lang.String, java.lang.String[], boolean)
     */
    @Override
    public String[] copyMessages(String sourceFolder, String destFolder, String[] mailIds, boolean fast) throws OXException {
        return new GuardGuestMailCopier().copyMessages(session, EMPTY_AUTH_TOKEN,mailIds,destFolder);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#deleteMessages(java.lang.String, java.lang.String[], boolean)
     */
    @Override
    public void deleteMessages(String folder, String[] mailIds, boolean hardDelete) throws OXException {
        if(hardDelete) {
            new GuardGuestMailDeleter().deleteMessages(session, EMPTY_AUTH_TOKEN, mailIds);
        }
        else {
            new GuardGuestMailMover().moveMessages(session, EMPTY_AUTH_TOKEN, mailIds, trashFolderName);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#getAllMessages(java.lang.String, com.openexchange.mail.IndexRange, com.openexchange.mail.MailSortField, com.openexchange.mail.OrderDirection, com.openexchange.mail.MailField[])
     */
    @Override
    public MailMessage[] getAllMessages(String folder, IndexRange indexRange, MailSortField sortField, OrderDirection order, MailField[] fields) throws OXException {
        final boolean markSeen = false;
        return new GuardGuestMailFetcher().fetchMessages(
            session,
            EMPTY_AUTH_TOKEN,
            folder,
            GuardGuestMailFetcher.ALL_IDS,
            indexRange,
            sortField,
            order,
            fields,
            markSeen);

    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#getAttachment(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public MailPart getAttachment(String folder, String mailId, String sequenceId) throws OXException {
        if (sequenceId != null) {
            MailMessage message = getMessage(folder, mailId, false);
            if (message != null) {
                return message.getEnclosedMailPart(Integer.parseInt(sequenceId) - 1);
            }
        }
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#getImageAttachment(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public MailPart getImageAttachment(String folder, String mailId, String contentId) throws OXException {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#getMessage(java.lang.String, java.lang.String, boolean)
     */
    @Override
    public MailMessage getMessage(String folder, String mailId, boolean markSeen) throws OXException {
        return new GuardGuestMailFetcher().fetchMessage(session, EMPTY_AUTH_TOKEN, folder, mailId, markSeen);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#getMessages(java.lang.String, java.lang.String[], com.openexchange.mail.MailField[])
     */
    @Override
    public MailMessage[] getMessages(String folder, String[] mailIds, MailField[] fields) throws OXException {
        final boolean markSeen = false;
        final MailSortField sortBy = null;
        final OrderDirection orderBy = null;
        final IndexRange nullIndexRange = null;
        return new GuardGuestMailFetcher().fetchMessages(
            session,
            EMPTY_AUTH_TOKEN,
            folder,
            mailIds,
            nullIndexRange,
            sortBy,
            orderBy,
            fields,
            markSeen);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#getThreadSortedMessages(java.lang.String, com.openexchange.mail.IndexRange, com.openexchange.mail.MailSortField, com.openexchange.mail.OrderDirection, com.openexchange.mail.search.SearchTerm, com.openexchange.mail.MailField[])
     */
    @Override
    public MailMessage[] getThreadSortedMessages(String folder, IndexRange indexRange, MailSortField sortField, OrderDirection order, SearchTerm<?> searchTerm, MailField[] fields) throws OXException {
        //Thread sorting is not supported for OX Guard Guest e-mails.
        return getAllMessages(folder, indexRange, sortField, order, fields);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#getUnreadMessages(java.lang.String, com.openexchange.mail.MailSortField, com.openexchange.mail.OrderDirection, com.openexchange.mail.MailField[], int)
     */
    @Override
    public MailMessage[] getUnreadMessages(String folder, MailSortField sortField, OrderDirection order, MailField[] fields, int limit) throws OXException {
        final IndexRange noIndexRange = null;
        return new GuardGuestMailFetcher().fetchUnreadMessages(
            session,
            EMPTY_AUTH_TOKEN,
            folder,
            GuardGuestMailFetcher.ALL_IDS,
            noIndexRange,
            sortField,
            order,
            fields,
            limit);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#moveMessages(java.lang.String, java.lang.String, java.lang.String[], boolean)
     */
    @Override
    public String[] moveMessages(String sourceFolder, String destFolder, String[] mailIds, boolean fast) throws OXException {
        new GuardGuestMailMover().moveMessages(session, EMPTY_AUTH_TOKEN, mailIds, destFolder);
        return mailIds;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#releaseResources()
     */
    @Override
    public void releaseResources() throws OXException {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#saveDraft(java.lang.String, com.openexchange.mail.dataobjects.compose.ComposedMailMessage)
     */
    @Override
    public MailMessage saveDraft(String draftFullname, ComposedMailMessage draftMail) throws OXException {
        String[] ids = appendMessages(GuardGuestFolderStorage.DRAFTS_NAME, new MailMessage[] {draftMail});
        if(ids != null && ids.length > 0) {
            MailMessage ret = (MailMessage) draftMail.clone();
            ret.setMailId(ids[0]);
            return getMessage(GuardGuestFolderStorage.DRAFTS_NAME, ids[0], false);
        }
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#searchMessages(java.lang.String, com.openexchange.mail.IndexRange, com.openexchange.mail.MailSortField, com.openexchange.mail.OrderDirection, com.openexchange.mail.search.SearchTerm, com.openexchange.mail.MailField[])
     */
    @Override
    public MailMessage[] searchMessages(String folder, IndexRange indexRange, MailSortField sortField, OrderDirection order, SearchTerm<?> searchTerm, MailField[] fields) throws OXException {
        final boolean markSeen = false;
        return new GuardGuestMailFetcher().fetchMessages(
            session,
            EMPTY_AUTH_TOKEN,
            folder,
            GuardGuestMailFetcher.ALL_IDS,
            indexRange,
            sortField,
            order,
            fields,
            markSeen);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#updateMessageColorLabel(java.lang.String, java.lang.String[], int)
     */
    @Override
    public void updateMessageColorLabel(String folder, String[] mailIds, int colorLabel) throws OXException {

        new GuardGuestMailUpdater().updateColorLabel(session, EMPTY_AUTH_TOKEN, mailIds, colorLabel);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#updateMessageUserFlags(java.lang.String, java.lang.String[], java.lang.String[], boolean)
     */
    @Override
    public void updateMessageUserFlags(String folder, String[] mailIds, String[] flags, boolean set) throws OXException {
        //no-op
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#updateMessageFlags(java.lang.String, java.lang.String[], int, boolean)
     */
    @Override
    public void updateMessageFlags(String folder, String[] mailIds, int flags, boolean set) throws OXException {
        new GuardGuestMailUpdater().updateMessageFlags(session, EMPTY_AUTH_TOKEN, mailIds, flags, set);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#updateMessageFlags(java.lang.String, java.lang.String[], int, java.lang.String[], boolean)
     */
    @Override
    public void updateMessageFlags(String folder, String[] mailIds, int flags, String[] userFlags, boolean set) throws OXException {
        updateMessageFlags(folder, mailIds, flags, set);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#getNewAndModifiedMessages(java.lang.String, com.openexchange.mail.MailField[])
     */
    @Override
    public MailMessage[] getNewAndModifiedMessages(String folder, MailField[] fields) throws OXException {
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#getDeletedMessages(java.lang.String, com.openexchange.mail.MailField[])
     */
    @Override
    public MailMessage[] getDeletedMessages(String folder, MailField[] fields) throws OXException {
        return EMPTY_RETVAL;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#getUnreadCount(java.lang.String, com.openexchange.mail.search.SearchTerm)
     */
    @Override
    public int getUnreadCount(String folder, SearchTerm<?> searchTerm) throws OXException {
        return getUnreadMessages(folder,MailSortField.SENT_DATE,OrderDirection.ASC,new MailField[] {MailField.ID},-1).length;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailStorage#supports(java.lang.Class)
     */
    @SuppressWarnings("unchecked")
    @Override
    public <T> T supports(Class<T> iface) throws OXException {
        return iface.isInstance(this) ? (T) this : null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.ISimplifiedThreadStructure#getThreadSortedMessages(java.lang.String, boolean, boolean, com.openexchange.mail.IndexRange, long, com.openexchange.mail.MailSortField, com.openexchange.mail.OrderDirection, com.openexchange.mail.MailField[], com.openexchange.mail.search.SearchTerm)
     */
    @Override
    public List<List<MailMessage>> getThreadSortedMessages(String folder, boolean includeSent, boolean cache, IndexRange indexRange, long max, MailSortField sortField, OrderDirection order, MailField[] fields, SearchTerm<?> searchTerm) throws OXException {
        return getThreadSortedList(folder, indexRange, sortField, order, fields);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.ISimplifiedThreadStructureEnhanced#getThreadSortedMessages(java.lang.String, boolean, boolean, com.openexchange.mail.IndexRange, long, com.openexchange.mail.MailSortField, com.openexchange.mail.OrderDirection, com.openexchange.mail.MailField[], java.lang.String[], com.openexchange.mail.search.SearchTerm)
     */
    @Override
    public List<List<MailMessage>> getThreadSortedMessages(String folder, boolean includeSent, boolean cache, IndexRange indexRange, long max, MailSortField sortField, OrderDirection order, MailField[] fields, String[] headerNames, SearchTerm<?> searchTerm) throws OXException {
        return getThreadSortedList(folder, indexRange, sortField, order, fields);
    }
}
