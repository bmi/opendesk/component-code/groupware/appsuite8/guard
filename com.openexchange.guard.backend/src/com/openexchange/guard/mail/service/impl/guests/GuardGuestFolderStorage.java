/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl.guests;

import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.authentication.AuthenticationTokenHandler;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.mail.Quota;
import com.openexchange.mail.Quota.Type;
import com.openexchange.mail.dataobjects.MailFolder;
import com.openexchange.mail.dataobjects.MailFolder.DefaultFolderType;
import com.openexchange.mail.dataobjects.MailFolderDescription;
import com.openexchange.mail.mime.MimeMailExceptionCode;
import com.openexchange.mail.permission.DefaultMailPermission;
import com.openexchange.mail.permission.MailPermission;
import com.openexchange.session.Session;

/**
 * {@link GuardGuestFolderStorage}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardGuestFolderStorage implements IGuardGuestFolderStorage {

    public final static String DEFAULT_NAME = "default";
    public final static String INBOX_NAME = "INBOX";
    public final static String TRASH_NAME = "TRASH";
    public final static String SENT_NAME = "SENT";
    public final static String DRAFTS_NAME = "DRAFTS";

    private final static String EMPTY_FOLDER_NAME = null;
    private MailFolder rootFolder;
    private final Session session;
    private final GuardAuthenticationToken authToken;

    /**
     * Initializes a new {@link GuardGuestFolderStorage}.
     *
     * @param session A session object from where the authentication token is obtained
     */
    public GuardGuestFolderStorage(Session session) {
        this(session, null);
    }

    /**
     * Initializes a new {@link GuardGuestFolderStorage}.
     *
     * @param session The user's session
     * @param authToken The authToken to use for authentication
     */
    public GuardGuestFolderStorage(Session session, GuardAuthenticationToken authToken) {
        this.session = session;
        this.authToken = authToken;
    }

    /**
     * Internal method to obtain the authentication token for accessing OX Guard
     *
     * @return The {@link GuardAuthenticationToken}
     * @throws OXException
     */
    private GuardAuthenticationToken getAuthToken() throws OXException {
        return this.authToken != null ?
            this.authToken :
            new AuthenticationTokenHandler().getForSession(this.session, CryptoType.PROTOCOL.PGP.getValue());
    }


    private MailPermission createPermission() {
       DefaultMailPermission defaultMailPermission = new DefaultMailPermission();
       defaultMailPermission.setEntity(this.session.getUserId());
       return defaultMailPermission;
    }

    private MailFolder createDefaultFolderInternal(String name, DefaultFolderType type) {
        MailFolder ret = new MailFolder();
        ret.setDefaultFolder(true);
        ret.setDefaultFolderType(type);
        ret.setDeletedMessageCount(0); //TODO
        ret.setExists(true);
        ret.setHoldsMessages(true);
        ret.setFullname(name);
        ret.setLiveAccess(true);
        ret.setMessageCount(0); //TODO
        ret.setName(name);
        ret.setNewMessageCount(0);
        ret.setOwner(null);
        ret.setOwnPermission(createPermission()); //TODO?
        ret.addPermission(createPermission());
        ret.setUnreadMessageCount(0);
        ret.setSeparator('/');
        return ret;
    }

    private MailFolder toRootFolder(MailFolder mailFolder) {
       mailFolder.setFullname(MailFolder.ROOT_FOLDER_ID);
       mailFolder.setName(MailFolder.ROOT_FOLDER_NAME);
       mailFolder.setRootFolder(true);
       mailFolder.setParentFullname(null);
       return mailFolder;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#exists(java.lang.String)
     */
    @Override
    public boolean exists(String fullName) throws OXException {
        // TODO Auto-generated method stub
        return false;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getFolder(java.lang.String)
     */
    @Override
    public MailFolder getFolder(String fullName) throws OXException {
        if(INBOX_NAME.equals(fullName) || DEFAULT_NAME.equals(fullName)) {
            return createDefaultFolderInternal(fullName, DefaultFolderType.INBOX);
        }
        else if(TRASH_NAME.equals(fullName)) {
            return createDefaultFolderInternal(fullName, DefaultFolderType.TRASH);
        }
        else if(SENT_NAME.equals(fullName)) {
            return createDefaultFolderInternal(fullName, DefaultFolderType.SENT);
        }
        else if(DRAFTS_NAME.equals(fullName)) {
            return createDefaultFolderInternal(fullName, DefaultFolderType.DRAFTS);
        }
        throw MimeMailExceptionCode.FOLDER_NOT_FOUND.create(fullName);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getSubfolders(java.lang.String, boolean)
     */
    @Override
    public MailFolder[] getSubfolders(String parentFullName, boolean all) throws OXException {
        return EMPTY_PATH;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getRootFolder()
     */
    @Override
    public MailFolder getRootFolder() throws OXException {
        if(rootFolder == null) {
            rootFolder = toRootFolder(createDefaultFolderInternal(null, DefaultFolderType.INBOX));
        }
        return rootFolder;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getDefaultFolderPrefix()
     */
    @Override
    public String getDefaultFolderPrefix() throws OXException {
        //TODO?
        return "";
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#checkDefaultFolders()
     */
    @Override
    public void checkDefaultFolders() throws OXException {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#createFolder(com.openexchange.mail.dataobjects.MailFolderDescription)
     */
    @Override
    public String createFolder(MailFolderDescription toCreate) throws OXException {
        throw GuardExceptionCodes.ACTION_NOT_SUPPORTED.create("createFolder");
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#updateFolder(java.lang.String, com.openexchange.mail.dataobjects.MailFolderDescription)
     */
    @Override
    public String updateFolder(String fullName, MailFolderDescription toUpdate) throws OXException {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#moveFolder(java.lang.String, java.lang.String)
     */
    @Override
    public String moveFolder(String fullName, String newFullName) throws OXException {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#renameFolder(java.lang.String, java.lang.String)
     */
    @Override
    public String renameFolder(String fullName, String newName) throws OXException {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#deleteFolder(java.lang.String)
     */
    @Override
    public String deleteFolder(String fullName) throws OXException {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#deleteFolder(java.lang.String, boolean)
     */
    @Override
    public String deleteFolder(String fullName, boolean hardDelete) throws OXException {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#clearFolder(java.lang.String)
     */
    @Override
    public void clearFolder(String fullName) throws OXException {
        clearFolder(fullName, false);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#clearFolder(java.lang.String, boolean)
     */
    @Override
    public void clearFolder(String fullName, boolean hardDelete) throws OXException {
        if(hardDelete) {
            new GuardGuestMailDeleter().deleteAllMessages(session, authToken, fullName);
        }
        else {
            new GuardGuestMailMover().moveAllMessages(session, getAuthToken(), fullName, TRASH_NAME);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getPath2DefaultFolder(java.lang.String)
     */
    @Override
    public MailFolder[] getPath2DefaultFolder(String fullName) throws OXException {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getStorageQuota(java.lang.String)
     */
    @Override
    public Quota getStorageQuota(String fullName) throws OXException {
        return getQuotas(fullName, new Type[] {Type.STORAGE})[0];
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getMessageQuota(java.lang.String)
     */
    @Override
    public Quota getMessageQuota(String fullName) throws OXException {
        return getQuotas(fullName, new Type[] {Type.MESSAGE})[0];
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getQuotas(java.lang.String, com.openexchange.mail.Quota.Type[])
     */
    @Override
    public Quota[] getQuotas(String fullName, Type[] types) throws OXException {
        return Quota.getUnlimitedQuotas(types);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getConfirmedHamFolder()
     */
    @Override
    public String getConfirmedHamFolder() throws OXException {
        return EMPTY_FOLDER_NAME;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getConfirmedSpamFolder()
     */
    @Override
    public String getConfirmedSpamFolder() throws OXException {
        return EMPTY_FOLDER_NAME;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getDraftsFolder()
     */
    @Override
    public String getDraftsFolder() throws OXException {
        return DRAFTS_NAME;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getSpamFolder()
     */
    @Override
    public String getSpamFolder() throws OXException {
        return EMPTY_FOLDER_NAME;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getSentFolder()
     */
    @Override
    public String getSentFolder() throws OXException {
        return SENT_NAME;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getTrashFolder()
     */
    @Override
    public String getTrashFolder() throws OXException {
        return TRASH_NAME;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#releaseResources()
     */
    @Override
    public void releaseResources() throws OXException {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailStorage#supports(java.lang.Class)
     */
    @Override
    public <T> T supports(Class<T> iface) throws OXException {
        // TODO Auto-generated method stub
        return null;
    }

}
