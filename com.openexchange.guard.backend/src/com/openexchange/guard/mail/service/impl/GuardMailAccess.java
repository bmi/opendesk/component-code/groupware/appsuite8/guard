/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl;

import com.openexchange.capabilities.CapabilityService;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.mail.api.DelegatingMailAccess;
import com.openexchange.mail.api.IMailFolderStorage;
import com.openexchange.mail.api.IMailMessageStorage;
import com.openexchange.mail.api.MailAccess;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;

/**
 * {@link GuardMailAccess}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.4
 */
public class GuardMailAccess extends DelegatingMailAccess {

    private static final long serialVersionUID = 3968669814403573518L;

    private transient IMailMessageStorage messageStorage;
    private transient final GuardAuthenticationToken authToken;
    private transient final ServiceLookup services;
    private transient CryptoType.PROTOCOL type;

    /**
     * Initializes a new {@link GuardMailAccess}.
     *
     * @param delegate The {@link MailAccess} to decorate
     * @param session The session
     * @param pgpMailRecognizer A strategy for recognizing PGP mails.
     * @param authToken The authentication token used for decrypting emails.
     * @param type Type of crypto
     */
    public GuardMailAccess(MailAccess<IMailFolderStorage, IMailMessageStorage> delegate, Session session, ServiceLookup services, GuardAuthenticationToken authToken, CryptoType.PROTOCOL type) {
        super(delegate, session);
        this.services = services;
        this.authToken = authToken;
        this.type = type;
    }

    /**
     * Initializes a new {@link GuardMailAccess}.
     *
     * @param delegate The {@link MailAccess} to decorate.
     * @param session The session
     * @param accountId The account ID
     * @param pgpMailRecognizer A strategy for recognizing PGP mail.
     * @param authToken
     * @param type Type of crypto
     */
    public GuardMailAccess(MailAccess<IMailFolderStorage, IMailMessageStorage> delegate, Session session, int accountId, ServiceLookup services, GuardAuthenticationToken authToken, CryptoType.PROTOCOL type) {
        super(delegate, session, accountId);
        this.services = services;
        this.authToken = authToken;
        this.type = type;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.mail.api.DelegatingMailAccess#getMessageStorage()
     */
    @Override
    public IMailMessageStorage getMessageStorage() throws OXException {

        CapabilityService capabilityService = services.getServiceSafe(CapabilityService.class);
        if (capabilityService.getCapabilities(session).contains("guard") || capabilityService.getCapabilities(session).contains("smime")) {
            if (messageStorage == null) {
                messageStorage = new GuardMessageStorage(session, super.getMessageStorage(), services, authToken, type);
            }
            return messageStorage;
        }
        return super.getMessageStorage();

    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.mail.api.DelegatingMailAccess#isCacheable()
     */
    @Override
    public boolean isCacheable() {
        //MailAccess instances are cached by user and not by session;
        //Non caching prevents a request to obtain a cached MailAccess for the same user but with another session (containing a guard auth-token).
        return false;
    }
}
