/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.internal.authentication;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.Lock;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.crypto.CryptoType.PROTOCOL;
import com.openexchange.exception.OXException;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.AbstractGuardAccess;
import com.openexchange.guard.internal.GuardApiImpl;
import com.openexchange.guard.osgi.Services;
import com.openexchange.java.Strings;
import com.openexchange.session.Session;
import com.openexchange.sessiond.SessiondService;

/**
 * {@link AuthenticationTokenHandler} handles the Guard authentication token
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.2
 */
public class AuthenticationTokenHandler extends AbstractGuardAccess {

    private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(AuthenticationTokenHandler.class);
    private static final String AUTH_TOKEN_PARAMETER = "GUARD_AUTH_TOKEN";

    /**
     * Internal method to destroy the authentication token on the remote Guard service.
     *
     * @param authToken The authentication token to destroy
     * @throws OXException
     * @throws JSONException
     */
    private void destroyAuthToken(GuardAuthenticationToken authToken) throws OXException {

        GuardApiImpl guardApi = getGuardApi(PGPCORE_ENDPOINT);
        if (guardApi != null) {
            JSONObject json = new JSONObject();
            json.putSafe("session", authToken.getGuardSessionId());
            guardApi.doCallPut(GuardApis.mapFor("action", "deleteauthtoken"), json, Void.class, null);
        } else {
            LOGGER.error("Endpoint not available " + PGPCORE_ENDPOINT);
        }
    }

    /**
     * Gets the authentication token for a given session
     *
     * @param session The session to get the authentication token for
     * @param markAsUsed Whether to mark the token as used or not
     * @param type The crypto type
     * @return An authentication token for the given session, or null if no authentication token was attached to the session
     * @throws OXException In case of error
     */
    public GuardAuthenticationToken getForSession(Session session, boolean markAsUsed, String type) throws OXException {
        if (Strings.isEmpty(type)) {
            type = "PGP";
        }
        final Lock lock = (Lock) session.getParameter(Session.PARAM_LOCK);
        lock.lock();
        try {
            SessionAuthToken tokens = new SessionAuthToken(session.getParameter(AUTH_TOKEN_PARAMETER));
            String token = tokens.getAuthToken(type);
            if (token != null) {
                GuardAuthenticationToken authToken = GuardAuthenticationToken.fromString(token);
                if (markAsUsed && authToken != null && authToken.getMinutesValid() < 0) {
                    authToken.setUsed();
                    setForSession(session, authToken, type);
                }
                return authToken;
            }
            return null;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Get all authentication tokens for session
     *
     * @param session The session to get the authentication token for
     * @return List of authTokens
     */
    public List<GuardAuthenticationToken> getAllForSession(Session session) {
        final Lock lock = (Lock) session.getParameter(Session.PARAM_LOCK);
        lock.lock();
        ArrayList<GuardAuthenticationToken> tokens = new ArrayList<GuardAuthenticationToken>();
        try {
            SessionAuthToken sessionTokens = new SessionAuthToken(session.getParameter(AUTH_TOKEN_PARAMETER));
            for (String token : sessionTokens.getAllTokens()) {
                if (token != null) {
                    GuardAuthenticationToken authToken = GuardAuthenticationToken.fromString(token);
                    tokens.add(authToken);
                }
            }
            return tokens;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Gets the authentication token for a given session and mark the token as "used"
     *
     * @param session The session to get he authentication token for
     * @return The authentication token for the given session, or null if no authentication token was attached to the session
     * @param type The crypto type
     * @throws OXException In case of error
     */
    public GuardAuthenticationToken getForSession(Session session, String type) throws OXException {
        return getForSession(session, true, type);
    }

    /**
     * Gets the authentication token for a given session and mark the token as "used"
     *
     * @param session The session to get the authentication token for
     * @return The authentication token for the given session
     * @param type The crypto type
     * @throws OXException If no such token was found for the given session
     */
    public GuardAuthenticationToken requireForSession(Session session, String type) throws OXException {
        GuardAuthenticationToken authToken = getForSession(session, true, type);
        if (authToken != null) {
            return authToken;
        }
        throw GuardExceptionCodes.MISSING_AUTH.create();
    }

    /**
     * Attaches the given authentication token for the associated session
     *
     * @param session The session
     * @param authToken The authentication token
     * @param type The crypto type
     * @throws OXException In case of error
     */
    public void setForSession(Session session, GuardAuthenticationToken authToken, String type) throws OXException {
        final Lock lock = (Lock) session.getParameter(Session.PARAM_LOCK);
        lock.lock();
        try {
            SessionAuthToken token = new SessionAuthToken(session.getParameter(AUTH_TOKEN_PARAMETER));
            token.replaceAuthToken(authToken.toString(), type);
            session.setParameter(AUTH_TOKEN_PARAMETER, token.toString());
        } finally {
            lock.unlock();
        }
        if (authToken.getMinutesValid() >= 0) {  // don't push for single use
            SessiondService sessionService = Services.getService(SessiondService.class);
            sessionService.storeSession(session.getSessionID(), false);
        }
    }

    /**
     * Internal method to remove and destroy an authentication token.
     *
     * @param session The session
     * @param destroy true, in order to invalidate the authentication on the Guard Server, false to just detach it from the session.
     * @param logout If system logout. If session being removed the hazelcast won't be notified
     * @throws OXException
     */
    private void removeForSessionInternal(Session session, boolean destroy, boolean logout, String type) throws OXException {
        List<GuardAuthenticationToken> authTokens = null;
        final Lock lock = (Lock) session.getParameter(Session.PARAM_LOCK);
        lock.lock();
        try {
            // None specified, get all
            if (PROTOCOL.NONE.getValue().equalsIgnoreCase(type) || Strings.isEmpty(type)) {
                authTokens = getAllForSession(session);
            } else {
                authTokens = Collections.singletonList(getForSession(session, false, type));
            }
            SessionAuthToken tokens = new SessionAuthToken(session.getParameter(AUTH_TOKEN_PARAMETER));
            tokens.removeAuthToken(type);
            session.setParameter(AUTH_TOKEN_PARAMETER, tokens.toString());
        } finally {
            lock.unlock();
        }

        // If destroy, remove each
        if (destroy && authTokens != null && authTokens.size() > 0) {
            for (GuardAuthenticationToken token : authTokens) {
                if (token != null)
                    destroyAuthToken(token);
            }
        }

        if (!logout) {
            if (authTokens != null && authTokens.size() > 0 && authTokens.get(0) != null && authTokens.get(0).getMinutesValid() >= 0) {  // don't push for single use
                SessiondService sessionService = Services.getService(SessiondService.class);
                sessionService.storeSession(session.getSessionID(), false);
            }
        }
    }

    /**
     * Removes an authentication token for the given session.
     * <p>
     * This will NOT destroy / invalidate an authentication token on the Guard server.
     *
     * @param session The session
     * @param type The crypto type
     * @throws OXException In case of error
     */
    public void removeForSession(Session session, String type) throws OXException {
        final boolean destroy = false;
        final boolean logout = false;
        removeForSessionInternal(session, destroy, logout, type);
    }

    /**
     * Removes an authentication token for the given session.
     * <p>
     * This will also destroy i.e. invalidate an authentication token on the Guard server as well.
     *
     *
     * @param session The session
     * @param type The crypto type
     * @throws OXException In case of error
     */
    public void destroyForSession(Session session, boolean logout, String type) throws OXException {
        final boolean destroy = true;
        removeForSessionInternal(session, destroy, logout, type);
    }
}
