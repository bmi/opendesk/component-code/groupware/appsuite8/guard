/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.osgi;

import java.util.Dictionary;
import java.util.Hashtable;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import com.openexchange.admin.plugins.OXContextPluginInterface;
import com.openexchange.ajax.requesthandler.crypto.CryptographicServiceAuthenticationFactory;
import com.openexchange.capabilities.CapabilityChecker;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.capabilities.FailureAwareCapabilityChecker;
import com.openexchange.config.ConfigurationService;
import com.openexchange.context.ContextService;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.groupware.alias.UserAliasStorage;
import com.openexchange.guard.api.GuardApi;
import com.openexchange.guard.guest.MailGuestService;
import com.openexchange.guard.guest.OxMailAccountCreatorService;
import com.openexchange.guard.guest.impl.MailGuestServiceImpl;
import com.openexchange.guard.guest.impl.OxMailAccountCreatorImpl;
import com.openexchange.guard.guest.rest.MailGuestRESTService;
import com.openexchange.guard.guest.rest.MailGuestRESTServiceImpl;
import com.openexchange.guard.interceptor.GuardAwareLoginHandler;
import com.openexchange.guard.interceptor.GuardAwareSessionInspector;
import com.openexchange.guard.interceptor.GuardProvisioningContextPlugin;
import com.openexchange.guard.interceptor.GuardUserServiceInterceptor;
import com.openexchange.guard.internal.AbstractGuardAccess;
import com.openexchange.guard.internal.GuardApiImpl;
import com.openexchange.guard.json.authentication.GuardAuthenticationFactory;
import com.openexchange.guard.mail.autocrypt.BackendAutoCryptService;
import com.openexchange.guard.mail.autocrypt.BackendAutoCryptServiceImpl;
import com.openexchange.guard.session.GuestSessionStorage;
import com.openexchange.guard.session.SessionEventHandler;
import com.openexchange.guard.session.TokenSessionStorage;
import com.openexchange.guard.transport.listener.GuardTransportListener;
import com.openexchange.java.Strings;
import com.openexchange.login.internal.LoginHandlerRegistry;
import com.openexchange.mail.transport.listener.MailTransportListener;
import com.openexchange.mailaccount.MailAccountStorageService;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.rest.client.httpclient.HttpClientService;
import com.openexchange.rest.client.httpclient.SpecificHttpClientConfigProvider;
import com.openexchange.serverconfig.ServerConfigService;
import com.openexchange.session.Session;
import com.openexchange.session.inspector.SessionInspectorService;
import com.openexchange.sessiond.SessiondService;
import com.openexchange.sessiond.event.SessiondEventHandler;
import com.openexchange.sessionstorage.SessionStorageParameterNamesProvider;
import com.openexchange.share.ShareService;
import com.openexchange.user.UserService;
import com.openexchange.user.interceptor.UserServiceInterceptor;
import com.openexchange.userconf.UserPermissionService;
import com.openexchange.version.VersionService;

/**
 * {@link GuardActivator}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach></a>
 * @since v7.8.0
 */
public class GuardActivator extends HousekeepingActivator {

    private static Logger logger = org.slf4j.LoggerFactory.getLogger(GuardActivator.class);
    private GuardAwareLoginHandler guardAuthTokenCleanupHandler;
    private GuardAwareSessionInspector sessionInspector;
    private CryptographicServiceAuthenticationFactory encryptionServiceAuthenticationFactory;

    private volatile ServiceRegistration<EventHandler> eventHandlerRegistration;

    /**
     * Initializes a new {@link GuardActivator}.
     */
    public GuardActivator() {
        super();
    }

    /**
     * Creates a Guard API endpoint based on passed parameters
     *
     * @param baseEndPoint  The base URL of the endpoint
     * @param endPointName Endpoint name
     * @return new API
     * @throws OXException
     */
    private GuardApi createApiEndpoint(String baseEndPoint, String endPointName) throws OXException {
        // Initialize Guard API instance
        GuardApiImpl guardApiImpl;
        {
            if (baseEndPoint.endsWith("guardadmin/")) {
                //Remove "guardadmin" in order to be backwards compatible to older (< 2.6) configurations
                baseEndPoint = baseEndPoint.replaceAll("guardadmin/$" /* replace at the end */, "");
            }
            String fullEndPoint = baseEndPoint + endPointName;
            guardApiImpl = new GuardApiImpl(fullEndPoint, getService(HttpClientService.class), getServiceSafe(ConfigurationService.class));
            AbstractGuardAccess.addGuardApi(endPointName, guardApiImpl);
        }
        return guardApiImpl;
    }

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { ConfigurationService.class, UserService.class, CapabilityService.class,
            IDBasedFileAccessFactory.class, ShareService.class, MailAccountStorageService.class, SessiondService.class,
            UserAliasStorage.class, ServerConfigService.class, ContextService.class, UserPermissionService.class, VersionService.class,
            HttpClientService.class};
    }

    @Override
    protected boolean stopOnServiceUnavailability() {
        return true;
    }

    @Override
    protected void startBundle() throws Exception {

        try {

            Services.setServiceLookup(this);
            GuardApi pgpCore;

            //Register Guard Endpoints
            {
                ConfigurationService service = getService(ConfigurationService.class);
                String propName = "com.openexchange.guard.endpoint";
                String baseEndPoint = service.getProperty(propName);
                if (Strings.isEmpty(baseEndPoint)) {
                    // No end-point
                    logger.warn("No Guard end-point defined via property {}", propName);
                    return;
                }
                //Create HTTP client configuration
                registerService(SpecificHttpClientConfigProvider.class, new GuardHttpClientConnetionConfiguration(getService(VersionService.class), service));

                // Ensure ending slash '/' character
                baseEndPoint = baseEndPoint.trim();
                if (!baseEndPoint.endsWith("/")) {
                    baseEndPoint = new StringBuilder(baseEndPoint).append('/').toString();
                }
                logger.info("Starting bundle {} using end point \"{}\"", context.getBundle().getSymbolicName(), baseEndPoint);

                //Create API endpoints
                GuardApi guardAdminApi = createApiEndpoint(baseEndPoint, AbstractGuardAccess.GUARDADMIN_ENDPOINT);
                createApiEndpoint(baseEndPoint, AbstractGuardAccess.LOGIN_ENDPOINT);
                createApiEndpoint(baseEndPoint, AbstractGuardAccess.CRYPTO_ENDPOINT);
                pgpCore = createApiEndpoint(baseEndPoint, AbstractGuardAccess.PGPCORE_ENDPOINT);
                createApiEndpoint(baseEndPoint, AbstractGuardAccess.GUEST_ENDPOINT);

                registerService(GuardApi.class, guardAdminApi);

                final SessiondEventHandler eventHandler = new SessiondEventHandler();
                eventHandler.addListener(new SessionEventHandler());
                eventHandlerRegistration = eventHandler.registerSessiondEventHandler(context);

            }

            // Register plugin interfaces
            {
                final Dictionary<String, String> props = new Hashtable<>(2);
                props.put("name", "OXContext");
                registerService(OXContextPluginInterface.class, new GuardProvisioningContextPlugin());
            }

            // Register interceptors
            registerService(UserServiceInterceptor.class, new GuardUserServiceInterceptor(this));
            sessionInspector = new GuardAwareSessionInspector();
            registerService(SessionInspectorService.class, sessionInspector);
            guardAuthTokenCleanupHandler = new GuardAwareLoginHandler();
            LoginHandlerRegistry.getInstance().addLoginHandler(guardAuthTokenCleanupHandler);

            registerService(SessionStorageParameterNamesProvider.class, new TokenSessionStorage());
            registerService(SessionStorageParameterNamesProvider.class, new GuestSessionStorage());

            // Register transport listener
            if (pgpCore != null) {
                registerService(BackendAutoCryptService.class, new BackendAutoCryptServiceImpl(this, pgpCore));
                trackService(BackendAutoCryptService.class);
            }

            Dictionary<String, Object> properties = new Hashtable<>(2);
            properties.put(Constants.SERVICE_RANKING, Integer.valueOf(100));
            registerService(MailTransportListener.class, new GuardTransportListener(this), properties);

            //Register Authentication factory
            encryptionServiceAuthenticationFactory = new GuardAuthenticationFactory(this);
            registerService(CryptographicServiceAuthenticationFactory.class, encryptionServiceAuthenticationFactory);

            // Announce Guard back-end as ready
            final String sCapability = "guard_backend_plugin";
            properties = new Hashtable<String, Object>(1);
            properties.put(CapabilityChecker.PROPERTY_CAPABILITIES, sCapability);
            registerService(CapabilityChecker.class, new FailureAwareCapabilityChecker() {

                @Override
                public FailureAwareCapabilityChecker.Result checkEnabled(String capability, Session session) throws OXException {
                    if (sCapability.equals(capability)) {
                        // Maybe check for certain conditions when to advertise "guard_backend_plugin" capability
                    }

                    return FailureAwareCapabilityChecker.Result.ENABLED;
                }
            }, properties);

            OxMailAccountCreatorService accountService = new OxMailAccountCreatorImpl(this);
            registerService(OxMailAccountCreatorService.class, accountService);
            trackService(OxMailAccountCreatorService.class);

            final MailGuestService mailGuestService = new MailGuestServiceImpl(this);
            registerService (MailGuestService.class, mailGuestService);
            trackService(MailGuestService.class);

            getService(CapabilityService.class).declareCapability(sCapability);

            registerService(MailGuestRESTService.class, new MailGuestRESTServiceImpl(this));

            openTrackers();

        } catch (Exception e) {
            logger.error("Failed starting bundle {}", context.getBundle().getSymbolicName(), e);
            throw e;
        }
    }

    @Override
    protected void stopBundle() throws Exception {
        if (guardAuthTokenCleanupHandler != null) {
            LoginHandlerRegistry.getInstance().removeLoginHandler(guardAuthTokenCleanupHandler);
        }
        if (sessionInspector != null) {
            unregisterService(sessionInspector);
        }

        final ServiceRegistration<EventHandler> eventHandlerRegistration = this.eventHandlerRegistration;
        if (null != eventHandlerRegistration) {
            eventHandlerRegistration.unregister();
            this.eventHandlerRegistration = null;
        }

        unregisterService(SessionInspectorService.class);
        unregisterService(GuardHttpClientConnetionConfiguration.class);
        unregisterService(encryptionServiceAuthenticationFactory);
        Services.setServiceLookup(null);
        super.stopBundle();
    }

}
