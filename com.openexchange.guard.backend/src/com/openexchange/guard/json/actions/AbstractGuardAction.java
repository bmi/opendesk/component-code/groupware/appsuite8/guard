/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.json.actions;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.requesthandler.AJAXActionService;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.exception.OXException;
import com.openexchange.guard.internal.AbstractGuardAccess;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.servlet.AjaxExceptionCodes;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link AbstractGuardAction}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 */
public abstract class AbstractGuardAction extends AbstractGuardAccess implements AJAXActionService {

    /** The OSGi service look-up */
    protected final ServiceLookup services;

    /**
     * Initializes a new {@link AbstractDriveShareAction}.
     *
     * @param services The OSGi service look-up
     */
    protected AbstractGuardAction(ServiceLookup services) {
        super();
        this.services = services;
    }

    @Override
    public AJAXRequestResult perform(AJAXRequestData requestData, ServerSession session) throws OXException {
        /*
         * Check module permissions
         */
        CapabilityService capabilityService = services.getService(CapabilityService.class);
        if (false == (capabilityService.getCapabilities(session).contains("guard") || capabilityService.getCapabilities(session).contains("smime"))) {
            throw AjaxExceptionCodes.NO_PERMISSION_FOR_MODULE.create("guard");
        }

        try {
            return doPerform(requestData, session);
        } catch (JSONException e) {
            throw AjaxExceptionCodes.JSON_ERROR.create(e, e.getMessage());
        }
    }


    /**
     * Extracts the raw JSON body from the given {@link AJAXRequestData} object.
     *
     * @param requestData The object to extract the raw JSON body from.
     * @return The raws JSON body extracted from the given request object.
     * @throws JSONException
     * @throws OXException
     */
    protected JSONObject parseStreamToJson(AJAXRequestData requestData) throws JSONException, OXException {
        if (!requestData.hasUploadStreamProvider()) {
            return null;
        }
        try {
            InputStream in = requestData.getUploadStream();
            byte[] data = IOUtils.toByteArray(in);
            String jsonString = new String(data, StandardCharsets.UTF_8);
            JSONObject json = new JSONObject(jsonString);
            requestData.setData(json);
            return json;
        } catch (IOException e) {
            throw AjaxExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Performs the given request.
     *
     * @param requestData The request data to process
     * @param session The associated session
     * @return The result
     * @throws OXException If an Open-Xchange error occurs
     * @throws JSONException If a JSON error occurs
     */
    protected abstract AJAXRequestResult doPerform(AJAXRequestData requestData, ServerSession session) throws OXException, JSONException;

}
