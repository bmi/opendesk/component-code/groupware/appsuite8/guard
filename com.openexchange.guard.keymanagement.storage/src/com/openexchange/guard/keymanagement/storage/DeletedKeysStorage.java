/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.storage;

import java.util.ArrayList;
import java.util.Date;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.DeletedKey;

/**
 * {@link DeletedKeysStorage} provides access to backed up deleted keys
 */
public interface DeletedKeysStorage {

    /**
     * Inserts keys into the storage
     *
     * @param deletedKeys The keys to insert
     * @throws OXException
     */
    public void insert(DeletedKey... deletedKeys) throws OXException;

    /**
     * Exposes every deleted key for a user for downloading by setting the exposed
     *
     * @param email The email of the keys to expose
     * @param cid Context id
     * @throws OXException
     */
    public void setExposedForEmail(String email, int cid) throws OXException;

    /**
     * Gets the first deleted key for a user
     *
     * @param email the email of the user to the get the deleted key's salt
     * @param cid the context id
     * @return the salt of the first found deleted key of the given user
     * @throws OXException
     */
    public DeletedKey getFirstForEmail(String email, int cid) throws OXException;

    /**
     * Removes the exposed flag from all deleted keys which have been exposed before the given date
     *
     * @param before the date to
     * @return the number of keys which have been reset to "unexposed"
     * @throws OXException
     */
    public int setAllUnexposed(Date before) throws OXException;

    /**
     * Gets a list of all deleted keys for the user, most recent first
     *
     * @param email
     * @param id
     * @param cid
     * @return
     * @throws OXException
     */
    public ArrayList<DeletedKey> getAllExposedForEmail(String email, int id, int cid) throws OXException;
}
