
package com.openexchange.guard.encryption;

import com.openexchange.exception.OXException;

/**
 * {@link EncryptedItemsStorage} Provides access to stored EncryptedItem elements
 */
public interface EncryptedItemsStorage {

    /**
     * Inserts a new item
     *
     * @param item the new item
     * @param recipientId the recipient's guest/user id
     * @param recipientContextId the recipient's context id
     * @throws OXException due an exception
     */
    void insertForRecipient(EncryptedItem item, int recipientId, int recipientContextId) throws OXException;

    /**
     * Store an item into an item table. If the XML length surpasses a certain length, it is being truncated
     *
     * @param itemId The item's identifier
     * @param recipientId The recipient's identifier
     * @param recipientCid The recipient's context identifier
     * @param owner The owner's identifier
     * @param ownerCid The owner's context identifier
     * @param expirationDate The expiration date of the item
     * @param type The type of the item
     * @param xml The XML
     * @param salt The salt
     * @throws OXException
     */
    void insertForRecipientTruncated(String itemId, int recipientId, int recipientCid, int owner, int ownerCid, long expirationDate, int type, String xml, String salt) throws OXException;

    /**
     * Gets an item by ID
     *
     * @param id the ID of the item
     * @param ownerId the owner's user ID
     * @param ownerContextId the owner's context id
     * @return the item with the given ID
     * @throws OXException due an exception
     */
    EncryptedItem getById(String id, int ownerId, int ownerContextId) throws OXException;



    /**
     * Get XML data from an item. Returns null if not found. User must be owner
     *
     * @param userid The user identifier
     * @param cid The context identifier
     * @param itemid The item identifier
     * @return The XML from the item
     * @throws OXException
     */
    String getXML(int userid, int cid, String itemid) throws OXException;
}
