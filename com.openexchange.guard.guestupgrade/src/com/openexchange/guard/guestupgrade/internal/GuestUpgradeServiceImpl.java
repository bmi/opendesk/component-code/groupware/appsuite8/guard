
package com.openexchange.guard.guestupgrade.internal;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.database.GuardShardingService;
import com.openexchange.guard.database.utils.GuardConnectionHelper;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.guestupgrade.GuestUpgradeService;
import com.openexchange.guard.guestupgrade.exceptions.GuardGuestUpgraderExceptionCodes;
import com.openexchange.guard.guestupgrade.osgi.Services;
import com.openexchange.guard.guestupgrade.storage.GuestUpgradeStorageService;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.guard.keymanagement.storage.PGPKeysStorage;
import com.openexchange.guard.oxapi.MailResolver;
import com.openexchange.guard.oxapi.guestUser.UserConfirm;
import com.openexchange.guard.user.OXUserService;

/**
 * {@link GuestUpgradeServiceImpl}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class GuestUpgradeServiceImpl implements GuestUpgradeService {

    private static final Logger logger = LoggerFactory.getLogger(GuestUpgradeServiceImpl.class);

    @Override
    public void upgrade(String email, String userid, String cid) throws OXException {

        //Getting key by email
        GuardKeyService keyService = Services.getService(GuardKeyService.class);
        GuardKeys keys = keyService.getKeys(email);
        if (keys == null) {
            logger.error("Unable to find guest account " + email);
            throw GuardGuestUpgraderExceptionCodes.UNKNOWN_GUEST_ACCOUNT.create(email);
        }

        int oxUserId = 0;
        int oxUserCid = 0;
        if (userid != null && cid != null) {
            oxUserId = Integer.parseInt(userid);
            oxUserCid = Integer.parseInt(cid);
        }

        if (oxUserId == 0) {
            MailResolver mailResolver = new MailResolver();
            JsonObject userdat = mailResolver.resolveUser(email);
            if (userdat != null && userdat.has(email)) {
                JsonObject user = userdat.get(email).getAsJsonObject();
                oxUserId = user.get("uid").getAsInt();
                oxUserCid = user.get("cid").getAsInt();
            } else {
                logger.error("No OX account found for " + email);
                throw GuardGuestUpgraderExceptionCodes.UNKNOWN_OX_ACCOUNT.create(email);
            }
        }
        //Getting user information from OX backend

        OXUserService userService = Services.getService(OXUserService.class);
        // Make sure it resolves to a non-Guest account
        try {
            if (userService.isGuest(oxUserId, oxUserCid)) {
                logger.error("Unable to upgrade Guest accout.  Main resolver returns a Guest account");
                throw GuardGuestUpgraderExceptionCodes.GUEST_ACCOUNT.create(email);
            }
        } catch (OXException ex) {
            throw GuardGuestUpgraderExceptionCodes.OX_ACCOUNT_ERROR.create(oxUserId, oxUserCid);
        }
        // Validate that the new userid/cid is associated with the email
        if (new UserConfirm().validateEmail(oxUserId, oxUserCid, email)) {
            if (oxUserId != keys.getUserid() || oxUserCid != keys.getContextid()) {
                try {
                    upgrade(keys.getUserid(), keys.getContextid(), oxUserId, oxUserCid);
                    logger.info(email + " Upgraded to OX account");

                } catch (Exception e) {
                    logger.error("Error upgrading account " + email);
                    throw GuardGuestUpgraderExceptionCodes.UPGRADE_ERROR.create(e, email, e.getMessage());
                }
            }
            else {
                logger.error("Already upgraded " + email);
                throw GuardGuestUpgraderExceptionCodes.ALREADY_UPGRADED.create(email);
            }
        }
    }

    @Override
    public void upgrade(int guestUserId, int guestContextId, int userId, int contextId) throws OXException {

        try (GuardConnectionHelper connections = new GuardConnectionHelper(contextId, guestContextId, guestUserId)) {

            //Start transactions
            connections.start();

            //Getting the guest's keys
            KeyTableStorage keyTableStorage = Services.getService(KeyTableStorage.class);
            List<GuardKeys> guestKeys = keyTableStorage.getKeysForUser(guestUserId, guestContextId);

            if (!guestKeys.isEmpty()) {

                // Store guest information into upgrade table
                GuardShardingService shardService = Services.getService(GuardShardingService.class);
                int shard = shardService.getShard(guestUserId, guestContextId);
                GuestUpgradeStorageService storageService = Services.getService(GuestUpgradeStorageService.class);
                storageService.addRecord(guestUserId, guestContextId, shard, userId, contextId);

                for (GuardKeys guestKey : guestKeys) {
                    //Setting the new user id for each guest key
                    guestKey.setUserid(userId);
                    guestKey.setContextid(contextId);
                    //Adding it to the main key table
                    keyTableStorage.insert(connections.getUserConnection(), guestKey, true);

                    //Updating the email mapping
                    EmailStorage emailStorage = Services.getService(EmailStorage.class);
                    emailStorage.insertOrUpdate(connections.getGuardConncetion(), guestKey.getEmail(), guestKey.getContextid(), guestKey.getUserid(),
                        0 /* regular ox guard user */);

                    //Updating PGP mapping
                    PGPKeysStorage pgpKeysStorage = Services.getService(PGPKeysStorage.class);
                    pgpKeysStorage.updateContextIdByKeyId(connections.getGuardConncetion(), guestKey.getKeyid(), contextId);

                    connections.commit();
                }
            }
        } catch (Exception e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e.getMessage());
        }
    }
}
