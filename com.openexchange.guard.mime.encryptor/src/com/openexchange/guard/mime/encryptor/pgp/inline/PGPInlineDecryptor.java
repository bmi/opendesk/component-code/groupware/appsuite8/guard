/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.encryptor.pgp.inline;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.streams.FilteringStream;
import com.openexchange.guard.common.util.GuardMimeHeaders;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.mime.encryptor.Decryptor;
import com.openexchange.guard.mime.encryptor.exceptions.MimeEncryptorExceptionCodes;
import com.openexchange.guard.mime.encryptor.pgp.impl.HTMLStripper;
import com.openexchange.guard.pgpcore.services.FromHeaderVerifier;
import com.openexchange.guard.pgpcore.services.PGPCryptoService;
import com.openexchange.guard.pgpcore.services.SignatureVerificationResultUtil;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.core.PGPDecryptionResult;
import com.openexchange.pgp.core.SignatureVerificationResult;

/**
 * {@link PGPInlineDecryptor}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.8.0
 */
public class PGPInlineDecryptor extends Decryptor {

    private static final String BEGIN_PGP_MESSAGE_MARKER = "-----BEGIN PGP MESSAGE-----";
    private static final String END_PGP_MESSAGE_MARKER = "-----END PGP MESSAGE-----";
    private static final int STREAM_INSPECTION_SIZE = 1024;
    private final PGPCryptoService pgpCryptoService;
    private final UserIdentity userIdentity;
    private final GuardConfigurationService guardConfigService;
    private static final Logger LOG = LoggerFactory.getLogger(PGPInlineDecryptor.class);

    /**
     * Initializes a new {@link PGPInlineDecryptor}.
     *
     * @param pgpCryptoService The {@link PGPCryptoService} to use for decrypting content.
     * @param userIdentity The identity of the user to decrypt the content for.
     */
    public PGPInlineDecryptor(PGPCryptoService pgpCryptoService, GuardConfigurationService guardConfigService, UserIdentity userIdentity) {
        this.pgpCryptoService = pgpCryptoService;
        this.userIdentity = userIdentity;
        this.guardConfigService = guardConfigService;
    }

    /**
     * Internal method to check if the fist n bytes of a given stream contain a PGP Message marker
     *
     * @param inputStream The InputStream
     * @param n The amount of bytes read from the stream while looking for a PGP Message marker
     * @return true, if the first n bytes of the given inputStream contains a PGP Message marker, false otherwise.
     * @throws IOException
     */
    private boolean isPGPInlineInputStream(InputStream inputStream, int n) throws IOException {
        byte[] peekedData = new byte[n];
        int read = inputStream.read(peekedData);
        if (read > 0) {
            String peekedContent = new String(peekedData, StandardCharsets.UTF_8);
            return peekedContent.contains(BEGIN_PGP_MESSAGE_MARKER);
        }
        return false;
    }

    /**
     * Internal method to update a parts file name after decryption.
     *
     * This removes the PGP file extension from the file name.
     *
     * @param bodyPart The part to update the file name for
     * @throws MessagingException
     */
    private void updateFileName(MimeBodyPart bodyPart) throws MessagingException {
        String fileName = bodyPart.getFileName();
        if (fileName != null && fileName.toLowerCase().endsWith(".pgp")) {
            bodyPart.setFileName(fileName.replaceAll("(?i)\\.pgp$", ""));
        }
    }

    /**
     * Decrypts a MimeMessage containing only text.
     *
     * @param message The message to decrypt
     * @return True, if the message has been decrypted and updated, False if no PGP content was found.
     * @throws OXException
     * @throws IOException
     * @throws MessagingException
     */
    private boolean decryptTextMessage(MimeMessage message) throws OXException, IOException, MessagingException {
        if (isPGPInlineInputStream(message.getInputStream(), STREAM_INSPECTION_SIZE)) {
            ByteArrayOutputStream decryptedContent = new ByteArrayOutputStream();
            PGPDecryptionResult result = pgpCryptoService.decrypt(
                new FilteringStream(message.getInputStream(), new FilteringStream.PrintableFilter()),
                decryptedContent, userIdentity);
            List<SignatureVerificationResult> signatureResults = result.getSignatureVerificationResults();
            message = addSignatureResults (message, signatureResults);
            message.setContent(
                getText(decryptedContent, message.getContentType(), result.getMDCVerificationResult().isPresent()),
                message.getContentType());
            return true;
        }
        return false;
    }

    /**
     * Add the results of the signature verification to the mime message
     * 
     * @param message
     * @param signatureResults
     * @return
     * @throws MessagingException
     * @throws OXException
     */
    private MimeMessage addSignatureResults(MimeMessage message, List<SignatureVerificationResult> signatureResults) throws MessagingException, OXException {
        message.removeHeader(GuardMimeHeaders.X_GUARD_SIGNATURE_RESULT);
        //We need to compare the FROM header of the Message with the actual User ID of the signature's issuer in order ensure that the signature was really created by the sender of the email.
        FromHeaderVerifier.verify(message, signatureResults);
        String[] signatures = SignatureVerificationResultUtil.toHeaders(signatureResults);
        for (String sig : signatures) {
            message.addHeader(GuardMimeHeaders.X_GUARD_SIGNATURE_RESULT, sig);
        }
        return message;
    }

    /**
     * Decrypts a Multipart body part containing text.
     *
     * @param bodyPart The part to decrypt
     * @return List of signatureResults.  Null if no text body found
     * @throws IOException
     * @throws OXException
     * @throws MessagingException
     */
    private List<SignatureVerificationResult> decryptTextPart(MimeBodyPart bodyPart) throws IOException, OXException, MessagingException {
        List<SignatureVerificationResult> signatureResults = null;
        if (isPGPInlineInputStream(bodyPart.getInputStream(), STREAM_INSPECTION_SIZE)) {
            //Decrypt body part and update it with the decoded data
            ByteArrayOutputStream decryptedContent = new ByteArrayOutputStream();
            Object bodyContent = bodyPart.getContent();
            String data = null;
            if(bodyContent != null && bodyContent instanceof InputStream) {
                ByteArrayOutputStream contentStream = new ByteArrayOutputStream();
                IOUtils.copy(((InputStream)bodyContent), contentStream);
                data = contentStream.toString();
            }
            else {
                data = (String) bodyContent;
            }
            if (bodyPart.getContentType() != null && bodyPart.getContentType().contains("html")) {  // If HTML bodypart, strip the html formatting
                data = stripHTML (data);
            }
            data = data + "\r";
            ByteArrayInputStream in = new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8));
            PGPDecryptionResult result = pgpCryptoService.decrypt(
                new FilteringStream(in, new FilteringStream.PrintableFilter()),
                decryptedContent, userIdentity);
            signatureResults = result.getSignatureVerificationResults();

            bodyPart.setContent(
                getText(decryptedContent, bodyPart.getContentType(), result.getMDCVerificationResult().isPresent()),
                bodyPart.getContentType());

            //Update PGP File extension
            updateFileName(bodyPart);
        }
        return signatureResults;
    }

    /**
     * Simple formatting of decrypted text based on html or plaintext
     * If HTML, and no HTML formatting in body, then convert cr to <br>
     * Required as a lot of mailvelope clients simply place plaintext into alternative without formatting
     * 
     * @param decryptedContent
     * @param type contenttype of the mail body part
     * @return
     * @throws OXException
     */
    private String getText(ByteArrayOutputStream decryptedContent, String type, boolean mdcVerified) throws OXException {
        String result = new String(decryptedContent.toByteArray(), StandardCharsets.UTF_8);
        if (!mdcVerified) {
            if (guardConfigService.getBooleanProperty(GuardProperty.failForMissingMDC, userIdentity.getOXUser().getId(), userIdentity.getOXUser().getContextId())) {
                throw MimeEncryptorExceptionCodes.MISSING_MDC.create();
            }
            LOG.warn("PGP Inline message missing MDC integrity check data.  Stripping HTML");
            if (type != null && type.contains("html")) {
                result = new HTMLStripper().stripAllHTML(result);
            } else {
                result = new HTMLStripper().convertTextHTML(result);
            }
        }
        if (type != null && type.contains("html")) {  // If type HTML
            if (result.contains("<br") || result.contains("<p>")) {  // If already has HTML formatting, return contents
                return result;
            }
            // Add line breaks
            result = result.replaceAll("\n", "<br>");
        }
        return result;
    }

    /**
     * Cleanup HTML markings.  Remove non base64 characters and HTML return characters
     * @param data
     * @return
     */
    private String stripHTML (String data) {
        if (data == null) {
            return null;
        }
        data = data.substring(data.indexOf(BEGIN_PGP_MESSAGE_MARKER));
        data = data.substring(0, data.indexOf(END_PGP_MESSAGE_MARKER) + END_PGP_MESSAGE_MARKER.length());
        data = data.replaceAll("\r<br>", "\r").replaceAll("<br>\r", "\r").replaceAll("<br>", "\r");
        StringBuilder sb = new StringBuilder();
        String [] lines = data.split("\r");
        for (String line: lines) {
            sb.append(line.trim().replaceAll("[^\\x20-\\x7E]", ""));
            sb.append("\r");
        }
        sb.append("\r");
        return sb.toString().replaceAll("\\<.*?>","");
    }

    /**
     * Decrypts a Multipart body part containing binary data.
     *
     * @param bodyPart The part to decrypt
     * @return List of signature results.
     * @throws IOException
     * @throws MessagingException
     * @throws OXException
     */
    private List<SignatureVerificationResult> decryptBinaryPart(MimeBodyPart bodyPart) throws IOException, MessagingException, OXException {
        //Decrypt body part and update it with the decoded data
        ByteArrayOutputStream decryptedContent = new ByteArrayOutputStream();
        PGPDecryptionResult result = pgpCryptoService.decrypt(bodyPart.getInputStream(), decryptedContent, userIdentity);
        List<SignatureVerificationResult> signatures = result.getSignatureVerificationResults();
        bodyPart.setContent(decryptedContent.toByteArray(), bodyPart.getContentType());

        //Update PGP File extension
        updateFileName(bodyPart);

        bodyPart.setHeader("Content-Type", "application/octet-stream");
        //javax.mail does the base64 encoding for us just by setting the encoding type
        bodyPart.setHeader("Content-Transfer-Encoding", "base64");
        return signatures;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.mime.encryptor.Decryptor#doDecrypt(javax.mail.internet.MimeMessage)
     */
    @Override
    public MimeMessage doDecrypt(MimeMessage message) throws OXException {
        try {
            if (!message.getContentType().contains("multipart")) {
                //Plain message
                if (message.getContentType().contains("html")) {
                    String plaintext = stripHTML((String) message.getContent());
                    message.setText(plaintext);
                    message.saveChanges();
                }
                decryptTextMessage(message);
            } else {
                //A multipart message
                Object content = message.getContent();
                ArrayList<SignatureVerificationResult> results = new ArrayList<SignatureVerificationResult>();
                message.setContent(parseMultipart((MimeMultipart) content, results));
                message = addSignatureResults(message, results);
            }
            message.addHeader("X-PGPFormat", "inline");
            message.saveChanges();

            return message;
        } catch (MessagingException | IOException e) {
            throw MimeEncryptorExceptionCodes.PROBLEM_DECODING.create(e);
        }
    }

    private MimeMultipart parseMultipart(MimeMultipart mp, ArrayList<SignatureVerificationResult> results) throws IOException, MessagingException, OXException {
        MimeMultipart newPart = new MimeMultipart();
        if (mp.getContentType().contains("alternative")) {
            newPart.setSubType("alternative");
        }
        for (int i = 0; i < mp.getCount(); i++) {
            MimeBodyPart bodyPart = (MimeBodyPart) mp.getBodyPart(i);
            String partContentType = bodyPart.getContentType();
            if (partContentType == null) {
                continue;
            }
            if (partContentType.contains("multipart")) {
                bodyPart.setContent(parseMultipart((MimeMultipart) bodyPart.getContent(), results));
                newPart.addBodyPart(bodyPart);
            } else {
                if (partContentType.toLowerCase().contains("text")) {
                    //Seems like a text attachment
                    List<SignatureVerificationResult> textResult = decryptTextPart(bodyPart);
                    if (textResult != null) {
                        newPart.addBodyPart(bodyPart);
                        results.addAll(textResult);
                    }
                } else {
                    //Seems like a binary attachment
                    if (bodyPart.getContent() instanceof InputStream) {
                        List<SignatureVerificationResult> bodyResult = decryptBinaryPart(bodyPart);
                        if (bodyResult != null) {
                            newPart.addBodyPart(bodyPart);
                            results.addAll(bodyResult);
                        }
                    } else {
                        // unk type
                    }
                }
            }
        }
        return newPart;

    }
}
