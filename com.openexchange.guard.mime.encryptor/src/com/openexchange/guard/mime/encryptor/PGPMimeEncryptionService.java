/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.encryptor;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.autocrypt.AutoCryptService;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.mime.encryptor.exceptions.MimeEncryptorExceptionCodes;
import com.openexchange.guard.mime.encryptor.osgi.Services;
import com.openexchange.guard.mime.encryptor.pgp.impl.PGPProcessMime;
import com.openexchange.guard.mime.encryptor.pgp.impl.SignMime;
import com.openexchange.guard.mime.services.GuardParsedMimeMessage;
import com.openexchange.guard.mime.services.MimeEncryptionService;
import com.openexchange.guard.mime.services.PGPMimeAttachmentExtractor;
import com.openexchange.guard.pgpcore.services.PGPCryptoService;
import com.openexchange.guard.pgpcore.services.PGPSigningService;
import com.openexchange.guard.user.GuardCapabilities;
import com.openexchange.guard.user.OXUserService;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.mail.PGPMimeService;
import com.openexchange.server.ServiceLookup;

/**
 * {@link PGPMimeEncryptionService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.8.0
 */
public class PGPMimeEncryptionService implements MimeEncryptionService {


    private final ServiceLookup services;
    private static Logger LOG = LoggerFactory.getLogger(PGPMimeEncryptionService.class);

    /**
     * Initializes a new {@link PGPMimeEncryptionService}.
     *
     * @param pgpCryptoService The {@link PGPCryptoService} to use for encrypting mime messages.
     */
    public PGPMimeEncryptionService(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Check if user has Guard-mail capability. Throw error if not
     * checkGuardMailCapable
     *
     * @param userId
     * @param contextId
     * @throws OXException Thrown if user doesn't have guard-mail
     */
    private void checkGuardMailCapable(int userId, int contextId) throws OXException {
        OXUserService userService = Services.getService(OXUserService.class);
        if (userService != null) {
            GuardCapabilities cap = userService.getGuardCapabilieties(contextId, userId);
            if (!cap.hasPermission(GuardCapabilities.Permissions.MAIL)) {
                throw MimeEncryptorExceptionCodes.SEND_NOT_AUTHORIZED.create();
            }
        }
    }

    /**
     * Send email for signing only. Not encypted
     *
     * @param response
     * @param userId
     * @param contextId
     * @param password
     * @param msg
     * @throws Exception
     */
    private void doSigning(OutputStream output, int userId, int contextId, String password, GuardParsedMimeMessage msg) throws Exception {
        GuardKeyService keyService = Services.getService(GuardKeyService.class);
        checkGuardMailCapable(userId, contextId);  // Make sure has guard-mail capability to send signed emails
        GuardKeys key = keyService.getKeys(userId, contextId);
        if (key == null) {
            throw MimeEncryptorExceptionCodes.SEND_UNABLE_FIND_KEYS.create("Unable to find singing keys");
        }
        SignMime signer = new SignMime(services.getService(PGPMimeService.class), services.getService(PGPSigningService.class));
        // Add autocrypt header if applicable
        AutoCryptService autocryptService = services.getService(AutoCryptService.class);
        if (autocryptService != null) {
            autocryptService.addOutgoingHeader(msg.getMessage(), key);
        }

        output.write(signer.signMime(msg, key, password).getBytes(StandardCharsets.UTF_8));
    }

    /**
     * Internal method to create a {@link ProcessMime} instance
     *
     * @return The new {@link ProcessMime} instance
     */
    private PGPProcessMime createMimeProcessor() {
        return new PGPProcessMime(services);
    }

    /**
     * Check the msg for unsupported actions
     * Currently checking for share links
     *
     * @param msg
     * @throws OXException
     */
    private void checkUnsupported(GuardParsedMimeMessage msg) throws OXException {
        // Check for attachment link
        String[] linkHeader;
        try {
            linkHeader = msg.getMessage().getHeader("X-Open-Xchange-Share-URL");
            if (linkHeader != null && linkHeader.length > 0) {
                LOG.error("Sending encrypted email using Drive Mail is currently not supported by Guard");
                throw GuardAuthExceptionCodes.FORBIDDEN.create();
            }
        } catch (MessagingException e1) {
            LOG.error("Problem checking incoming email for unsupported options", e1);
        }
    }

    /**
     * Do email encryption +/- signing and send response
     *
     * @param response
     * @param userId
     * @param contextId
     * @param password
     * @param msg
     * @throws IOException
     * @throws OXException
     */
    private void doEncrypt(OutputStream output, int userId, int contextId, String password, GuardParsedMimeMessage msg) throws IOException, OXException {
        checkUnsupported(msg);
        PGPProcessMime mimeProcessor = createMimeProcessor();
        output.write(mimeProcessor.doEncrypt(msg, userId, contextId, password).getBytes(StandardCharsets.UTF_8));
    }

    private void doDecrypt(OutputStream output, GuardParsedMimeMessage msg, UserIdentity userIdentity) throws IOException, OXException, MessagingException {
        PGPProcessMime mimeProcessor = createMimeProcessor();
        MimeMessage resultMessage = mimeProcessor.doDecrypt(msg.getMessage(), userIdentity);
        resultMessage.writeTo(output);
    }

    @Override
    public CryptoType.PROTOCOL getCryptoType() {
        return CryptoType.PROTOCOL.PGP;
    }

    @Override
    public void doEncryption(GuardParsedMimeMessage mimeMessage, int userId, int contextId, String password, OutputStream output) throws OXException {
        try {
            if (mimeMessage.isSign() && !mimeMessage.isEncrypt() && !mimeMessage.isDraft()) {  // not signing draft messages
                doSigning(output, userId, contextId, password, mimeMessage);
                return;
            }
            doEncrypt(output, userId, contextId, password, mimeMessage);
        } catch (OXException e) {
            throw e;
        } catch (Exception e) {
            throw MimeEncryptorExceptionCodes.SEND_EMAIL_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public void doDecryption(GuardParsedMimeMessage mimeMessage, int userId, int contextId, UserIdentity userIdentity, OutputStream output) throws OXException {
        try {
            doDecrypt(output, mimeMessage, userIdentity);
        } catch (OXException e) {
            throw e;
        } catch (Exception e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }


    @Override
    public MimeMessage doDecryption(MimeMessage mimeMessage, UserIdentity userIdentity) throws OXException {
        PGPProcessMime mimeProcessor = createMimeProcessor();
        return mimeProcessor.doDecrypt(mimeMessage, userIdentity);
    }


    @Override
    public Part doDecryption(MimeMessage mimeMessage, String attachmentName, UserIdentity userIdentity) throws OXException {
        mimeMessage = doDecryption(mimeMessage, userIdentity);
        try {
            return new PGPMimeAttachmentExtractor().getAttachmentFromMessage(mimeMessage, attachmentName);
        } catch (@SuppressWarnings("unused") MessagingException | IOException e) {
            throw MimeEncryptorExceptionCodes.PROBLEM_DECODING.create();
        }
    }
}
