/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.encryptor.exceptions;

import com.openexchange.i18n.LocalizableStrings;

/**
 * {@link MimeEncryptorExceptionExceptionMessages}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public final class MimeEncryptorExceptionExceptionMessages implements LocalizableStrings {

    // Unable to send an email to email address
    public static final String SEND_EMAIL_ERROR = "Unable to send email to '%1$s'";

    // User does not have email encryption enabled, not authorized to send a new encrypted email
    public static final String SEND_NOT_AUTHORIZED = "User does not have email encryption enabled and is not authorized to send.";

    // Unable to find encryption keys for all recipients
    public static final String SEND_UNABLE_FIND_KEYS = "Unable to find keys for all recipients";

    // No recipients specified, so can't encrypt
    public static final String NO_RECIPIENTS = "Unable to encrypt, no recipients";

    // Unknown email body type.  Failed to encrypt
    public static final String UNKONWN_BODY_TYPE = "Unable to encrypt due to unknown body type";

    // Password missing or authentication invalid
    public static final String PASSWORD_BAD = "Password is missing or authentication invalid";

    // Problem decrypting the attachment
    public static final String PROBLEM_DECODING_ENCRYPTED_ATTACHMENT = "There was a problem decoding the encrypted attachment";

    // When a guest replies, they can only reply to the sender and people cc'd in the email.  They cannot add recipients.  One or more extra recipients found and that isn't allowed.  Chain refers to the group of people originally in the email
    public static final String RECIPIENT_NOT_IN_CHAIN = "One or more of the recipients was not in the original email chain, '%1$s";

    // The message is missing some of the required security information.  Without this, we can't be sure the message is intact and hasn't been altered.
    public static final String MISSING_MDC = "Unable to decrypt this message.  It is missing Modification Detection Code, and the system is unable to verify the integrity of the message.";

    private MimeEncryptorExceptionExceptionMessages() {}
}
