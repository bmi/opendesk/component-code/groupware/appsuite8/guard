/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.clt;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.rmi.RemoteException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import javax.management.MBeanException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import com.openexchange.auth.rmi.RemoteAuthenticator;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.java.Strings;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.management.maintenance.GuardMaintenanceMBean;
import com.openexchange.guard.management.maintenance.internal.TestUserResult;

/**
 * {@link Guard}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 */
public class Guard extends AbstractGuardCLT {

    private final char[] optionsWithArgs = { 't', 'e', 'n', 'u', 'j', 'w', 'o', 'x', 'g', 'm', 'r', 'c', 'a' };

    private final Boolean requiresAuth;

    /**
     * @param args
     */
    public static void main(String[] args) {
        new Guard(args).execute(args);
    }

    /**
     * Initialises a new {@link Guard}.
     */

    public Guard(String[] args) {
        super();
        requiresAuth = needsAuth(args);
    }

    /**
     * Return true if ags includes action that requires authentication
     *
     * @param args
     * @return
     */
    private boolean needsAuth(String[] args) {
        for (String arg : args) {
            if (arg.trim().equals("-D") || arg.trim().equals("--delete")) {
                return Boolean.TRUE;
            }
            if (arg.trim().equals("-m") || arg.trim().equals("--import")) {
                return Boolean.TRUE;
            }
            if (arg.trim().equals("-M") || arg.trim().equals("--masterKey")) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.cli.AbstractRmiCLI#administrativeAuth(java.lang.String, java.lang.String, com.openexchange.cli.CommandLine, com.openexchange.auth.rmi.RemoteAuthenticator)
     */
    @Override
    protected void administrativeAuth(String login, String password, CommandLine cmd, RemoteAuthenticator authenticator) throws RemoteException {
        authenticator.doAuthentication(login, password);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.cli.AbstractRmiCLI#addOptions(com.openexchange.cli.Options)
     */
    @Override
    protected void addOptions(Options options) {
        options.addOption("i", "init", false, "Initialise guard.");
        options.addOption("c", "change-user", true, "Change a Guard user's email. \n   Change params --newEmail (new email)\nguard -c john@somehwere.com --newEmail johhny@somewhere.com ");
        options.addOption(null, "newEmail", true, "New email address, used with change-user");
        options.addOption("d", "directory", true, "Full path of the 'oxguardpass' file (defaults to current directory). This flag should be used in conjunction with the '--init' switch.\ne.g. guard --init --directory /opt/open-xchange/etc");
        options.addOption("t", "test", true, "Test the specified e-mail address against the MailResolver\ne.g. guard --test john@somewhere.com");
        options.addOption("e", "reset", true, "Resets the specified e-mail address and sends a new password to the user\ne.g. guard --reset john@somewhere.com");
        options.addOption("n", "remove-pin", true, "Removes the PIN for the specified user\ne.g. guard --remove-pin john@somewhere.com");
        options.addOption("u", "upgrade", true, "Upgrades the specified guest account to an OX account\ne.g. guard --upgrade john@somewhere.com\nIf error, try specifying id and context of new user\nguard --upgrade john@somwhere.com --id 2 --context 3");
        options.addOption(null, "context", true, "Context of a user");
        options.addOption(null, "id", true, "ID of a user");
        options.addOption("D", "delete", true, "Deletes a specified Guard user");
        options.addOption("g", "guestreset", true, "Reset a Guest account.  Creates and sends a link to the guest that allows them to create new keys. \ne.g. guard -g guest@gmail -f admin@somwhere.com");
        options.addOption("m", "import", true, "Import a System PGP Public Key");
        options.addOption("j", "jmx-user", true, "JMX user");
        options.addOption("w", "jmx-password", true, "JMX password");
        options.addOption("o", "jmx-port", true, "JMX port (default: '9999')");
        options.addOption("x", "jmx-host", true, "JMX host (default: 'localhost')");
        options.addOption("r", "repair", true, "Check database integrity and repair if able.  Specify argument update or dryRun \ne.g. guard -r update");
        options.addOption("M", "masterKey", false, "Create new master key");
        options.addOption(null, "clean", false, "Clean Guard database caches (pre-generated keys, remote lookup keys)");
        options.addOption("f", "fromEmail", true, "For Guest reset.  Sepcifies user that creates the share link. Required for templates, formatting, etc");
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.cli.AbstractRmiCLI#invoke(com.openexchange.cli.Options, com.openexchange.cli.CommandLine, java.lang.String)
     */
    @Override
    protected Void invoke(Options options, CommandLine cmd, String optRmiHostName) {
        // Get the optional JMX params
        String jmxHost = "localhost";
        if (cmd.hasOption('x')) {
            jmxHost = cmd.getOptionValue('x');
        }
        String jmxPort = "9999";
        if (cmd.hasOption('o')) {
            jmxPort = cmd.getOptionValue('o');
        }
        String jmxUser = null;
        if (cmd.hasOption('j')) {
            jmxUser = cmd.getOptionValue('j');
        }
        String jmxPassword = null;
        if (cmd.hasOption('w')) {
            jmxPassword = cmd.getOptionValue('w');
        }

        // Get the operation name and the operation params
        if (cmd.hasOption('i')) {
            String directory;
            if (cmd.hasOption('d')) {
                directory = cmd.getOptionValue('d');
            } else {
                directory = DEFAULT_CONF_FOLDER;
            }
            try {
                initialise(directory);
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(-1);
            }
            System.exit(0);
        }

        try {
            if (cmd.hasOption('e')) {
                String email = cmd.getOptionValue('e');
                reset(createProxy(jmxHost, jmxPort, jmxUser, jmxPassword), email);
            } else if (cmd.hasOption('n')) {
                String email = cmd.getOptionValue('n');
                removePin(createProxy(jmxHost, jmxPort, jmxUser, jmxPassword), email);
            } else if (cmd.hasOption('c')) {
                String email = cmd.getOptionValue('c');
                String newEmail = cmd.getOptionValue("newEmail");
                changeUser(createProxy(jmxHost, jmxPort, jmxUser, jmxPassword), email, newEmail);
            } else if (cmd.hasOption('u')) {
                String email = cmd.getOptionValue('u');
                String cid = cmd.getOptionValue("context");
                String userid = cmd.getOptionValue("id");
                upgrade(createProxy(jmxHost, jmxPort, jmxUser, jmxPassword), email, userid, cid);
            } else if (cmd.hasOption('t')) {
                String email = cmd.getOptionValue('t');
                test(createProxy(jmxHost, jmxPort, jmxUser, jmxPassword), email);
            } else if (cmd.hasOption('g')) {
                String email = cmd.getOptionValue('g');
                String fromEmail = cmd.getOptionValue('f');
                int fromId = 0, fromCid = 0;
                resetGuest(createProxy(jmxHost, jmxPort, jmxUser, jmxPassword), email, fromEmail, fromId, fromCid);
            } else if (cmd.hasOption('D')) {
                String email = cmd.getOptionValue('D');
                delete(createProxy(jmxHost, jmxPort, jmxUser, jmxPassword), email);
            } else if (cmd.hasOption('m')) {
                String file = cmd.getOptionValue('m');
                importPublic(createProxy(jmxHost, jmxPort, jmxUser, jmxPassword), file);
            } else if (cmd.hasOption('M')) {
                createMasterKey(createProxy(jmxHost, jmxPort, jmxUser, jmxPassword));
            } else if (cmd.hasOption('r')) {
                String dry = cmd.getOptionValue('r');
                if (dry != null && dry.toLowerCase().startsWith("update")) {
                    repairDatabase(createProxy(jmxHost, jmxPort, jmxUser, jmxPassword), false);
                } else if (dry != null && dry.toLowerCase().startsWith("dry")) {
                    System.out.println("Dry run");
                    repairDatabase(createProxy(jmxHost, jmxPort, jmxUser, jmxPassword), true);
                }
            } else if (cmd.hasOption("clean")) {
                clean(createProxy(jmxHost, jmxPort, jmxUser, jmxPassword));
            } else {
                printHelp(options);
                System.exit(0);
            }
        } catch (Exception e) {
            if (e.getCause() instanceof OXException) {
                System.err.println(e.getCause().getMessage());
            } else {
                System.err.println("Operation failed.\n");
                e.printStackTrace();
            }
            System.exit(-1);
        }
        return null;
    }

    private void reset(GuardMaintenanceMBean proxy, String email) throws MBeanException {
        proxy.reset(email, "pgp");
        System.out.println("Email with a new password has been sent.");
    }

    private void removePin(GuardMaintenanceMBean proxy, String email) throws MBeanException {
        proxy.removePIN(email);
        System.out.println("PIN has been removed.");
    }

    private void upgrade(GuardMaintenanceMBean proxy, String email, String userid, String cid) throws MBeanException {
        proxy.upgrade(email, userid, cid);
        System.out.println(String.format("Guest (%s) has been upgraded.", email));
    }

    private void delete(GuardMaintenanceMBean proxy, String email) throws MBeanException {
        proxy.deleteUser(email);
        System.out.println(String.format("User (%s) has been deleted.", email));
    }

    private void resetGuest(GuardMaintenanceMBean proxy, String email, String fromEmail, int fromId, int fromCid) throws MBeanException {
        proxy.resetGuest(email, fromEmail, fromId, fromCid);
        System.out.println(String.format("Guest (%s) has been sent a reset link.", email));
    }

    private void importPublic(GuardMaintenanceMBean proxy, String filename) throws MBeanException {
        proxy.importPublic(filename);
        System.out.println("PGP Key Imported");
    }

    private void clean(GuardMaintenanceMBean proxy) throws MBeanException {
        proxy.clean();
        System.out.println("Done");
    }

    private void test(GuardMaintenanceMBean proxy, String email) throws MBeanException {
        TestUserResult testResult = proxy.testUser(email);
        System.out.println("User id: " + testResult.getUserId());
        System.out.println("Context: " + testResult.getContextId());
        System.out.println("Language: " + testResult.getLanguageString());
        System.out.println("Name: " + testResult.getDisplayName());
    }

    private void repairDatabase(GuardMaintenanceMBean proxy, boolean dryRun) throws MBeanException {
        StringBuilder results = proxy.repair(dryRun);
        System.out.println(results);
        System.out.println("Repair procedure complete");
    }

    private void changeUser(GuardMaintenanceMBean proxy, String email, String newEmail) throws MBeanException {
        if (email == null) {
            System.out.println("Identifying email address required.  Please enter with option -e");
            return;
        }
        if (newEmail == null) {
            System.out.println("No new parameters found.  Please enter new change.  -n for new email, -c for new context, or -i for new userid");
            return;
        }
        proxy.changeUser(email, newEmail);
        System.out.println("Done");
    }

    private void createMasterKey(GuardMaintenanceMBean proxy) throws MBeanException {
        int newIndex = proxy.createMaster();
        System.out.print("New master keys created with index ");
        System.out.println(newIndex);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.cli.AbstractCLI#checkOptions(com.openexchange.cli.CommandLine)
     */
    @Override
    protected void checkOptions(CommandLine cmd) {
        boolean missingArg = false;

        for (char c : optionsWithArgs) {
            if (cmd.hasOption(c) && cmd.getOptionValue(c) == null) {
                missingArg = true;
            }
        }

        if (missingArg) {
            printHelp(options);
            System.exit(-1);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.cli.AbstractCLI#requiresAdministrativePermission()
     */
    @Override
    protected Boolean requiresAdministrativePermission() {
        return requiresAuth;
    }

    /*
     * (non-Javadoc)
     *
     * @see )com.openexchange.cli.AbstractCLI#getFooter()
     */
    @Override
    protected String getFooter() {
        return "Command line tool for OX Guard";
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.cli.AbstractCLI#getName()
     */
    @Override
    protected String getName() {
        return "guard [-i [-d /custom/path]] [-t john.doe@example.com] [-u joan.doe@ox.io] [-e john.doe@example.com] [-n joan.doe@example.com] [-j jmxUser] " + "[-w jmxPassword] [-x jmx.host] [-o <JMX_PORT>] [-p <RMI_PORT>] [--responsetimeout <TIMEOUT>]";
    }

    private void initialise(String directory) throws IOException {
        // Verify that the specified path is a directory
        File f = new File(directory);
        if (!f.exists()) {
            System.err.println("The directory '" + directory + "' does not exist. Please specify a valid directory with the '--directory' option.");
            System.exit(-1);
        }
        if (!f.isDirectory()) {
            System.err.println("The '" + directory + "' is not a directory. Please specify a valid directory with the '--directory' option.");
            System.exit(-1);
        }

        File file = new File(directory + "/oxguardpass");
        if (file.exists()) {
            System.out.println("'oxguardpass' already exist in folder " + directory + ". Remove it or define a different destination by using parameter '-d' and run 'guard -i' again.");
            System.exit(-1);
        }

        // Generate MC and RC
        System.out.print("Generating MC salt...");
        String mcSalt = CipherUtil.generateSalt();
        System.out.println("done.");
        System.out.print("Generating RC salt...");
        String rcSalt = CipherUtil.generateSalt();
        System.out.println("done.");

        // Write to file
        File masterPasswordFile = new File(f, "oxguardpass");
        System.out.print("Saving to " + masterPasswordFile.getAbsolutePath() + "...");
        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(masterPasswordFile), StandardCharsets.UTF_8));
        try {
            writer.write("MC" + mcSalt + "\n");
            writer.write("RC" + rcSalt + "\n");
        } finally {
            if (writer != null) {
                writer.close();
            }
        }

        System.out.println("done.");

        fixPermissions(directory + "/oxguardpass");
    }

    /**
     * Set the correct permissions for the file
     *
     * @param path The path to set the permissions. Must point to a file
     * @throws IOException If an I/O error occurs
     * @throws IllegalArgumentException if the path is not pointing to a file
     */
    private void fixPermissions(String path) throws IOException {
        // Verify that the path is a file
        File f = new File(path);
        if (!f.isFile()) {
            throw new IllegalArgumentException("The path '" + path + "' is not pointing to a file.");
        }
        // Set the ownership
        System.out.print("Setting the ownership to root:open-xchange...");
        executeCommand("chown root:open-xchange " + path);

        // Set the permissions
        System.out.print("Setting permissions to 0640...");
        executeCommand("chmod 0640 " + path);
    }

    /**
     * Execute the specified command
     *
     * @param command The command to execute
     * @throws IOException If an I/O error occurs
     */
    private void executeCommand(String command) throws IOException {
        Runtime rt = Runtime.getRuntime();
        Process chown = rt.exec(command);

        StreamProcessor outputStream = new StreamProcessor(chown.getInputStream());
        outputStream.start();

        StreamProcessor errorStream = new StreamProcessor(chown.getErrorStream());
        errorStream.start();

        int retValue = -1;
        try {
            retValue = chown.waitFor();
            String error = errorStream.getOutput();
            if (error.contains("Operation not permitted")) {
                System.err.println("\nNot enough permissions. This tool must run as root");
            } else if (!Strings.isEmpty(error)) {
                System.err.println(error);
            }

            String output = outputStream.getOutput();
            if (!Strings.isEmpty(output)) {
                System.out.println(output);
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }

        if (retValue != 0) {
            System.exit(-1);
        }
        System.out.println("done.");
    }



    /**
     * A simple {@link InputStream} processor
     */
    private class StreamProcessor extends Thread {

        private final InputStream inputStream;

        private final BlockingQueue<String> output;

        /**
         * Initialises a new {@link StreamProcessor}.
         *
         * @param inputStream The input stream to process
         */
        StreamProcessor(InputStream inputStream) {
            this.inputStream = inputStream;
            output = new ArrayBlockingQueue<String>(1);
        }

        @Override
        public void run() {
            try {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String line = null;
                StringBuilder builder = new StringBuilder();
                while ((line = bufferedReader.readLine()) != null) {
                    builder.append(line);
                }
                output.offer(builder.toString());
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

        /**
         * Returns the output of the stream
         *
         * @return the output of the stream
         * @throws InterruptedException
         */
        public String getOutput() throws InterruptedException {
            return output.take();
        }

    }
}
