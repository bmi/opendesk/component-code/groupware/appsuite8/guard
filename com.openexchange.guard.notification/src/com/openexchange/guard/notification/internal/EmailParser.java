package com.openexchange.guard.notification.internal;

import java.io.UnsupportedEncodingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.guard.common.util.IDNUtil;

public class EmailParser {

	private static Logger LOG = LoggerFactory.getLogger(EmailParser.class);

	public static InternetAddress parseEmailAddress(String address) throws AddressException, UnsupportedEncodingException {
        try {
            if (address.contains("<")) {
                String name = address.substring(0, address.indexOf("<"));
                if (!name.contains("\"")) {
                    address = "\"" + name.trim() + "\" " + address.substring(address.indexOf("<"));
                }
                String email = address.substring(address.indexOf("<") + 1);
                if (email.contains(">")) {
                	email = email.substring(0, email.indexOf(">"));
                }
                return new InternetAddress(IDNUtil.aceEmail(email), name, "UTF-8");
            }
        } catch (Exception e) {
            LOG.error("Problem parsing address ", e);
        }
        InternetAddress[] recips = InternetAddress.parse(IDNUtil.aceEmail(address), false);
        if (recips.length > 0) {
            return new InternetAddress(recips[0].getAddress(), recips[0].getPersonal(), "UTF-8");
        }
        return null;
    }
}
