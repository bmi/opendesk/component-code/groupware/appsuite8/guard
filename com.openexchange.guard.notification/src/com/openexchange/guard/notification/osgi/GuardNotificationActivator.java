/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.notification.osgi;

import org.slf4j.Logger;
import com.openexchange.config.ConfigurationService;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.notification.GuardNotificationService;
import com.openexchange.guard.notification.internal.GuardNotificationServiceImpl;
import com.openexchange.guard.notification.internal.Services;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.guard.translation.GuardTranslationService;
import com.openexchange.net.ssl.SSLSocketFactoryProvider;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link GuardNotificationActivator}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public class GuardNotificationActivator extends HousekeepingActivator {

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { GuardConfigurationService.class, GuardTranslationService.class, GuardRatifierService.class, ConfigurationService.class };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void startBundle() throws Exception {
        Logger logger = org.slf4j.LoggerFactory.getLogger(GuardNotificationActivator.class);
        logger.info("Starting bundle: {}", context.getBundle().getSymbolicName());
        Services.setServiceLookup(this);
        String attachIpPropertyName = "com.openexchange.mail.addClientIPAddress";
        registerService(GuardNotificationService.class, new GuardNotificationServiceImpl(
            this.getService(GuardRatifierService.class),
            this.getService(GuardConfigurationService.class),
            this.getService(ConfigurationService.class).getBoolProperty(attachIpPropertyName, false)));
        logger.info("GuardNotificationService registered.");
        trackService(SSLSocketFactoryProvider.class);
        openTrackers();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void stopBundle() throws Exception {
        Logger logger = org.slf4j.LoggerFactory.getLogger(GuardNotificationActivator.class);
        logger.info("Stopping bundle: {}", this.context.getBundle().getSymbolicName());
        unregisterService(GuardNotificationService.class);
        logger.info("GuardNotificationService unregistered.");
        Services.setServiceLookup(null);
        super.stopBundle();
    }
}
