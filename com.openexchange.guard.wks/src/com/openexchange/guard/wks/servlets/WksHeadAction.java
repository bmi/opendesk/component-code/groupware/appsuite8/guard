/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.wks.servlets;

import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.wks.internal.GetKeysByHu;

/**
 * {@link WksHeadAction} Handles incoming WebKey Srvice get actions
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.2
 */
public class WksHeadAction {

    private static final Logger logger = LoggerFactory.getLogger(WksHeadAction.class);

    private final HttpServletResponse response;
    private final String search;
    private final String domain;

    /**
     * Initializes a new {@link WksHeadAction}.
     *
     * @param response Response object
     * @param search The search hash
     * @param domain The domain
     */
    public WksHeadAction(HttpServletResponse response, String search, String domain) {
        super();
        this.response = response;
        this.search = search;
        this.domain = domain;
    }


    public void doAction() throws OXException {
        if(GetKeysByHu.hasKeys(search, domain)) {
            response.setContentType("application/octet-stream");
            logger.debug("WKS: Key found");
        }
        else {
            logger.debug("WKS: No Key found");
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}
