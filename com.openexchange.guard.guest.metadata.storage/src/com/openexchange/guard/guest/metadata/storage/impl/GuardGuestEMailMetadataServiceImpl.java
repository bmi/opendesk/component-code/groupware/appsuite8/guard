/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.metadata.storage.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.GuardShardingService;
import com.openexchange.guard.database.utils.GuardConnectionWrapper;
import com.openexchange.guard.email.storage.ogEmail.Email;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService;
import com.openexchange.guard.guest.metadata.storage.GuardGuestEmailMetadata;
import com.openexchange.guard.guestupgrade.storage.GuestUpgradeStorageService;

/**
 * {@link GuardGuestEMailMetadataServiceImpl}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardGuestEMailMetadataServiceImpl implements GuardGuestEMailMetadataService {

    private static final String COLUMN_ITEM_ID       = "itemID";
    private static final String COLUMN_VERSION       = "version";
    private static final String COLUMN_FOLDER_ID     = "folderId";
    private static final String COLUMN_CONTENT_TYPE  = "contentType";
    private static final String COLUMN_MIME_TYPE     = "mimeType";
    private static final String COLUMN_FROM          = "mailFrom";
    private static final String COLUMN_TO            = "mailTo";
    private static final String COLUMN_CC            = "mailCc";
    private static final String COLUMN_BCC           = "mailBcc";
    private static final String COLUMN_SUBJECT       = "subject";
    private static final String COLUMN_MESSAGE_FLAGS = "messageFlags";
    private static final String COLUMN_SIZE          = "size";
    private static final String COLUMN_SENT_DATE     = "sentDate";
    private static final String COLUMN_RECEIVED_DATE = "receivedDate";
    private static final String COLUMN_COLOR_LABEL   = "colorLabel";

    private final GuardDatabaseService guardDatabaseService;
    private GuardShardingService shardingService;
    private final GuestUpgradeStorageService guestUpgradeService;

    /**
     * Initializes a new {@link GuardGuestEMailMetadataServiceImpl}.
     *
     * @param guardDatabaseService The database service
     */
    public GuardGuestEMailMetadataServiceImpl(GuardDatabaseService guardDatabaseService, GuardShardingService shardingService, GuestUpgradeStorageService guestUpgradeService) {
        this.shardingService = Objects.requireNonNull(shardingService,"shardingService must not be null");
        this.guardDatabaseService = Objects.requireNonNull(guardDatabaseService, "guardDatabaseService must not be null");
        this.guestUpgradeService = guestUpgradeService;
    }

    /**
     * Transforms the given addresses in a comma separated string representation
     *
     * @param addresses The set of addresses
     * @return The comma separated string representation of the given array
     */
    private String concatAddresses(String[] addresses) {
        if (addresses == null) {
            return null;
        }
        //map to a comma separated representation
        StringBuilder result = Arrays.stream(addresses).collect(StringBuilder::new, (stringBuilder, string) -> stringBuilder.append(string + ","), StringBuilder::append);
        //remove the last comma
        return result.replace(result.toString().lastIndexOf(','), result.toString().lastIndexOf(',') + 1, "").toString();
    }

    private String[] splitAddressess(String string) throws OXException {
        if(string == null) {
            return null;
        }
        try {
            InternetAddress[] addresses = InternetAddress.parse(string);
            ArrayList<String> results = new ArrayList<String>();
            for (InternetAddress addr : addresses) {
                results.add(addr.toString());
            }
            return results.toArray(new String[results.size()]);
        } catch (AddressException e) {
            throw OXException.general("Unable to parse email message address", e);
        }
    }

    private ArrayList<GuardGuestEmailMetadata> parseResult(ResultSet result) throws SQLException, OXException {
        ArrayList<GuardGuestEmailMetadata> ret = new ArrayList<GuardGuestEmailMetadata>();
        while (result.next()) {
            ret.add(new GuardGuestEmailMetadata(result.getString(COLUMN_ITEM_ID), result.getString(COLUMN_VERSION))
                .setFolderId(result.getString(COLUMN_FOLDER_ID))
                .setContentType(result.getString(COLUMN_CONTENT_TYPE))
                .setMimeType(result.getString(COLUMN_MIME_TYPE))
                .setFrom(splitAddressess(result.getString(COLUMN_FROM)))
                .setTo(splitAddressess(result.getString(COLUMN_TO)))
                .setCc(splitAddressess(result.getString(COLUMN_CC)))
                .setBcc(splitAddressess(result.getString(COLUMN_BCC)))
                .setSubject(result.getString(COLUMN_SUBJECT))
                .setMessageFlags(result.getInt(COLUMN_MESSAGE_FLAGS))
                .setSize(result.getLong(COLUMN_SIZE))
                .setReceivedDate(result.getTimestamp(COLUMN_RECEIVED_DATE).getTime() != 0 ? new Date(result.getTimestamp(COLUMN_RECEIVED_DATE).getTime()) : null)
                .setSentDate(result.getTimestamp(COLUMN_SENT_DATE).getTime() != 0 ? new Date(result.getTimestamp(COLUMN_SENT_DATE).getTime()) : null)
                .setColorLabel(result.getString(COLUMN_COLOR_LABEL)));
        }
        return ret;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService#get(int, int, java.lang.String, java.lang.String)
     */
    @Override
    public GuardGuestEmailMetadata get(int contextId, int userId, String id) throws OXException {
        int shard =0;
        if (contextId < 0) {
            shard = this.shardingService.getShard(userId, contextId);
        } else {
            Email guest = guestUpgradeService.getGuestRecord(null, userId, contextId);
            if (guest != null) {
                shard = guest.getShardingId();
            }
        }
        Connection connection = guardDatabaseService.getReadOnlyForShard(shard);
        try (PreparedStatement stmt = connection.prepareStatement(GuestEmailMetaDataSql.SELECT_BY_ID)){
            stmt.setString(1, id);
            ArrayList<GuardGuestEmailMetadata> result = parseResult(stmt.executeQuery());
            return result.size() > 0 ? result.get(0) : null;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            guardDatabaseService.backReadOnlyForShard(shard, connection);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService#getForFolder(int, int, java.lang.String)
     */
    @Override
    public Collection<GuardGuestEmailMetadata> getForFolder(int contextId, int userId, String folderId) throws OXException {
        final int shard = this.shardingService.getShard(userId, contextId);
        Connection connection = guardDatabaseService.getReadOnlyForShard(shard);
        try ( PreparedStatement stmt = connection.prepareStatement(GuestEmailMetaDataSql.SELECT_BY_FOLDER)){
            stmt.setInt(1, userId);
            stmt.setInt(2, contextId);
            stmt.setString(3, folderId);
            return parseResult(stmt.executeQuery());
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            guardDatabaseService.backReadOnlyForShard(shard, connection);
        }
    }
    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService#insert(com.openexchange.guard.guest.metadata.storage.GuardGuestEmailMetadata)
     */
    @Override
    public void insert(int contextId, int userId, GuardGuestEmailMetadata guestEmailMetaData) throws OXException {
        final int shard = this.shardingService.getShard(userId, contextId);
        Connection connection = guardDatabaseService.getWritableForShard(shard);
        try (PreparedStatement stmt = connection.prepareStatement(GuestEmailMetaDataSql.INSERT)){
            stmt.setString(1, guestEmailMetaData.getId());
            stmt.setString(2, guestEmailMetaData.getGuardGuestItemVersion());
            stmt.setInt(3, userId);
            stmt.setInt(4, contextId);
            stmt.setString(5, guestEmailMetaData.getFolderId());
            stmt.setString(6, guestEmailMetaData.getContentType());
            stmt.setString(7, guestEmailMetaData.getMimeType());
            stmt.setString(8, concatAddresses(guestEmailMetaData.getFrom()));
            stmt.setString(9, concatAddresses(guestEmailMetaData.getTo()));
            stmt.setString(10, concatAddresses(guestEmailMetaData.getCc()));
            stmt.setString(11, concatAddresses(guestEmailMetaData.getBcc()));
            stmt.setString(12, guestEmailMetaData.getSubject());
            stmt.setInt(13, guestEmailMetaData.getMessageFlags());
            stmt.setBigDecimal(14, BigDecimal.valueOf(guestEmailMetaData.getSize()));
            stmt.setTimestamp(15, guestEmailMetaData.getSentDate() != null ? new Timestamp(guestEmailMetaData.getSentDate().getTime()) : new Timestamp(0));
            stmt.setTimestamp(16, guestEmailMetaData.getReceivedDate() != null ? new Timestamp(guestEmailMetaData.getReceivedDate().getTime()) : new Timestamp(0));
            stmt.setString(17, guestEmailMetaData.getColorLabel());
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            guardDatabaseService.backWritableForShard(shard, connection);
        }
    }

    @Override
    public GuardGuestEmailMetadata createMetaDataFrom(String itemId, String folderId, MimeMessage message, int defaultMessageFlags) throws MessagingException {
        return new GuardGuestEmailMetadata(itemId, GuardGuestEmailMetadata.CURRENT_VERSION)
            .setFolderId(folderId)
            .setContentType(message.getContentType())
            .setMimeType(message.getContentType())
            .setFrom(message.getFrom() != null ? Arrays.stream(message.getFrom()).map(f -> f.toString()).toArray(String[]::new) : null)
            .setTo(message.getRecipients(RecipientType.TO) != null ? Arrays.stream(message.getRecipients(RecipientType.TO)).map(a -> a.toString()).toArray(String[]::new) : null)
            .setCc(message.getRecipients(RecipientType.CC) != null ? Arrays.stream(message.getRecipients(RecipientType.CC)).map(a -> a.toString()).toArray(String[]::new) : null)
            .setBcc(message.getRecipients(RecipientType.BCC) != null ? Arrays.stream(message.getRecipients(RecipientType.BCC)).map(a -> a.toString()).toArray(String[]::new) : null)
            .setSubject(message.getSubject())
            .setSize(message.getSize())
            .setSentDate(message.getSentDate())
            .setReceivedDate(message.getReceivedDate() == null ? message.getSentDate() : message.getReceivedDate())
            .setMessageFlags(defaultMessageFlags);
    }
    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService#updateMessageFlags(java.lang.String, int)
     */
    @Override
    public void updateMessageFlags(int contextId, int userId, String itemId, int messageFlags) throws OXException {
        final int shard = this.shardingService.getShard(userId, contextId);
        Connection connection = guardDatabaseService.getWritableForShard(shard);
        try (PreparedStatement stmt = connection.prepareStatement(GuestEmailMetaDataSql.UPDATE_MESSAGE_FLAGS)){
            stmt.setInt(1, messageFlags);
            stmt.setString(2, itemId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            guardDatabaseService.backWritableForShard(shard, connection);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService#updateColorLabel(java.lang.String, int)
     */
    @Override
    public void updateColorLabel(int contextId, int userId, String itemId, int colorLabel) throws OXException {
        final int shard = this.shardingService.getShard(userId, contextId);
        Connection connection = guardDatabaseService.getWritableForShard(shard);
        try(PreparedStatement stmt = connection.prepareStatement(GuestEmailMetaDataSql.UPDATE_COLOR_LABEL)){
            stmt.setInt(1, colorLabel);
            stmt.setString(2, itemId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        }
        finally {
            guardDatabaseService.backWritableForShard(shard, connection);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService#updateFolderId(int, int, java.lang.String, java.lang.String)
     */
    @Override
    public void updateFolderId(int contextId, int userId, String itemId, String folderId) throws OXException {
        final int shard = this.shardingService.getShard(userId, contextId);
        Connection connection = guardDatabaseService.getWritableForShard(shard);
        try(PreparedStatement stmt = connection.prepareStatement(GuestEmailMetaDataSql.UPDATE_FOLDER_ID)){
            stmt.setString(1, folderId);
            stmt.setString(2, itemId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        }
        finally {
            guardDatabaseService.backWritableForShard(shard, connection);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService#updateFolderIds(int, int, java.lang.String, java.lang.String)
     */
    @Override
    public void updateAllFolderIdsInFolder(int contextId, int userId, String folderId, String newFolderId) throws OXException {
        final int shard = this.shardingService.getShard(userId, contextId);
        Connection connection = guardDatabaseService.getWritableForShard(shard);
        try(PreparedStatement stmt = connection.prepareStatement(GuestEmailMetaDataSql.UPDATE_ALLL_FOLDERS_FOR_GUEST)){
            stmt.setString(1, newFolderId);
            stmt.setString(2, folderId);
            stmt.setInt(3, userId);
            stmt.setInt(4, contextId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        }
        finally {
            guardDatabaseService.backWritableForShard(shard, connection);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService#delete(int, int, com.openexchange.guard.guest.metadata.storage.GuardGuestEmailMetadata)
     */
    @Override
    public int delete(int shardId, GuardGuestEmailMetadata guestEmailMetaData) throws OXException {
        Connection connection = guardDatabaseService.getWritableForShard(shardId);
        try(PreparedStatement stmt = connection.prepareStatement(GuestEmailMetaDataSql.DELETE_BY_ID)){
            stmt.setString(1, guestEmailMetaData.getId());
            return stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        }
        finally {
           guardDatabaseService.backWritableForShard(shardId, connection);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService#deleteUserMetaData(int, int)
     */
    @Override
    public int deleteUserMetaData(int userId, int contextId) throws OXException {
        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(userId, contextId, 0);
        Connection connection = connectionWrapper.getConnection();
        try(PreparedStatement stmt = connection.prepareStatement(GuestEmailMetaDataSql.DELETE_BY_USER)){
            stmt.setInt(1,  userId);
            stmt.setInt(2, contextId);
            return stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        }
        finally {
           guardDatabaseService.backWritable(connectionWrapper);
        }

    }

}
