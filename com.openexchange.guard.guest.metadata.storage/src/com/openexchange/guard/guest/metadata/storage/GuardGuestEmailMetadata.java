/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.metadata.storage;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

/**
 * {@link GuardGuestEmailMetadata}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardGuestEmailMetadata implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -10220532681910653L;
    private static final String SHORTENER_STRING = "...";
    private static final int MAX_SUBJECT_LENGTH = 500;

    private String id;
    private final String version;
    private String       folderId;
    private String       contentType;   //?
    private String       mimeType;         //?
    private String[]     from;
    private String[]     to;
    private String[]     cc;
    private String[]     bcc;
    private String       subject;
    private long         size;
    private Date         sentDate;
    private Date         receivedDate;
    private int          messageFlags;
    private String colorLabel;
    /*--extend copy cstor when adding new fields */

    public final static String CURRENT_VERSION = "1";

    /**
     *
     * Initializes a new {@link GuardGuestEmailMetadata}.
     *
     * @param id The unique ID of an item
     */
    public GuardGuestEmailMetadata(String id) {
       this(id, CURRENT_VERSION);
    }

    /**
     * Initializes a new {@link GuardGuestEmailMetadata}.
     *
     * @param id The unique ID of an item
     * @param version A version number describing the item's data format.
     */
    public GuardGuestEmailMetadata(String id, String version) {
        this.id = id;
        this.version = version;
    }

    /**
     * Copy constructor
     *
     * Initializes a new {@link GuardGuestEmailMetadata}.
     *
     * @param source The original {@link GuardGuestEmailMetadata} instance to copy the data from
     */
    public GuardGuestEmailMetadata(GuardGuestEmailMetadata source) {
        this.id = source.id;
        this.version = source.version;
        this.folderId = source.folderId;
        this.contentType = source.contentType;
        this.mimeType = source.mimeType;
        this.from = source.from != null ? Arrays.copyOf(source.from, source.from.length) : null;
        this.to = source.to != null ? Arrays.copyOf(source.to, source.to.length) : null;
        this.cc = source.cc != null ? Arrays.copyOf(source.cc, source.cc.length) : null;
        this.bcc = source.bcc != null ? Arrays.copyOf(source.bcc, source.bcc.length) : null;
        this.subject = trimValue(source.subject, MAX_SUBJECT_LENGTH);
        this.size = source.size;
        this.sentDate = source.sentDate != null ? (Date) source.sentDate.clone() : null;
        this.receivedDate = source.receivedDate != null ? (Date) source.receivedDate.clone() : null;
        this.messageFlags = source.messageFlags;
    }

    private String trimValue(String subject, int maxLength) {
        if(subject != null) {
            if(subject.length() > maxLength) {
                int length = maxLength - SHORTENER_STRING.length();
                subject = subject.substring(0, length) + SHORTENER_STRING;
            }
        }
        return subject;
    }

    public String getId() {
        return id;
    }

    public GuardGuestEmailMetadata setId(String newId) {
        this.id = newId;
        return this;
    }

    public String getGuardGuestItemVersion() {
        return version;
    }

    public String getFolderId() {
        return folderId;
    }

    public GuardGuestEmailMetadata setFolderId(String folderId) {
        this.folderId = folderId;
        return this;
    }

    public String getContentType() {
        return contentType;
    }

    public GuardGuestEmailMetadata setContentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public String getMimeType() {
        return mimeType;
    }

    public GuardGuestEmailMetadata setMimeType(String mimeType) {
        this.mimeType = mimeType;
        return this;
    }

    public String[] getFrom() {
        return from;
    }

    public GuardGuestEmailMetadata setFrom(String[] from) {
        this.from = from;
        return this;
    }

    public String[] getTo() {
        return to;
    }

    public GuardGuestEmailMetadata setTo(String[] to) {
        this.to = to;
        return this;
    }

    public String[] getCc() {
        return cc;
    }

    public GuardGuestEmailMetadata setCc(String[] cc) {
        this.cc = cc;
        return this;
    }

    public String[] getBcc() {
        return bcc;
    }

    public GuardGuestEmailMetadata setBcc(String[] bcc) {
        this.bcc = bcc;
        return this;
    }

    public String getSubject() {
        return subject;
    }

    public GuardGuestEmailMetadata setSubject(String subject) {
        this.subject = trimValue(subject, MAX_SUBJECT_LENGTH);
        return this;
    }

    public long getSize() {
        return size;
    }

    public GuardGuestEmailMetadata setSize(long size) {
        this.size = size;
        return this;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public GuardGuestEmailMetadata setSentDate(Date sentDate) {
        this.sentDate = sentDate;
        return this;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public GuardGuestEmailMetadata setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
        return this;
    }

    public String getColorLabel() {
        return colorLabel;
    }

    public GuardGuestEmailMetadata setColorLabel(String colorLabel) {
        this.colorLabel = colorLabel;
        return this;
    }

    public int getMessageFlags() {
        return messageFlags;
    }

    public GuardGuestEmailMetadata setMessageFlags(int messageFlags) {
        this.messageFlags = messageFlags;
        return this;
    }

    public boolean hasMessageFlags(int flags) {
        int comp = messageFlags & flags;
        return comp == flags;
    }
}
