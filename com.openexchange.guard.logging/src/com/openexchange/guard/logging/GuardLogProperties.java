/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.logging;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.slf4j.spi.MDCAdapter;
import com.openexchange.exception.OXException;
import ch.qos.logback.classic.util.LogbackMDCAdapter;

/**
 * 
 * {@link GuardLogProperties}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public final class GuardLogProperties {

    /**
     * Enumeration of log properties' names.
     */
    public static enum Name {
        /**
         * com.openexchange.guard.guest.sessionId
         */
        GUEST_SESSION_ID("com.openexchange.guard.guest.sessionId"),
        /**
         * com.openexchange.guard.guest.userId
         */
        GUEST_USER_ID("com.openexchange.guard.guest.userId"),
        /**
         * com.openexchange.guard.guest.contextId
         */
        GUEST_CONTEXT_ID("com.openexchange.guard.guest.contextId"),
        /**
         * com.openexchange.guard.guest.mail
         */
        GUEST_MAIL("com.openexchange.guard.guest.mail"),
        /**
         * com.openexchange.session.authId
         */
        SESSION_AUTH_ID("com.openexchange.session.authId"),
        /**
         * com.openexchange.session.sessionId
         */
        SESSION_SESSION_ID("com.openexchange.session.sessionId"),
        /**
         * com.openexchange.session.userId
         */
        SESSION_USER_ID("com.openexchange.session.userId"),
        /**
         * com.openexchange.session.userName
         * <p>
         * The session's full login information; e.g. <code>"user1@foobar.org"</code>
         */
        SESSION_USER_NAME("com.openexchange.session.userName"),
        /**
         * com.openexchange.session.contextId
         */
        SESSION_CONTEXT_ID("com.openexchange.session.contextId"),
        /**
         * com.openexchange.session.clientId
         */
        SESSION_CLIENT_ID("com.openexchange.session.clientId"),
        /**
         * com.openexchange.session.session
         */
        SESSION_SESSION("com.openexchange.session.session"),
        /**
         * com.openexchange.session.loginName
         * <p>
         * The session's login name (<i>not</i> the full login string)
         */
        SESSION_LOGIN_NAME("com.openexchange.session.loginName"),

        ;

        private final String name;

        private Name(final String name) {
            this.name = name;
        }

        /**
         * Gets the name
         *
         * @return The name
         */
        public String getName() {
            return name;
        }

        private static final Map<String, Name> STRING2NAME;
        static {
            final Name[] values = Name.values();
            final Map<String, Name> m = new HashMap<String, Name>(values.length);
            for (final Name name : values) {
                m.put(name.getName(), name);
            }
            STRING2NAME = m;
        }

        /**
         * Gets the associated {@code Name} enum.
         *
         * @param sName The name string
         * @return The {@code Name} enum or <code>null</code>
         */
        public static Name nameFor(final String sName) {
            return null == sName ? null : STRING2NAME.get(sName);
        }
    }

    /**
     * Initializes a new {@link GuardLogProperties}.
     */
    private GuardLogProperties() {
        super();
    }

    private static final ConcurrentMap<Class<? extends MDCAdapter>, Field> FIELD_CACHE = new ConcurrentHashMap<Class<? extends MDCAdapter>, Field>(4, 0.9f, 1);

    @SuppressWarnings("unchecked")
    private static InheritableThreadLocal<Map<String, String>> getPropertiesMap(final MDCAdapter mdcAdapter) throws OXException {
        try {
            final Class<? extends MDCAdapter> clazz = mdcAdapter.getClass();
            Field field = FIELD_CACHE.get(clazz);
            if (null == field) {
                for (final Field f : clazz.getDeclaredFields()) {
                    final Class<?> fieldClazz = f.getType();
                    if (InheritableThreadLocal.class.isAssignableFrom(fieldClazz)) {
                        field = f;
                        break;
                    }
                }
                if (null == field) {
                    throw new NoSuchFieldException("InheritableThreadLocal");
                }
                field.setAccessible(true);
                FIELD_CACHE.put(clazz, field);
            }
            return (InheritableThreadLocal<Map<String, String>>) field.get(mdcAdapter);
        } catch (SecurityException e) {
            throw OXException.general(e.getMessage(), e);
        } catch (IllegalArgumentException e) {
            throw OXException.general(e.getMessage(), e);
        } catch (NoSuchFieldException e) {
            throw OXException.general(e.getMessage(), e);
        } catch (IllegalAccessException e) {
            throw OXException.general(e.getMessage(), e);
        }
    }

    /**
     * Gets the properties for current thread.
     * <p>
     * <b>Be careful!</b> Returned map is a read-only reference, not a copy.
     *
     * @return The unmodifiable properties
     */
    public static Map<String, String> getPropertyMap() {
        try {
            final MDCAdapter mdcAdapter = MDC.getMDCAdapter();
            if (mdcAdapter instanceof LogbackMDCAdapter) {
                return ((LogbackMDCAdapter) mdcAdapter).getPropertyMap();
            }
            final org.slf4j.Logger logger = LoggerFactory.getLogger(GuardLogProperties.class);
            logger.warn("Unexpected MDC adapter: {}", mdcAdapter.getClass().getName());
            return Collections.unmodifiableMap(getPropertiesMap(mdcAdapter).get());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Removes all log properties associated with current thread.
     */
    public static void removeLogProperties() {
        MDC.clear();
    }

    /**
     * Puts given (<code>LogProperties.Name</code> + value) pairs to properties.
     *
     * @param args The arguments
     */
    public static void putProperties(final Object... args) {
        if (null == args) {
            return;
        }
        final int length = args.length;
        if ((length % 2) != 0) {
            return;
        }
        for (int i = 0; i < length; i += 2) {
            final GuardLogProperties.Name name = (GuardLogProperties.Name) args[i];
            final Object arg = args[i + 1];
            if (null != arg) {
                MDC.put(name.getName(), arg.toString());
            }
        }
    }

    /**
     * Gets the thread-local log property associated with specified name.
     *
     * @param name The property name
     * @return The log property or <code>null</code> if absent
     */
    public static String get(final GuardLogProperties.Name name) {
        return getLogProperty(name);
    }

    /**
     * Gets the thread-local log property associated with specified name.
     *
     * @param name The property name
     * @return The log property or <code>null</code> if absent
     */
    public static String getLogProperty(final GuardLogProperties.Name name) {
        if (null == name) {
            return null;
        }
        return MDC.get(name.getName());
    }

    /**
     * Appends denoted property
     *
     * @param name The name
     */
    public static void append(GuardLogProperties.Name name, String value) {
        if (null == name || null == value) {
            return;
        }
        String sName = name.getName();
        String prev = MDC.get(sName);
        MDC.put(sName, null == prev ? value : new StringBuilder(prev).append(',').append(value).toString());
    }

    /**
     * Removes denoted property
     *
     * @param name The name
     */
    public static void remove(final GuardLogProperties.Name name) {
        removeProperty(name);
    }

    /**
     * Removes denoted property
     *
     * @param name The name
     */
    public static void removeProperty(final GuardLogProperties.Name name) {
        if (null != name) {
            MDC.remove(name.getName());
        }
    }

    /**
     * Removes denoted properties
     *
     * @param names The names
     */
    public static void removeProperties(final GuardLogProperties.Name... names) {
        if (null != names) {
            for (final Name name : names) {
                MDC.remove(name.getName());
            }
        }
    }

    /**
     * Removes denoted properties
     *
     * @param names The names
     */
    public static void removeProperties(final Collection<GuardLogProperties.Name> names) {
        if (null != names) {
            for (final Name name : names) {
                MDC.remove(name.getName());
            }
        }
    }

    /**
     * Puts specified log property. A <code>null</code> value removes the property.
     *
     * @param name The property name
     * @param value The property value
     */
    public static void put(final GuardLogProperties.Name name, final Object value) {
        putProperty(name, value);
    }

    /**
     * Puts specified log property. A <code>null</code> value removes the property.
     *
     * @param name The property name
     * @param value The property value
     */
    public static void putProperty(final GuardLogProperties.Name name, final Object value) {
        if (null == name) {
            return;
        }
        if (null == value) {
            MDC.remove(name.getName());
        } else {
            MDC.put(name.getName(), value.toString());
        }
    }

    /**
     * Get the thread local LogProperties and pretty-prints them into a Sting.
     * The String will contain one ore more lines formatted like:
     * <pre>
     * "propertyName1=propertyValue1"
     * "propertyName2=propertyValue2"
     * "propertyName3=propertyValue3"
     * </pre>
     * where the properties are sorted alphabetically.
     */
    public static String getAndPrettyPrint() {
        return getAndPrettyPrint(Collections.<GuardLogProperties.Name> emptySet());
    }

    /**
     * Get the thread local LogProperties and pretty-prints them into a Sting.
     * The String will contain one ore more lines formatted like:
     * <pre>
     * "propertyName1=propertyValue1"
     * "propertyName2=propertyValue2"
     * "propertyName3=propertyValue3"
     * </pre>
     * where the properties are sorted alphabetically.
     *
     * @param nonMatching The property name to ignore
     */
    public static String getAndPrettyPrint(final GuardLogProperties.Name nonMatching) {
        return getAndPrettyPrint(EnumSet.<GuardLogProperties.Name> of(nonMatching));
    }

    /**
     * Get the thread local LogProperties and pretty-prints them into a Sting.
     * The String will contain one ore more lines formatted like:
     * <pre>
     * "propertyName1=propertyValue1"
     * "propertyName2=propertyValue2"
     * "propertyName3=propertyValue3"
     * </pre>
     * where the properties are sorted alphabetically.
     *
     * @param nonMatching The property names to ignore
     */
    public static String getAndPrettyPrint(final GuardLogProperties.Name... nonMatching) {
        return getAndPrettyPrint(EnumSet.<GuardLogProperties.Name> copyOf(Arrays.asList(nonMatching)));
    }

    /**
     * Get the thread local LogProperties and pretty-prints them into a Sting.
     * The String will contain one ore more lines formatted like:
     * <pre>
     * "propertyName1=propertyValue1"
     * "propertyName2=propertyValue2"
     * "propertyName3=propertyValue3"
     * </pre>
     * where the properties are sorted alphabetically.
     *
     * @param nonMatching The property names to ignore
     */
    public static String getAndPrettyPrint(final Collection<GuardLogProperties.Name> nonMatching) {
        final Set<String> nonMatchingNames;
        if (null == nonMatching) {
            nonMatchingNames = null;
        } else {
            nonMatchingNames = new HashSet<String>(nonMatching.size());
            for (final GuardLogProperties.Name name : nonMatching) {
                nonMatchingNames.add(name.getName());
            }
        }
        // If we have additional log properties from the ThreadLocal add it to the logBuilder
        final StringBuilder logBuilder = new StringBuilder(1024);
        // Sort the properties for readability
        final Map<String, String> sorted = new TreeMap<String, String>();
        final String sep = System.getProperty("line.separator");
        for (final Entry<String, String> propertyEntry : getPropertyMap().entrySet()) {
            final String propertyName = propertyEntry.getKey();
            if (null == nonMatchingNames || !nonMatchingNames.contains(propertyName)) {
                final String value = propertyEntry.getValue();
                if (null != value) {
                    sorted.put(propertyName, value);
                }
            }
        }
        // And add them to the logBuilder
        for (final Map.Entry<String, String> propertyEntry : sorted.entrySet()) {
            logBuilder.append(propertyEntry.getKey()).append('=').append(propertyEntry.getValue()).append(sep);
        }
        return logBuilder.toString();
    }
}
