/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.smime;

import java.security.AlgorithmParameters;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.certificatemanagement.commons.SmimePrivateKeys;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.smime.exceptions.SmimeExceptionCodes;

/**
 * {@link SmimeCryptoUtil} - Contains cryptographic utility methods for encrypting and decrypting S/MIME private keys
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.6
 */
public class SmimeCryptoKeyUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmimeCryptoKeyUtil.class);
    private static final int KEY_LENGTH = 256;

    /**
     * Internal KDF method to derive a {@link SecretKey} from a given password and salt
     *
     * @param password The password
     * @param salt The salt
     * @return The {@link SecretKey} created for the given password and salt
     * @throws OXException
     */
    private static SecretKey getKey(String password, String salt) throws OXException {
        if (null == password || null == salt) {
            throw SmimeExceptionCodes.SMIME_ERROR.create("No password");
        }
        try {
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1", "BC");
            KeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes("UTF-8"), 5000, KEY_LENGTH);
            SecretKey tmpKey = factory.generateSecret(spec);
            SecretKey secretKey = new SecretKeySpec(tmpKey.getEncoded(), "AES");
            return secretKey;
        } catch (Exception e) {
            throw SmimeExceptionCodes.SMIME_ERROR.create(e.getMessage());
        }
    }

    /**
     * Internal method to encrypt
     *
     * @param type The type
     * @param data The data to encrypt
     * @param secretKey The key to use
     * @return The encrypted data as encoded string
     */
    private static String encrypt(String type, byte[] data, Key secretKey) {
        try {

            //TODO-SMIME-CRYPTO: The given type is actually not being used for encryption and only prefixed with the returned data ?

            Cipher cipher = getCipher();
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            AlgorithmParameters parameters = cipher.getParameters();

            byte[] iv = parameters.getParameterSpec(IvParameterSpec.class).getIV();
            byte[] ciphertext = cipher.doFinal(data);

            String encrypted = java.util.Base64.getEncoder().encodeToString(ciphertext);
            String iVString = Base64.getEncoder().encodeToString(iv);
            String combo = type + "!" + iVString + encrypted;

            return combo;
        } catch (Exception e) {
            LOGGER.trace("Unable to encrypt", e);
            return "";
        }
    }

    /**
     * Internal method to decrypt
     *
     * @param data The encoded data to decrypt
     * @param secretKey The key to use
     * @return The decrypted data as
     */
    private static byte[] decrypt(String data, Key secretKey) {
        try {
            Cipher ciper = getCipher();

            byte[] iv64 = data.substring(0, 24).getBytes("UTF-8");
            byte[] iv = Base64.getDecoder().decode(iv64);

            final String encryptedData = data.substring(24);
            byte[] databytes = Base64.getDecoder().decode(encryptedData);

            ciper.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
            return ciper.doFinal(databytes);
        } catch (Exception e) {
            // Bad password
            LOGGER.trace("Unable to decrypt", e);
            return null;
        }
    }

    /**
     * Internal method to get the actual symmetric {@link Cipher} used for encryption
     *
     * @return The {@link Cipher} which should be used for encryption
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     */
    private static Cipher getCipher() throws NoSuchAlgorithmException, NoSuchPaddingException {
        return Cipher.getInstance("AES/CBC/PKCS5Padding");
    }

    /**
     * Symmetrically encrypts the given {@link PrivateKey} with a salted password
     *
     * @param key The key to encrypt
     * @param password The password to use for encryption
     * @param salt The salt
     * @return The encrypted key as base64 encoded data, prefixed with the algorithm (TODO-SMIME-CRYPTO: the algorithm is actually not being used for encryption?!) and IV used
     * @throws OXException
     */
    public static String encryptPrivateKey(PrivateKey key, String password, String salt) throws OXException {
        try {
            return encrypt(key.getAlgorithm(), key.getEncoded(), getKey(CipherUtil.getSHA(password, salt), salt));
        } catch (Exception e) {
            throw SmimeExceptionCodes.SMIME_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Checks if the password is able to decrypt the private part of the given {@link SmimeKey}
     * <p>
     * This is a convenience method for calling {@link #decryptPrivateKey(SmimeKeys, String)} and catching the {@link GuardAuthExceptionCodes.BAD_PASSWORD} exception.
     *
     * @param encryptedKey The {@link SmimeKey} containing the private part decrypt
     * @param password The password to check
     * @return <code>True</code> if the given password is able to decrypt the private part of the key, <code>false</code> otherwise.
     * @throws OXException due an decryption error
     */
    public static boolean canDecryptPrivateKey(SmimeKeys encryptedKey, String password) throws OXException {
        return canDecryptPrivateKey(encryptedKey.getPrivateKey(), password);
    }

    /**
     * Checks if the password is able to decrypt the {@link SmimePrivateKeys}
     * <p>
     * This is a convenience method for calling {@link #decryptPrivateKey(SmimePrivateKeys, String)} and catching the {@link GuardAuthExceptionCodes.BAD_PASSWORD} exception.
     *
     * @param encryptedKey The {@link SmimePrivateKeys}
     * @param password The password to check
     * @return <code>True</code> if the given password is able to decrypt the private key, <code>false</code> otherwise.
     * @throws OXException due an decryption error
     */
    public static boolean canDecryptPrivateKey(SmimePrivateKeys encryptedKey, String password) throws OXException {
        try {
            decryptPrivateKey(encryptedKey, password);
            return true;
        } catch (OXException e) {
            if (e.similarTo(GuardAuthExceptionCodes.BAD_PASSWORD)) {
                return false;
            }
            throw e;
        }
    }

    /**
     * Decrypts {@link SmimeKeys}' private key data to {@link PrivateKey} instance.
     * 
     * <p>
     * This is a convenience method for calling
     * <code>
     * decryptPrivateKey(encryptedKey.getPrivateKey(), password);
     * </code>
     * </p>
     *
     * @param encryptedKey The {@link SmimeKeys} containing the private key to decrypt
     * @param password The password required for decryption. It will be converted in a hash before it is used as password.
     * @return The decrypted {@link PrivateKey} instance
     * @throws OXException The exception code is {@link GuardAuthExceptionCodes#BAD_PASSWORD} in case the password was wrong, or another code due an unexpected error
     */
    public static PrivateKey decryptPrivateKey(SmimeKeys encryptedKey, String password) throws OXException {
        return decryptPrivateKey(encryptedKey.getPrivateKey(), password);
    }

    /**
     * Decrypts {@link SmimePrivateKey}'s key data to a {@link PrivateKey} instance
     *
     * @param encryptedPrivateKey The {@link SmimePrivateKey} to decrypt
     * @param password The password required for decryption. It will be converted in a hash before it is used as password.
     * @return The decrypted {@link PrivateKey} instance
     * @throws OXException The exception code is {@link GuardAuthExceptionCodes#BAD_PASSWORD} in case the password was wrong, or another code due an unexpected error
     */
    public static PrivateKey decryptPrivateKey(SmimePrivateKeys encryptedPrivateKey, String password) throws OXException {
        return decryptPrivateKey(encryptedPrivateKey.getEncryptedKeyData(), password, encryptedPrivateKey.getSalt());
    }

    /**
     * Decrypts raw encrypted data to a {@link PrivateKey} instance
     * 
     * @param encryptedPrivateKeyData The data to decrypt
     * @param password The password required for decryption. It will be converted in a hash before it is used as password.
     * @param salt The salt required
     * @return The decrypted {@link PrivateKey} instance
     * @throws OXException The exception code is {@link GuardAuthExceptionCodes#BAD_PASSWORD} in case the password was wrong, or another code due an unexpected error
     */
    public static PrivateKey decryptPrivateKey(String encryptedPrivateKeyData, String password, String salt) throws OXException {
        return decryptPrivateKey(encryptedPrivateKeyData, password, salt, true);
    }

    /**
     * Decrypts raw encrypted data to a {@link PrivateKey} instance
     *
     * @param encryptedPrivateKeyData The data to decrypt
     * @param password The password required for decryption
     * @param salt The salt required
     * @param hashPassword <code>True</code> if the given password should be hashed before using it, or <code>False</code> if the given password should be used as stated.
     * @return The decrypted {@link PrivateKey} instance
     * @throws OXException The exception code is {@link GuardAuthExceptionCodes#BAD_PASSWORD} in case the password was wrong, or another code due an unexpected error
     */
    public static PrivateKey decryptPrivateKey(String encryptedPrivateKeyData, String password, String salt, boolean hashPassword) throws OXException {

        if (encryptedPrivateKeyData.indexOf("!") < 0) {
            throw SmimeExceptionCodes.CRYPTO_ERROR.create("Bad encr data");
        }

        try {
            String type = encryptedPrivateKeyData.substring(0, encryptedPrivateKeyData.indexOf("!"));
            String encrData = encryptedPrivateKeyData.substring(encryptedPrivateKeyData.indexOf("!") + 1);
            String actualPassword = hashPassword ? CipherUtil.getSHA(password, salt) : password;
            byte[] decrypted = decrypt(encrData, getKey(actualPassword, salt));
            if (decrypted != null) {
                KeyFactory keyFact = KeyFactory.getInstance(type, "BC");
                return keyFact.generatePrivate(new PKCS8EncodedKeySpec(decrypted));
            }
        } catch (Exception e) {
            throw SmimeExceptionCodes.CRYPTO_ERROR.create(e, "Error decrypting private key");
        }
        throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
    }
}
