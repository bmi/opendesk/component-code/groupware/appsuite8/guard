/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime;

import com.openexchange.guard.configuration.GuardProp;

/**
 * {@link SmimeProperty} defines all S/MIME properties. Please keep this enum up-to-date.
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 */
public enum SmimeProperty implements GuardProp {

    /**
     * If the Certificate authority database storage should be started
     */
    caDatabase(Boolean.TRUE),
    /**
     * Group ID associated with trusted certificate authorities
     */
    caGroupId(Integer.valueOf(0)),
    /**
     * Check X509 certificates against revoke lists
     */
    checkCRL(Boolean.FALSE),
    /**
     * If the database storage for smime should be started for public and private key storage
     */
    databaseStorage(Boolean.TRUE),

    ;

    private static final String EMPTY = "";

    private static final String PREFIX = "com.openexchange.smime.";

    private final String fqn;

    private final Object defaultValue;

    private SmimeProperty() {
        this(EMPTY);
    }

    private SmimeProperty(Object defaultValue) {
        this(defaultValue, PREFIX);
    }

    private SmimeProperty(Object defaultValue, String fqn) {
        this.defaultValue = defaultValue;
        this.fqn = fqn;
    }

    /**
     * Returns the fully qualified name of the property
     *
     * @return the fully qualified name of the property
     */
    @Override
    public String getFQPropertyName() {
        return fqn + name();
    }

    /**
     * Returns the default value of this property
     *
     * @return the default value of this property
     */
    @Override
    public <T extends Object> T getDefaultValue(Class<T> cls) {
        if (defaultValue.getClass().isAssignableFrom(cls)) {
            return cls.cast(defaultValue);
        }
        throw new IllegalArgumentException("The object cannot be converted to the specified type '" + cls.getCanonicalName() + "'");
    }
}
