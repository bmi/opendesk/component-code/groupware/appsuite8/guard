package com.openexchange.guard.backend.mailfilter;

import org.slf4j.Logger;
import com.openexchange.guard.backend.mailfilter.sieve.GuardEncryptAction;
import com.openexchange.guard.backend.mailfilter.sieve.GuardSignatureExecuteTest;
import com.openexchange.mail.filter.json.v2.json.mapper.parser.action.external.FilterActionRegistry;
import com.openexchange.mail.filter.json.v2.json.mapper.parser.action.external.SieveFilterAction;
import com.openexchange.mail.filter.json.v2.json.mapper.parser.test.external.ExecuteTestRegistry;
import com.openexchange.mail.filter.json.v2.json.mapper.parser.test.external.SieveExecuteTest;
import com.openexchange.osgi.HousekeepingActivator;

public class Activator extends HousekeepingActivator {

    private static Logger logger = org.slf4j.LoggerFactory.getLogger(Activator.class);

    @Override
    protected Class<?>[] getNeededServices() {
        return null;
    }

    @Override
    protected void startBundle() throws Exception {
        // Sieve tests
        logger.info("Starting Guard MailFilter bundle");
        trackService(FilterActionRegistry.class);
        trackService(ExecuteTestRegistry.class);
        registerService(SieveExecuteTest.class, new GuardSignatureExecuteTest(this));
        registerService(SieveFilterAction.class, new GuardEncryptAction(this));

        openTrackers();
    }

}
