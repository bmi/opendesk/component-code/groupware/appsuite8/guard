/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.backend.mailfilter.sieve;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.jsieve.SieveException;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.jsieve.commands.JSONMatchType;
import com.openexchange.jsieve.commands.MatchType;
import com.openexchange.jsieve.commands.TestCommand;
import com.openexchange.mail.filter.json.v2.json.fields.BodyTestField;
import com.openexchange.mail.filter.json.v2.json.fields.GeneralField;
import com.openexchange.mail.filter.json.v2.json.mapper.parser.CommandParserJSONUtil;
import com.openexchange.mail.filter.json.v2.json.mapper.parser.test.NotTestCommandUtil;
import com.openexchange.mail.filter.json.v2.json.mapper.parser.test.external.ExecuteTestRegistry;
import com.openexchange.mail.filter.json.v2.json.mapper.parser.test.external.SieveExecuteTest;
import com.openexchange.mail.filter.json.v2.mapper.ArgumentUtil;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link GuardSignatureExecuteTest}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v7.10.4
 */
public class GuardSignatureExecuteTest implements SieveExecuteTest {

    /**
     * Name of the Test
     */
    private static final String NAME = "guard_verify";
    /**
     * Signature is simply an is match type
     */
    private static final MatchType type = MatchType.is;

    private final ServiceLookup services;


    /**
     * Required Guard capability
     */
    private static final String GUARD_CAPABILITY = "guard-mail";

    /**
     * Initializes a new {@link GuardSignatureExecuteTest}.
     *
     * @param services The {@link ServiceLookup}
     * @param command The {@link ExecuteTestRegistry}
     */
    public GuardSignatureExecuteTest(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Gets the {@link ExecuteTestRegistry}
     *
     * @return
     * @throws OXException
     */
    private ExecuteTestRegistry getRegistry() throws OXException {
        return services.getServiceSafe(ExecuteTestRegistry.class);
    }

    /**
     * Return the LeanConfigurationService
     *
     * @return The {@link LeanConfigurationService}
     * @throws OXException
     */
    private LeanConfigurationService getConfig() throws OXException {
        return services.getServiceSafe(LeanConfigurationService.class);
    }

    /**
     * Return the configured Script name
     *
     * @return
     * @throws OXException
     */
    private String getScript() throws OXException {
        return getConfig().getProperty(SieveConfig.guardSignatureScript);
    }

    /**
     * Gets the capability service
     *
     * @return The {@link CapabilityService}
     * @throws OXException if unavailable
     */
    private CapabilityService getCapabilityService() throws OXException {
        return services.getServiceSafe(CapabilityService.class);
    }

    @Override
    public String getJsonName() {
        return NAME;
    }

    /**
     * Whether this test is enabled or not
     *
     * @return <code>true</code> if it is enabled, <code>false</code> otherwise
     * @throws OXException
     */
    private boolean isEnabled() throws OXException {
        return getConfig().getBooleanProperty(SieveConfig.sieveEnabled);
    }

    @Override
    public boolean isCommandSupported(Set<String> capabilities) throws OXException {
        if (capabilities.contains("vnd.dovecot.execute")) {
            return isEnabled();
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean isApplicable(TestCommand command) throws OXException {
        List<Object> args = command.getArguments();
        // Confirm this execute command refers to the signature verification script
        return ((args.size() == 3) && (args.get(1) instanceof List) && ((List<String>) args.get(1)).contains(getScript()));
    }

    @Override
    public void parse(JSONObject jsonObject, TestCommand command, boolean transformToNotMatcher) throws OXException {
        try {
            if (transformToNotMatcher) {
                jsonObject.put(BodyTestField.comparison.name(), type.getNotName());
            } else {
                jsonObject.put(BodyTestField.comparison.name(), type.toString());
            }
            jsonObject.put(GeneralField.id.name(), NAME);
        } catch (JSONException e) {
            throw OXException.general("Problem creating Signature test", e);
        }
    }

    @Override
    public boolean isApplicable(JSONObject jsonObject) throws OXException {
        try {
            return (jsonObject.has(GeneralField.id.name()) && jsonObject.get(GeneralField.id.name()).equals(NAME));
        } catch (JSONException e) {
            throw OXException.general("Problem testing for Signature filter test", e);
        }
    }

    @Override
    public TestCommand parse(JSONObject jsonObject, ServerSession session) throws OXException {
        try {
            // Verify permission
            if (getCapabilityService().getCapabilities(session.getUserId(), session.getContextId()).contains(GUARD_CAPABILITY)) {
                String matcher = "";
                if (jsonObject.has(BodyTestField.comparison.name())) {
                    matcher = CommandParserJSONUtil.getString(jsonObject, BodyTestField.comparison.name(), NAME);
                }
                boolean negated = false;
                if (MatchType.is.getNotName().equals(matcher)) {
                    negated = true;
                }
                final List<Object> argList = new ArrayList<Object>();
                argList.add(ArgumentUtil.createTagArgument("pipe"));
                argList.add(CommandParserJSONUtil.stringToList(getScript()));
                final List<String> args = new ArrayList<String>(2);
                args.add(Integer.toString(session.getUserId()));
                args.add(Integer.toString(session.getContextId()));
                argList.add(args);
                if (negated) {
                    return NotTestCommandUtil.wrapTestCommand(new TestCommand(getRegistry(), argList, new ArrayList<TestCommand>()));
                }
                return new TestCommand(getRegistry(), argList, new ArrayList<TestCommand>());
            }
        } catch (SieveException e) {
            throw OXException.general("Problem testing signature", e);
        }
        throw OXException.noPermissionForModule("Guard");
    }

    private static final Map<String, String> ARGUMENTS;
    private static final Map<String, String> MATCH_TYPES;
    private static final List<JSONMatchType> JSON_MATCH_TYPES;

    static {
        // Arguments
        Map<String, String> args = new HashMap<>();
        args.put(":pipe", "");
        ARGUMENTS = Collections.unmodifiableMap(args);

        // Match Types
        final Map<String, String> is_match_types = new HashMap<String, String>(1);
        is_match_types.put(MatchType.is.getArgumentName(), MatchType.is.getRequire());
        MATCH_TYPES = Collections.unmodifiableMap(is_match_types);

        //JSON match types
        final List<JSONMatchType> json_match_types = Collections.synchronizedList(new ArrayList<JSONMatchType>(2));
        // add normal matcher
        json_match_types.add(new JSONMatchType(MatchType.is.name(), MatchType.is.getRequire(), 0));
        // add not matcher
        json_match_types.add(new JSONMatchType(MatchType.is.getNotName(), MatchType.is.getRequire(), 2));
        JSON_MATCH_TYPES = Collections.unmodifiableList(json_match_types);
    }


    @Override
    public Map<String, String> getOtherArguments() {
        return ARGUMENTS;
    }

    @Override
    public Map<String, String> getMatchTypes() {
        return MATCH_TYPES;
    }

    @Override
    public List<JSONMatchType> getJsonMatchTypes() {
        return JSON_MATCH_TYPES;
    }

}
