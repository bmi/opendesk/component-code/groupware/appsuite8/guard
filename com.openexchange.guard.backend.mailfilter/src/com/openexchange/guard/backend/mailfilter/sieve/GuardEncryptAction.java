/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.backend.mailfilter.sieve;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import org.apache.jsieve.SieveException;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.jsieve.commands.ActionCommand;
import com.openexchange.mail.filter.json.v2.json.fields.GeneralField;
import com.openexchange.mail.filter.json.v2.json.mapper.parser.CommandParserJSONUtil;
import com.openexchange.mail.filter.json.v2.json.mapper.parser.action.external.FilterActionRegistry;
import com.openexchange.mail.filter.json.v2.json.mapper.parser.action.external.SieveFilterAction;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link GuardEncryptAction}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v7.10.4
 */
public class GuardEncryptAction implements SieveFilterAction {

    /**
     * Required Guard capability
     */
    private final static String GUARD_CAPABILITY = "guard-mail";
    private final ServiceLookup services;
    /*
     * Name of this Action
     */
    private static final String NAME = "guard_encrypt";

    public GuardEncryptAction(ServiceLookup services) {
        this.services = services;
    }

    @Override
    public String getJsonName() {
        return NAME;
    }

    /**
     * Gets the {@link FilterActioNRegistry}
     *
     * @return
     * @throws OXException
     */
    private FilterActionRegistry getRegistry() throws OXException {
        return services.getServiceSafe(FilterActionRegistry.class);
    }

    /**
     * Return the LeanConfigurationService
     *
     * @return The {@link LeanConfigurationService}
     * @throws OXException
     */
    private LeanConfigurationService getConfig() throws OXException {
        return services.getServiceSafe(LeanConfigurationService.class);
    }

    /**
     * Obtains the defined script from configuration
     *
     * @return The script name
     * @throws OXException
     */
    private String getScript() throws OXException {
        return getConfig().getProperty(SieveConfig.guardEncryptScript);
    }

    /**
     * Checks whether this action is enabled or not
     *
     * @return <code>true</code> if it is enabled, <code>false</code> otherwise
     * @throws OXException
     */
    private boolean isEnabled() throws OXException {
        return getConfig().getBooleanProperty(SieveConfig.sieveEnabled);
    }

    @Override
    public boolean isCommandSupported(Set<String> capabilities) throws OXException {
        if (capabilities.contains("vnd.dovecot.filter")) {
            return isEnabled();
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean isApplicable(ActionCommand actionCommand) throws OXException {
        final ArrayList<Object> arguments = actionCommand.getArguments();
        // Confirm that this Filter command refers to the encrypt action
        return (!arguments.isEmpty() && arguments.get(0) instanceof ArrayList && ((ArrayList<String>) arguments.get(0)).contains(getScript()));
    }

    @Override
    public void parse(JSONObject jsonObject, ActionCommand actionCommand) throws OXException {
        try {
            jsonObject.put(GeneralField.id.name(), NAME);
        } catch (JSONException e) {
            throw OXException.general("Problem creating mailfilter rule", e);
        }
    }

    @Override
    public boolean isApplicable(JSONObject jsonObject) throws OXException {
        try {
            return (jsonObject.has("id") && jsonObject.get("id").equals(NAME));
        } catch (JSONException e) {
            throw OXException.general("Error testing Encrypt Action", e);
        }
    }

    @Override
    public ActionCommand parse(JSONObject jsonObject, ServerSession session) throws OXException {
        // Verify permission
        if (services.getServiceSafe(CapabilityService.class).getCapabilities(session.getUserId(), session.getContextId()).contains(GUARD_CAPABILITY)) {
            try {
                // Add required arguments, name of the script, and the user Id and context
                final ArrayList<Object> arrayList = new ArrayList<Object>();
                arrayList.add(CommandParserJSONUtil.stringToList(getScript()));
                final List<String> args = new ArrayList<String>(2);
                args.add(Integer.toString(session.getUserId()));
                args.add(Integer.toString(session.getContextId()));
                arrayList.add(args);
                return new ActionCommand(getRegistry(), arrayList);
            } catch (SieveException ex) {
                throw OXException.general("Problem executing encryption sieve filter", ex);
            }
        }
        throw OXException.noPermissionForModule("guard");  // Doesn't have the capability to perform this action
    }

    @Override
    public Hashtable<String, Integer> getTagArgs() {
        return new Hashtable<String, Integer>();
    }

}
