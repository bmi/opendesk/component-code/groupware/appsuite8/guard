/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.update.osgi;

import org.slf4j.Logger;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.keymanagement.services.KeyCreationService;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.guard.settings.GlobalSettingsStorage;
import com.openexchange.guard.update.GuardUpdateService;
import com.openexchange.guard.update.internal.GuardKeysUpdater;
import com.openexchange.guard.update.internal.GuardUpdateServiceImpl;
import com.openexchange.guard.update.keys.MissingPGPPreferences;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link UpdateActivator}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 2.4.0
 */
public class UpdateActivator extends HousekeepingActivator {

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { GlobalSettingsStorage.class, GuardConfigurationService.class, MasterKeyService.class,
            KeyCreationService.class, KeyTableStorage.class };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void startBundle() throws Exception {
        Logger logger = org.slf4j.LoggerFactory.getLogger(UpdateActivator.class);
        logger.info("Starting bundle: {}", context.getBundle().getSymbolicName());

        Services.setServiceLookup(this);

        GuardKeysUpdater updater = new GuardKeysUpdater(this.getService(KeyTableStorage.class))
            .addUpgradeService(new MissingPGPPreferences(this.getService(KeyTableStorage.class)));
        registerService(GuardKeysUpdater.class, updater);
        registerService(GuardUpdateService.class, new GuardUpdateServiceImpl());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void stopBundle() throws Exception {
        Logger logger = org.slf4j.LoggerFactory.getLogger(UpdateActivator.class);
        logger.info("Stopping bundle: {}", this.context.getBundle().getSymbolicName());
        unregisterService(GuardUpdateService.class);

        Services.setServiceLookup(null);

        super.stopBundle();
    }
}
