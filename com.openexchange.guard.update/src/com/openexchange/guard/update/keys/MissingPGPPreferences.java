/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.update.keys;

import java.util.Date;
import java.util.Iterator;
import org.bouncycastle.bcpg.CompressionAlgorithmTags;
import org.bouncycastle.bcpg.HashAlgorithmTags;
import org.bouncycastle.bcpg.SymmetricKeyAlgorithmTags;
import org.bouncycastle.bcpg.UserIDPacket;
import org.bouncycastle.bcpg.sig.Features;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureGenerator;
import org.bouncycastle.openpgp.PGPSignatureSubpacketGenerator;
import org.bouncycastle.openpgp.PGPSignatureSubpacketVector;
import org.bouncycastle.openpgp.operator.bc.BcPGPContentSignerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.util.PGPUtil;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.guard.update.KeyUpgradeTask;

/**
 * Update task to check the userID signature preferences
 * If missing, add required preferences
 * Add MDC flag if missing
 * {@link MissingPGPPreferences}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class MissingPGPPreferences implements KeyUpgradeTask {

    private static final int[] PREFERRED_SYMETRIC_ALGORITHMS = new int[] { SymmetricKeyAlgorithmTags.AES_256, SymmetricKeyAlgorithmTags.AES_192, SymmetricKeyAlgorithmTags.AES_128 };
    private static int[] PREFERRED_HASH_ALGORITHMS = new int[] { HashAlgorithmTags.SHA512, HashAlgorithmTags.SHA384, HashAlgorithmTags.SHA256, HashAlgorithmTags.SHA224, HashAlgorithmTags.SHA1 };
    private static int[] PREFERRED_COPMRESSION = new int[] { CompressionAlgorithmTags.ZLIB, CompressionAlgorithmTags.BZIP2, CompressionAlgorithmTags.ZIP, CompressionAlgorithmTags.UNCOMPRESSED };


    private final KeyTableStorage ogKeyTableStorage;
    private static final Logger LOG = LoggerFactory.getLogger(MissingPGPPreferences.class);

    public MissingPGPPreferences (KeyTableStorage ogKeyTableStorage) {
        this.ogKeyTableStorage = ogKeyTableStorage;
    }
    /* (non-Javadoc)
     * @see com.openexchange.guard.update.KeyUpgradeService#handles(int)
     */
    @Override
    public boolean handles(int version) {
        if (version == 1) {
            return true;
        }
        return false;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.update.KeyUpgradeService#doUpgrade(com.openexchange.guard.keymanagement.commons.GuardKeys, java.lang.String)
     */
    @Override
    public void doUpgrade(GuardKeys key, String password) throws OXException {
        PGPSecretKey masterSecretKey = key.getPGPSecretKey();
        if(masterSecretKey == null || masterSecretKey.isPrivateKeyEmpty()) {
           LOG.info("No private key available for upgrade of public key preferences.  Skipping");
           return;
        }
        if (masterSecretKey.getPublicKey().getAlgorithm() > 3) {  // Only applies to RSA keys.  Return if not RSA
            return;
        }
        //Getting the private key used for signing
        PGPPrivateKey signingPrivateKey = null;
        try {
            signingPrivateKey = PGPUtil.decodePrivate(masterSecretKey, password, key.getSalt());
        } catch (PGPException e) {
            throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
        }
        Iterator<PGPPublicKey> publicKeys = key.getPGPPublicKeyRing().getPublicKeys();

        while(publicKeys.hasNext()) {
            PGPPublicKey pkey = publicKeys.next();
            PGPPublicKey modifiedKey = pkey;
            boolean modified = false;
            Iterator<byte[]> userIds = pkey.getRawUserIDs();
            while (userIds.hasNext()) {
                byte[] rawId = userIds.next();
                UserIDPacket userpacket = new UserIDPacket(rawId);
                Iterator<PGPSignature> sigs = pkey.getSignaturesForID(rawId);
                while (sigs.hasNext()) {
                    PGPSignature sig = sigs.next();
                    if (sig.isCertification() && sig.getKeyID() == key.getKeyid()) {
                        int[] preferedSymmetricAlgorithms = getPreferedSymmetricAlgorithms(sig);
                        // If no preferredSymmetricAlgorithms, or missing MDC flag, do correction
                        if(preferedSymmetricAlgorithms != null &&
                            (preferedSymmetricAlgorithms.length == 0 || !hasMDCCheck(sig))) {
                            PGPPublicKey p = PGPPublicKey.removeCertification(modifiedKey, sig);
                            if(p != null) {
                                modifiedKey = p;
                                try {
                                    modifiedKey = addPreferredSettings(
                                        modifiedKey,
                                        userpacket.getID(),
                                        signingPrivateKey,
                                        getValidSubpacket(pkey, sig));
                                    LOG.info("Key Update done, added missing preferences/mdc for key with userId " + userpacket.getID() + " KeyId: " + p.getKeyID() + " Signature:" + sig.hashCode());
                                } catch (PGPException e) {
                                    throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e);
                                }
                                modified = true;
                            }
                        }
                    }
                }
            }
            if (modified) {
                key.setPGPPublicKeyRing(PGPPublicKeyRing.insertPublicKey(key.getPGPPublicKeyRing(), modifiedKey));
                ogKeyTableStorage.updatePublicAndPrivateKey(key);
            }
        }
    }

    /**
     * Get the last valid SubPacketVector in the key
     * @param pkey
     * @param signature
     * @return
     */
    private PGPSignatureSubpacketVector getValidSubpacket (PGPPublicKey pkey, PGPSignature signature) {
        if (signature.getHashedSubPackets() != null) {
            int[] preferedSymmetricAlgorithms = getPreferedSymmetricAlgorithms(signature);
            if (preferedSymmetricAlgorithms != null && preferedSymmetricAlgorithms.length > 0) {
                return signature.getHashedSubPackets();
            }
        }
        Iterator<byte[]> userIds = pkey.getRawUserIDs();
        PGPSignatureSubpacketVector lastValid = null;
        Date mostRecentSignatureDate = null;
        while (userIds.hasNext()) {
            byte[] rawId = userIds.next();
            Iterator<PGPSignature> sigs = pkey.getSignaturesForID(rawId);
            while (sigs.hasNext()) {
                PGPSignature sig = sigs.next();
                if (sig.getKeyID() == pkey.getKeyID() && sig.getHashedSubPackets() != null) {
                    int[] preferedSymmetricAlgorithms = getPreferedSymmetricAlgorithms(sig);
                    if (preferedSymmetricAlgorithms != null && preferedSymmetricAlgorithms.length > 0 && sig.getHashedSubPackets().getKeyFlags() != 0) {
                        if (sig.getHashedSubPackets().isPrimaryUserID()) {
                            return sig.getHashedSubPackets();
                        }
                        Date signatureCreationTime = sig.getHashedSubPackets().getSignatureCreationTime();
                        if (mostRecentSignatureDate == null || (signatureCreationTime != null && signatureCreationTime.getTime() > mostRecentSignatureDate.getTime()) ) {
                            mostRecentSignatureDate = signatureCreationTime;
                            lastValid = sig.getHashedSubPackets();
                        }
                    }
                }
            }
        }
        return lastValid;
    }

    /**
     * Get the symmetricAlgorithms for a signature
     * @param signature
     * @return
     */
    private int[] getPreferedSymmetricAlgorithms(PGPSignature signature) {
        PGPSignatureSubpacketVector hashedSubPackets = signature.getHashedSubPackets();
        if(hashedSubPackets != null) {
            int[] ret = hashedSubPackets.getPreferredSymmetricAlgorithms();
            if(ret == null) {
                return new int[] {};
            }
            else {
                return ret;
            }
        }
        return null;
    }

    /**
     * Check if signature has the MDC flag set
     * @param signature
     * @return
     */
    private boolean hasMDCCheck (PGPSignature signature) {
        PGPSignatureSubpacketVector hashedSubPackets = signature.getHashedSubPackets();
        return hashedSubPackets.getFeatures() != null && hashedSubPackets.getFeatures().supportsFeature(Features.FEATURE_MODIFICATION_DETECTION);
    }

    /**
     * Add the required settings to the signature.
     * Must be the users signature (it will be resigned)
     * @param publicKey
     * @param userId
     * @param privateKey
     * @param keyFlags
     * @return
     * @throws PGPException
     */
    private PGPPublicKey addPreferredSettings(PGPPublicKey publicKey, String userId, PGPPrivateKey privateKey, PGPSignatureSubpacketVector hashedSubPackets) throws PGPException {
        PGPSignatureSubpacketGenerator signatureSubpacketGenerator = new PGPSignatureSubpacketGenerator();
        if (hashedSubPackets != null && hashedSubPackets.getPreferredSymmetricAlgorithms() != null && hashedSubPackets.getPreferredSymmetricAlgorithms().length > 0) {
            signatureSubpacketGenerator.setPreferredSymmetricAlgorithms(false, hashedSubPackets.getPreferredSymmetricAlgorithms());
        } else {
            signatureSubpacketGenerator.setPreferredSymmetricAlgorithms(false, PREFERRED_SYMETRIC_ALGORITHMS);
        }
        if (hashedSubPackets != null && hashedSubPackets.getPreferredHashAlgorithms() != null && hashedSubPackets.getPreferredHashAlgorithms().length > 0) {
            signatureSubpacketGenerator.setPreferredHashAlgorithms(false, hashedSubPackets.getPreferredHashAlgorithms());
        } else {
            signatureSubpacketGenerator.setPreferredHashAlgorithms(false, PREFERRED_HASH_ALGORITHMS);
        }
        if (hashedSubPackets != null && hashedSubPackets.getPreferredCompressionAlgorithms() != null && hashedSubPackets.getPreferredCompressionAlgorithms().length > 0) {
            signatureSubpacketGenerator.setPreferredCompressionAlgorithms(false, hashedSubPackets.getPreferredCompressionAlgorithms());
        } else {
            signatureSubpacketGenerator.setPreferredCompressionAlgorithms(false, PREFERRED_COPMRESSION);
        }
        signatureSubpacketGenerator.setFeature(true, Features.FEATURE_MODIFICATION_DETECTION);
        if (hashedSubPackets != null) {
            signatureSubpacketGenerator.setKeyFlags(false, hashedSubPackets.getKeyFlags());
            signatureSubpacketGenerator.setKeyExpirationTime(true, hashedSubPackets.getKeyExpirationTime());
            signatureSubpacketGenerator.setSignatureExpirationTime(false, hashedSubPackets.getSignatureExpirationTime());
            if (hashedSubPackets.getSignatureCreationTime() != null) {
                signatureSubpacketGenerator.setSignatureCreationTime(true, hashedSubPackets.getSignatureCreationTime());
            }
        }

        PGPSignatureSubpacketVector symAlgoSignature = signatureSubpacketGenerator.generate();

        //Create a wrapping signature around it
        PGPSignatureGenerator generator = new PGPSignatureGenerator(
            new BcPGPContentSignerBuilder(PGPPublicKey.RSA_GENERAL, org.bouncycastle.openpgp.PGPUtil.SHA1));
        generator.init(PGPSignature.POSITIVE_CERTIFICATION, privateKey);
        generator.setHashedSubpackets(symAlgoSignature);
        PGPSignature finalSignature = generator.generateCertification(userId, publicKey);
        //Add the signature and return a new modified key
        return PGPPublicKey.addCertification(publicKey, userId, finalSignature);
    }

}
