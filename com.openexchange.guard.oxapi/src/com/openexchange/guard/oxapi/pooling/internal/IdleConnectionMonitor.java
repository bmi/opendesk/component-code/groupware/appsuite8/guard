/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi.pooling.internal;

import java.util.concurrent.TimeUnit;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.pool.ConnPoolControl;
import org.apache.http.pool.PoolStats;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.oxapi.osgi.Services;

public class IdleConnectionMonitor extends Thread {

    private static Logger LOG = LoggerFactory.getLogger(IdleConnectionMonitor.class);

    private final HttpClientConnectionManager connMgr;
    private final ConnPoolControl<HttpRoute> poolControl;
    private volatile boolean shutdown;
    private int lastcount = 0;
    private final String name;

    public IdleConnectionMonitor(HttpClientConnectionManager connMgr, ConnPoolControl<HttpRoute> poolControl, String name) {
        super();
        this.connMgr = connMgr;
        this.poolControl = poolControl;
        this.name = name;
    }

    @Override
    public void run() {
        try {
            while (!shutdown) {
                synchronized (this) {
                    wait(2000);
                    getStats();
                    connMgr.closeExpiredConnections();
                    connMgr.closeIdleConnections(Services.getService(GuardConfigurationService.class).getIntProperty(GuardProperty.oxBackendIdleTime), TimeUnit.SECONDS);
                }
            }
        } catch (InterruptedException ex) {
            shutdown();
        } catch (Exception e) {
            LOG.debug("Error in idle monitor loop", e);
        }
    }

    public void shutdown() {
        synchronized (this) {
            shutdown = true;
            notifyAll();
        }
    }

    private void getStats() {
        PoolStats stats =  poolControl.getTotalStats();
        int total = stats.getAvailable() + stats.getLeased();
        if (total != lastcount) {
            LOG.debug("HTTP " + name + " pool connections- In Pool: " + stats.getAvailable() + " Active: " + stats.getLeased() + " Hold: " + stats.getPending());
        }
        lastcount = total;
    }

}
