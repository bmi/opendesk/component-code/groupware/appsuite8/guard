/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.java.Strings;
import com.openexchange.guard.common.util.JsonUtil;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.oxapi.exceptions.OXApiExceptionCodes;
import com.openexchange.guard.oxapi.exceptions.OXApiExceptionFactory;
import com.openexchange.guard.oxapi.osgi.Services;
import com.openexchange.guard.oxapi.pooling.HttpConnectionPoolService;
import com.openexchange.guard.oxapi.streaming.JsonStreamHandler;
import com.openexchange.guard.oxapi.streaming.StreamHandler;
import com.openexchange.guard.oxapi.streaming.StreamedContentBody;
import com.openexchange.guard.oxapi.utils.ResponseHelper;

/**
 * API class for communicating with OX servers
 *
 * @author greg
 */
//TODO: maybe split this class into more manageable sub parts.
public class Api {

    private static Logger LOG = LoggerFactory.getLogger(Api.class);

    private HttpContext httpContext = null;

    private HttpClient httpClient = null;

    private String session;

    private OxCookie cookie;

    private String useragent;

    public static String USER_AGENT = "Open-Xchange Guard Server";

    /**
     * Constructor for api. Logs in, and gets httpContext and Session ID
     *
     * @param username
     * @param password
     */
    public Api(String username, String password) {
        login(username, password);
    }

    /**
     * Constructor from cookie
     *
     * @param ck cookie
     * @param session sessionId
     * @param userAgent User-agent
     */
    public Api(OxCookie ck, String session, String userAgent) {
        this.cookie = ck;
        this.session = session;
        this.useragent = Strings.removeCarriageReturn(userAgent);
    }

    public Api(OxCookie ck, HttpServletRequest request) {
        this.cookie = ck;
        this.session = Strings.removeCarriageReturn(request.getParameter("session"));
        this.useragent = Strings.removeCarriageReturn(request.getHeader("User-Agent"));
    }

    public Api(HttpServletRequest request) {
        this(new OxCookie(request.getCookies()), request);
    }

    /**
     * Close the httpClient and void context;
     */
    public void dispose() {
        this.httpClient.getConnectionManager().shutdown();
        this.httpContext = null;
        this.session = "";
    }

    /**
     * Login and populate the httpContext and SessionID
     *
     * @param username
     * @param password
     * @return session id or an empty string if something went wrong
     */
    private String login(String username, String password) {
        HttpResponse response = null;
        try {
            HttpConnectionPoolService poolService = Services.getService(HttpConnectionPoolService.class);
            HttpClient httpClient = poolService.getCookielessClient();
            CookieStore cookieStore = new BasicCookieStore();
            this.httpContext = new BasicHttpContext();
            this.httpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
            HttpPost request = new HttpPost(getRequestPath() + "login?action=login");
            List<NameValuePair> paramsv = new ArrayList<NameValuePair>(2);
            paramsv.add(new BasicNameValuePair("name", username));
            paramsv.add(new BasicNameValuePair("password", password));
            request.setEntity(new UrlEncodedFormEntity(paramsv));
            request.addHeader("content-type", "application/x-www-form-urlencoded");
            response = httpClient.execute(request, httpContext);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), StandardCharsets.UTF_8));
            try {
                String json = reader.readLine();
                if (json != null) {
                    JsonParser parser = new JsonParser();
                    JsonObject result = (JsonObject) parser.parse(json);
                    this.session = result.get("session").getAsString();
                }
            } finally {
                reader.close();
                request.releaseConnection();
            }
            return session;
        } catch (Exception ex) {
            LOG.error("Login error for user " + username, ex);
            // handle exception here
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
        return "";
    }

    public OxCookie getCookie() {
        return this.cookie;
    }

    public HttpResponse getResponse(String params) throws IllegalStateException, IOException, OXException {
        if (httpClient == null) {
            HttpConnectionPoolService poolService = Services.getService(HttpConnectionPoolService.class);
            httpClient = poolService.getCookielessClient();
        }

        HttpGet getRequest = new HttpGet(getRequestPath() + params + "&session=" + session);
        getRequest.addHeader("accept", "application/json");
        getRequest.setHeader("Connection", "close");
        HttpResponse response = null;
        if (httpContext == null) {// If login in by cookie, add header and execute
            getRequest.addHeader("Cookie", cookie.getCookieHeader());
            getRequest.setHeader("User-Agent", useragent);
            response = httpClient.execute(getRequest);

        } else { // If logging into by previous login httpcontext, then use that
            response = httpClient.execute(getRequest, httpContext);
        }
        if (response.getStatusLine().getStatusCode() != 200) {
            try {
                HttpClientUtils.closeQuietly(response);
                getRequest.releaseConnection();
            } catch (Exception e2) {
                LOG.error("unable to close failed connection at API getRequest", e2);
            }
            throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());
        }
        return response;
    }

    public HttpResponse putRequest(String params, String data) throws IllegalStateException, IOException, OXException {

        if (httpClient == null) {
            HttpConnectionPoolService poolService = Services.getService(HttpConnectionPoolService.class);
            httpClient = poolService.getCookielessClient();
        }

        HttpPut putRequest = new HttpPut(getRequestPath() + params + "&session=" + session);
        StringEntity entity = new StringEntity(data);
        entity.setContentType("text/javascript;charset=UTF-8");
        putRequest.addHeader("accept", "application/json");
        putRequest.setHeader("Connection", "close");

        putRequest.setEntity(entity);
        HttpResponse response = null;
        if (httpContext == null) {// If login in by cookie, add header and execute
            putRequest.addHeader("Cookie", cookie.getCookieHeader());
            putRequest.setHeader("User-Agent", useragent);
            response = httpClient.execute(putRequest);
        } else { // If logging into by previous login httpcontext, then use that
            response = httpClient.execute(putRequest, httpContext);
        }
        if (response.getStatusLine().getStatusCode() != 200) {
            try {
                HttpClientUtils.closeQuietly(response);
                putRequest.releaseConnection();
            } catch (Exception e2) {
                LOG.error("unable to close failed connection at API putRequest", e2);
            }
            throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());
        }
        // putRequest.releaseConnection();
        return response;

    }

    public JsonObject postRequest(String data, String params) throws ClientProtocolException, IOException, OXException {
        if (httpClient == null) {
            HttpConnectionPoolService poolService = Services.getService(HttpConnectionPoolService.class);
            httpClient = poolService.getCookielessClient();
        }
        HttpPost postRequest = new HttpPost(getRequestPath() + params);
        StringEntity input = new StringEntity(data);
        input.setContentType("application/json");
        postRequest.setEntity(input);
        HttpResponse response = null;
        if (httpContext == null) {// If login in by cookie, add header and execute
            postRequest.addHeader("Cookie", cookie.getCookieHeader());
            postRequest.setHeader("User-Agent", useragent);
            response = httpClient.execute(postRequest);
        } else { // If logging into by previous login httpcontext, then use that
            response = httpClient.execute(postRequest, httpContext);
        }

        if (response.getStatusLine().getStatusCode() != 200) {
            try {
                HttpClientUtils.closeQuietly(response);
                postRequest.releaseConnection();
            } catch (Exception e2) {
                LOG.error("unable to close failed connection at API postRequest", e2);
            }
            throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), StandardCharsets.UTF_8));
        JsonObject result;
        try {
            String json = reader.readLine();
            JsonParser parser = new JsonParser();
            result = (JsonObject) parser.parse(json == null ? "" : json);
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        reader.close();
        HttpClientUtils.closeQuietly(response);
        postRequest.releaseConnection();
        return result;
    }

    /**
     * For uploading encrypted files to server
     *
     * @param data The json with folder, etc
     * @param params
     * @param attachments List of files, encrypted
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws OXException
     */
    public String filePost(String data, String params, HttpEntity entity) throws ClientProtocolException, IOException, OXException {
        if (httpClient == null) {
            HttpConnectionPoolService poolService = Services.getService(HttpConnectionPoolService.class);
            httpClient = poolService.getCookielessClient();
        }

        HttpPost postRequest = new HttpPost(getRequestPath() + params);
        postRequest.setEntity(entity);
        HttpResponse response = null;
        if (httpContext == null) {// If login in by cookie, add header and execute
            postRequest.addHeader("cookie", cookie.getCookieHeader());
            postRequest.setHeader("User-Agent", useragent);
            response = httpClient.execute(postRequest);
        } else { // If logging into by previous login httpcontext, then use that
            response = httpClient.execute(postRequest, httpContext);
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), StandardCharsets.UTF_8));
        String result;
        try {
            result = reader.readLine();
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        HttpClientUtils.closeQuietly(response);
        postRequest.releaseConnection();
        if (Utilities.checkFail(result)) {
            throw OXApiExceptionFactory.create(result, response.getFirstHeader("Content-Type"));
        }
        return result;
    }

    /**
     * Multipart post for email
     */
    private JsonObject multipartPost(String params, HttpEntity entity) throws ClientProtocolException, IOException, OXException {
        if (httpClient == null) {
            HttpConnectionPoolService poolService = Services.getService(HttpConnectionPoolService.class);
            httpClient = poolService.getCookielessClient();
        }
        HttpPost postRequest = new HttpPost(getRequestPath() + params);
        postRequest.setEntity(entity);
        HttpResponse response = null;
        if (httpContext == null) {// If login in by cookie, add header and execute
            postRequest.addHeader("cookie", cookie.getCookieHeader());
            postRequest.setHeader("User-Agent", useragent);
            response = httpClient.execute(postRequest);
        } else { // If logging into by previous login httpcontext, then use that
            response = httpClient.execute(postRequest, httpContext);
        }
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode == HttpStatus.SC_OK) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), StandardCharsets.UTF_8));
            String json;
            try {
                json = reader.readLine();
            } finally {
                if (reader != null) {
                    reader.close();
                }
            }
            HttpClientUtils.closeQuietly(response);
            postRequest.releaseConnection();
            if (json == null) {
                LOG.error("Error working on multipart post, null JSON response");
                return null;
            }
            if (json.contains("{")) {
                int i = json.indexOf("{");
                int j = json.lastIndexOf("}");
                json = json.substring(i, j + 1);
            }
            try {
                JsonParser parser = new JsonParser();
                JsonObject result = (JsonObject) parser.parse(json);
                return result;
            } catch (Exception ex) {
                LOG.error("Error working on multipart post", ex, json);
                return null;
            }
        }
        else {
            throw OXApiExceptionCodes.WRONG_ERROR_CODE.create(Integer.toString(statusCode));
        }
    }

    public JsonObject sendMail(JsonObject mailObject) throws UnsupportedEncodingException {
        MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, null, StandardCharsets.UTF_8);
        entity.addPart("json_0", new StringBody(new Gson().toJson(mailObject.getAsJsonObject("data")), ContentType.APPLICATION_JSON.getMimeType(), ContentType.APPLICATION_JSON.getCharset()));
        JsonObject ret = sendMail(entity);
        return ret;
    }

    public JsonObject sendMail(HttpEntity entity) {
        String params = "mail?action=new";
        params += "&session=" + session;
        try {
            return (multipartPost(params, entity));
        } catch (ClientProtocolException e) {
            LOG.error("Error sending mail", e);
        } catch (IOException e) {
            LOG.error("Error sending mail", e);
        } catch (OXException e) {
            LOG.error("Error sending mail", e);
        }
        return null;
    }

    /**
     * Send an email through the backend in MIME format
     *
     * @param data - email data
     * @param folder - folder if should be stored. Null if sending
     * @return
     * @throws UnsupportedEncodingException
     */
    public String sendMimeMail(String data, String folder) throws UnsupportedEncodingException {
        String params = "mail?action=new";
        params += "&session=" + session;
        if (folder != null) {
            params += "&folder=" + URLEncoder.encode(folder, "UTF-8");
            params += "&flags=32"; //Marks the email as "read" while putting it in the folder
        } else {
            params += "&copy2Sent=false";
        }

        HttpResponse response = null;
        try {
            response = putRequest(params, data);
            HttpEntity ent = response.getEntity();
            String dat = EntityUtils.toString(ent, StandardCharsets.UTF_8);
            return (dat);
        } catch (ClientProtocolException e) {
            LOG.error("Error sending mail", e);
        } catch (IOException e) {
            LOG.error("Error sending mail", e);
        } catch (OXException e) {
            LOG.error("Error sending mail", e);
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
        return null;
    }

    public JsonObject saveSentMail(HttpEntity entity, String sentfolder) {
        String params = "mail?action=new";
        try {
            params += "&folder=" + URLEncoder.encode(sentfolder, "UTF-8") + "&session=" + session;
        } catch (UnsupportedEncodingException e1) {
            LOG.error("Impossible, UTF-8 encoding failed");
        }
        try {
            return (multipartPost(params, entity));
        } catch (ClientProtocolException e) {
            LOG.error("Error saving sent mail", e);
        } catch (IOException e) {
            LOG.error("IO error saving sent mail", e);
        } catch (OXException e) {
            LOG.error("Error saving sent mail", e);
        }
        return null;
    }

    public JsonObject saveDraftMail(HttpEntity entity) {
        String params = "mail?action=new&session=" + session;
        try {
            return (multipartPost(params, entity));
        } catch (ClientProtocolException e) {
            LOG.error("Error saving sent mail", e);
        } catch (IOException e) {
            LOG.error("IO error saving sent mail", e);
        } catch (OXException e) {
            LOG.error("Error saving sent mail", e);
        }
        return null;
    }

    public JsonObject saveInBox(HttpEntity entity) {
        String params = "mail?action=new";
        params += "&folder=default0/INBOX&session=" + session;
        try {
            return (multipartPost(params, entity));
        } catch (ClientProtocolException e) {
            LOG.error("Error saving inbox", e);
        } catch (IOException e) {
            LOG.error("Error saving inbox", e);
        } catch (OXException e) {
            LOG.error("Error saving inbox", e);
        }
        return null;
    }

    public ApiResponse getPlainAttachment(String id, String attachment, String folder) throws UnsupportedEncodingException {
        String params = "mail?action=attachment" + "&folder=" + URLEncoder.encode(folder, "UTF-8") + "&id=" + URLEncoder.encode(id, "UTF-8") + "&attachment=" + attachment;
        HttpResponse response = null;
        try {
            response = getResponse(params);
            return new ApiResponse(response);
        } catch (Exception e) {
            LOG.error("Error getting attachment", e);
            HttpClientUtils.closeQuietly(response);
            return null;
        }
    }

    public String getMime(String ref) throws UnsupportedEncodingException {
        int i = ref.lastIndexOf(("/"));
        if (i < -1) {
            return null;
        }
        String folder = ref.substring(0, i);
        String id = ref.substring(i + 1);
        return getMime(id, folder);
    }

    public String getMime(String id, String folder) throws UnsupportedEncodingException {
        String params = "mail?action=get" + "&folder=" + URLEncoder.encode(folder, "UTF-8") + "&id=" + URLEncoder.encode(id, "UTF-8") + "&src=1&save=1";
        HttpResponse response = null;
        try {
            response = getResponse(params);
            HttpEntity ent = response.getEntity();
            String data = EntityUtils.toString(ent, StandardCharsets.UTF_8);
            return (data);
        } catch (Exception ex) {
            LOG.error("Error getting MIME type", ex);
            return "";
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
    }

    public String getMail(String id, String folder) throws UnsupportedEncodingException {
        String params = "mail?action=get" + "&folder=" + URLEncoder.encode(folder, "UTF-8") + "&id=" + URLEncoder.encode(id, "UTF-8");

        HttpResponse response = null;
        try {
            response = getResponse(params);
            HttpEntity ent = response.getEntity();
            String data = EntityUtils.toString(ent, StandardCharsets.UTF_8);
            return (data);
        } catch (Exception ex) {
            LOG.error("Error getting mail " + id + " from folder " + folder, ex);
            return "";
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
    }

    /**
     * Check JSON data for error and log
     * @param data
     * @param objectName  - name of the object that was being retrieved
     * @return
     */
    private boolean checkError (JsonObject data, String objectName) {
        if (data != null && data.has("data")) {
            return false;
        }
        if (data != null && data.has("error")) {
            LOG.error("Problem getting " + objectName + " from middleware.  " + data.get("error").getAsString());
            return true;
        }
        LOG.error("Problem getting " + objectName + " from middleware.  Incorrect or no middleware response.");
        return true;
    }

    public int getContextId() {
        String params = "config/context_id?session=" + session;

        HttpResponse response = null;
        try {
            response = getResponse(params);
            JsonObject data = ResponseHelper.getJson(response);
            if (checkError(data, "contextId")) {
                return 0;
            }
            return (data.get("data").getAsInt());
        } catch (Exception ex) {
            LOG.error("Error getting contextId", ex);
            return 0;
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
    }

    public JsonObject getUserData(int userid) {
        String params = "user?action=get&session=" + session;
        if (userid > 0) {
            params += "&id=" + userid;
        }

        HttpResponse response = null;
        try {
            response = getResponse(params);
            JsonObject data = ResponseHelper.getJson(response);
            return data;
        } catch (Exception ex) {
            LOG.error("Error getting user information", ex);
            return null;
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
    }

    public String getFolder(int id) {
        String params = "folders?action=get" + "&id=" + id;

        HttpResponse response = null;
        try {
            response = getResponse(params);
            HttpEntity ent = response.getEntity();
            String data = EntityUtils.toString(ent, StandardCharsets.UTF_8);
            return (data);
        } catch (Exception ex) {
            LOG.error("Error getting folder " + id, ex);
            return "";
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
    }

    public String updateFolder(int id, String json) {
        String params = "folders?action=update" + "&id=" + id;

        HttpResponse response = null;
        try {
            response = putRequest(params, json);
            HttpEntity ent = response.getEntity();
            String data = EntityUtils.toString(ent, StandardCharsets.UTF_8);
            return (data);
        } catch (Exception ex) {
            LOG.error("Error updating folder " + id, ex);
            return "";
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
    }

    public String getFolders() {
        String params = "folders?action=list&parent=1&tree=1&all=0&altNames=true&timezone=UTC&columns=1%2C2%2C3%2C4%2C5%2C6%2C20%2C23%2C300%2C301%2C302%2C304%2C305%2C306%2C307%2C308%2C309%2C310%2C311%2C312%2C313%2C314%2C315%2C316%2C317%2C3010%2C3020%2C3030";

        HttpResponse response = null;
        try {
            response = getResponse(params);
            HttpEntity ent = response.getEntity();
            String data = EntityUtils.toString(ent, StandardCharsets.UTF_8);
            return (data);
        } catch (Exception e) {
            LOG.error("Error listing folders", e);
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
        return null;
    }

    public boolean verifyLogin() {
        int contextId = getContextId();
        return contextId > 0;
    }

    public JsonObject getInfoItemJson(String id, String folder) throws UnsupportedEncodingException {
        String json = getInfoItemJson(id, folder, null);
        JsonParser parser = new JsonParser();
        JsonObject result = (JsonObject) parser.parse(json);
        return result;
    }

    public String getInfoItemJson(String id, String folder, String version) throws UnsupportedEncodingException {
        String params = "files?action=get&id=" + URLEncoder.encode(id, "UTF-8") + "&folder=" + URLEncoder.encode(folder, "UTF-8") + "&version=" + version + "&session=" + session;
        if (version == null) {
            params = "files?action=get&id=" + URLEncoder.encode(id, "UTF-8") + "&folder=" + URLEncoder.encode(folder, "UTF-8") + "&session=" + session;
        }
        HttpResponse response = null;
        try {
            response = getResponse(params);
            HttpEntity ent = response.getEntity();
            String data = EntityUtils.toString(ent, StandardCharsets.UTF_8);
            return (data);
        } catch (Exception ex) {
            LOG.error("Error getting item " + id + " from folder " + folder + "in version " + version, ex);
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
        return null;
    }

    /**
     * Get the default vcard from the backend
     *
     * @return
     */
    public byte[] getVcard() {
        String params = "contacts?action=getVcard&session=" + session;

        HttpResponse response = null;
        try {
            response = getResponse(params);
            HttpEntity ent = response.getEntity();
            byte[] data = EntityUtils.toByteArray(ent);
            return (data);
        } catch (Exception ex) {
            LOG.error("Error getting vcard ", ex);
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
        return null;
    }

    public JsonObject importVcard(HttpEntity entity, String folder) throws UnsupportedEncodingException {
        String params = "import?action=VCARD&folder=" + URLEncoder.encode(folder, "UTF-8") + "&session=" + session;
        try {
            JsonObject response = multipartPost(params, entity);
            return (response);
        } catch (Exception ex) {
            LOG.error("Error importing vcard ", ex);
        }
        return null;
    }

    public byte[] getPlainTextInfoStore(int id, String folder, String version) throws UnsupportedEncodingException {
        String params = "files?action=document&id=" + id + "&folder=" + URLEncoder.encode(folder, "UTF-8") + "&version=" + version + "&session=" + session;

        HttpResponse response = null;
        try {
            response = getResponse(params);
            HttpEntity ent = response.getEntity();
            byte[] info = EntityUtils.toByteArray(ent);
            return (info);
        } catch (Exception e) {
            LOG.error("error getting item " + id + " from folder " + folder + " in version " + version + " in plaintext", e);
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
        return null;
    }

    /**
     * Opens a remote stream to an infostore/drive item
     *
     * @param id The id of the item to open
     * @param folder the if of the item's folder
     * @param version the version of the item
     * @return a stream to the remote item
     * @throws OXException
     */
    public InputStream openDriveItem(String fileId, long fileSize, String folderId, String fileVersion) throws OXException {
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        boolean ssl = configService.getBooleanProperty(GuardProperty.backendSSL);
        String host = configService.getProperty(GuardProperty.restApiHostname);
        int port = configService.getIntProperty(GuardProperty.oxBackendPort);

        URIBuilder builder = new URIBuilder().setScheme("http" + (ssl ? "s" : "")).setHost(host).setPort(port).setPath("/ajax/files").setParameter("action", "document").setParameter("id", fileId).setParameter("folder", String.valueOf(folderId)).setParameter("session", String.valueOf(session));
        if (fileVersion != null) {
            builder.setParameter("version", String.valueOf(fileVersion));
        }

        try {
            HttpGet getRequest = new HttpGet(builder.build());
            getRequest.addHeader("accept", "application/json");
            getRequest.setHeader("Connection", "close");
            HttpResponse response = null;
            if (httpContext == null) {
                //using the given cookie
                getRequest.addHeader("Cookie", cookie.getCookieHeader());
                getRequest.setHeader("User-Agent", useragent);
                response = httpClient.execute(getRequest);
            } else {
                //using the current HTTP-context
                response = httpClient.execute(getRequest, httpContext);
            }

            //Checking response
            Integer returnStatus = response.getStatusLine().getStatusCode();
            if (returnStatus.toString().startsWith("2") /* Status 2xx Success */) {
                HttpEntity responseEntity = response.getEntity();
                return responseEntity.getContent();
            } else {
                //Error handling
                HttpClientUtils.closeQuietly(response);
                getRequest.releaseConnection();
                throw GuardCoreExceptionCodes.IO_ERROR.create(String.format("Error reading input stream from remote drive item id " + fileId + " folder " + folderId + "d. HTTP-Error: " + response.toString()));
            }
        } catch (URISyntaxException e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, "Could not construct redirect location for URI '" + builder.toString() + "'");
        } catch (IOException e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Retrieve non-encrypted/encrypted infostore object as attachment
     *
     * @param id
     * @param folder
     * @param version
     * @return
     * @throws IOException
     */
    public ApiResponse getInfoStoreAttach(String id, String folder, String version) throws IOException {
        String params = "files?action=document&id=" + URLEncoder.encode(id, "UTF-8") + "&folder=" + URLEncoder.encode(folder, "UTF-8") + ((version != null) ? ("&version=" + version) : "") + "&session=" + session;
        HttpResponse response = null;
        try {
            return new ApiResponse(getResponse(params));
        } catch (Exception ex) {
            LOG.error("Error getting infostore attachment " + id + " from folder " + folder + " in version " + version, ex);
            HttpClientUtils.closeQuietly(response);
            return null;
        }
    }

    public InlineImage getInlineImage(String url) throws IOException {
        final String filenameRegex = "\"(.*?)\"";
        final Pattern pattern = Pattern.compile(filenameRegex, Pattern.DOTALL);
        HttpResponse response = null;
        try {
            url = url.replace("&amp;", "&");  // Maintain url encoding except for the &  Bug 47911
            if (url.contains("file?action") || url.contains("image/mail") || url.contains("image/snippet")) {
                int index = 0;
                if (url.contains("file?action")) {
                    index = url.indexOf("file?action");
                }
                if (url.contains("image/mail")) {
                    index = url.indexOf("image/mail");
                }
                if (url.contains("image/snippet")) {
                    index = url.indexOf("image/snippet");
                }
                if (url.contains("image/snippet")) {
                    index = url.indexOf("image/snippet");
                }
                String params = url.substring(index);
                response = getResponse(params);
                HttpEntity ent = response.getEntity();

                String filename = null;
                org.apache.http.Header[] h = response.getAllHeaders();

                for (int j = 0; j < h.length; j++) {
                    if (h[j].getValue().contains("filename")) {
                        filename = h[j].getValue();
                        Matcher matcher = pattern.matcher(filename);
                        if (matcher.find()) {
                            filename = matcher.group(1);
                        }
                        break;
                    }
                }
                byte[] image = EntityUtils.toByteArray(ent);
                InlineImage ret = new InlineImage(filename, ent.getContentType().toString(), image);
                return ret;
            }
        } catch (Exception ex) {
            LOG.error("Error getting inline image from " + url, ex);
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
        return null;
    }

    public String getCapabilities() {
        String params = "capabilities?action=all";
        String dat = null;

        HttpResponse response = null;
        try {
            response = getResponse(params);
            HttpEntity ent = response.getEntity();
            dat = EntityUtils.toString(ent, StandardCharsets.UTF_8);
        } catch (Exception e) {
            LOG.error("Error retrieving all capabilities", e);
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
        return (dat);
    }

    public String getConfig(String module) {
        String params = "config/" + module + "?session=" + session;
        String dat = null;

        HttpResponse response = null;
        try {
            response = getResponse(params);
            HttpEntity ent = response.getEntity();
            dat = EntityUtils.toString(ent, StandardCharsets.UTF_8);
        } catch (Exception e) {
            LOG.error("Error retrieving configuration", e);
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
        return (dat);
    }

    public String getSentFolder() {
        String config = getConfig("mail");
        JsonObject data = JsonUtil.parseAsJsonObject(config);
        return (data.get("data").getAsJsonObject().get("folder").getAsJsonObject().get("sent").getAsString());
    }

    public ArrayList<String> getUserEmails() {
        String params = "account?action=all&columns=1007&session=" + session;
        String dat = null;
        ArrayList<String> e = new ArrayList<String>();

        HttpResponse response = null;
        try {
            response = getResponse(params);
            HttpEntity ent = response.getEntity();
            dat = EntityUtils.toString(ent, StandardCharsets.UTF_8);
            JsonObject json = JsonUtil.parseAsJsonObject(dat);
            JsonArray emails = json.get("data").getAsJsonArray();
            for (int i = 0; i < emails.size(); i++) {
                e.add(emails.get(i).getAsString());
            }
        } catch (Exception ex) {
            LOG.error("Error retrieving configuration", ex);
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
        return (e);
    }

    public String getPrimary() {
        try {
            String data = getConfig("mail");
            JsonObject json = data == null ? new JsonObject() : JsonUtil.parseAsJsonObject(data);
            if (json.has("data") && json.get("data").getAsJsonObject().has("defaultaddress")) {
                return (json.get("data").getAsJsonObject().get("defaultaddress").getAsString());
            } else {
                json = getUserData(0);
                if (json != null && json.has("data")) {
                    return (json.get("data").getAsJsonObject().get("email1").getAsString());
                } else {
                    LOG.error("Error getting primary email address ");
                    return ("");
                }

            }
        } catch (Exception e) {
            LOG.error("Error getting primary email address ", e);
            return ("");
        }

    }

    public void deleteItem(String id, int folder) {
        deleteItem(id, Integer.toString(folder));
    }

    public void deleteItem(String id, String folder) {
        String params = "files?action=delete&hardDelete=true&timestamp=2116800000000";

        HttpResponse response = null;
        try {
            String data = "[{\"id\":\"" + id + "\",\"folder\":\"" + folder + "\"}]";
            response = putRequest(params, data);
        } catch (Exception e) {
            LOG.error("Error deleting item " + id + " in folder " + folder, e);
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
    }

    public void deleteMail(String id, String folder) {
        String params = "mail?action=delete&session=" + session;

        HttpResponse response = null;
        try {
            String data = "[{\"id\":\"" + id + "\",\"folder\":\"" + folder + "\"}]";
            response = putRequest(params, data);
        } catch (Exception e) {
            LOG.error("Error deleting item " + id + " in folder " + folder, e);
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
    }

    public void handleRef(String msgref, boolean reply, int userid, int cid) {
        int i = msgref.lastIndexOf("/");
        if (i > 0) {
            try {
                String defaultDraft = getConfig("mail/folder/drafts");
                if (defaultDraft == null) {
                    LOG.error("Problem getting draft folder");
                    return;
                }
                JsonParser parser = new JsonParser();
                JsonObject draft = (JsonObject) parser.parse(defaultDraft);
                String id = msgref.substring(i + 1);
                String folder = msgref.substring(0, i);
                if (reply) {
                    // mark as reply
                    setReplied(id, folder);
                    return;
                }
                if (msgref.startsWith(draft.get("data").getAsString())) {
                    if (deleteDraft(userid, cid)) {
                        // If draft folder, then delete
                        deleteDraft(id, folder);
                    }
                    return;
                }
                // otherwise, mark as forward
                setForward(id, folder);

            } catch (Exception ex) {
                LOG.error("Problem getting draft folder ", ex);
                return;
            }
        }
    }

    /**
     * Get the 'com.openexchange.mail.deleteDraftOnTransport' property for the specified user in the specified context
     *
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The value of the 'com.openexchange.mail.deleteDraftOnTransport' property
     * @throws OXException If the property cannot be returned. In that case the default value of 'false' will be returned
     */
    private boolean deleteDraft(int userId, int contextId) throws OXException {
        GuardConfigurationService guardConfigurationService = Services.getService(GuardConfigurationService.class);
        return guardConfigurationService.getBooleanProperty("com.openexchange.mail.deleteDraftOnTransport", userId, contextId, false);
    }

    public void setReplied(String id, String folder) throws UnsupportedEncodingException {
        setFlag(id, folder, 1);
    }

    public void setForward(String id, String folder) throws UnsupportedEncodingException {
        setFlag(id, folder, 256);
    }

    // Set mail flag
    public void setFlag(String id, String folder, int flag) throws UnsupportedEncodingException {
        String params = "mail?action=update&id=" + URLEncoder.encode(id, "UTF-8") + "&folder=" + URLEncoder.encode(folder, "UTF-8");
        JsonObject data = new JsonObject();
        data.addProperty("flags", flag);
        data.addProperty("value", true);
        HttpResponse response = null;
        try {
            response = putRequest(params, data.toString());
        } catch (Exception e) {
            LOG.error("Problem marking as read", e);
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
    }

    // Delete message from draft folder
    public void deleteDraft(String id, String folder) {
        String params = "mail?action=delete&timestamp=2116800000000";
        JsonObject data = new JsonObject();
        data.addProperty("folder", folder);
        data.addProperty("id", id);
        HttpResponse response = null;
        try {
            response = putRequest(params, "[" + data.toString() + "]");
        } catch (IllegalStateException | IOException e) {
            LOG.error("Problem deleting from draft", e);
        } catch (OXException e) {
            LOG.error("Problem deleting from draft", e);
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
    }

    /**
     * Uploads a new input stream to drive
     *
     * @param inputStream The InputStream to upload
     * @param streamHandler A handler to define a operation performed on the stream data during upload
     * @param fileName The new name of the file
     * @param contentType The new content type of the file
     * @param jsonContent The new meta data content of the file
     * @param jsonHandler The JSON handler
     * @return The JSON response from the backend if the upload was successful
     * @throws OXException
     */
    public JsonObject uploadDriveNewStreamItem(InputStream inputStream, StreamHandler streamHandler, String fileName, ContentType contentType, InputStream jsonStream, JsonStreamHandler jsonHandler) throws OXException {
        return uploadDriveStreamItem(inputStream, streamHandler, fileName, null, contentType, jsonStream, jsonHandler, false);
    }

    /**
     * Uploads an input stream and updates an already existing item
     *
     * @param inputStream The InputStream to upload
     * @param streamHandler A handler to define a operation performed on the stream data during upload
     * @param fileName The new name of the file
     * @param fileId The file identifier to update
     * @param contentType The new content type of the file
     * @param jsonContent The new meta data content of the file
     * @param jsonHandler The JSON handler
     * @return The JSON response from the backend if the upload was successful
     * @throws OXException
     */
    public JsonObject uploadDriveUpdatedStreamItem(InputStream inputStream, StreamHandler streamHandler, String fileName, String fileId, ContentType contentType, InputStream jsonStream, JsonStreamHandler jsonHandler) throws OXException {
        return uploadDriveStreamItem(inputStream, streamHandler, fileName, fileId, contentType, jsonStream, jsonHandler, true);
    }

    /**
     * Uploads an input stream to drive
     *
     * @param inputStream The InputStream to upload
     * @param streamHandler A handler to define a operation performed on the stream data during upload
     * @param fileName The new name of the file
     * @param fileId The file identifier to update if in update mode
     * @param jsonContent The new meta data content of the file
     * @param contentType The new content type of the file
     * @param update <code>true</code> if in update mode
     * @return The JSON response from the backend if the upload was successful
     * @throws OXException
     */
    private JsonObject uploadDriveStreamItem(InputStream inputStream, StreamHandler streamHandler, String fileName, String fileId, ContentType contentType, InputStream jsonStream, JsonStreamHandler jsonHandler, boolean update) throws OXException {
        try {
            if (httpClient == null) {
                //httpClient = OxDbConn.httpClient;
                HttpConnectionPoolService poolService = Services.getService(HttpConnectionPoolService.class);
                httpClient = poolService.getCookielessClient();
            }

            //Multipart entity
            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            entity.addPart("file_0", new StreamedContentBody(inputStream, 4096, contentType, streamHandler, fileName));
            entity.addPart("json", new StreamedContentBody(jsonStream, 4096, ContentType.TEXT_PLAIN, jsonHandler, null));

            //Posting the data
            String params = "files?action=" + (update ? "update" : "new") + "&filename=" + URLEncoder.encode(fileName, "UTF-8") + "&session=" + this.session;
            if (update) {
                Date date = new Date();
                params = params + "&timestamp=" + date.getTime() + "&id=" + fileId;
            }

            HttpPost postRequest = new HttpPost(getRequestPath() + params);
            postRequest.setEntity(entity);
            postRequest.addHeader("Connection", "Keep-Alive");
            HttpResponse response = null;
            if (httpContext == null) {// If login in by cookie, add header and execute
                postRequest.addHeader("cookie", cookie.getCookieHeader());
                postRequest.setHeader("User-Agent", useragent);
                response = httpClient.execute(postRequest);
            } else { // If logging into by previous login httpcontext, then use that
                response = httpClient.execute(postRequest, httpContext);
            }

            Integer returnStatus = response.getStatusLine().getStatusCode();
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), StandardCharsets.UTF_8));
            String result;
            try {
                result = reader.readLine();
            } finally {
                if (reader != null) {
                    reader.close();
                }
            }
            HttpClientUtils.closeQuietly(response);
            postRequest.releaseConnection();
            if (!returnStatus.toString().startsWith("2") /* Status 2xx Success */) {
                throw new IOException(String.format("Error uploading file. HTTP Error %s", returnStatus));
            }
            //Getting the new file ID
            JsonObject jsonResponse = JsonUtil.extractObject(result);
            if (jsonResponse.has("data")) {
                return jsonResponse;
            } else if (jsonResponse.has("error")) {
                throw OXApiExceptionFactory.create(result, response.getFirstHeader("Content-Type"));
            } else {
                throw new IOException("No data in json");
            }
        } catch (IOException e) {
            throw OXApiExceptionCodes.HANDLE_SERVER_RESPONSE_ERROR.create(e);
        }
    }

    /**
     * Temporary helper for constructing the request path
     *
     * @throws OXException
     */
    private String getRequestPath() throws OXException {
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        boolean ssl = configService.getBooleanProperty(GuardProperty.backendSSL);
        String host = configService.getProperty(GuardProperty.restApiHostname);
        int port = configService.getIntProperty(GuardProperty.oxBackendPort);
        String path = configService.getProperty(GuardProperty.oxBackendPath);

        StringBuilder builder = new StringBuilder();
        builder.append("http");
        if (ssl) {
            builder.append("s");
        }
        builder.append("://").append(host).append(":").append(port).append(path);
        return builder.toString();
    }
}
