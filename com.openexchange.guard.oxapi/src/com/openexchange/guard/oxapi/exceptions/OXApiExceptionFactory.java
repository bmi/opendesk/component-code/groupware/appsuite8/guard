
package com.openexchange.guard.oxapi.exceptions;

import org.apache.http.Header;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.openexchange.guard.common.util.JsonUtil;

/**
 * {@link OXApiExceptionFactory} Simple factory helper for creating OXApiExceptions from middleware responses
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class OXApiExceptionFactory {

    /**
     * Creates a new {@link OXApiException} from middleware response data
     *
     * @param errorData the error data received from the OX middleware
     * @param contentTypeHeader The content type of the error response from the OX middleware
     * @return
     */
    public static OXApiException create(String errorData, Header contentTypeHeader) {

        JsonObject errorObject = JsonUtil.extractObject(errorData);

        String error = errorObject.get("error").getAsString();

        JsonArray jsonErrorParams = errorObject.get("error_params").getAsJsonArray();
        String[] errorParams = new String[jsonErrorParams.size()];
        for (int i = 0; i < jsonErrorParams.size(); i++) {
            errorParams[i] = jsonErrorParams.get(i).getAsString();
        }

        String categories = errorObject.get("categories").getAsString();

        int category = errorObject.get("category").getAsInt();

        String code = errorObject.get("code").getAsString();

        String errorId = errorObject.get("error_id").getAsString();

        String errorDescription = errorObject.get("error_desc").getAsString();

        String[] errorStack = null;
        if(errorObject.has("error_stack")) {
            JsonArray jsonErrorStack = errorObject.get("error_stack").getAsJsonArray();
            errorStack = new String[jsonErrorStack.size()];
            for (int i = 0; i < jsonErrorStack.size(); i++) {
                errorStack[i] = jsonErrorStack.get(i).getAsString();
            }
        }

        return new OXApiException(errorData,
            contentTypeHeader,
            error,
            errorParams,
            categories,
            category,
            code,
            errorId,
            errorDescription,
            errorStack);
    }
}
