/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi.exceptions;

import com.openexchange.i18n.LocalizableStrings;

/**
 * {@link OXApiExceptionMessages}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class OXApiExceptionMessages implements LocalizableStrings {

    // An error occurred when communicating with the middleware
    public static final String HANDLE_SERVER_RESPONSE_ERROR_MSG = "An error occurred while processing the middleware response.";
    // Received a wrong middleware response code.  Code appended
    public static final String WRONG_ERROR_CODE_MSG = "Received middleware response contains wrong error code %1$s.";
    // Error when creating a session ID.  Error appended.
    public static final String SESSION_CREATION_ERROR_MSG = "Error while creating session ID: %1$s.";
}
