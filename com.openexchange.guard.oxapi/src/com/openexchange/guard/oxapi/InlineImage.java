/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi;

/**
 * {@link InlineImage}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.1
 */
public class InlineImage {

    private final String fileName;
    private final String contentType;
    private final byte[] content;

    /**
     * Initializes a new {@link InlineImage}.
     * 
     * @param fileName
     * @param contentType
     * @param content
     */
    public InlineImage(String fileName, String contentType, byte[] content) {
        this.fileName = fileName;
        this.contentType = contentType;
        this.content = content;
    }

    /**
     * Gets the fileName
     *
     * @return The fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Gets the contentType
     *
     * @return The contentType
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * Gets the content
     *
     * @return The content
     */
    public byte[] getContent() {
        return content;
    }
}
