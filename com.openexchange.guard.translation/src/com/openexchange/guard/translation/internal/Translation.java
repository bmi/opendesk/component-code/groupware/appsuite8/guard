/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.translation.internal;

import java.util.ArrayList;

public class Translation {

    private String msgId;

    private String plural;

    private String[] translations;

    private String translation;

    /**
     * Initialises a new {@link Translation}.
     * 
     * @param id The identifier of the translation
     * @param translation The translation
     */
    public Translation(String id, String translation) {
        msgId = id;
        this.translation = translation;
    }

    /**
     * Initialises a new {@link Translation}.
     * 
     * @param id The identifier
     * @param plural The plural form
     * @param translations The available translations
     */
    public Translation(String id, String plural, ArrayList<String> translations) {
        msgId = id;
        this.plural = plural;
        this.translations = translations.toArray(new String[translations.size()]);
    }

    /**
     * Gets the msgId
     *
     * @return The msgId
     */
    public String getMsgId() {
        return msgId;
    }

    /**
     * Gets the plural
     *
     * @return The plural
     */
    public String getPlural() {
        return plural;
    }

    /**
     * Gets the translations
     *
     * @return The translations
     */
    public String[] getTranslations() {
        return translations;
    }

    /**
     * Gets the translation
     *
     * @return The translation
     */
    public String getTranslation() {
        return translation;
    }

}
