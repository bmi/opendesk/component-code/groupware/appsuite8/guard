package com.openexchange.guard.translation;

/**
 * {@link TemplateTransformation} allows dynamically modifying a template before translation
 */
public interface TemplateTransformation {
    /**
     * Transforms the given template
     * @param template the template
     * @return the transformed template
     */
    String transformTemplate(String template);
}
