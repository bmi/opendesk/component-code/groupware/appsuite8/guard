/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.translation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * {@link ConditionalVissibleTemplateTransformation} allows to define conditional elements in templates.
 *
 * <p>Put the following pattern in your template in order define conditional content: #if(name)..#endif
 *
 * <p>If the condition passed to the constructor is true, {@link ConditionalVissibleTemplateTransformation} will remove the outer parts of the pattern, which are #if(name) and #endif, while the inner text will be remaining.
 * If the condition is false, the whole pattern including the inner text will be removed.
 */
public class ConditionalVissibleTemplateTransformation implements TemplateTransformation {

    private final String name;
    boolean transform;

    /**
     *
     * Initializes a new {@link ConditionalVissibleTemplateTransformation}.
     * 
     * @param name The name of the condition
     * @param condition true, if the inner text of the condition pattern should remain, false, if the whole pattern should be removed
     */
    public ConditionalVissibleTemplateTransformation(String name, boolean condition) {
        this.name = name;
        this.transform = condition;
    }

    @Override
    public String transformTemplate(String template) {
        String regex = "#if\\((" + name + ")\\)(.*?)(#endif)";
        Pattern pattern = Pattern.compile(regex, Pattern.DOTALL);
        Matcher matcher = pattern.matcher(template);
        while (matcher.find()) {
            if (matcher.group(1).equals(name)) {
                if (transform) {
                    //removing everything
                    template = matcher.replaceAll("");
                } else {
                    //Only keeping the middle group (without the #if and #endif groups)
                    template = matcher.replaceAll("$2");
                }
            }
        }
        return template;
    }
}
