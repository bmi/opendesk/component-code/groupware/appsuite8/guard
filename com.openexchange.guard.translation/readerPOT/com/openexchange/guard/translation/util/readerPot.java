/**
 * Create POT file from reader.html and scripts/reader.js
 * Simple parsing for i18n tags
 * If translating string "text", formatting is
 * In Javascript, looks for i18n.t("text")
 * In HTML, looks for data-i18n="text", can have data-i18n="[property]text;"
 */

package com.openexchange.guard.translation.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class translation {

    public String id = "";
    public String trans = "";
    public List<String> comments = new ArrayList<String>();

    public translation(String id, String trans) {
        this.id = id;
        this.trans = trans;
    }

    public void addComment(String comment) {
        comments.add(comment);
    }

    public String getComments() {
        StringBuilder data = new StringBuilder();
        for (String i : comments) {
            data.append(i).append("\r\n");
        }
        return (data.toString());
    }
}

public class readerPot {

    private static HashMap<String, translation> map;
    private static String header = "msgid \"\"\r\n" + "msgstr \"\"\r\n" + "\"Project-Id-Version: node-gettext\\n\"\r\n" + "\"MIME-Version: 1.0\\n\"\r\n" + "\"Content-Type: text/plain; charset=UTF-8\\n\"\r\n" + "\"Content-Transfer-Encoding: 8bit\\n\"\r\n" + "\"Plural-Forms: nplurals=2; plural=(n != 1)\\n\"\r\n" + "\"POT-Creation-Date: \\n\"\r\n" + "\"PO-Revision-Date: \\n\"\r\n" + "\"Last-Translator: \\n\"\r\n" + "\"Language-Team: \\n\"\r\n\r\n";

    public static void main(String[] args) {

        try {
            String readerLocation = readerPot.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath() + "../../guest-reader/";
            map = new HashMap<String, translation>();
            String pattern = "i18n\\.t\\((\\\"|\\')[^(\\\"|\\')]+";
            read(readerLocation + "scripts/reader.js", pattern);
            pattern = "data-i18n[ ]*=[ ]*\"[^\"]*";
            read(readerLocation + "reader.html", pattern);
            writePOT(readerLocation);
            System.out.println("Done");
        } catch (Exception ex) {
            System.out.println(ex);
        }

    }

    public static void read(String filename, String pattern) throws IOException {
        Pattern jspattern = Pattern.compile(pattern);
        System.out.println("Reading file " + filename);
        File f = new File(filename);
        if (!f.exists()) {
            System.out.println("File not found " + filename);
            return;
        }
        BufferedReader br = new BufferedReader(new FileReader(filename));
        String line;
        String buffer = "";
        int lineNum = 1;
        try {
            while ((line = br.readLine()) != null) {
                Matcher jsmatcher = jspattern.matcher(line.trim());
                if (jsmatcher.find()) {
                    String id = add(jsmatcher.group());
                    if (!buffer.equals("")) {
                        addComment("#. " + buffer, id);
                    }
                    addComment("#: " + filename + ":" + lineNum, id);
                }
                if (line.contains("//#.")) {
                    buffer = line.replace("//#.", "").trim();
                } else
                    buffer = "";
                lineNum++;
            }
        } finally {
            if (br != null) {
                br.close();
            }
        }
    }

    public static String add(String data) {
        if (data.contains("\"")) {
            data = data.substring(data.indexOf("\"") + 1);
        } else {
            data = data.substring(data.indexOf("'") + 1);
        }
        if (data.contains("[")) {
            String remark = data.substring(data.indexOf("["), data.indexOf("]") + 1);
            data = data.replace(remark, "");
            if (data.contains(";")) {
                String nextdata = data.substring(data.indexOf(";") + 1);
                add(nextdata);
                data = data.substring(0, data.indexOf(";"));
            }
        }
        if (!map.containsKey(data)) {
            translation tr = new translation(data, "");
            map.put(data, tr);
        }
        return (data);
    }

    public static void addComment(String comment, String id) {
        map.get(id).addComment(comment);
    }

    public static void writePOT(String readerLocation) {
        Writer writer;
        try {
            System.out.println("Writing POT file");
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(readerLocation + "/l10n/translation.pot"), "utf-8"));
            try {
                writer.write(header);
                for (String key : map.keySet()) {
                    writer.write(map.get(key).getComments());
                    writer.write("msgid \"" + key + "\"\r\n");
                    writer.write("msgstr \"\"\r\n\r\n");
                }
            } finally {
                if (writer != null) {
                    writer.close();
                }
            }

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
}
