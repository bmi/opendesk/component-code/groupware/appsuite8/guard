package com.openexchange.guard.guestreset.exceptions;

import com.openexchange.i18n.LocalizableStrings;

public class GuestResetExceptionMessages implements LocalizableStrings {

    // Unable to find the guest account
    public static final String UNKNOWN_GUEST_ACCOUNT_MSG = "Unable to find guest account";
    // Unable to reset the user with the specified email address
    public static final String UNABLE_TO_RESET_MSG = "Unable to reset user with email '%1$s'.";

    private GuestResetExceptionMessages() {
        super();
    }
}