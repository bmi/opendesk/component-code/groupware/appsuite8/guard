/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.dns.internal;

import java.io.IOException;
import java.time.Duration;
import java.util.List;
import org.xbill.DNS.Message;
import org.xbill.DNS.Resolver;
import org.xbill.DNS.ResolverListener;
import org.xbill.DNS.TSIG;

/**
 * {@link AbstractResolverDecorator} is an abstract decorator which does not adding behavior and just delegates.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public abstract class AbstractResolverDecorator implements Resolver {

    private final Resolver decoratedResolver;

    /**
     * Initializes a new {@link AbstractResolverDecorator}.
     * 
     * @param decoratedResolver
     */
    public AbstractResolverDecorator(Resolver decoratedResolver) {
        this.decoratedResolver = decoratedResolver;
    }

    /**
     * Gets the decorated Resolver
     * 
     * @rteturn The decorated resolver
     */
    public Resolver getDecoratedResolver() {
        return decoratedResolver;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.xbill.DNS.Resolver#setPort(int)
     */
    @Override
    public void setPort(int port) {
        decoratedResolver.setPort(port);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.xbill.DNS.Resolver#setTCP(boolean)
     */
    @Override
    public void setTCP(boolean flag) {
        decoratedResolver.setTCP(flag);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.xbill.DNS.Resolver#setIgnoreTruncation(boolean)
     */
    @Override
    public void setIgnoreTruncation(boolean flag) {
        decoratedResolver.setIgnoreTruncation(flag);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.xbill.DNS.Resolver#setEDNS(int)
     */
    @Override
    public void setEDNS(int level) {
        decoratedResolver.setEDNS(level);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.xbill.DNS.Resolver#setEDNS(int, int, int, java.util.List)
     */
    @Override
    public void setEDNS(int level, int payloadSize, int flags, List options) {
        decoratedResolver.setEDNS(level, payloadSize, flags, options);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.xbill.DNS.Resolver#setTSIGKey(org.xbill.DNS.TSIG)
     */
    @Override
    public void setTSIGKey(TSIG key) {
        decoratedResolver.setTSIGKey(key);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.xbill.DNS.Resolver#setTimeout(int, int)
     */
    @Override
    public void setTimeout(int secs, int msecs) {
        decoratedResolver.setTimeout(secs, msecs);
    }

    @Override
    public void setTimeout(Duration timeout) {
        decoratedResolver.setTimeout(timeout);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.xbill.DNS.Resolver#setTimeout(int)
     */
    @Override
    public void setTimeout(int secs) {
        decoratedResolver.setTimeout(secs);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.xbill.DNS.Resolver#send(org.xbill.DNS.Message)
     */
    @Override
    public Message send(Message query) throws IOException {
        return decoratedResolver.send(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.xbill.DNS.Resolver#sendAsync(org.xbill.DNS.Message, org.xbill.DNS.ResolverListener)
     */
    @Override
    public Object sendAsync(Message query, ResolverListener listener) {
        return decoratedResolver.sendAsync(query, listener);
    }
}
