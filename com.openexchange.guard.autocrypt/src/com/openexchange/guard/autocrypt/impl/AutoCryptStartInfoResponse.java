/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.autocrypt.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * {@link AutoCryptStartInfoResponse}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.2
 */
public class AutoCryptStartInfoResponse {

    private String format;
    private String start;

    public AutoCryptStartInfoResponse (String format, String start) {
        this.format = format;
        this.start = start;
    }

    public String getFormat() {
        return this.format;
    }

    public String getStart() {
        return this.start;
    }

    public int getLength() {
        if (!format.matches("numeric[0-9]+x[0-9]+")) return 0;
        Pattern pattern = Pattern.compile("([0-9]+)");
        Matcher matcher = pattern.matcher(format);
        int total = 0;
        if (matcher.find()) {
            int first = Integer.parseInt(matcher.group(1));
            if (matcher.find()) {
                total = first * Integer.parseInt(matcher.group(1));
            }
        }
        return total;
    }

}
