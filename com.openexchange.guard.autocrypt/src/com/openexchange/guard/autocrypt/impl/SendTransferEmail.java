/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.autocrypt.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Arrays;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.openpgp.PGPEncryptedData;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.export.KeyExportUtil;
import com.openexchange.guard.mailcreator.MailCreatorService;
import com.openexchange.guard.notification.GuardNotificationService;
import com.openexchange.guard.translation.GuardTranslationService;

/**
 * {@link SendTransferEmail}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v7.10.1
 */
public class SendTransferEmail {

    private static final String AUTOCRYPT_TEMPLATE = "autocryptTransfer.html";

    MailCreatorService mailCreator;
    GuardTranslationService translationService;
    GuardConfigurationService guardConfigService;
    GuardNotificationService notificationService;

    public SendTransferEmail (MailCreatorService mailCreator, GuardTranslationService translationService, GuardConfigurationService guardConfigService, GuardNotificationService notificationService) {
        this.mailCreator = mailCreator;
        this.translationService = translationService;
        this.guardConfigService = guardConfigService;
        this.notificationService = notificationService;
    }

    /**
     * Generate a secure random based numeric password, with dashes every 4
     * @return
     */
    private String generatePasscode () {
        SecureRandom rand = new SecureRandom();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 9; i++) {
            int digits = rand.nextInt(10000);
            sb.append(String.format("%04d", digits));
            if (i < 8) {
                sb.append("-");
            }
        }
        return sb.toString();
    }

    /**
     * Add header to the pgp armored body with the password details
     * @param pgpbody
     * @param passcode
     * @return
     */
    private String addHeader(String pgpbody, String passcode) {
        if (pgpbody.indexOf("BEGIN PGP MESSAGE") > 0) {
            int start = pgpbody.indexOf("\n", pgpbody.indexOf("BEGIN PGP MESSAGE"));
            StringBuilder sb = new StringBuilder();
            sb.append(pgpbody.substring(0, start));
            sb.append("\nPassphrase-Format: numeric9x4");
            sb.append("\nPassphrase-Begin: " + passcode.substring(0, 2));
            sb.append("\n");
            sb.append(pgpbody.substring(start+1));
            return sb.toString();
        }
        return pgpbody;
    }

    /**
     * Generate the body part containing the encrypted secret key
     * @param keyring
     * @param passcode
     * @return
     * @throws OXException
     */
    private BodyPart generateBodyPart(PGPSecretKeyRing keyring, String passcode) throws OXException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (InputStream in = new ByteArrayInputStream(KeyExportUtil.armorPGPObject(keyring.getEncoded()).getBytes(StandardCharsets.UTF_8)))
        {
            // Autocrypt standards specifies AES 128
            AutoCryptCrypto.encryptSymmetric(in, out, PGPEncryptedData.AES_128, passcode.toCharArray());
            BodyPart attachment = new MimeBodyPart();
            attachment.setText(addHeader(out.toString(StandardCharsets.UTF_8.name()), passcode));
            attachment.setHeader("Content-Type", "application/autocrypt-setup");
            attachment.setHeader("Content-Dispostition", "attachment; filename=\"autocrypt-setup-message.html\"");
            return attachment;

        } catch (IOException | PGPException | MessagingException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, "Unable to create autocrypt transfer email");
        } finally {
            IOUtils.closeQuietly(out);
        }
    }

    /**
     * Sends an email to the user containing their key encrypted with a random passcode
     * @param keyring
     * @param key
     * @param host host of the user
     * @return
     * @throws OXException
     */
    public TransferResponse sendEmail(PGPSecretKeyRing keyring, GuardKeys key, String host) throws OXException {
        String passcode = generatePasscode();
        int templId = guardConfigService.getIntProperty(GuardProperty.templateID, key.getUserid(), key.getContextid());
        String template = translationService.getTranslation(AUTOCRYPT_TEMPLATE, key.getLanguage(), templId);
        JsonObject email = mailCreator.templToEmail(template, key.getEmail(),
            Arrays.asList( key.getEmail(), key.getEmail() ),
            host, key.getUserid(), key.getContextid());
        JsonObject headers = new JsonObject();
        headers.addProperty("Autocrypt-Setup-Message", "v1");
        email.get("data").getAsJsonObject().add("headers", headers);
        notificationService.send(email, Arrays.asList(generateBodyPart(keyring, passcode)), key.getUserid(), key.getContextid(), null);
        return new TransferResponse(passcode);
    }

}
