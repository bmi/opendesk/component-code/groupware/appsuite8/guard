/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.autocrypt.database.impl;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.autocrypt.database.AutoCryptKey;
import com.openexchange.guard.autocrypt.database.AutoCryptStorageService;
import com.openexchange.guard.autocrypt.impl.AutoCryptUtils;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.guard.database.utils.GuardConnectionWrapper;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.OGPGPKey;
import com.openexchange.guard.keymanagement.commons.OGPGPKeyRing;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;

/**
 * {@link AutoCryptStorageServiceImpl}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.2
 */
public class AutoCryptStorageServiceImpl implements AutoCryptStorageService {

    GuardDatabaseService guardDatabaseService;
    private static Logger LOG = LoggerFactory.getLogger(AutoCryptStorageServiceImpl.class);

    public AutoCryptStorageServiceImpl(GuardDatabaseService guardDatabaseService) {
        this.guardDatabaseService = guardDatabaseService;
    }

    private AutoCryptKey createKeyFromResultset(ResultSet rs, int userId, int cid, String email) throws OXException, SQLException {
        return new AutoCryptKey(
            userId,
            cid,
            email,
            rs.getBytes("pubkey"),
            rs.getLong("lastUpdate"),
            rs.getString("preference"),
            rs.getString("hash"),
            rs.getBoolean("verified"));
    }

    private OGPGPKeyRing createKeyRing (AutoCryptKey key) {
        return new OGPGPKeyRing(PGPKeysUtil.getFingerPrint(key.getPGPPublicKeyRing().getPublicKey().getFingerprint()), key.getPGPPublicKeyRing(), false, key.getVerified());
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.autocrypt.database.AutoCryptStorageService#getKey(java.lang.String, int, int)
     */
    @Override
    public AutoCryptKey getKey(String email, int userId, int cid) throws OXException {
        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getReadOnly(userId, cid, 0);
        Connection connection = connectionWrapper.getConnection();
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(AutoCryptSql.SELECT_KEY);
            stmt.setInt(1, userId);
            stmt.setInt(2, cid);
            stmt.setString(3, email);
            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return createKeyFromResultset(resultSet, userId, cid, email);
            }
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.autocrypt.database.AutoCryptStorageService#storeAutoCryptKey(com.openexchange.guard.autocrypt.database.AutoCryptKey)
     */
    @Override
    public boolean storeAutoCryptKey(AutoCryptKey key) throws OXException {
        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(key.getUserId(), key.getCid(), 0);
        Connection connection = connectionWrapper.getConnection();
        PreparedStatement stmt = null;
        try {
            String hash = AutoCryptUtils.getHash(key);
            String fingerprint = PGPKeysUtil.getFingerPrint(key.getPGPPublicKeyRing().getPublicKey().getFingerprint());
            stmt = connection.prepareStatement(AutoCryptSql.INSERT_KEY);
            stmt.setInt(1, key.getUserId());
            stmt.setInt(2, key.getCid());
            stmt.setString(3, key.getEmail());
            stmt.setBytes(4, key.getPGPPublicKeyRing().getEncoded());
            stmt.setString(5, hash);
            stmt.setLong(6, key.getLastUpdate());
            stmt.setString(7, key.getPreferenceString());
            stmt.setString(8, fingerprint);
            stmt.setBoolean(9, key.getVerified());
            stmt.setBytes(10, key.getPGPPublicKeyRing().getEncoded());
            stmt.setString(11, hash);
            stmt.setLong(12, key.getLastUpdate());
            stmt.setString(13, key.getPreferenceString());
            stmt.setString(14, fingerprint);
            stmt.setBoolean(15, key.getVerified());
            stmt.execute();
            return true;

        } catch (SQLException | IOException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
    }

    @Override
    public List<OGPGPKeyRing> getAllKeys(int userId, int cid) throws OXException {
        return getAllKeys(userId, cid, false);
    }

    @Override
    public List<OGPGPKeyRing> getAllVerifiedKeys(int userId, int cid) throws OXException {
        return getAllKeys(userId, cid, true);
    }


    public List<OGPGPKeyRing> getAllKeys(int userId, int cid, boolean verified) throws OXException {
        ArrayList<OGPGPKeyRing> keys = new ArrayList<OGPGPKeyRing>();
        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getReadOnly(userId, cid, 0);
        Connection connection = connectionWrapper.getConnection();
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            if (verified) {
                stmt = connection.prepareStatement(AutoCryptSql.SELECT_ALL_VERIFIED_KEYS);
            } else {
                stmt = connection.prepareStatement(AutoCryptSql.SELECT_ALL_KEYS);
            }
            stmt.setInt(1, userId);
            stmt.setInt(2, cid);
            resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                keys.add(createKeyRing(createKeyFromResultset(resultSet, userId, cid, resultSet.getString("email"))));
            }
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
        return keys;
    }

    @Override
    public boolean deleteKey(int userId, int cid, String fingerprint) throws OXException {
        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(userId, cid, 0);
        Connection connection = connectionWrapper.getConnection();
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(AutoCryptSql.DELETE_KEY);
            stmt.setInt(1, userId);
            stmt.setInt(2, cid);
            stmt.setString(3, fingerprint);

            stmt.execute();
            return true;

        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }

    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.autocrypt.database.AutoCryptStorageService#verifyKey(int, int, java.lang.String, boolean)
     */
    @Override
    public boolean verifyKey(int userId, int cid, String fingerprint, boolean verified) throws OXException {
        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(userId, cid, 0);
        Connection connection = connectionWrapper.getConnection();
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(AutoCryptSql.SET_VERIFIED);
            stmt.setBoolean(1, verified);
            stmt.setInt(2, userId);
            stmt.setInt(3, cid);
            stmt.setString(4, fingerprint);
            stmt.execute();
            return true;

        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.autocrypt.database.AutoCryptStorageService#deleteAllForUser(int, int)
     */
    @Override
    public void deleteAllForUser(int userId, int contextId) throws OXException {
        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(userId, contextId, 0);
        Connection connection = connectionWrapper.getConnection();
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(AutoCryptSql.DELETE_FOR_USER);
            stmt.setInt(1, userId);
            stmt.setInt(2, contextId);
            stmt.execute();
            return;

        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
    }


}
