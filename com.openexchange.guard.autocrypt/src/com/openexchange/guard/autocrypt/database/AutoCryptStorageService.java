/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.autocrypt.database;

import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.OGPGPKeyRing;

/**
 * {@link AutoCryptStorageService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.2
 */
public interface AutoCryptStorageService {

    /**
     * Get key specified by email address
     * @param email
     * @param userId
     * @param cid
     * @return
     * @throws OXException
     */
    public AutoCryptKey getKey (String email, int userId, int cid) throws OXException;

    /**
     * Store and autocrypt key
     * @param key
     * @return
     * @throws OXException
     */
    public boolean storeAutoCryptKey (AutoCryptKey key) throws OXException;

    /**
     * Get all keys associated with user
     * @param userId
     * @param cid
     * @return
     * @throws OXException
     */
    public List<OGPGPKeyRing> getAllKeys (int userId, int cid) throws OXException;

    /**
     * Delete autocrypt key
     * @param userId
     * @param cid
     * @param fingerprint
     * @return
     * @throws OXException
     */
    boolean deleteKey(int userId, int cid, String fingerprint) throws OXException;

    boolean verifyKey(int userId, int cid, String fingerprint, boolean verified) throws OXException;

    /**
     * @param userId
     * @param cid
     * @return
     * @throws OXException
     */
    List<OGPGPKeyRing> getAllVerifiedKeys(int userId, int cid) throws OXException;

    /**
     * Deletes all autocrypt keys for a user
     * @param userId
     * @param contextId
     * @throws OXException
     */
    public void deleteAllForUser (int userId, int contextId) throws OXException;

}
