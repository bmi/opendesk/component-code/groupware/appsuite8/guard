/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.autocrypt.database;

import com.openexchange.database.AbstractCreateTableImpl;

/**
 * {@link CreateAutoCryptTable}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.2
 */
public class CreateAutoCryptTable extends AbstractCreateTableImpl {

    private String createAutoCryptTable() {
        return "CREATE TABLE `og_AutoCrypt` (" +
            "`id`  int NOT NULL ," +
            "`cid`  int NOT NULL ," +
            "`email`  varchar(255) NOT NULL ," +
            "`pubkey`  varbinary(4096) NULL ," +
            "`hash`  varchar(255) NULL ," +
            "`lastUpdate`  bigint NULL," +
            "`preference`  varchar(255) NULL ," +
            "`fingerprint` varchar(255) NULL," +
            "`verified` bit(1) NOT NULL DEFAULT b'0'," +
            "PRIMARY KEY (`id`, `cid`, `email`)" +
            ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
    }


    /* (non-Javadoc)
     * @see com.openexchange.database.CreateTableService#requiredTables()
     */
    @Override
    public String[] requiredTables() {
        return new String[] {};
    }

    /* (non-Javadoc)
     * @see com.openexchange.database.CreateTableService#tablesToCreate()
     */
    @Override
    public String[] tablesToCreate() {
        return new String[] { "og_AutoCrypt" };
    }

    /* (non-Javadoc)
     * @see com.openexchange.database.AbstractCreateTableImpl#getCreateStatements()
     */
    @Override
    protected String[] getCreateStatements() {
        return new String[] { createAutoCryptTable() };
    }

}
