/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.inputvalidation;

import com.openexchange.exception.OXException;
import com.openexchange.guard.inputvalidation.exceptions.InputValidationExceptionCodes;

/**
 * {@link AbstractInputValidator} validates a given input
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public abstract class AbstractInputValidator<T> implements InputValidator<T> {

    /* (non-Javadoc)
     * @see com.openexchange.guard.inputvalidation.InputValidator#getValue(java.lang.Object)
     */
    @Override
    public T getInput(T input) {
        return isValid(input) ? input : null;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.inputvalidation.InputValidator#assertIsValid(java.lang.Object)
     */
    @Override
    public void assertIsValid(T input) throws OXException {
        assertIsValid(input, "user input");
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.inputvalidation.InputValidator#assertIsValid(java.lang.Object, java.lang.String)
     */
    @Override
    public void assertIsValid(T input, String inputFieldName) throws OXException {
        if (!isValid(input)) {
            throw InputValidationExceptionCodes.INVALID_INPUT_ERROR.create(inputFieldName);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.inputvalidation.InputValidator#assertInput(java.lang.Object)
     */
    @Override
    public T assertInput(T input) throws OXException {
        assertIsValid(input);
        return input;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.inputvalidation.InputValidator#assertInput(java.lang.Object, java.lang.String)
     */
    @Override
    public T assertInput(T input, String inputFieldName) throws OXException {
        assertIsValid(input, inputFieldName);
        return input;
    }
}
