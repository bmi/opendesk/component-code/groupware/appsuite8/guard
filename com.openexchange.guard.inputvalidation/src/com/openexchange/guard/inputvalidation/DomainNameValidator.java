/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.inputvalidation;

import java.net.InetAddress;
import java.net.UnknownHostException;
import org.apache.commons.validator.routines.DomainValidator;

/**
 * {@link DomainNameValidator}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.2
 */
public class DomainNameValidator extends AbstractInputValidator<String> {

    /*
     * (non-Javadoc)
     * 
     * @see com.openexchange.guard.inputvalidation.InputValidator#isValid(java.lang.Object)
     */
    @Override
    public boolean isValid(String input) {
        return DomainValidator.getInstance().isValid(input);
    }

    /**
     * Check if domain refers to a local system address, i.e. 192.168/16, 10/8, 172.16/12, etc
     * isLocalAddress
     *
     * @param domain
     * @return True if refers to a local IP
     */
    public static boolean isLocalAddress(String domain) {
        InetAddress addr;
        try {
            addr = InetAddress.getByName(domain);
            return addr.isSiteLocalAddress();
        } catch (UnknownHostException e) {
            return false;
        }
    }
}
