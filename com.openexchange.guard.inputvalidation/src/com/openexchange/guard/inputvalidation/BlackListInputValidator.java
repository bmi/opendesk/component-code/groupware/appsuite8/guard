/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.inputvalidation;

/**
 * {@link BlackListInputValidator} performs simple black list validation based on equals
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public class BlackListInputValidator<T> extends AbstractInputValidator<T> {

    private final T[] invalidInputs;

    /**
     * Initializes a new {@link BlackListInputValidator}.
     * 
     * @param invalidInputs The black list, i.e. a list of not allowed input values which considered to be not valid.
     */
    public BlackListInputValidator(T[] invalidInputs) {
        this.invalidInputs = invalidInputs;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.openexchange.guard.inputvalidation.InputValidator#isValid(java.lang.Object)
     */
    @Override
    public boolean isValid(T input) {
        for (T invalidInput : invalidInputs) {
            if (invalidInput.equals(input)) {
                return false;
            }
        }
        return true;
    }
}
