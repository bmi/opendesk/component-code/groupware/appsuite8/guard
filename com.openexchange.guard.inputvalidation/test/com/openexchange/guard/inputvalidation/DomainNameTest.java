/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.inputvalidation;

import java.util.HashMap;
import java.util.Map.Entry;
import org.junit.Assert;
import org.junit.Test;

/**
 * {@link DomainNameTest}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.2
 */
public class DomainNameTest {
    static final boolean VALID = true;
    static final boolean INVALID = false;
    private static final HashMap<String, Boolean> domains;

    static {
        domains = new HashMap<String, Boolean>();

        domains.put("example.com", VALID);
        domains.put("example.com.", VALID);
        domains.put("subdomain.example.com", VALID);
        domains.put("subsubdomain.subdomain.example.com", VALID);

        domains.put("jksldfjdlf", INVALID);
        domains.put("jksldfjdlf.", INVALID);
        domains.put("example.com/endpoint?action=something", INVALID);
        domains.put("example.com/", INVALID);
    }

    private void testDomainName(String domain, boolean expectedValid) {
        boolean valid = new DomainNameValidator().isValid(domain);
        if(expectedValid) {
            Assert.assertTrue("The domain name " + domain + " should be considered as valid", valid);
        }
        else {
            Assert.assertFalse("The domain name " + domain + " should NOT be considered as valid", valid);
        }
    }

    @Test
    public void testDomainNames() throws Exception {
        for(Entry<String, Boolean> domain : domains.entrySet()) {
            testDomainName(domain.getKey(), domain.getValue());
        }
    }
}
