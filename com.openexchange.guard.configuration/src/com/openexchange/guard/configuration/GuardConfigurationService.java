/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.configuration;

import java.io.File;
import com.openexchange.config.Reloadable;
import com.openexchange.exception.OXException;

/**
 * {@link GuardConfigurationService}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public interface GuardConfigurationService extends Reloadable {

    /**
     * Fetches the value of specified for the given String
     *
     * @param property The key the configuration should be fetched
     * @return The string value of the property
     */
    String getProperty(String property);

    /**
     * Fetches a property from a specified property file
     * @param fileName the name of the property file
     * @param property the property to fetch
     * @return the value of the property
     */
    String getPropertyFromFile(String fileName, String property);

    /**
     * Fetches the value of specified {@link GuardProp}
     *
     * @param property The {@link GuardProp} name to fetch
     * @return The string value of the property
     */
    String getProperty(GuardProp property);

    /**
     * Fetches the value of specified {@link GuardProp}
     *
     * @param property The {@link GuardProp} name to fetch
     * @return The integer value of the property
     */
    int getIntProperty(GuardProp property);

    /**
     * Fetches the value of specified {@link GuardProp}
     *
     * @param property The {@link GuardProp} name to fetch
     * @return The the boolean value of the property
     */
    boolean getBooleanProperty(GuardProp property);

    /**
     * Fetches the value of specified {@link GuardProp}
     *
     * @param property The {@link GuardProp} name to fetch
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The string value of the property
     */
    String getProperty(GuardProp property, int userId, int contextId);

    /**
     * Fetches the value of specified {@link GuardProp}
     *
     * @param property The {@link GuardProp} name to fetch
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The integer value of the property
     */
    int getIntProperty(GuardProp property, int userId, int contextId);

    /**
     * Fetches the value of specified {@link GuardProp}
     *
     * @param property The {@link GuardProp} name to fetch
     * @param userId The user identifier
     * @param contextId The context identifier
     * @return The the boolean value of the property
     */
    boolean getBooleanProperty(GuardProp property, int userId, int contextId);

    /**
     * Fetches the string value of the specified OX property for the specified user in the specified context
     *
     * @param property The name of the OX property
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param defaultValue The default value to return if the property does not exist
     * @return The string value if the property exists, or <code>null</code>
     */
    String getProperty(String property, int userId, int contextId, String defaultValue);

    /**
     * Fetches the integer value of the specified OX property for the specified user in the specified context
     *
     * @param property The name of the OX property
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param defaultValue The default value to return if the property does not exist
     * @return The integer value if the property exists, or <code>-1</code>
     */
    int getIntProperty(String property, int userId, int contextId, int defaultValue);

    /**
     * Fetches the boolean value of the specified OX property for the specified user in the specified context
     *
     * @param property The name of the OX property
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param defaultValue The default value to return if the property does not exist
     * @return The boolean value if the property exists
     */
    boolean getBooleanProperty(String property, int userId, int contextId, boolean defaultValue);

    /**
     * Gets the file denoted by given file name.
     *
     * @param fileName The file name
     * @return The file or <code>null</code>
     */
    File getPropertyFile(String propertyFileName);

    /**
     * Gets the directory denoted by given directory name.
     *
     * @param directoryName The directory name
     * @return The directory or <code>null</code>
     */
    File getPropertyDirectory(String propertyDirectory);

    /**
     * Gets a common server configuration
     *
     * @param host the host to get the configuration for
     * @param configKey the key of the configuration value to get
     * @return the configuration value for the parameters
     * @throws OXException
     */
    <T> T getServerConfiguration(String host,String configKey) throws OXException;

    /**
     * Gets a user specific server configuration
     * @param host the host to get the configuration for
     * @param userid the id of the user to get the configuration for
     * @param cid the context of the user to get the configuration for
     * @param configKey the key of the configuration value to get
     * @return the configuration value for the parameters
     * @throws OXException
     */
    <T> T getServerConfiguration(String host, int userid, int cid, String configKey) throws OXException;
}
