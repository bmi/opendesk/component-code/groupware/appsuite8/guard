/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.configuration;

/**
 * {@link GuardProperty} defines all OX Guard properties. Please keep this enum up-to-date.
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public enum GuardProperty implements GuardProp {

    /**
     * Defines whether Guard accepts unsigned SRV records when performing HKP Server lookup via DNS.
     * <br>
     * <br>
     * If false OX Guard will reject SRV Records which are not DNSSEC signed.
     * <br>
     * If true OX Guard logs a warning message when accepting non DNSSEC records.
     */
    allowUnsignedSRVRecords(true, GuardProperty.PREFIX + "dns."),
    /**
     * Defines the API user agent
     */
    apiUserAgent("Open-Xchange Guard Server"),
    /**
     * Specify encryption strength and key length.
     * AES_Key_Length: 256
     */
    aesKeyLength(256),
    /**
     * Attach help file to outgoing emails
     */
    attachHelpFile(true),
    /**
     * Maximum lifetime of an authentication-token.
     */
    authLifeTime("1W"),
    /**
     * Autocrypt enabled
     */
    autoCryptEnabled(true),
    /**
     * Autocrypt outgoing prefer-encrypt set to mutual
     */
    autoCryptMutual(true),
    /**
     * Defines how long someone will be locked out after bad attempts.
     * Defaults to 10 minutes
     */
    badMinuteLock(10),
    /**
     * Defines how many times a person can attempt to unlock an
     * encrypted item before being locked out.
     * Defaults to 5
     */
    badPasswordCount(5),
    /**
     * Base domain defines the domain used to generate support links.
     * For example, a URL may be constructed using
     * https://baseDomain/uiWebPath/link
     * If empty, externalEmailUrl used
     */
    baseDomain(""),
    /**
     * The timeout for all HTTP(S) connections in ms
     */
    connectionTimeout(10000),
    /**
     * At what hour of the day should the Guard service execute the
     * internal maintenance cron jobs? Possible values are:
     * 0 - 23
     */
    cronHour(2),
    /**
     * Specifies the MySQL password for accessing the Guard database.
     */
    databasePassword,
    /**
     * Specifies the MySQL username for accessing the Guard database.
     */
    databaseUsername,
    /**
     * Specifies the base name for the Guard databases. On initialisation
     * Guard will create a database with the baseName, then additional
     * Guest shards with the name baseName_#
     * Default base name = 'oxguard'
     */
    dbSchemaBaseName("oxguard"),
    defaultLanguage("en_US"),
    defaultAdvanced(false),
    demo(false),
    /**
     * The amount of hours specifying how long a deleted and exposed key will be
     * marked as "exposed", or 0 for disabling automatic reset of exposed keys.
     * Note: Resetting is scheduled once a day
     * default: 168 hours = 1 Week
     */
    exposedKeyDurationInHours(168),
    /**
     * When Guard sends an encrypted eMail to members, they may not be using the webmail UI
     * to read the email. A help file is attached, and a link will be provided to log into
     * their webmail to read the encrypted item. This setting is used to point to a generic
     * log in for the webmail system. Sent to multiple recipients, so not customised to
     * the individual recipient. OK domain:port. HTTPS will always be added
     */
    externalEmailURL("example.com"),
    /**
     * When Guard sends an eMail to external recipients those recipients will be able to
     * access the encrypted content by opening a link in that eMail. The description and
     * the link of that eMail are not encrypted and always readable by the recipient. The
     * link points to the Guard reader for external recipients, a servlet to decrypt and
     * display the encrypted eMail content. Specify which domain and path should be used
     * The Https link will be created dynamically by Guard.
     * This value will be used as the default unless over-written by cascade value
     * 'com.openexchange.guard.externalReaderURL'
     */
    externalReaderPath("example.com/guard/reader/reader.html"),
    externalReaderURL,
    /**
     * If the PGP message does not have MDC tags attached, either 1) message is rejected
     * or 2) a warning message is displayed to the user and the email is converted to
     * plaintext.  Default is false, which displays the message.  Setting to true
     * will result in emails being rejected if the MDC data doesn't exist
     */
    failForMissingMDC(false),
    /**
     * The directory used for buffering uploaded data.
     * Default: empty which falls back to use "java.io.tmpdir"
     */
    fileUploadDirectory,
    /**
     * The threshold in bytes at which uploaded data will be buffered to disk.
     * NOTE: Guard does only buffer non sensitive or encrypted data to disk.
     * default: 10240 = 10 Kb
     */
    fileUploadBufferThreshhold(10240),
    /**
     * Defines if Guard should only establish secure TLS connections to HKP servers queried via DNS SVR lookup.
     */
    forceTLSForHKPSRV(false),
    fromEmail,
    /**
     * Guest Caching.  Enabled local caching for Guest users/data
     */
    guestCaching(true),

    /**
     * Guest email accounts that aren't used after this number of days are cleaned.  The mail items are removed, and guest scheduled for removal.
     *
     */
    guestCleanedAfterDaysOfInactivity(365),
    /**
     * Specifies the SMTP server password.
     */
    guestSMTPPassword,
    /**
     * Specifies the SMTP server port.
     */
    guestSMTPPort(25),
    /**
     * Specifies the SMTP server information for replies of external recipients. Those recipients
     * are able to decrypt, display and reply to eMails they receive via the link to the Guard
     * reader in those encrypted eMails.
     * The SMTP server is also used for sending password reset e-mails
     */
    guestSMTPServer,
    /**
     * Specifies the SMTP username.
     */
    guestSMTPUsername,
    /**
     * Specifies email address to use in MAIL FROM
     */
    guestSMTPMailFrom(""),
    /**
     * Interval in seconds to check the RSA cache and re-populate if
     * less than 'rsacachecount'
     */
    keyCacheCheckInterval(30),
    /**
     * PGP Keys can have an expiration date. The default is 10 years, or 3650 days
     * Set the number of days the keys will be valid for. The user will have to
     * create new keys after this date.
     * Set to 0 if no expiration date
     */
    keyValidDays(3650),
    /**
     * Max key size allowed from remote servers. Default 150kb
     */
    maxRemoteKeySize(150000),
    /**
     * Outgoing e-mails all get assigned a mailID. This is usually in a format of
     * a random ID followed by a domain. By default, this domain will be
     * the AppSuite domain from com.openexchange.guard.externalEmailURL
     * Other domain can be configured here
     */
    mailIdDomain,
    /**
     * Mail resolver URL. Guard needs to be able to lookup an email
     * address against the list of OX users. By default, it will
     * try to do this against the OX backend. If there is a custom
     * mail resolver, set it here. The email address will be appended to
     * the end of the URL
     * details here: http://oxpedia.org/wiki/index.php?title=AppSuite:GuardMailResolver
     */
    mailResolverUrl,
    /**
     * Specifies the username for the basic HTTP authentication used for accessing the mail resolver.
     * If no username is specified here, com.openexchange.guard.restApiUsername will be used as fallback (default)
     */
    basicAuthUsername(GuardProperty.EMPTY,GuardProperty.PREFIX + "mailResolverUrl."),
    /**
     * Specifies the password for the basic HTTP authentication used for accessing the mail resolver.
     * If no password is specified here, com.openexchange.guard.restApiPassword will be used as fallback (default)
     */
    basicAuthPassword(GuardProperty.EMPTY,GuardProperty.PREFIX + "mailResolverUrl."),
    /**
     * Index for master key encrypting database
     */
    masterKeyIndex(0),
    /**
     * Path to storage of oxguardpass files
     */
    masterKeyPath("/opt/open-xchange/guard"),
    /**
     * Max number of connections to same URL (route)
     */
    maxHttpConnections(50),
    /**
     * Max number of connections from Guard to backends
     */
    maxTotalConnections(50),
    /**
     * Minimum password length (default=6)
     */
    minPasswordLength(6),
    /**
     * Length of the randomly generated passwords when a user resets
     * password. (default=10)
     */
    newPassLength(10),
    /**
     * When creating a new Guest, option to send them a first password
     * in a seperate email, or let them assign password on first login
     */
    newGuestsRequirePassword(false),
    /**
     * If the user should not be able to delete the private keys,
     * only retract, select true here
     * can also set configuration cascade value
     * 'com.openexchange.capability.guard-nodeleteprivate=true'
     * If either is true, then the user won't be able to delete his/her keys
     */
    noDeletePrivate(false),
    /**
     * Option to deny users the ability to delete the password recovery.
     * There will be no way to retrieve the password
     * if the recovery is deleted and the user forgets
     * their password.  Users, though, may want to delete the recovery for
     * security reasons.
     * Can also set configuration cascade value
     * com.openexchange.capability.guard-nodeleterecovery=true
     * If either is true, then the user will not be able to
     * delete their password recovery
     * Default: false, allowing the user the option to delete
     */
    noDeleteRecovery(false),
    /**
     * If password recovery is disabled, then there will be no way to
     * recover passwords. This will increase the security of the
     * system, but will result in complete data loss if password
     * is lost
     * can also set configuration cascade value
     * 'com.openexchange.capability.guard-norecovery=true'
     * If either is true, then no recovery will be available
     * Default = false
     */
    noRecovery(false),
    okPrivate(true),
    /**
     * HTTP connections to the backend are kept open for faster
     * response. This is the timeout setting that will close idle
     * connections.
     */
    oxBackendIdleTime(60),
    /**
     * URL used to communicated directly with the OX backend
     */
    oxBackendPath("/ajax/"),
    /**
     * Port for communicating with the OX Backend/REST API.
     * Defaults to 8009
     */
    oxBackendPort(8009),
    oxGuardDatabaseHostname("localhost"),
    /**
     * Defines the connection timeout used for init connections to the guard database
     */
    oxGuardDatabaseInitConnectionTimeout(15000),
    /**
     * For read-only slave (optional)
     */
    oxGuardDatabaseRead,
    /**
     * Specifies the hostname / IP address of Guard guest shards
     * This is for the database shards used when sending to Guest users
     * Defines where the NEXT shard will go when created
     * Stores the Guest keys.
     */
    oxGuardShardDatabase("localhost"),
    /**
     * Defines the connection timeout used for init connections to the shard database
     */
    oxGuardShardDatabaseInitConnectionTimeout(15000),
    /**
     * For readonly slave (optional)
     */
    oxGuardShardRead,
    /*
     * E-mails with new passwords are sent to internal OX users when they have a new email
     * sent to them. These password e-mails are used for password reset, and when a new
     * key is generated for a user. This can be configured through the senders configuration
     * cascade, but should be sent here for system email address
     */
    /**
     * Specifies the sender e-mail address for the password reset e-mail
     */
    passwordFromAddress,
    /**
     * Specifies the display name for the password reset e-mail
     */
    passwordFromName,
    /**
     * Optional PIN service for Guest emails.  Sender assigns a PIN to the newly created Guest account.
     * The sender must get this PIN to the user using another method (phone, text, etc)
     */
    pinEnabled(false),
    /**
     * PGP Key's from the remote servers are stored in a cache for a set period of time
     * before the remote servers are queried again. Set the time for the cache here
     */
    pgpCacheDays(7),
    /**
     * PGP lookup server white-list. The PGP public key server normally
     * only shares keys that have already been created unless the
     * the recipient and sender are local. This prevents DOS attacks
     * with multiple PGP Public key searches for users that don't have
     * keys generated yet. If, however, you have a trusted sender / PGP
     * lookup, you can add the IP address here. PGP Public key requests
     * from this key range will result in keys generated for the recipients
     * as long as they have an AppSuite account
     * Comma delim CDIR notation or distinct IP. eg "= 10.0.100.0/24, 192.168.10.3"
     */
    publicKeyWhitelist,
    /**
     * Configurable name for Guard
     */
    productName("Guard"),
    /**
     * Depreciated.  Now moved to trustedPGP and untrustedPGPDirectory
     */
    publicPGPDirectory(""),
    /**
     * Remote Public Key lookup. You can list trusted PGP Servers here, preferably
     * servers that have verified keys. Comma separated list. Can specify port
     * https only on port 443.
     * In addition it is possible to add an optional base request path;
     * for example hkp://example.org:11371/custom/path/to/pks/lookup?
     */
    trustedPGPDirectory(""),
    /**
     * Remote Public Key lookup. You can list untrusted Public PGP Servers here.
     * Keys will be used for sending, but not trusted for verification
     * Comma separated list. Can specify port
     * https only on port 443.
     * In addition it is possible to add an optional base request path;
     * for example hkp://example.org:11371/custom/path/to/pks/lookup?
     */
    untrustedPGPDirectory(""),

    /**
     * Timeout when searching for remote keys.  Total elapsed time in milliseconds before giving up search for keys.
     * This time should be less than the timeout between middleware and Guard
     */
    remoteKeyLookupTimeout(10000),

    /**
     * Specifies the hostname of the Open-Xchange REST API server. The REST API is a
     * service on the Open-Xchange backend. Use 'localhost' in case that the Guard service
     * is deployed on the same system as the Open-Xchange backend. In case that the REST
     * API is deployed on a separate system ensure that Guard can connect, see clustering
     * documentation for Guard for more details.
     */
    restApiHostname("localhost"),
    /**
     * Specifies the authentication password for the basic HTTP authentication
     * as the Open-Xchange REST API requires such.
     */
    restApiPassword,
    /**
     * Specifies the authentication username for the basic HTTP authentication
     * as the Open-Xchange REST API requires such.
     */
    restApiUsername,
    /**
     * RSA keys are pre-generated in the background, encrypted, and
     * stored for future user keys. RSA key generation is the most
     * time consuming function and the RSA cache significantly
     * improves new user creation time
     */
    rsaCache(true),
    /**
     * Number of RSA keys to pre-generate
     */
    rsaCacheCount(100),
    /**
     * Bit certainty for RSA key generation. Higher numbers assure the
     * number is in fact prime but time consuming. Lower is much
     * faster. May need to be lower if not using cache
     */
    rsaCertainty(256),
    /**
     * Specify encryption strength and key length.
     * This used for Proprietary Guard (version 1.2 and earlier).
     * NOT used for PGP
     * RSA_Key_Length: 2048 (Recco 2048)
     */
    rsaKeyLength(2048),
    secureReply(true),
    /**
     * Guest users data are placed in databases oxguard_x. After set
     * number of users, another database shard is created
     */
    shardSize(10000),
    showStatus(true),
    /**
     * Per default the connection between the Guard backend and the configured Open-Xchange
     * REST API host is unencrypted. Even though that Guard will never transmit unencrypted
     * e-mails to or from the REST API you can optionally encrypt the whole communication between
     * those two components by using SSL. Please note: Enabling SSL might decrease performance
     * and/or create more system load due to additional encoding of the HTTP streams.
     */
    backendSSL(false),
    /**
     * Specifies the password for accessing the support API of OX Guard
     */
    supportApiPassword,
    /**
     * Specifies the username for accessing the support API of OX Guard
     */
    supportApiUsername,
    /**
     * If authentication against middleware fails, Guard will delay and retry,
     * allowing for possible delay in Hazelcast syncing session data.  Time in ms
     */
    sessionSyncDelay(3000),
    /**
     * Use TLS when delivering to the SMTP server when available
     */
    useStartTLS(true),
    /**
     * Defines the template identifier
     */
    templateID(0),
    /**
     * Specifies the path for the templates
     */
    templatesDirectory("/opt/open-xchange/templates/guard"),
    /**
     * Defines how long temporary tokens should last before expiring.
     * Time in hours
     */
    temporaryTokenLifespan(48),
    /**
     * A threshold value which defines which key sources are trusted.
     * Every key source with a trust value equals or higher than this value is considered to be marked as "trusted".
     */
    trustThreshold(4, GuardProperty.PREFIX + "keySources."),
    /**
     * The trust level for keys created by OX Guard
     */
    trustLevelGuard(5, GuardProperty.PREFIX + "keySources."),
    /**
     * The trust level for keys uploaded by a user
     */
    trustLevelGuardUserUploaded(4, GuardProperty.PREFIX + "keySources."),
    /**
     * The trust level for keys uploaded by a user and shared among users in the same context
     */
    trustLevelGuardUserShared(3, GuardProperty.PREFIX + "keySources."),
    /**
     * The trust level for keys fetched from public HKP servers
     */
    trustLevelHKPPublicServer(1, GuardProperty.PREFIX + "keySources."),
    /**
     * The trust level for keys fetched from HKP servers marked as trusted
     */
    trustLevelHKPTrustedServer(5, GuardProperty.PREFIX + "keySources."),
    /**
     * The trust level for keys fetched from HKP servers queried via SRV DNS record
     */
    trustLevelHKPSRVServer(4, GuardProperty.PREFIX + "keySources."),
    /**
     * The trust level for keys fetched from HKP servers queried via DNSSEC protected SRV DNS record
     */
    trustLevelHKPSRVDNSSECServer(4, GuardProperty.PREFIX + "keySources."),
    /**
     * The trust for keys generated by incoming autocrypt headers
     */
    trustLevelAutoCrypt(2, GuardProperty.PREFIX + "keySources."),
    /**
     * The trust for keys generated by incoming autocrypt headers AND manual verified by the user
     */
    trustLevelAutoCryptUserVerified(5, GuardProperty.PREFIX + "keySources."),
    /**
     * The trust for keys fetched from a WKS server
     */
    trustLevelWKSServer(3, GuardProperty.PREFIX + "keySources."),
    /**
     * Defines the version of the 'Upgrader'
     */
    version(7, GuardProperty.PREFIX + "upgrader."),
    /**
     * Defines type of file storage.  File, sproxyd
     */
    fileStorageType("", GuardProperty.PREFIX + "storage.file."),
    /**
     * Defines the temporary upload directory for Guard Drive files in case of that the storage
     * type (com.openexchange.guard.storage.type) has been set to 'file'.
     */
    uploadDirectory("/var/spool/open-xchange/guard/uploads", GuardProperty.PREFIX + "storage.file."),
    /**
     * Filestore name to use for S3 storage
     * S3 configuration for the filestore is the same for other middleware S3 storage
     * I.E. com.openexchange.filestore.s3.[filestore].endpoint etc
     */
    s3FileStore(GuardProperty.EMPTY, GuardProperty.PREFIX + "storage.s3."),
    /**
     * Value of server version from Java manifest
     */
    serverVersion("n/a"),

    ;

    private static final String EMPTY = "";

    private static final String PREFIX = "com.openexchange.guard.";

    private final String fqn;

    private final Object defaultValue;

    private GuardProperty() {
        this(EMPTY);
    }

    private GuardProperty(Object defaultValue) {
        this(defaultValue, PREFIX);
    }

    private GuardProperty(Object defaultValue, String fqn) {
        this.defaultValue = defaultValue;
        this.fqn = fqn;
    }

    /**
     * Returns the fully qualified name of the property
     *
     * @return the fully qualified name of the property
     */
    @Override
    public String getFQPropertyName() {
        return fqn + name();
    }

    /**
     * Returns the default value of this property
     *
     * @return the default value of this property
     */
    @Override
    public <T extends Object> T getDefaultValue(Class<T> cls) {
        if (defaultValue.getClass().isAssignableFrom(cls)) {
            return cls.cast(defaultValue);
        } else {
            throw new IllegalArgumentException("The object cannot be converted to the specified type '" + cls.getCanonicalName() + "'");
        }
    }
}
