/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.configuration.osgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.cascade.ConfigViewFactory;
import com.openexchange.groupware.settings.PreferencesItemService;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.internal.AbstractGuardSetting;
import com.openexchange.guard.configuration.internal.AdvancedUserSetting;
import com.openexchange.guard.configuration.internal.GuardConfigurationServiceImpl;
import com.openexchange.jslob.ConfigTreeEquivalent;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.serverconfig.ServerConfigService;

/**
 * {@link GuardConfigActivator}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class GuardConfigActivator extends HousekeepingActivator {

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { ConfigViewFactory.class, ConfigurationService.class,
            ServerConfigService.class };
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() throws Exception {
        Logger logger = LoggerFactory.getLogger(GuardConfigActivator.class);
        logger.info("Starting bundle {}", context.getBundle().getSymbolicName());

        Services.setServiceLookup(this);

        GuardConfigurationService guardConfigService = new GuardConfigurationServiceImpl(this);
        registerService(GuardConfigurationService.class, guardConfigService);
        registerSetting(new AdvancedUserSetting(guardConfigService));
        trackService(GuardConfigurationService.class);
        openTrackers();

        logger.info("GuardConfigurationService registered.");
    }

    private <V> void registerSetting(AbstractGuardSetting<V> setting) {
        registerService(PreferencesItemService.class, setting, null);
        registerService(ConfigTreeEquivalent.class, setting, null);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.HousekeepingActivator#stopBundle()
     */
    @Override
    protected void stopBundle() throws Exception {
        Logger logger = LoggerFactory.getLogger(GuardConfigActivator.class);

        logger.info("Stopping bundle {}", context.getBundle().getSymbolicName());
        unregisterService(GuardConfigurationService.class);
        Services.setServiceLookup(null);
        logger.info("GuardConfigurationService registered.");

        super.stopBundle();
    }
}
