/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services.impl;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.slf4j.Logger;
import com.openexchange.exception.OXException;
import com.openexchange.guard.autocrypt.database.AutoCryptStorageService;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.storage.OGPGPKeysStorage;
import com.openexchange.guard.pgpcore.services.GuardKeyRingRetrievalStrategy;
import com.openexchange.guard.pgpcore.services.PGPCryptoService;
import com.openexchange.guard.pgpcore.services.exceptions.PGPCoreServicesExceptionCodes;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.core.PGPDecrypter;
import com.openexchange.pgp.core.PGPDecryptionResult;
import com.openexchange.pgp.core.PGPEncrypter;
import com.openexchange.pgp.core.PGPKeyRetrievalStrategy;
import com.openexchange.pgp.core.PGPSessionKeyDecrypter;
import com.openexchange.pgp.keys.common.PGPSymmetricKey;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;

/**
 * {@link PGPCryptoServiceImpl}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public class PGPCryptoServiceImpl implements PGPCryptoService {

    private final GuardKeyRingRetrievalStrategy recipientKeyRingRetrievalStrategy;
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(PGPCryptoServiceImpl.class);
    private GuardKeyService guardKeyService;
    private OGPGPKeysStorage keyStorage;
    private AutoCryptStorageService autocryptService;

    /**
     * Initializes a new {@link PGPCryptoServiceImpl}.
     *
     * @param pgpKeyRetrievalStrategy the strategy for retrieving keys for decryption.
     * @param recipientKeyRingRetrievalStrategy the strategy for retrieving keys for encryption
     */
    public PGPCryptoServiceImpl(GuardKeyRingRetrievalStrategy recipientKeyRingRetrievalStrategy, GuardKeyService guardKeyService, OGPGPKeysStorage keyStorage, AutoCryptStorageService autocryptService) {
        this.recipientKeyRingRetrievalStrategy = recipientKeyRingRetrievalStrategy;
        this.guardKeyService = guardKeyService;
        this.keyStorage = keyStorage;
        this.autocryptService = autocryptService;
    }

    /**
     * Internal method to return a {@link PGPKeyRetrievalStrategy} used to perform signature verification.
     *
     * @param key The key to get the strategy for.
     * @return The strategy for the given key to use.
     */
    private PGPKeyRetrievalStrategy getKeyRetrievalStrategyFor(GuardKeys key) {
        return new DefaultPGPKeyRetrievalStrategy(guardKeyService, keyStorage, autocryptService, key.getUserid(), key.getContextid());
    }

    /**
     * Internal method to return a {@link PGPKeyRetrievalStrategy} used to perform signature verification.
     *
     * @param userIdentity The identity to get the strategy for.
     * @return The strategy for the given key to use.
     */
    private PGPKeyRetrievalStrategy getKeyRetrievalStrategyFor(UserIdentity userIdentity) {
       if(userIdentity != null && userIdentity.isOXUser() && !userIdentity.getOXUser().isGuest()) {
           return new DefaultPGPKeyRetrievalStrategy(guardKeyService, keyStorage, autocryptService, userIdentity.getOXUser().getId(), userIdentity.getOXUser().getContextId());
       } else {
           return new DBKeyRetrievalStrategy(guardKeyService);
       }
    }

    /**
     * Fetches the public keys for the given identities
     *
     * @param recipients The identities to fetch the public keys for
     * @return A set of public keys for the given identities
     * @throws OXException If a key was not found for a given identity
     */
    PGPPublicKey[] getRecipientKeys(List<String> recipients) throws OXException {

        ArrayList<PGPPublicKey> ret = new ArrayList<>();
        for (String recipient : recipients) {
            GuardKeys guardKeys = recipientKeyRingRetrievalStrategy.getKeyRingForRecipient(recipient);
            if (guardKeys != null) {
                PGPPublicKey key = guardKeys.getPGPPublicKey();
                if (key != null) {
                    ret.add(key);
                } else {
                    throw PGPCoreServicesExceptionCodes.RECIPIENT_KEY_NOT_FOUND.create(recipient);
                }
            } else {
                throw PGPCoreServicesExceptionCodes.RECIPIENT_KEY_NOT_FOUND.create(recipient);
            }
        }
        return ret.toArray(new PGPPublicKey[ret.size()]);
    }

    /**
     * Fetches the Guard key for the given identity
     *
     * @param identity The identity to get the signing key for
     * @return The signing key for the given identity
     * @throws OXException If the key was not found
     */
    GuardKeys getSignerGuardKey(String identity) throws OXException {
        GuardKeys guardKeys = recipientKeyRingRetrievalStrategy.getKeyRingForRecipient(identity);
        if (guardKeys != null) {
            return guardKeys;
        } else {
            throw PGPCoreServicesExceptionCodes.SIGNING_KEY_NOT_FOUND.create(identity);
        }
    }

    /**
     * Extracts the PGP signing key from the given Guard key
     *
     * @param guardKey The key to get the singing key for
     * @return The signing key for the given Guard key
     * @throws OXException If no signing key was found
     */
    PGPSecretKey getSignerKey(GuardKeys guardKey) throws OXException {
        PGPSecretKey signingKey = PGPKeysUtil.getSigningKey(guardKey.getPGPSecretKeyRing());
        if (signingKey != null) {
            return signingKey;
        } else {
            throw PGPCoreServicesExceptionCodes.SIGNING_KEY_NOT_FOUND.create(guardKey.getKeyid());
        }
    }

    private String listToString(List l) {
        return l.toString().replace("[", "").replace("]", "").replace(", ", ",");
    }

    private String keyListToString(PGPPublicKey... keys) {
        StringBuilder builder = new StringBuilder();
        for (PGPPublicKey key : keys) {
            builder.append(key.getKeyID());
            builder.append(", ");
        }
        return builder.toString();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.pgpcore.services.PGPCryptoService#encrypt(java.io.InputStream, java.io.OutputStream, boolean, com.openexchange.guard.pgpcore.services.PGPPublicKey[])
     */
    @Override
    public void encrypt(InputStream input, OutputStream output, boolean armored, List<String> recipients) throws OXException {
        try {
            logger.debug("Encrypting data for recipients: " + listToString(recipients) + ". ASCII-Armor = " + armored + ".");
            PGPPublicKey[] recipientKeys = getRecipientKeys(recipients);
            new PGPEncrypter().encrypt(input, output, armored, recipientKeys);
        } catch (OXException e) {
            throw e;
        } catch (Exception e) {
            throw PGPCoreServicesExceptionCodes.ENCRYPTION_ERROR.create(e, e.getMessage());
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.pgpcore.services.PGPCryptoService#encrypt(java.io.InputStream, java.io.OutputStream, boolean, org.bouncycastle.openpgp.PGPPublicKey[])
     */
    @Override
    public void encrypt(InputStream input, OutputStream output, boolean armored, RecipKey... recipientKeys) throws OXException {
        try {
            //Getting recipient keys suitable for encryption
            List<PGPPublicKey> pgpRecipientKeysList = new ArrayList<PGPPublicKey>();
            for (RecipKey recipientKey : recipientKeys) {
                final PGPPublicKey encryptionKey = recipientKey.getEncryptionKey();
                if(encryptionKey != null) {
                    pgpRecipientKeysList.add(encryptionKey);
                }
            }
            PGPPublicKey[] pgpRecipientKeys = pgpRecipientKeysList.toArray(new PGPPublicKey[pgpRecipientKeysList.size()]);
            logger.debug("Encrypting data for recipients: " + keyListToString(pgpRecipientKeys) + ". ASCII-Armor = " + armored + ".");
            new PGPEncrypter().encrypt(input, output, armored, pgpRecipientKeys);
        } catch (OXException e) {
            throw e;
        } catch (Exception e) {
            throw PGPCoreServicesExceptionCodes.ENCRYPTION_ERROR.create(e, e.getMessage());
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.pgpcore.services.PGPCryptoService#encryptSigned(java.io.InputStream, java.io.OutputStream, boolean, com.openexchange.guard.pgpcore.services.PGPSecretKey, char[],
     * com.openexchange.guard.pgpcore.services.PGPPublicKey[])
     */
    @Override
    public void encryptSigned(InputStream input, OutputStream output, boolean armored, UserIdentity signerIdentity, List<String> recipients) throws OXException {
        try {
            logger.debug("Encrypting data for recipients: " + listToString(recipients) + ". Signging as: " + signerIdentity.getIdentity() + ". ASCII-Armor = " + armored + ".");
            PGPPublicKey[] recipientKeys = getRecipientKeys(recipients);
            GuardKeys guardKey = getSignerGuardKey(signerIdentity.getIdentity());
            PGPSecretKey signingPGPKey = getSignerKey(guardKey);
            char[] hashedPassword = CipherUtil.getSHA(new String(signerIdentity.getPassword()), guardKey.getSalt()).toCharArray();
            new PGPEncrypter().encryptSigned(input, output, armored, signingPGPKey, hashedPassword, recipientKeys);
        } catch (OXException e) {
            throw e;
        } catch (Exception e) {
            throw PGPCoreServicesExceptionCodes.ENCRYPTION_ERROR.create(e, e.getMessage());
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.pgpcore.services.PGPCryptoService#encryptSigned(java.io.InputStream, java.io.OutputStream, boolean, org.bouncycastle.openpgp.PGPSecretKey, java.lang.String, org.bouncycastle.openpgp.PGPPublicKey[])
     */
    @Override
    public void encryptSigned(InputStream input, OutputStream output, boolean armored, GuardKeys signingKey, String password, RecipKey... recipientKeys) throws OXException {
        try {
            //Getting recipient keys suitable for encryption
            List<PGPPublicKey> pgpRecipientKeysList = new ArrayList<PGPPublicKey>();
            for (RecipKey recipientKey : recipientKeys) {
                pgpRecipientKeysList.add(recipientKey.getEncryptionKey());
            }
            PGPPublicKey[] pgpRecipientKeys = pgpRecipientKeysList.toArray(new PGPPublicKey[pgpRecipientKeysList.size()]);
            PGPSecretKey signingPGPKey = getSignerKey(signingKey);
            char[] hashedPassword = CipherUtil.getSHA(password, signingKey.getSalt()).toCharArray();
            logger.debug("Encrypting data for recipients: " + keyListToString(pgpRecipientKeys) + ". Signging as: " + signingKey.getEmail() + ". ASCII-Armor = " + armored + ".");
            new PGPEncrypter().encryptSigned(input, output, armored, signingPGPKey, hashedPassword, pgpRecipientKeys);
        } catch (OXException e) {
            throw e;
        } catch (Exception e) {
            throw PGPCoreServicesExceptionCodes.ENCRYPTION_ERROR.create(e, e.getMessage());
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.pgpcore.services.PGPCryptoService#decrypt(java.io.InputStream, java.io.OutputStream, java.lang.String, char[])
     */
    @Override
    public PGPDecryptionResult decrypt(InputStream input, OutputStream output, UserIdentity recipient) throws OXException {
        try {

            if(recipient.getPGPSessionKey() != null){
                //Using a symmetric key for decryption provided by the user identity.
                logger.debug("Decrypting data for : " + recipient.getIdentity());
                return decrypt(input, output, recipient.getPGPSessionKey(), recipient);
            }
            else {
                //Retrieving a PGP private key for decryption using the key retrieval strategy.
                logger.debug("Decrypting data for : " + recipient.getIdentity() + " using a password for the PGP key.");
                PGPDecrypter decrypter = new PGPDecrypter(this.getKeyRetrievalStrategyFor(recipient));
                return decrypter.decrypt(input, output, recipient.getIdentity(), recipient.getPassword());
            }
        } catch (OXException e) {
            logger.error("Problem with decrypting ", e);
            throw e;
        } catch (Exception e) {
            throw PGPCoreServicesExceptionCodes.DECRYPTION_ERROR.create(e, e.getMessage());
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.pgpcore.services.PGPCryptoService#decrypt(java.io.InputStream, java.io.OutputStream, com.openexchange.guard.keymanagement.commons.GuardKeys, java.lang.String)
     */
    @Override
    public PGPDecryptionResult decrypt(InputStream input, OutputStream output, GuardKeys key, String password) throws OXException {
        try {
            logger.debug("Decrypting data for : " + key.getEmail());
            PGPDecrypter decrypter = new PGPDecrypter(
                key.getPGPSecretKey() /* using a specific key */,
                getKeyRetrievalStrategyFor(key) /* strategy used to get public keys for signature verification */);
            return decrypter.decrypt(input, output, key.getEmail(), password.toCharArray());
        } catch (OXException e) {
            throw e;
        } catch (Exception e) {
            throw PGPCoreServicesExceptionCodes.DECRYPTION_ERROR.create(e, e.getMessage());
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.pgpcore.services.PGPCryptoService#decryptSymmetric(java.io.InputStream, java.io.OutputStream, java.lang.String)
     */
    @Override
    public PGPDecryptionResult decrypt(InputStream input, OutputStream output, PGPSymmetricKey symmetricKey, UserIdentity recipient) throws OXException {
        try {
            logger.debug("Decrypting data with symmetric session key");
            PGPSessionKeyDecrypter symmetricDecrypter = new PGPSessionKeyDecrypter(
                symmetricKey,
                new PublicKeyOnlyKeyRingRetrievalStrategy(getKeyRetrievalStrategyFor(recipient)));
            return symmetricDecrypter.decrypt(input, output);
        } catch (OXException e) {
            throw e;
        } catch (Exception e) {
            throw PGPCoreServicesExceptionCodes.DECRYPTION_ERROR.create(e, e.getMessage());
        }
    }
}
