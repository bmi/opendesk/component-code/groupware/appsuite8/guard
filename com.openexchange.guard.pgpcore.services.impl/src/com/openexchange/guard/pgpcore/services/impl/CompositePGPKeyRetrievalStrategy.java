/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services.impl;

import java.util.Objects;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import com.openexchange.pgp.core.PGPKeyRetrievalStrategy;

/**
 * {@link CompositePGPKeyRetrievalStrategy} implements a composite pattern for a {@link PGPKeyRetrievalStrategy}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.0
 */
public class CompositePGPKeyRetrievalStrategy implements PGPKeyRetrievalStrategy {

    private final PGPKeyRetrievalStrategy[] strategies;

    /**
     * Initializes a new {@link CompositePGPKeyRetrievalStrategy}.
     *
     * @param strategies The strategies to use
     */
    public CompositePGPKeyRetrievalStrategy(PGPKeyRetrievalStrategy... strategies) {
        this.strategies = Objects.requireNonNull(strategies, "strategies must not be null");
    }

    /* (non-Javadoc)
     * @see com.openexchange.pgp.core.PGPKeyRetrievalStrategy#getSecretKey(long, java.lang.String, char[])
     */
    @Override
    public PGPPrivateKey getSecretKey(long keyId, String userIdentity, char[] password) throws Exception {
        for (PGPKeyRetrievalStrategy strategy : strategies) {
           PGPPrivateKey privateKey = strategy.getSecretKey(keyId, userIdentity, password);
           if(privateKey != null) {
               return privateKey;
           }
        }
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.pgp.core.PGPKeyRetrievalStrategy#getPublicKey(long)
     */
    @Override
    public PGPPublicKey getPublicKey(long keyId) throws Exception {
        for (PGPKeyRetrievalStrategy strategy : strategies) {
           PGPPublicKey publicKey = strategy.getPublicKey(keyId);
           if(publicKey != null) {
               return publicKey;
           }
        }
        return null;
    }

}
