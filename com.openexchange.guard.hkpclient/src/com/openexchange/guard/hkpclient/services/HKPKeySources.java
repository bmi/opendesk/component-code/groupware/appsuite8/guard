/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.hkpclient.services;

import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.keymanagement.commons.trust.KeySource;
import com.openexchange.guard.keymanagement.commons.trust.KeySourceFactory;

/**
 * {@link HKPKeySources} defines a set of default HKP key sources ({@link KeySource}).
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class HKPKeySources {

    /**
     * The {@link KeySource} for public available key servers (for example keys.gnupg.net)
     */
    static KeySource PUBLIC_HKP_REMOTE_SERVER;

    /**
     * The {@link KeySource} for trusted public, or private key servers
     */
    static KeySource TRUSTED_HKP_REMOTE_SERVER;

    /**
     * The {@link KeySource} for key servers discovered by a SRV DNS Record
     */
    static KeySource SRV_HKP_REMOTE_SERVER;

    /**
     * The {@link KeySource} for key servers discovered by a SRV DNS Record
     */
    static KeySource SRV_HKP_DNSSEC_REMOTE_SERVER;

    /**
     * Initializes all HKP key source values
     * @param keySourceFactory
     */
    public static void initialize(KeySourceFactory keySourceFactory) {
        PUBLIC_HKP_REMOTE_SERVER = keySourceFactory.create("HKP_PUBLIC_SERVER", GuardProperty.trustLevelHKPPublicServer);
        TRUSTED_HKP_REMOTE_SERVER = keySourceFactory.create("HKP_TRUSTED_SERVER", GuardProperty.trustLevelHKPTrustedServer);
        SRV_HKP_REMOTE_SERVER = keySourceFactory.create("HKP_SRV_SERVER", GuardProperty.trustLevelHKPSRVServer);
        SRV_HKP_DNSSEC_REMOTE_SERVER = keySourceFactory.create("HKP_SRV_DNSSEC_SERVER", GuardProperty.trustLevelHKPSRVDNSSECServer);
    }
}
