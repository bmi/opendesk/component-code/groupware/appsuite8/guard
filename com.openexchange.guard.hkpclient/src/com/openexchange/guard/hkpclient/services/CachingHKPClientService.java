
package com.openexchange.guard.hkpclient.services;

import java.util.Collection;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.hkpclient.osgi.Services;
import com.openexchange.guard.keymanagement.commons.CachedKey;
import com.openexchange.guard.keymanagement.storage.RemoteKeyCacheStorage;

/**
 *
 * {@link CachingHKPClientService} Service for querying guard's remote key cache
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class CachingHKPClientService implements HKPClientService {

    private static final Logger logger = LoggerFactory.getLogger(CachingHKPClientService.class);
    private final HKPClientService delegate;

    /**
     * Initializes a new {@link CachingHKPClientService}.
     */
    public CachingHKPClientService() {
        this(null);
    }

    /**
     * Initializes a new {@link CachingHKPClientService}.
     *
     * @param delegate The service to delegate search queries to in a public key ring was not found
     */
    public CachingHKPClientService(HKPClientService delegate) {
        this.delegate = delegate;
    }

    /**
     * Internal method to query the OX Guard remote key cache for the given email
     *
     * @param email The email to query
     * @return The key for the given email from the cache, or null if no such key was found in the cache
     * @throws Exception
     */
    private Collection<CachedKey> findInCache(String email) throws Exception {
        RemoteKeyCacheStorage remoteKeyCacheStorage = Services.getService(RemoteKeyCacheStorage.class);
        List<CachedKey> cachedKeys = remoteKeyCacheStorage.getByEmail(email);
        return cachedKeys;
    }

    private CachedKey findInCache(Long id) throws Exception {
        RemoteKeyCacheStorage remoteKeyCacheStorage = Services.getService(RemoteKeyCacheStorage.class);
        CachedKey cachedKey = remoteKeyCacheStorage.getByID(id);
        return cachedKey;
    }

    /**
     * Internal method to convert a {@link RemoteKeyResult} to a {@link CachedKey}.
     *
     * @param remoteKeyResult the object to convert.
     * @return The converted object.
     */
    private CachedKey toCachedKey(RemoteKeyResult remoteKeyResult) {
        return new CachedKey(remoteKeyResult.getRing(), remoteKeyResult.getSource());
    }

    /**
     * Internal method to store one or more rings in the cache for later re-use
     *
     * @param rings the key rings to save to the cache
     * @throws OXException
     */
    private void writeToCache(RemoteKeyResult... rings) throws OXException {
        RemoteKeyCacheStorage remoteKeyCacheStorage = Services.getService(RemoteKeyCacheStorage.class);
        for (RemoteKeyResult ring : rings) {
            remoteKeyCacheStorage.insert(toCachedKey(ring));
        }
    }

    /**
     * Internal method for searching for public keys within the cache or delegate if not found
     *
     * @param clientToken an identification token put into the X-UI-INTERNAL-ACCOUNT-ID header, or null not not set the header
     * @param email The email to get the public key rings for
     * @return A list for key rings found for the given email
     * @throws Exception
     */
    private Collection<RemoteKeyResult> findAllInternal(String clientToken, String email, int userId, int cid, int timeout) throws Exception {

        //Searching keys in the cache
        Collection<CachedKey> ret = null;
        try {
            ret = findInCache(email);
            if (ret != null && ret.size() > 0) {
                logger.debug("Remote keys found in cache");
                return RemoteKeyResult.createCollectionFrom(ret);
            }
        } catch (Exception e) {
            logger.error("Error querying HKP key cache", e);
        }

        Collection<RemoteKeyResult> result = null;
        //Search delegate
        if (delegate != null) {
            result = delegate.find(clientToken, email, userId, cid, timeout);
        }

        if (result != null && !result.isEmpty()) {
            //Only save from trusted sources
            Collection<RemoteKeyResult> trustedResults = RemoteKeyResult.getTrusted(result, userId, cid);
            if(trustedResults != null && trustedResults.size() > 0) {
                logger.debug("Writing {} found keys from trusted HKP server into cache", trustedResults.size());
                writeToCache(trustedResults.toArray(new RemoteKeyResult[trustedResults.size()]));
            }
        }
        return result;
    }

    /**
     * Search for a key by long ID in the remote key cache
     *
     * @param clientToken
     * @param id
     * @param email
     * @param userId ID of the sender
     * @param cid Context of the sender
     * @param timeout Time remaining for lookup
     * @return
     * @throws Exception
     */
    private RemoteKeyResult findInternal(String clientToken, Long id, String email, int userId, int cid, int timeout) throws Exception {

        CachedKey cachedKey = findInCache(id);
        if (cachedKey != null) {
            return new RemoteKeyResult(cachedKey.getPublicKeyRing(), cachedKey.getKeySource());
        }

        RemoteKeyResult result = null;
        //Search delegate
        if (delegate != null) {
            result = delegate.find(clientToken, email, id, userId, cid, timeout);
        }

        if (result != null && result.hasResult()) {
            if (result.getSource().isTrusted(userId, cid)) { // Only save from trusted sources
                logger.debug("Writing found keys from HKP server into cache");
                writeToCache(result);
            }
        }
        return result;
    }

    @Override
    public Collection<RemoteKeyResult> find(String clientToken, String email, int userId, int cid, int timeout) throws OXException {
        try {
            return findAllInternal(clientToken, email, userId, cid, timeout);
        } catch (Exception e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public RemoteKeyResult find(String clientToken, String email, Long id, int userId, int cid, int timeout) throws OXException {
        try {
            return findInternal(clientToken, id, email, userId, cid, timeout);
        } catch (Exception e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }
}
