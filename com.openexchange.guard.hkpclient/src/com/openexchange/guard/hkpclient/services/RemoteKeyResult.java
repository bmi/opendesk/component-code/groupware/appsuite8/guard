
package com.openexchange.guard.hkpclient.services;

import java.util.ArrayList;
import java.util.Collection;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.CachedKey;
import com.openexchange.guard.keymanagement.commons.trust.KeySource;

/**
 * Handle the result of a remote PGP lookup
 * Assign trust level depending on the type of lookup
 *
 * @author Greg Hill
 *
 */
public class RemoteKeyResult {

    private PGPPublicKeyRing ring = null;
    private KeySource source;

    // Constructors
    public RemoteKeyResult() {
        this.ring = null;
    }

    public RemoteKeyResult(PGPPublicKeyRing ring, KeySource source) {
        this.ring = ring;
        this.source = source;
    }

    public PGPPublicKeyRing getRing() {
        return ring;
    }

    public void setRing(PGPPublicKeyRing ring) {
        this.ring = ring;
    }

    public KeySource getSource() {
        return source;
    }

    public boolean hasResult() {
        return ring != null;
    }

    /**
     * Creates a result collection from the given PGPPublicKeyRing collection
     * @param keyRings The PGPPublicKeyRing collection to create the results from
     * @param source The source of the keyRings
     * @return A remote RemoteKeyResult collection created from the given key rings
     */
    public static Collection<RemoteKeyResult> createCollectionFrom(Collection<PGPPublicKeyRing> keyRings, KeySource source) {
        ArrayList<RemoteKeyResult> ret = new ArrayList<RemoteKeyResult>();
        for (PGPPublicKeyRing keyRing : keyRings) {
            ret.add(new RemoteKeyResult(keyRing, source));
        }
        return ret;
    }

    /**
     * Creates a result collection from the given CachedKey collection
     *
     * @param cachedKeyRings The CachedKey collection to create the results from
     * @return A remote RemoteKeyResult collection created from the given key rings
     */
    public static Collection<RemoteKeyResult> createCollectionFrom(Collection<CachedKey> cachedKeyRings){
        ArrayList<RemoteKeyResult> ret = new ArrayList<RemoteKeyResult>();
        for (CachedKey cachedKey : cachedKeyRings) {
            ret.add(new RemoteKeyResult(cachedKey.getPublicKeyRing(), cachedKey.getKeySource()));
        }
        return ret;
    }

    /**
     * Extracts a collection of PGPPublicKeyRing from a collection of RemoteKeyResult
     *
     * @param keyResults The collection to extract the key rings from
     * @return A collection of extracted key rings
     */
    public static Collection<PGPPublicKeyRing> getRingsFrom(Collection<RemoteKeyResult> keyResults) {
        Collection<PGPPublicKeyRing> ret = new ArrayList<PGPPublicKeyRing>();
        if (keyResults != null) {
            for (RemoteKeyResult remoteKeyResult : keyResults) {
                ret.add(remoteKeyResult.getRing());
            }
        }
        return ret;
    }

    /**
     * Checks if all results in a given collection are considered to come from a trusted source
     *
     * @param keyResults The collection
     * @param userId ID of the sender
     * @param cid CID of the sender
     * @return True, if all results in the given collection come from a trusted source, false if a least one result does not come from a trusted source
     * @throws OXException
     */
    public static boolean areTrusted(Collection<RemoteKeyResult> keyResults, int userId, int cid) throws OXException {
        return getTrusted(keyResults, userId, cid).size() == keyResults.size();
    }

    /**
     * Gets the sub set of results which comes from a trusted source
     *
     * @param keyResults The results
     * @param userId ID of the sender
     * @param cid CID of the sender
     * @return A sub set of results which comes from a trusted source
     * @throws OXException
     */
    public static Collection<RemoteKeyResult> getTrusted(Collection<RemoteKeyResult> keyResults, int userId, int cid) throws OXException {
        Collection<RemoteKeyResult> ret = new ArrayList<RemoteKeyResult>();
        for (RemoteKeyResult remoteKeyResult : keyResults) {
            if (remoteKeyResult.getSource().isTrusted(userId, cid)) {
                ret.add(remoteKeyResult);
            }
        }
        return ret;
    }
}
